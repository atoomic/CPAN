package Catmandu::Atom;

=head1 NAME

Catmandu::Atom - modules for working with Atom feeds

=head1 VERSION

Version 0.02

=cut

our $VERSION = '0.02';

=head1 MODULES

=over

=item * L<Catmandu::Exporter::Atom>

=item * L<Catmandu::Importer::Atom>

=back

=head1 AUTHOR

Nicolas Steenlant, C<< <nicolas.steenlant at ugent.be> >>

=head1 CONTRIBUTOR

Vitali Peil, C<< <vitali.peil at uni-bielefeld.de> >>

=cut

1;
