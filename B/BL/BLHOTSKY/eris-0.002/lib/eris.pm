# ABSTRACT: Eris is the Greek Goddess of Chaos
use strict;
use warnings;
package eris;

our $VERSION = 0.001;

1;

__END__

=pod

=encoding UTF-8

=head1 NAME

eris - Eris is the Greek Goddess of Chaos

=head1 VERSION

version 0.002

=head1 SYNOPSIS

eris exists to transform unstructuted, chaotic log data into structured messages.

=head1 AUTHOR

Brad Lhotsky <brad@divisionbyzero.net>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2015 by Brad Lhotsky.

This is free software, licensed under:

  The (three-clause) BSD License

=cut
