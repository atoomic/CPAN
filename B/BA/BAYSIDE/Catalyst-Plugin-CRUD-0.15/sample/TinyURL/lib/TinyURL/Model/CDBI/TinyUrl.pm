package TinyURL::Model::CDBI::TinyUrl;

use strict;

=head1 NAME

tinyurl::Model::CDBI::TinyUrl - CDBI Table Class

=head1 SYNOPSIS

See L<TinyURL>

=head1 DESCRIPTION

CDBI Table Class.

=head1 AUTHOR

Jun Shimizu

=head1 LICENSE

This library is free software . You can redistribute it and/or modify
it under the same terms as perl itself.

=cut

1;
