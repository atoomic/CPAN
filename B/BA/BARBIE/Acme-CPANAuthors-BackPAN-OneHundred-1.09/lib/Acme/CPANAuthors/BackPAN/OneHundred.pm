package Acme::CPANAuthors::BackPAN::OneHundred;
use strict;
use warnings;

{
    no strict "vars";
    $VERSION = "1.09";
}

use Acme::CPANAuthors::Register (

    'ADAMK'         => 'Adam Kennedy',
    'AUTRIJUS'      => 'Audrey Tang',
    'BARBIE'        => 'Barbie',
    'BDFOY'         => 'brian d foy',
    'BINGOS'        => 'Chris Williams',
    'BOBTFISH'      => 'Tomas Doran',
    'DAGOLDEN'      => 'David Golden',
    'DMAKI'         => 'Daisuke Maki',
    'DMUEY'         => 'Daniel Muey',
    'DROLSKY'       => 'Dave Rolsky',
    'ETHER'         => 'Karen Etheridge',
    'FAYLAND'       => 'Fayland Lin',
    'FLORA'         => 'Florian Ragwitz',
    'GETTY'         => 'Torsten Raudssus',
    'GUGOD'         => 'Liu Kang Min',
    'INGY'          => 'Ingy dot Net',
    'JGNI'          => 'John Imrie',
    'KENTNL'        => 'Kent Fredric',
    'LBROCARD'      => 'Leon Brocard',
    'LEOCHARRE'     => 'Leo Charre',
    'MANWAR'        => 'Mohammad S Anwar',
    'MARCEL'        => '???',
    'MIYAGAWA'      => 'Tatsuhiko Miyagawa',
    'MLEHMANN'      => '???',
    'MRAMBERG'      => 'Marcus Ramberg',
    'NEILB'         => 'Neil Bowers',
    'NUFFIN'        => 'Yuval Kogman',
    'PERLANCAR'     => 'perlancar',
    'PEVANS'        => 'Paul Evans',
    'PLICEASE'      => 'Graham Ollis',
    'PSIXDISTS'     => 'Perl 6 Modules',
    'RJBS'          => 'Ricardo SIGNES',
    'RSAVAGE'       => 'Ron Savage',
    'SALVA'         => 'Salvador Fandino Garcia',
    'SHARYANTO'     => 'Steven Haryanto',
    'SHLOMIF'       => 'Shlomi Fish',
    'SIMON'         => 'Simon Cozens',
    'SKIM'          => 'Michal Spacek',
    'SMUELLER'      => 'Steffen Mueller',
    'SZABGAB'       => 'Gabor Szabo',
    'TOBYINK'       => 'Toby Inkster',
    'TOKUHIROM'     => '???',
    'ZOFFIX'        => 'Zoffix Znet',

);

q<
We are programmed just to do
Anything you want us to

We are the robots, we are the robots
We are the robots, we are the robots

Lyrics copyright Ralf H�tter
>

__END__

=encoding UTF-8

=head1 NAME

Acme::CPANAuthors::BackPAN::OneHundred - The CPAN Authors who have 100+ distributions on BackPAN

=head1 DESCRIPTION

This class provides a hash of CPAN authors' PAUSE ID and name to be 
used with the C<Acme::CPANAuthors> module.

This module was created to capture all those CPAN Authors who have valiantly
submitted their modules and distributions to CPAN, and now have the honour of
having submitted 100 or more distributions to CPAN. 

Note that the CPAN authors listed here may not be maintaining 100 or more
distributions on CPAN, but have submitted 100 or more distributions to PAUSE, 
where some older distributions may have been deprecated or adopted by other 
authors. The numbers here represent the number of distributions a CPAN author 
has listed on BackPAN.

See L<http://backpan.cpantesters.org>.

=head1 THE AUTHORS

   1.  888  PERLANCAR     perlancar
   2.  510  SHARYANTO     Steven Haryanto
   3.  479  PSIXDISTS     Perl 6 Modules
   4.  313  TOBYINK       Toby Inkster
   5.  309  RJBS          Ricardo SIGNES
   6.  293  ZOFFIX        Zoffix Znet
   7.  287  ADAMK         Adam Kennedy
   8.  251  TOKUHIROM     ???
   9.  240  MIYAGAWA      Tatsuhiko Miyagawa
  10.  222  ETHER         Karen Etheridge
  11.  218  INGY          Ingy dot Net
  12.  217  BINGOS        Chris Williams
  13.  210  FLORA         Florian Ragwitz
  14.  202  JGNI          John Imrie
  15.  195  DAGOLDEN      David Golden
  16.  192  SMUELLER      Steffen Mueller
  17.  182  MARCEL        ???
  18.  174  KENTNL        Kent Fredric
  19.  172  BOBTFISH      Tomas Doran
  20.  161  NUFFIN        Yuval Kogman
  21.  156  DROLSKY       Dave Rolsky
  22.  149  PEVANS        Paul Evans
  23.  138  GUGOD         Liu Kang Min
  24.  138  RSAVAGE       Ron Savage
  25.  137  DMAKI         Daisuke Maki
  26.  137  NEILB         Neil Bowers
  27.  137  SKIM          Michal Spacek
  28.  134  BARBIE        Barbie
  29.  133  BDFOY         brian d foy
  30.  126  PLICEASE      Graham Ollis
  31.  125  AUTRIJUS      Audrey Tang
  32.  125  SHLOMIF       Shlomi Fish
  33.  122  FAYLAND       Fayland Lin
  34.  122  SZABGAB       Gabor Szabo
  35.  121  SIMON         Simon Cozens
  36.  120  MANWAR        Mohammad S Anwar
  37.  111  LBROCARD      Leon Brocard
  38.  111  MLEHMANN      ???
  39.  111  MRAMBERG      Marcus Ramberg
  40.  111  SALVA         Salvador Fandino Garcia
  41.  109  DMUEY         Daniel Muey
  42.  105  GETTY         Torsten Raudssus
  43.  101  LEOCHARRE     Leo Charre

List last updated: 2016-04-22T05:57:08

=head1 MAINTENANCE

If you are aware of any CPAN author that has attained the heady heights of 100
distributions on CPAN, and who is not listed here, please send me their ID/name
via email or RT, and I will update the module. If there are any mistakes, 
please contact me as soon as possible, and I'll amend the entry right away.

=head1 SEE ALSO

L<Acme::CPANAuthors> - Main class to manipulate this one

L<Acme::CPANAuthors::CPAN::OneHundred> - 100+ distributions on CPAN.

=head1 SUPPORT

Bugs, patches and feature requests can be reported at:

=over 4

=item * RT: CPAN's request tracker

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=Acme-CPANAuthors-BackPAN-OneHundred>

=item * GitHub

L<http://github.com/barbie/acme-cpanauthors-backpan-onehundred>

=back

There are no known bugs at the time of this release. However, if you spot a
bug or are experiencing difficulties that are not explained within the POD
documentation, please send an email to barbie@cpan.org or submit a bug to 
the RT queue. However, it would help greatly if you are able to pinpoint 
problems or even supply a patch. 

Fixes are dependent upon their severity and my availability. Should a fix 
not be forthcoming, please feel free to (politely) remind me.

=head1 ACKNOWLEDGEMENTS

Thanks to Kenichi Ishigaki for writing C<Acme::CPANAuthors>.

=head1 AUTHOR

  Barbie, <barbie@cpan.org>
  for Miss Barbell Productions <http://www.missbarbell.co.uk>.

=head1 COPYRIGHT & LICENSE

  Copyright 2014-2016 Barbie for Miss Barbell Productions.

  This distribution is free software; you can redistribute it and/or
  modify it under the Artistic License 2.0.

=cut
