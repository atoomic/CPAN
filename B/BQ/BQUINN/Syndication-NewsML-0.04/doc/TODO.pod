$Id: TODO.pod,v 0.5 2001/09/26 01:19:39 brendan Exp $

=for html
<div id="content">
<H1>
TODO
</H1>
</div>
<div id="content">

=begin text

TODO

=over 4

=item *
Allow creation/modification of NewsML documents as well as just reading them

=item *
A Syndication::NewsML::Simple type module that takes the pain out of accessing all these methods.

=item *
Better error checking

=item *
Better handling of error reporting -- don't just croak() every time something goes wrong

=item *
POD docs of each class (!)

=item *
Or at least some kind of class/method map so people know what they can call

=item *
Separate out the classes into more than one file!

=item *
Finish off t/4_reuters.t and examples/NewsMLParser.pl

=item *
Possibly change the way the constructor works... call it Syndication::NewsML::Parser?

=item *
The rest of the TODO list ;-)

=back

=end text



=head2 Would be nice

=for text
** This file was automatically generated from **
** doc/Changes.pod. To edit it, see there.    **


=for html </div>
