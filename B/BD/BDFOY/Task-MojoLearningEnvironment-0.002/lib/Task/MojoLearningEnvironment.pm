package Task::MojoLearningEnvironment;
use strict;

use warnings;
no warnings;

use subs qw();
use vars qw($VERSION);

$VERSION = '0.002';

=encoding utf8

=head1 NAME

Task::MojoLearningEnvironment -  Everything you need to play with Mojolicious, and more

=head1 SYNOPSIS

	% cpan Task::MojoLearningEnvironment

	% cpanm Task::MojoLearningEnvironment

=head1 DESCRIPTION

This module exists to specify the dependencies I want to use for
my Mojolicious classes. If you think I should add something else,
send a pull request.

=head1 SOURCE AVAILABILITY

This source is in a Git repository:

	https://github.com/briandfoy/Task-MojoLearningEnvironment

=head1 AUTHOR

brian d foy, C<< <bdfoy@cpan.org> >>

=head1 COPYRIGHT AND LICENSE

Copyright ©  2015, brian d foy <bdfoy@cpan.org>. All rights reserved.

You may redistribute this under the same terms as Perl itself.

=cut

1;
