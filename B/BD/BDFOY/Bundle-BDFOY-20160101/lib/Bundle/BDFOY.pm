package Bundle::BDFOY;

$VERSION = '20160101';

1;

__END__

=encoding utf8

=head1 NAME

Bundle::BDFOY - install all modules by BDFOY

=head1 SYNOPSIS

	% cpan Bundle::BDFOY

=head1 CONTENTS

App::Module::Lister

App::PPI::Dumper

App::scriptdist

Brick

Bundle::BDFOY

Business::ISBN

Business::ISBN::Data

Business::ISMN

Business::ISSN

Business::US::USPS::WebTools

CACertOrg::CA

Chemistry::Elements

ConfigReader::Simple

CPAN::PackageDetails

Crypt::Rijndael

Data::Constraint

Distribution::Cooker

Distribution::Guess::BuildSystem

Dumbbench

File::Find::Closures

File::Fingerprint

Geo::GeoNames

Git::Github::Creator

github_creator

grepurl

HTML::SimpleLinkExtor

HTTP::Cookies::Chrome

HTTP::Cookies::iCab

HTTP::Cookies::Mozilla

HTTP::Cookies::Omniweb

HTTP::Cookies::Safari

HTTP::SimpleLinkChecker

HTTP::Size

IO::Interactive

Log::Log4perl::Appender::ScreenColoredLevels::UsingMyColors

Mac::Errors

Mac::iPhoto::Shell

Mac::iTerm::LaunchPad

Mac::OSVersion

Mac::Path::Util

Mac::PropertyList

MacOSX::Alias

Math::NoCarry

Module::Extract::DeclaredMinimumPerl

Module::Extract::Namespaces

Module::Extract::Use

Module::Extract::VERSION

Module::Release

Module::Release::Git

Module::Starter::AddModule

Modulino::Demo

MyCPAN::App::DPAN

MyCPAN::Indexer

Net::MAC::Vendor

Net::SSH::Perl::ProxiedIPC

Net::SSH::Perl::WithSocks

Netscape::Bookmarks

Object::Iterate

PeGS::PDF

Perl::MinimumVersion::Fast

Perl::Version

perlbench

PerlPowerTools

Pod::InDesign::TaggedText

Pod::InDesign::TaggedText::TPR

Pod::Perldoc::ToToc

Pod::PseudoPod::DocBook

Pod::PseudoPod::PerlTricks

Pod::SpeakIt::MacSpeech

Pod::WordML

Polyglot

PPI::App::ppi_version::BDFOY

Psychic::Ninja

ReturnValue

Roman::Unicode

scriptdist

Set::CrossProduct

SourceCode::LineCounter::Perl

String::Sprintf

Surveyor::App

Surveyor::Benchmark::HTMLEntities

Surveyor::Benchmark::SchwartzianTransform::SortFilesBySize

Task::MasteringPerl

Task::MojoLearningEnvironment

Test::Data

Test::Env

Test::File

Test::HTTPStatus

Test::ISBN

Test::Manifest

Test::Output

Test::Prereq

Test::URI

Test::WWW::Accessibility

Tie::BoundedInteger

Tie::Cycle

Tie::StringArray

Tie::Timely

Tie::Toggle

Unicode::Casing

Unicode::Support

Unicode::Tussle

WordPress::Grep

=head1 AUTHOR

brian d foy, C<< <bdfoy@cpan.org> >>

=head1 COPYRIGHT AND LICENSE

Copyright © 2006-2016, brian d foy <bdfoy@cpan.org>. All rights reserved.

You can use this module under the same terms as Perl itself.

=cut




