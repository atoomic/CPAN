use 5.008;
use strict;
use warnings;

package Task::BDFOY;

our $AUTHORITY = 'cpan:BDFOY';
our $VERSION   = '21060101';

1;

__END__

=pod

=encoding utf-8

=head1 NAME

Task::BDFOY - All of BDFOY's distributions

=head1 DESCRIPTION

Task::BDFOY installs the modules authored or maintained by brian d foy.

=over 4

=item * App::Module::Lister

=item * App::PPI::Dumper

=item * App::scriptdist

=item * Brick

=item * Bundle::BDFOY

=item * Business::ISBN

=item * Business::ISBN::Data

=item * Business::ISMN

=item * Business::ISSN

=item * Business::US::USPS::WebTools

=item * CACertOrg::CA

=item * Chemistry::Elements

=item * ConfigReader::Simple

=item * CPAN::PackageDetails

=item * Crypt::Rijndael

=item * Data::Constraint

=item * Distribution::Cooker

=item * Distribution::Guess::BuildSystem

=item * Dumbbench

=item * File::Find::Closures

=item * File::Fingerprint

=item * Geo::GeoNames

=item * Git::Github::Creator

=item * github_creator

=item * grepurl

=item * HTML::SimpleLinkExtor

=item * HTTP::Cookies::Chrome

=item * HTTP::Cookies::iCab

=item * HTTP::Cookies::Mozilla

=item * HTTP::Cookies::Omniweb

=item * HTTP::Cookies::Safari

=item * HTTP::SimpleLinkChecker

=item * HTTP::Size

=item * IO::Interactive

=item * Log::Log4perl::Appender::ScreenColoredLevels::UsingMyColors

=item * Mac::Errors

=item * Mac::iPhoto::Shell

=item * Mac::iTerm::LaunchPad

=item * Mac::OSVersion

=item * Mac::Path::Util

=item * Mac::PropertyList

=item * MacOSX::Alias

=item * Math::NoCarry

=item * Module::Extract::DeclaredMinimumPerl

=item * Module::Extract::Namespaces

=item * Module::Extract::Use

=item * Module::Extract::VERSION

=item * Module::Release

=item * Module::Release::Git

=item * Module::Starter::AddModule

=item * Modulino::Demo

=item * MyCPAN::App::DPAN

=item * MyCPAN::Indexer

=item * Net::MAC::Vendor

=item * Net::SSH::Perl::ProxiedIPC

=item * Net::SSH::Perl::WithSocks

=item * Netscape::Bookmarks

=item * Object::Iterate

=item * PeGS::PDF

=item * Perl::MinimumVersion::Fast

=item * Perl::Version

=item * perlbench

=item * PerlPowerTools

=item * Pod::InDesign::TaggedText

=item * Pod::InDesign::TaggedText::TPR

=item * Pod::Perldoc::ToToc

=item * Pod::PseudoPod::DocBook

=item * Pod::PseudoPod::PerlTricks

=item * Pod::SpeakIt::MacSpeech

=item * Pod::WordML

=item * Polyglot

=item * PPI::App::ppi_version::BDFOY

=item * Psychic::Ninja

=item * ReturnValue

=item * Roman::Unicode

=item * scriptdist

=item * Set::CrossProduct

=item * SourceCode::LineCounter::Perl

=item * String::Sprintf

=item * Surveyor::App

=item * Surveyor::Benchmark::HTMLEntities

=item * Surveyor::Benchmark::SchwartzianTransform::SortFilesBySize

=item * Task::MasteringPerl

=item * Task::MojoLearningEnvironment

=item * Test::Data

=item * Test::Env

=item * Test::File

=item * Test::HTTPStatus

=item * Test::ISBN

=item * Test::Manifest

=item * Test::Output

=item * Test::Prereq

=item * Test::URI

=item * Test::WWW::Accessibility

=item * Tie::BoundedInteger

=item * Tie::Cycle

=item * Tie::StringArray

=item * Tie::Timely

=item * Tie::Toggle

=item * Unicode::Casing

=item * Unicode::Support

=item * Unicode::Tussle

=item * weather

=item * weblint++

=item * webreaper

=item * WordPress::Grep

=back

=head1 BUGS

Please report any bugs to the GitHub project
L<https://github.com/briandfoy/Bundle-BDFOY>.

=head1 SEE ALSO

L<Task>.

=head1 AUTHOR

brian d foy C<< <bdfoy@gmail.com> >>

=head1 COPYRIGHT AND LICENCE

Copyright (c) 2014-2016, brian d foy.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
