### 03-submission.t ###########################################################
# This file tests the simple calls to the job submit function

### Includes ##################################################################

# Safe Perl
use warnings;
use strict;
use Carp;

# use Test::More tests => 5;
use Test::More
        (qx(which qsub))
        ? (tests => 5)
        : (skip_all => "SGE not available on this system");
use Test::Exception;
use MooseX::Types::Path::Class qw(Dir File);
use File::Temp;
use File::pushd;
use File::ShareDir;
use HPCI;

use FindBin;
$ENV{PATH} = "$FindBin::Bin/../bin:$ENV{PATH}";

### Tests #####################################################################

-d 'scratch' or mkdir 'scratch';

my $cluster = 'SGE';

{
	my $group = HPCI->group(
		cluster  => $cluster,
		base_dir => 'scratch',
		name     => 'T_Mail'
	);

	$group->stage(
		command               => "sleep 1",
		name                  => "mailjob",
		extra_sge_args_string => '-M jmacdonald@oicr.on.ca -m b',
	);

	my $results = $group->execute();
	printf STDERR Dumper($results);
}

done_testing();
