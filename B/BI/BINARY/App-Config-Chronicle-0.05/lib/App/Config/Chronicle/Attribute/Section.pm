package App::Config::Chronicle::Attribute::Section;
$App::Config::Chronicle::Attribute::Section::VERSION = '0.05';
use Moose;
use namespace::autoclean;
extends 'App::Config::Chronicle::Node';

=head1 NAME

App::Config::Chronicle::Attribute::Section

=head1 VERSION

version 0.05

=cut

__PACKAGE__->meta->make_immutable;
1;

=head1 NOTE

This class isn't intended for consumption outside of App::Config::Chronicle.

=cut
