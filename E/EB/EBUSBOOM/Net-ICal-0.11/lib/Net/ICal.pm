# This file was automatically generated by SWIG
package Net::ICal;
require Exporter;
require DynaLoader;
@ISA = qw(Exporter DynaLoader);
package Net::ICal;
bootstrap Net::ICal;
var_Net__ICal_init();
@EXPORT = qw( );

use Net::ICal::Component;
use Net::ICal::Property;
use Net::ICal::Parameter;
use Net::ICal::Value;

$VERSION = "0.11";

1;

