#!/usr/bin/perl
# -*- Mode: perl -*-
#======================================================================
# FILE: Types.pm
# CREATOR: eric 16 August 1999
#
# DESCRIPTION:
#   
#
#  $Id: Types.pm,v 1.4 2000/05/24 04:41:32 eric Exp $
#  $Locker:  $
#
# (C) COPYRIGHT 2000, Eric Busboom, http://www.softwarestudio.org
#
# This package is free software and is provided "as is" without express
# or implied warranty.  It may be used, redistributed and/or modified
# under the same terms as perl itself. ( Either the Artistic License or the
# GPL. ) 
#
# The Original Code is Types.pm. The Initial Developer of the Original
# Code is Eric Busboom
#
#======================================================================

package Types;


1;

