package DBIx::SQLEngine::Criteria::NumericLesser;

use DBIx::SQLEngine::Criteria::Lesser;
@ISA = 'DBIx::SQLEngine::Criteria::Lesser';

1;

__END__

########################################################################

=head1 NAME

DBIx::SQLEngine::Criteria::NumericLesser - Old name for Lesser

=head1 SYNOPSIS

  my $crit = DBIx::SQLEngine::Criteria::Lesser->new( $expr, $value );


=head1 DESCRIPTION

DBIx::SQLEngine::Criteria::NumericLesser is the old name for DBIx::SQLEngine::Criteria::Lesser.


=head1 SEE ALSO

See L<DBIx::SQLEngine::Criteria> and L<DBIx::SQLEngine::Criteria::Comparison>
for more information on using these objects.

See L<DBIx::SQLEngine> for the overall interface and developer documentation.

See L<DBIx::SQLEngine::Docs::ReadMe> for general information about
this distribution, including installation and license information.

=cut
