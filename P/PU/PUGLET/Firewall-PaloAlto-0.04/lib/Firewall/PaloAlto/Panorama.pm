package Firewall::PaloAlto::Panorama;
use Moose;

use 5.006;
use strict;
use warnings;

use XML::Simple;
use LWP::UserAgent;
use Carp;
use Data::Dumper qw(Dumper);
use Modern::Perl;
use Params::Validate qw(:all);

use List::Flatten;

with 'Firewall::PaloAlto::Common';

our $VERSION = '0.02';


=head1 NAME

Firewall::PaloAlto::Panorama - Interact with a Palo Alto Panorama controller's API through Perl.

=cut

=head1 SYNOPSIS

The Firewall::PaloAlto::Panorama module provides interfaces into the XML API of a Palo Alto Panorama contoller.

    use Firewall::PaloAlto::Panorama;

    my $fw = Firewall::PaloAlto::Panorama->new(host => 'panorama.local', username => 'admin', password => 'admin');
    $fw->connect();
=cut

=head1 CLASS METHODS

=head2 new(%parameters)

The constructor generates a new object to interact with a specific firewall.

The host, username and password parameters are mandatory.
If not specified, SSL is used, but it can be disabled using the argument ssl => 0

Detailed debugging can be turned on using the debu => 1 argument. It is off by default.

    my $palo = Firewall::PaloAlto::Panorama->new(host => 'panorama.local', username => 'admin', password => 'admin', ssl => 0, debug => 1);

B<Parameters>

=over 

=item * 
host - the hostname or IP of the Panorama to connect to.

=item *
username - a username to connect to the Panorama.

=item *
password - a password to connect to the Panorama.

=item *
ssl (optional, default: 1) - use SSL to connect to the firewall. 

=item *
debug (optional, default: 0) - print debugging messages.

=back

=cut


=head1 OBJECT METHODS 

=head2 connect()

The connect functions connects to the Palo Alto, validates and saves the API key.
It has no parameters

 $pa->connect();

=cut

sub connect {
    my $self = shift; 

    my $user_agent = LWP::UserAgent->new(ssl_opts => { verify_hostname => 0 });
    my $key_request = HTTP::Request->new(GET => $self->base_uri.'type=keygen&user='.$self->username.'&password='.$self->password);

    $self->_debug_print("Key Request", "");

    my $key_response = $user_agent->request($key_request);

    my $pa_response = $self->_check_response_and_parse($key_response);

    if ($pa_response->{status} eq 'success') {
        $self->_api_key($pa_response->{result}->{key});
        return 1;
    }
    else {
        return 0;
    }   
}

=head2 managed_device($action, serial => 'device_serial')

Adds or removes a devices from Panorama.

    $pano->managed_device('set', serial => '001801003449');

=cut

sub managed_device {
    my $self = shift;
    my $action = shift;
    my (%args) = @_;

    if ($action eq 'set' || $action eq 'get' || $action eq 'delete') {
        validate(@_, { serial => 1 });
    }
    else { croak "Unknown action: $action"; }

    my $requester = $self->_create_requester(type => 'config', action => $action);

    $requester->(xpath => "/config/mgt-config/devices", element => "<entry name='$args{serial}'/>");
}
    

=head2 template($action, %parameters)

Creates or retreives a new template

 $pano->template('set', name => "New_Template"); 

B<Parameters>

=over

=item * $action - perform an action: ['get' | 'set' | 'delete']

=item * name - the name of the template

=item * description - a description of the template

=back

=cut

sub template {
    my $self = shift;
    my $action = shift;
    my (%args) = @_;

    if ($action eq 'set') { 
        validate(@_, { name => 1, description => 0 }); 
        #If no description supplied, the description is set to nothing.
        $args{description} //= $args{name};
    }
    elsif ($action eq 'get' or $action eq 'delete') { 
        validate(@_, { name => 1 }) 
    }
    else { croak "Unknown action: $action"; }


    my $template_name = delete $args{name};
    my $requester = $self->_create_requester(type => 'config', action => $action);

    my $elements = $self->_generate_elements(%args);

    return $requester->(xpath => "/config/devices/entry[\@name='localhost.localdomain']/template/entry[\@name='$template_name']", element => $elements);
}


=head2 template_stack($action, %parameters)

Creates or retreives a new template stack.

 $pano->template_stack('set', name => "New_Template", templates => [ Template1, Template2 ], description => "A Template Stack"); 

B<Parameters>

=over

=item * $action - perform an action: ['get' | 'set' | 'delete']

=item * name - the name of the template stack

=item * templates - an anonymous array of templates that make up the stack

=item * description - a description of the template

=back

=cut

sub template_stack {
    my $self = shift;
    my $action = shift;
    my (%args) = @_;

    if ($action eq 'set') { 
        validate(@_, { name => 1, templates => { type => ARRAYREF }, description => 0 }); 
    }
    elsif ($action eq 'get' or $action eq 'delete') { 
        validate(@_, { name => 1 }) 
    }
    else { croak "Unknown action: $action"; }

    my $stack_name = delete $args{name};
    my $requester = $self->_create_requester(type => 'config', action => $action);

    my $elements = $self->_generate_elements(%args);

    return $requester->(xpath => "/config/devices/entry[\@name='localhost.localdomain']/template-stack/entry[\@name='$stack_name']", element => $elements);
}

=head2 devicegroup($action, %parameters)

Creates or retreives a new device group.

 $pano->devicegroup('set', name => "DG_Name", description => "A Device Group"); 

B<Parameters>

=over

=item * $action - perform an action: ['get' | 'set' | 'delete']

=item * name - the name of the device group

=item * description - a description of the device group

=back

=cut

sub devicegroup {
    my $self = shift;
    my $action = shift;
    my (%args) = @_;

    if ($action eq 'set') {
        validate(@_, { name => 1, description => 0 });
        $args{description} //= '';
    }
    elsif ($action eq 'get' or $action eq 'delete') {
        validate(@_, { name => 1 })
    }
    else { croak "Unknown action: $action"; }

    my $dg_name= delete $args{name};
    my $requester = $self->_create_requester(type => 'config', action => $action);

    my $elements = $self->_generate_elements(%args);

    return $requester->(xpath => "/config/devices/entry[\@name='localhost.localdomain']/device-group/entry[\@name='$dg_name']", element => $elements);
}

=head2 vsys($action, %parameters)

Creates a new virtual system through a template

 $pano->vsys('set', template => "DG_Name", name => "VSys Name", max_sessions => 100, max_rules => 100); 

B<Parameters>

=over

=item * $action - perform an action: ['get' | 'set' | 'delete']

=item * template - the name of the template to provision the vsys through

=item * name - the name of the new virtual system

=item * max_sessions - the maximum number of sessions the vsys can supprot

=item * max_rules - the maximum number of security rules the vsys can support

=back

=cut

sub vsys {
    my $self = shift;
    my $action = shift;

    my (%args) = @_;

    if ($action eq 'set') { 
        validate(@_, { template => 1, name => 1, max_sessions => 1, max_rules => 1 }); 
        #Modify the names of the rules and session keys
        $args{'max-sessions'} = delete $args{max_sessions};
        $args{'max-security-rules'} = delete $args{max_rules};
    }
    elsif ($action eq 'get' or $action eq 'delete') { 
        validate(@_, { template => 1, name => 1}) 
    }
    else { croak "Unknown action: $action"; }

    my $template_name= delete $args{template};
    my $vsys_name= delete $args{name};

    my $requester = $self->_create_requester(type => 'config', action => $action);

    my $elements = $self->_generate_elements(%args);

    return $requester->(xpath => "/config/devices/entry[\@name='localhost.localdomain']/template/entry[\@name='$template_name']/config/devices/entry[\@name='localhost.localdomain']/vsys/entry[\@name='$vsys_name']/import/resource", 
                        element => $elements);
}

=head2 subinterface($action, %parameters)

Creates a new subinterface through a template

 $pano->subinterface('set', template => "Template_Name", parent => ethernet1/1, tag => 16, comment => 'Subinterface 16');

B<Parameters>

=over

=item * $action - perform an action: ['get' | 'set' | 'delete']

=item * template - the name of the template to provision the interface into

=item * parent - the parent interface to place the sub-interface under

=item * tag -  the sub-interface ID and VLAN tag 

=item * comment - a string to describe the sub-interface

=back

=cut

sub subinterface {
    my $self = shift;
    my $action = shift;
    my (%args) = @_;

    if ($action eq 'set') { 
        validate(@_, { template => 1, parent => 1, tag => 1, comment => 0 }); 
    }
    elsif ($action eq 'get' or $action eq 'delete') { 
        validate(@_, { template => 1, name => 1}) 
    }
    else { croak "Unknown action: $action"; }

    my $template_name= delete $args{template};
    my $parent = delete $args{parent};

    #We get the characters from the parent which allow us to match the parent's full name
    my %parent_full_name = ( ae => 'aggregate-ethernet',
                             ethernet => 'ethernet',
                         );
    my ($parent_string) = $parent =~ /[a-zA-Z]+/;
    croak "Unkown parent interface" if undef $parent_string;
    say $parent_string;
    

    my $requester = $self->_create_requester(type => 'config', action => $action);

    my $elements = $self->_generate_elements(%args);

    return $requester->(xpath => "/config/devices/entry[\@name='localhost.localdomain']/template/entry[\@name='$template_name']/config/devices/entry[\@name='localhost.localdomain']/network/interface/$parent_string/entry[\@name='$parent']/layer3/units/entry[\@name='$parent.$args{tag}']", 
                        element => $elements);
}

=head2 virtual_router($action, %parameters)

Creates a new virtual router through a template

 $pano->virtual_router('set', template => "Template_Name", name => "VR Name", interface => [ 'ethernet1/1', 'ethernet1/2']); 

B<Parameters>

=over

=item * $action - perform an action: ['get' | 'set' | 'delete']

=item * template - the name of the template to provision the interface into

=item * name - the name of the virtual router

=item * interface - an anonymous list of interfaces to add to the virtual router

=back

=cut

sub virtual_router {
    my $self = shift;
    my $action = shift;
    my (%args) = @_;

    my $template_name = delete $args{template};
    my $vr_name = delete $args{name};

    my $requester = $self->_create_requester(type => 'config', action => $action);

    my $elements = $self->_generate_elements(%args);

    return $requester->(xpath => "/config/devices/entry[\@name='localhost.localdomain']/template/entry[\@name='$template_name']/config/devices/entry[\@name='localhost.localdomain']/network/virtual-router/entry[\@name='$vr_name']", 
                        element => $elements);
}

=head1 OPERATIONAL COMMANDS

=head2 commit_panorama() 

Commits the current candidate configuration on the Panorama device

    $pano->commit_panorama();

=cut

sub commit_panorama {
    my $self = shift;

    my $requester = $self->_create_requester(type => 'commit');

    return $requester->(cmd => '<commit></commit>');
}


=head2 commit_template

Commit a template to the device that it is associated with

=cut

sub commit_template {
    my $self = shift;
    my %args = @_;

    validate(@_, { template => 1 });

    my $requester = $self->_create_requester(type => 'commit', action => 'all');

    return $requester->(cmd => "<commit-all><template><name>$args{template}</name></template></commit-all>");
}

=head2 commit_device_group

Commit a device_group to the device that it is associated with

=cut

sub commit_device_group {
    my $self = shift;
    my %args = @_;

    validate(@_, { device_group => 1 });

    my $requester = $self->_create_requester(type => 'commit', action => 'all');

    return $requester->(cmd => "<commit-all><shared-policy><device-group><entry name='$args{device_group}'/></device-group></shared-policy></commit-all>");
}
=head1 AUTHOR

Greg Foletta, C<< <greg at foletta.org> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-firewall-paloalto at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=Firewall-PaloAlto>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.



=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc Firewall::PaloAlto


You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=Firewall-PaloAlto>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/Firewall-PaloAlto>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/Firewall-PaloAlto>

=item * Search CPAN

L<http://search.cpan.org/dist/Firewall-PaloAlto/>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

Copyright 2015 Greg Foletta.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


=cut

1; # End of Firewall::PaloAlto
