package Firewall::PaloAlto::SystemInfo;

use Moose;

use 5.006;
use strict;
use warnings;
use Modern::Perl;

use Data::Dumper;

our $VERSION = '0.01';

=head1 NAME

Firewall::PaloAlto::SystemInfo - Class returned when requesting system information from the firewall.

=cut

=head1 CLASS METHODS

=head2 new($http_response)

Taken an HTTP::Response object, which is returned from the API call.

=cut

=head1 OBJECT METHODS

=head2 status
Returns 1 if the API call was successful, or 0 if it was not.
=cut 
has status => ( is => 'ro' );

has 'threat-release-date' => (  is => 'ro',
                                reader => 'threat_release_date');
has 'av-release-date' => (  is => 'ro',
                            reader => 'av_release_date');
has 'vm-mac-count' => (     is => 'ro',
                            reader => 'vm_mac_count');
=head2 ip_address
Returns the IPv4 address of the management interface
=cut
has 'ip-address' => (       is => 'ro',
                            reader => 'ip_address');
=head2 ipv6address
Returns the IPv6 address of the management interface
=cut
has 'ipv6-address' => (     is => 'ro',
                            reader => 'ipv6address');
=head2 platform_family
Returns a string denoting the platform family {'PA-VM', '3000', '5000', '7000'}.
=cut
has 'platform-family' => (  is => 'ro',
                            reader => 'platform_family');
has 'wf-private-version' =>(    is => 'ro',
                                reader => 'wf_private_version');
has 'app-version' => (  is => 'ro',
                        reader => 'app_version');
has 'ipv6-default-gateway' => (  is => 'ro',
                                reader => 'ipv6_gw');
has 'family' => ( is => 'ro');
has 'wf-private-release-date' => (  is => 'ro',
                                    reader => 'wf_private_release_date');
=head2 multi_vsys
Returns 'off' or 'on' depending on whether the device supports multiple virtual systems.
=cut
has 'multi-vsys' => (   is =>'ro',
                        reader => 'multi_vsys');
has 'vm-uuid' => (  is => 'ro',
                    reader => 'vm_uuid');
has 'wildfire-release-date'=> (     is => 'ro',
                                    reader => 'wildfire_release_date');
=head2 time
Returns a string denoting the current time on the device, e.g. Tue Dec 15 05:59:49 2015
=cut
has 'time' => ( is => 'ro');
has 'logdb-version' => (    is => 'ro',
                            reader => 'logdb_version');
has 'av-version' => (   is =>'ro',
                        reader => 'av_version');
has 'vm-cpuid' => ( is => 'ro',
                    reader => 'vm_cpuid');
=head2 devicename
Returns the hostname of the device.
=cut
has 'devicename' => (   is => 'ro');
has 'netmask' => ( is => 'ro');
has 'vpn-disabl-mode' => (  is => 'ro',
                            reader => 'vpn_disable_mode');
has 'global-protect-datafile-version' => (  is => 'ro',
                                            reader => 'gp_datafile_version');
=head2 serial
Returns the serial number of the device.
=cut
has 'serial' => ( is => 'ro');
has 'global-protect-client-package-version' => (    is => 'ro',
                                                    reader => 'gp_protect_version');
has 'vm-license' => (   is => 'ro',
                        reader => 'vm_license');
has 'hostname' => ( is => 'ro');
has 'threat-version' => (   is => 'ro',
                            reader => 'threat_version');
=head2 model
Returns a string denoting the model of the device, e.g. 'PA-7050'.
=cut
has 'model' => ( is => 'ro');
has 'url-db' => (   is => 'ro',
                    reader => 'url_db');
has 'sw-version' => (   is => 'ro',
                        reader => 'version');
has 'vm-mac-base' => (  is => 'ro',
                        reader => 'vm_mac_base');
has 'wildfire-version' => (     is => 'ro',
                                reader => 'wf_version');
has 'global-protect-datafile-release-date' => ( is => 'ro',
                                                reader => 'gp_datafile_date');
=head2 uptime
Returns a string denoting the uptime of the device, e.g. '98 days, 5:03:25'.
=cut
has 'uptime' => ( is => 'ro');
has 'default-gateway' => (  is => 'ro',
                            reader => 'gw');
has 'ipv6-link-local-address' => (  is => 'ro',
                                    reader => 'ipv6_link_local');
has 'operational-mode' => ( is => 'ro',
                            reader => 'mode');
has 'url-filtering-version' => (    is => 'ro',
                                    reader => 'url_filtering_version');
has 'mac-address' => (  is => 'ro',
                        reader => 'macaddress');
has 'app-release-date' => ( is => 'ro',
                            reader => 'app_release_date');
                        

around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;
    my $http_response = shift;

    if ($http_response->{status} eq 'success') {
        $http_response->{result}->{system}->{status} = 1;
    }
    else {
        $http_response->{result}->{system}->{status} = 0;
    }


    return $class->$orig($http_response->{result}->{system});
};


=head1 AUTHOR

Greg Foletta, C<< <greg at foletta.org> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-firewall-paloalto-common at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=Firewall-PaloAlto-Common>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.




=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc Firewall::PaloAlto::Common


You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=Firewall-PaloAlto-Common>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/Firewall-PaloAlto-Common>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/Firewall-PaloAlto-Common>

=item * Search CPAN

L<http://search.cpan.org/dist/Firewall-PaloAlto-Common/>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

Copyright 2015 Greg Foletta.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


=cut

1; # End of Firewall::PaloAlto::Common
