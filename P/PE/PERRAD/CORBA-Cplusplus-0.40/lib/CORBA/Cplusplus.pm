use strict;
use warnings;

package CORBA::Cplusplus;

our $VERSION = '0.40';

use CORBA::Cplusplus::LiteralVisitor;
use CORBA::Cplusplus::NameVisitor;
use CORBA::Cplusplus::LengthVisitor;
use CORBA::Cplusplus::TypeVisitor;
use CORBA::Cplusplus::IncludeVisitor;

1;

