package Acme::PERLANCAR::Test;

use 5.010001;
use strict;
use warnings;

1;

=pod

=encoding UTF-8

=head1 NAME

Acme::PERLANCAR::Test - Distribution to test various things

=head1 VERSION

This document describes version 0.28 of Acme::PERLANCAR::Test (from Perl distribution Acme-PERLANCAR-Test), released on 2015-11-19.

=head1 DESCRIPTION

You should not be using this module. This distribution contains various tests
that are mostly only of concern to me.

=head1 HOMEPAGE

Please visit the project's homepage at L<https://metacpan.org/release/Acme-PERLANCAR-Test>.

=head1 SOURCE

Source repository is at L<https://github.com/perlancar/perl-Acme-PERLANCAR-Test>.

=head1 BUGS

Please report any bugs or feature requests on the bugtracker website L<https://rt.cpan.org/Public/Dist/Display.html?Name=Acme-PERLANCAR-Test>

When submitting a bug or request, please include a test-file or a
patch to an existing test-file that illustrates the bug or desired
feature.

=head1 AUTHOR

perlancar <perlancar@cpan.org>

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2015 by perlancar@cpan.org.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut

__END__
# ABSTRACT: Distribution to test various things

# DATE
# DIST
# VERSION

