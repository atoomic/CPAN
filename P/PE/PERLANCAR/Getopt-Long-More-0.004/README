SYNOPSIS

     use Getopt::Long::More; # imports GetOptions as well as optspec; you can also
                             # explicitly import Configure, GetOptionsFromArray,
                             # GetOptionsFromString
    
     my %opts;
     GetOptions(
         # just like in Getopt::Long
         'foo=s' => \$opts{foo},
         'bar'   => sub { ... },
    
         # but if you want to specify extra stuffs...
         'baz'   => optspec(
             # at least specify this, for Getopt::Long
             handler => \$opts{baz},
    
             # specify that this option is required
             required => 1,
    
             # specify this for default value
             default => 10,
    
             # specify this if you want nicer usage message
             summary => 'Blah blah blah',
    
             # specify longer (multiparagraphs) of text for POD, in POD format
             description => <<'_',
    Blah blah ...
    blah

    Blah blah ... blah blah _

             # provide completion from a list of strings
             # completion => [qw/apple apricot banana/],
    
             # provide more advanced completion routine
             completion => sub {
                 require Complete::Util;
                 my %args = @_;
                 Complete::Util::complete_array_elem(
                     word => $args{word},
                     array => [ ... ],
                 );
             },
         ),
     );

DESCRIPTION

    This module is a wrapper and drop-in replacement for Getopt::Long. It
    provides the same interface as Getopt::Long and, unlike other wrappers
    like Getopt::Long::Complete or Getopt::Long::Modern it does not change
    default configuration and all Getopt::Long configuration are supported.
    In fact, Getopt::Long::More behaves much like Getopt::Long until you
    start to use optspec object as one or more option handlers.

OPTSPEC OBJECT

    In addition to using scalarref, arrayref, hashref, or coderef as the
    option handler as Getopt::Long allows, Getopt::Long::More also allows
    using optspec object as option handler. This allows you to specify more
    stuffs. Optspec object is created using the optspec function which
    accepts a list of property name-property value pairs:

     '--fruit=s' => optspec(
         handler => \$opts{fruit},
         default => 'apple',
         summary => 'Supply name of fruit to order',
         completion => [qw/apple apricot banana/],
         ...
     )

    At least the handler property must be specified, as this will be passed
    to Getopt::Long when parsing options. In addition to that, these other
    properties are also recognized:

 required => bool

    Set this to 1 to specify that the option is required.

 default => any

    Provide default for the option.

 summary => str

    Provide a short summary message for the option. This is used when
    generating usage/help message.

 description => str

    Provide a longer (multiparagraph) text, in POD format. Will be used to
    generate POD.

 completion => array|code

    Provide completion routine. Can also be a simple array of strings.

    Completion routine will be passed a hash argument, with at least the
    following keys: word (str, the word to be completed). It is expected to
    return a completion answer structure (see Complete for mor edetails)
    which is usually just an array of strings.

FUNCTIONS

 Configure

    See Getopt::Long documentation.

 GetOptionsFromArray

    See Getopt::Long documentation.

 GetOptionsFromString

    See Getopt::Long documentation.

 GetOptions

    See Getopt::Long documentation.

 HelpMessage(@opts_spec) => str

    Will print a usage/help message and exit. Sample result:

     myapp [options]
    
     Options:
       --fruit=s     Supply name of fruit to order (default: apple)
       --debug       Enable debug mode
       --help|?      Print help message and exit
       --version     Print usage message and exit

 VersionMessage

    See Getopt::Long documentation.

 OptionsPod(@opts_spec) => str

    Will generate a POD containing list of options. The text will be taken
    from the summary and description properties of optspec objects. Example
    result:

     =head1 OPTIONS
    
     =head2 --fruit|f=s
    
     Supply name of fruit to order.
    
     Blah blah blah
     blah blah ...
    
     =head2 --debug
    
     =head2 --version
    
     Display program version and exit.
    
     =head2 --help
    
     Display help message and exit.

 optspec(%props) => obj

    Create optspec object. See "OPTSPEC OBJECT".

COMPLETION

    Getopt::Long::Mode supports shell tab completion. To activate tab
    completion, put your script (e.g. myapp.pl) in PATH and in bash shell
    type:

     % complete -C myapp.pl myapp.pl

    You can then complete option names (or option values or command-line
    arguments too, if you provide completion properties). You can also use
    shcompgen to activate shell completion; shcompgen supports several
    shells and various modules.

    Tab completion functionality is provided by Complete::Getopt::Long.
    Note that this module assumes no_ignore_case and does not support
    things like getopt_compat (starting option with + instead of --).

FAQ

 How do I provide completion for command-line arguments:

    Use the option spec <>:

     GetOptions(
         ...
         '<>' => optspec(
             handler => \&process,
             completion => sub {
                 ...
             },
         ),
     );

SEE ALSO

    Getopt::Long

    Other Getopt::Long wrappers that provide extra features:
    Getopt::Long::Complete, Getopt::Long::Descriptive.

    If you want less features instead of more: Getopt::Long::Less,
    Getopt::Long::EvenLess.

