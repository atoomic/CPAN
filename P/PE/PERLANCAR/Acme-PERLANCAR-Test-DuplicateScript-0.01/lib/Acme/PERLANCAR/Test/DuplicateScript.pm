package Acme::PERLANCAR::Test::DuplicateScript;

our $DATE = '2016-02-10'; # DATE
our $VERSION = '0.01'; # VERSION

1;
# ABSTRACT: Test duplicate script names in a distribution

__END__

=pod

=encoding UTF-8

=head1 NAME

Acme::PERLANCAR::Test::DuplicateScript - Test duplicate script names in a distribution

=head1 VERSION

This document describes version 0.01 of Acme::PERLANCAR::Test::DuplicateScript (from Perl distribution Acme-PERLANCAR-Test-DuplicateScript), released on 2016-02-10.

=head1 DESCRIPTION

This distribution contains the following scripts:

 bin/test-duplicate-script
 script/test-duplicate-script

which have the same base name. This distribution is for testing purposes only.

=head1 HOMEPAGE

Please visit the project's homepage at L<https://metacpan.org/release/Acme-PERLANCAR-Test-DuplicateScript>.

=head1 SOURCE

Source repository is at L<https://github.com/perlancar/perl-Acme-PERLANCAR-Test-DuplicateScript>.

=head1 BUGS

Please report any bugs or feature requests on the bugtracker website L<https://rt.cpan.org/Public/Dist/Display.html?Name=Acme-PERLANCAR-Test-DuplicateScript>

When submitting a bug or request, please include a test-file or a
patch to an existing test-file that illustrates the bug or desired
feature.

=head1 AUTHOR

perlancar <perlancar@cpan.org>

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2016 by perlancar@cpan.org.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
