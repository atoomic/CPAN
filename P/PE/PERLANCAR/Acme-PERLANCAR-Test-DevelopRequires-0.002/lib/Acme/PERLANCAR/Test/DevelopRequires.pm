package Acme::PERLANCAR::Test::DevelopRequires;

our $DATE = '2016-10-29'; # DATE
our $VERSION = '0.002'; # VERSION

use 5.010001;
use strict;
use warnings;

# from Regexp::IPv4
our $re = qr/(?:(?:25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{1,2})(?:\.(?:25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{1,2})){3})/;

1;
# ABSTRACT: Distribution that has a DevelopRequires prereq

__END__

=pod

=encoding UTF-8

=head1 NAME

Acme::PERLANCAR::Test::DevelopRequires - Distribution that has a DevelopRequires prereq

=head1 VERSION

This document describes version 0.002 of Acme::PERLANCAR::Test::DevelopRequires (from Perl distribution Acme-PERLANCAR-Test-DevelopRequires), released on 2016-10-29.

=head1 DESCRIPTION

This distribution is created for testing.

=head1 HOMEPAGE

Please visit the project's homepage at L<https://metacpan.org/release/Acme-PERLANCAR-Test-DevelopRequires>.

=head1 SOURCE

Source repository is at L<https://github.com/perlancar/perl-Acme-PERLANCAR-Test-DevelopRequires>.

=head1 BUGS

Please report any bugs or feature requests on the bugtracker website L<https://rt.cpan.org/Public/Dist/Display.html?Name=Acme-PERLANCAR-Test-DevelopRequires>

When submitting a bug or request, please include a test-file or a
patch to an existing test-file that illustrates the bug or desired
feature.

=head1 AUTHOR

perlancar <perlancar@cpan.org>

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2016 by perlancar@cpan.org.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
