DESCRIPTION

    This module provides several convenient functions related to version
    numbers, e.g. for comparing them.

FUNCTIONS

 cmp_version($v1, $v2) => -1|0|1

    Equivalent to:

     version->parse($v1) <=> version->parse($v2)

 version_eq($v1, $v2) => BOOL

 version_ne($v1, $v2) => BOOL

 version_lt($v1, $v2) => BOOL

 version_le($v1, $v2) => BOOL

 version_gt($v1, $v2) => BOOL

 version_ge($v1, $v2) => BOOL

 version_between($v, $v1, $v2[, $v1b, $v2b, ...]) => BOOL

 version_in($v, $v1[, $v2, ...]) => BOOL

 add_version($v, $increment) => $new_v

    Add $increment to version $v. Both increment and version must match:

     /\Av?\d{1,3}(?:\.\d{1,3}){0,2}\z/

    so trial/dev releases like v1.2.3_1 are not currently supported. Some
    examples:

     0.1 + 0.1 -> 0.2
     0.01 + 0.001 -> 0.011
     0.01 + 0.1 -> 0.11
     0.9 + 0.1 -> 1.0
     0.99 + 0.1 -> 1.09
     1.1.0 + 0.0.1 -> 1.1.1

 subtract_version($v, $decrement) => $new_v

    Subtract $decrement from version $v. This is the reverse operation for
    add_version.

    Will die if the result is negative.

SEE ALSO

    version

