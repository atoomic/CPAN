package App::ValidateCPANMetaWithSah;

our $DATE = '2017-01-08'; # DATE
our $VERSION = '0.002'; # VERSION

1;
# ABSTRACT: Validate CPAN Meta specification with Sah schema

__END__

=pod

=encoding UTF-8

=head1 NAME

App::ValidateCPANMetaWithSah - Validate CPAN Meta specification with Sah schema

=head1 VERSION

This document describes version 0.002 of App::ValidateCPANMetaWithSah (from Perl distribution App-ValidateCPANMetaWithSah), released on 2017-01-08.

=head1 SYNOPSIS

See the included L<validate-cpan-meta-with-sah>.

=head1 DESCRIPTION

This is a proof-of-concept for validating CPAN Meta specification with Sah
schema.

=head1 HOMEPAGE

Please visit the project's homepage at L<https://metacpan.org/release/App-ValidateCPANMetaWithSah>.

=head1 SOURCE

Source repository is at L<https://github.com/perlancar/perl-App-ValidateCPANMetaWithSah>.

=head1 BUGS

Please report any bugs or feature requests on the bugtracker website L<https://rt.cpan.org/Public/Dist/Display.html?Name=App-ValidateCPANMetaWithSah>

When submitting a bug or request, please include a test-file or a
patch to an existing test-file that illustrates the bug or desired
feature.

=head1 SEE ALSO

L<CPAN::Meta>, L<CPAN::Meta::Validator>

=head1 AUTHOR

perlancar <perlancar@cpan.org>

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2017 by perlancar@cpan.org.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
