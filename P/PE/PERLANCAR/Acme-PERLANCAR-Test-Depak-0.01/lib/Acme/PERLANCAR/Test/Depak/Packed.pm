package Acme::PERLANCAR::Test::Depak::Packed;

our $DATE = '2015-10-21'; # DATE
our $VERSION = '0.01'; # VERSION

our %PACKED_MODULES = %{ {"Acme::PERLANCAR::Test::Depak"=>0.01,"Acme::PERLANCAR::Test::Depak::Packed"=>0.01,"File::HomeDir"=>1.00,"PERLANCAR::File::HomeDir"=>0.02} }; # PACKED_MODULES
our @PACKED_DISTS = @{["File-HomeDir","PERLANCAR-File-HomeDir"]}; # PACKED_DISTS

1;
# ABSTRACT: Test module (just an empty module for metadata)

__END__

=pod

=cut
