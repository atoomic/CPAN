package App::BenchStartupCompleters;

our $DATE = '2015-08-19'; # DATE
our $VERSION = '0.02'; # VERSION

1;
# ABSTRACT: Benchmark startup time of some shell completers

__END__

=pod

=encoding UTF-8

=head1 NAME

App::BenchStartupCompleters - Benchmark startup time of some shell completers

=head1 VERSION

This document describes version 0.02 of App::BenchStartupCompleters (from Perl distribution App-BenchStartupCompleters), released on 2015-08-19.

=head1 SYNOPSIS

See the included script L<bench-startup-completers>.

=head1 HOMEPAGE

Please visit the project's homepage at L<https://metacpan.org/release/App-BenchStartupCompleters>.

=head1 SOURCE

Source repository is at L<https://github.com/perlancar/perl-App-BenchStartupCompleters>.

=head1 BUGS

Please report any bugs or feature requests on the bugtracker website L<https://rt.cpan.org/Public/Dist/Display.html?Name=App-BenchStartupCompleters>

When submitting a bug or request, please include a test-file or a
patch to an existing test-file that illustrates the bug or desired
feature.

=head1 AUTHOR

perlancar <perlancar@cpan.org>

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2015 by perlancar@cpan.org.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
