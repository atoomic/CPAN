#  You may distribute under the terms of either the GNU General Public License
#  or the Artistic License (the same terms as Perl itself)
#
#  (C) Paul Evans, 2015 -- leonerd@leonerd.org.uk

package Device::BusPirate::Chip::MAX7219;

use strict;
use warnings;

our $VERSION = '0.03';

=head1 NAME

C<Device::BusPirate::Chip::MAX7219> - deprecated

=head1 DESCRIPTION

This module has moved. it now lives at L<Device::Chip::MAX7219> because it now
implements the L<Device::Chip> interface.

To use it, replace

   my $pirate = Device::BusPirate->new( @pirate_args );
   my $max = $pirate->mount_chip( "MAX7219" )->get;

with

   my $max = Device::Chip::MAX7219->new;
   $max->connect(
      Device::Chip::Adapter::BusPirate->new( @pirate_args )
   )->get;

Then proceed to use the C<$max> device as before.

=cut

0x55AA;
