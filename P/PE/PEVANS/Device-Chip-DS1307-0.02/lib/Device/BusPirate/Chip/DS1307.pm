#  You may distribute under the terms of either the GNU General Public License
#  or the Artistic License (the same terms as Perl itself)
#
#  (C) Paul Evans, 2015 -- leonerd@leonerd.org.uk

package Device::BusPirate::Chip::DS1307;

use strict;
use warnings;

our $VERSION = '0.02';

=head1 NAME

C<Device::BusPirate::Chip::DS1307> - deprecated

=head1 DESCRIPTION

This module has moved. it now lives at L<Device::Chip::DS1307> because it
now implements the L<Device::Chip> interface.

To use it, replace

   my $pirate = Device::BusPirate->new( @pirate_args );
   my $chip = $pirate->mount_chip( "DS1307" )->get;

with

   my $chip = Device::Chip::DS1307->new;
   $chip->connect(
      Device::Chip::Adapter::BusPirate->new( @pirate_args )
   )->get;

Then proceed to use the C<$chip> device as before.

=cut

0x55AA;
