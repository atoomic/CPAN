#!/usr/bin/perl

use strict;
use warnings;

use Test::More;
use Test::Async::HTTP 0.02; # ->GET
use t::Util;

use IO::Async::Loop;
use Net::Async::Matrix;
use Future;

my $matrix = Net::Async::Matrix->new(
   ua => my $ua = Test::Async::HTTP->new,
   server => "localserver.test",

   make_delay => sub { Future->new },
);

IO::Async::Loop->new->add( $matrix ); # for ->loop->new_future
matrix_login( $matrix, $ua );

my $room = matrix_join_room( $matrix, $ua,
   {  type       => "m.room.member",
      room_id    => "!room:localserver.test",
      state_key  => '@sender:localserver.test',
      membership => "join",
   },
);

$matrix->stop;
pass( '$matrix->stop' );

$matrix->start;

my $p = next_pending_not_events( $ua );
is( $p->request->uri->path, "/_matrix/client/api/v1/initialSync", '$req->uri->path' );

respond_json( $p, {
   end      => "next_token_here",
   presence => [],
   rooms => [
      {
         membership => "join",
         room_id    => "!room:localserver.test",
         messages   => {},
         state      => [],
      },
   ],
});

pass( '$matrix->start does not fail' );

done_testing;
