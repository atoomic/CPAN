#!/usr/bin/perl
#
# Copyright 2011, Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Functional tests for the AdExtensionOverrideService.
#
# Author: David Torres <api.davidtorres@gmail.com>

use strict;
use lib qw(t/util);

use Test::More (tests => 23);
use TestClientUtils qw(get_test_client);
use TestAPIUtils qw(get_api_package create_campaign delete_campaign
                    create_ad_group delete_ad_group create_text_ad
                    delete_text_ad create_campaign_location_extension
                    delete_campaign_ad_extension get_location_for_address);

# Getting API client for functional tests.
my $client = get_test_client();
if (!$client) {
  plan skip_all => "Can't create test client, make sure your " .
      "t/testdata/client.properties is properly set. Skipping functional " .
      "tests.";
}

# Test can use CampaignCriterionService related classes.
use_ok(get_api_package($client, "Address"));
use_ok(get_api_package($client, "AdExtensionOverride"));
use_ok(get_api_package($client, "AdExtensionOverrideOperation"));
use_ok(get_api_package($client, "AdExtensionOverridePage"));
use_ok(get_api_package($client, "AdExtensionOverrideReturnValue"));
use_ok(get_api_package($client, "AdExtensionOverrideSelector"));
use_ok(get_api_package($client, "LocationExtension"));
use_ok(get_api_package($client, "LocationOverrideInfo"));
use_ok(get_api_package($client, "OverrideInfo"));

# API dependencies for this test.
my $campaign = create_campaign($client);
my $ad_group = create_ad_group($client, $campaign->get_id());
my $text_ad = create_text_ad($client, $ad_group->get_id());
my $campaign_ad_extension =
    create_campaign_location_extension($client, $campaign->get_id());
my $address = get_api_package($client, "Address")->new({
  streetAddress => "1600 Amphitheatre Pkwy, Mountain View",
  countryCode => "US"
});
my $location = get_location_for_address($client, $address);

# Test add ad extension override.
my $location_extension =
    get_api_package($client, "LocationExtension")->new({
      id => $campaign_ad_extension->get_adExtension()->get_id(),
      address => $location->get_address(),
      geoPoint => $location->get_geoPoint(),
      encodedLocation => $location->get_encodedLocation(),
      source => "ADWORDS_FRONTEND",
    });
my $override_info = get_api_package($client, "OverrideInfo")->new({
  LocationOverrideInfo =>
      get_api_package($client, "LocationOverrideInfo")->new({
        radius => 20,
        radiusUnits => "KILOMETERS"
      })
});
my $ad_extension_override =
    get_api_package($client, "AdExtensionOverride")->new({
      adId => $text_ad->get_id(),
      overrideInfo => $override_info,
      adExtension => $location_extension
    });
my $result = $client->AdExtensionOverrideService()->mutate({
  operations => [get_api_package($client, "AdExtensionOverrideOperation")->new({
    operator => "ADD",
    operand => $ad_extension_override
  })]
});
ok($result, "Result from adding AdExtensionOverride is valid");
isa_ok($result, get_api_package($client, "AdExtensionOverrideReturnValue"));
ok($result->get_value(), "Result value is valid");
is(scalar(@{$result->get_value()}), 1, "Results array size is correct");
ok($result->get_value()->[0], "Returned AdExtensionOverride is valid");
isa_ok($result->get_value()->[0],
       get_api_package($client, "AdExtensionOverride"));
$ad_extension_override = $result->get_value()->[0];

# Test get ad extension override.
my $selector = get_api_package($client, "AdExtensionOverrideSelector")->new({
  campaignIds => [$campaign->get_id()],
  statuses => ["ACTIVE"]
});
$result = $client->AdExtensionOverrideService()->get({
  selector => $selector
});
ok($result, "Result from getting ad extension override is valid");
isa_ok($result, get_api_package($client, "AdExtensionOverridePage"));
ok($result->get_entries(), "Result entries is valid");
is(scalar(@{$result->get_entries()}), 1, "Result entries length is correct");
isa_ok($result->get_entries()->[0],
       get_api_package($client, "AdExtensionOverride"));
is($result->get_entries()->[0]->get_adId(), $text_ad->get_id(),
   "Ad id of ad extension override is correct");
isa_ok($result->get_entries()->[0]->get_adExtension(),
       get_api_package($client, "LocationExtension"));
is($result->get_entries()->[0]->get_adExtension()->get_id(),
   $campaign_ad_extension->get_adExtension()->get_id(),
   "Returned ad extension override id is correct");

# Cleaning up test dependencies.
delete_campaign_ad_extension($client, $campaign->get_id(),
    $campaign_ad_extension->get_adExtension()->get_id());
delete_text_ad($client, $text_ad->get_id());
delete_ad_group($client, $ad_group->get_id());
delete_campaign($client, $campaign->get_id());
