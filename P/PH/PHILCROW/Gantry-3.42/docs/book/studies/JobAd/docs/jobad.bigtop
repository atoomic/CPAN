config {
    engine MP13;
    template_engine TT;
    Init Std {  }
    SQL Postgres {  }
    Control Gantry { dbix 1; }
    Model GantryDBIxClass {  }
    SiteLook GantryDefault {  }
    HttpdConf Gantry { gantry_conf 1; }
    Conf Gantry { conffile `/etc/gantry.conf.2`; instance jobad; }
}
app JobAd {
    config {
        dbconn `dbi:Pg:dbname=jobad.db` => no_accessor;
        dbuser postgres;
        template_wrapper `genwrapper.tt` => no_accessor;
        root `/home/pcrow/srcgantry/docs/book/studies/JobAd/html` => no_accessor;
        auth_dbconn `dbi:Pg:dbname=sample_auth.db` => no_accessor;
        auth_dbuser postgres;
    }
    table job {
        field id {
            is int4, primary_key, auto;
        }
        field title {
            is varchar;
            label Title;
            html_form_type text;
        }
        field description {
            is varchar;
            label Description;
            html_form_type text;
        }
        field created {
            is datetime;
        }
        field modified {
            is datetime;
        }
        foreign_display `%title`;
        field travel_percent {
            is varchar;
            label `Travel Percent`;
            html_form_type text;
        }
        field on_site {
            is varchar;
            label `On Site`;
            html_form_type select;
            html_form_options
                `On-Site` => onsite,
                `Partial Telecommute` => parttele,
                Telecommute => tele;
        }
        field time_percent {
            is varchar;
            label `Time Percent`;
            html_form_type select;
            html_form_options
                `Full Time` => fulltime,
                `Part Time` => parttime,
                Contract => contract;
        }
        data
            title => `Jr. Meeting Attender`,
            description => `Attend meetings and work on projects`,
            travel_percent => `5%`,
            on_site => onsite,
            time_percent => fulltime;
        data
            title => `Sr. Meeting Attender`,
            description => `Attend meetings and lead projects`,
            travel_percent => `5%`,
            on_site => onsite,
            time_percent => fulltime;
        data
            title => `Chief Financial Obfuscator`,
            description => `Top level financial manager`,
            travel_percent => `5%`,
            on_site => onsite,
            time_percent => fulltime;
    }
    controller Job is AutoCRUD {
        controls_table job;
        rel_location job;
        text_description job;
        page_link_label Job;
        method do_main is main_listing {
            cols title, description;
            header_options Add;
            row_options Edit, Delete;
            title Job;
        }
        method form is AutoCRUD_form {
            all_fields_but id, created, modified;
            extra_keys
                legend => `$self->path_info =~ /edit/i ? 'Edit' : 'Add'`;
        }
        literal Location
            `    require valid-user`;
    }
    table skill {
        field id {
            is int4, primary_key, auto;
        }
        field ident {
            is varchar;
            label Ident;
            html_form_type text;
        }
        field description {
            is varchar;
            label Description;
            html_form_type text;
        }
        field created {
            is datetime;
        }
        field modified {
            is datetime;
        }
        foreign_display `%ident`;
        data
            ident => mtg_convene,
            description => `Convene and run meetings`;
        data
            ident => mtg_attend,
            description => `Attend meetings`;
        data
            ident => good_drive,
            description => `Hold valid driver license and have clean driving record`;
        data
            ident => cook_book_max,
            description => `Have 15+ years practical experience cooking books.`;
    }
    controller Skill is AutoCRUD {
        controls_table skill;
        rel_location skill;
        text_description skill;
        page_link_label Skill;
        method do_main is main_listing {
            cols ident, description;
            header_options Add;
            row_options Edit, Delete;
            title Skill;
        }
        method form is AutoCRUD_form {
            all_fields_but id, created, modified;
            extra_keys
                legend => `$self->path_info =~ /edit/i ? 'Edit' : 'Add'`;
        }
        literal Location
            `    require valid-user`;
    }
    table position {
        field id {
            is int4, primary_key, auto;
        }
        field ident {
            is varchar;
            label Ident;
            html_form_type text;
        }
        field created {
            is datetime;
        }
        field modified {
            is datetime;
        }
        foreign_display `%ident`;
        field job {
            is int4;
            label Job;
            refers_to job;
            html_form_type select;
        }
        field n_openings {
            is int4;
            label `N Openings`;
            html_form_type text;
            html_form_default_value `qr{^\d $}`;
        }
        field location {
            is varchar;
            label Location;
            html_form_type text;
        }
        field closes {
            is date;
            label Closes;
            html_form_type text;
            date_select_text `Select Date`;
        }
        field boss {
            is varchar;
            label Boss;
            html_form_type text;
        }
        field pay {
            is varchar;
            label Pay;
            html_form_type text;
        }
    }
    controller Position is AutoCRUD {
        controls_table position;
        rel_location position;
        text_description position;
        page_link_label Position;
        method do_main is main_listing {
            cols ident, n_openings, closes;
            header_options Add;
            row_options Edit, Delete;
            title Position;
        }
        method form is AutoCRUD_form {
            all_fields_but id, created, modified;
            extra_keys
                legend => `$self->path_info =~ /edit/i ? 'Edit' : 'Add'`,
                javascript => `$self->calendar_month_js( 'position' )`;
            form_name position;
        }
        uses Gantry::Plugins::Calendar;
        literal Location
            `    require group HR`;
    }
    join_table job_skill {
        joins job => skill;
    }
    literal PerlTop
      `    use lib '/home/pcrow/srcgantry/docs/book/studies/JobAd/lib';
    use Gantry::Control::C::Authen qw/-Engine=MP13 -TemplateEngine=TT/;
    use Gantry::Control::C::Authz  qw/-Engine=MP13 -TemplateEngine=TT/;
    use Gantry::Control::C::Authz::PageBased
            qw/-Engine=MP13 -TemplateEngine=TT/;

    use Gantry::Control::C::Users;
    use Gantry::Control::C::Pages;
    use Gantry::Control::C::Groups;`;
    literal Location
      `    AuthType Basic
    AuthName "Our Company"

    PerlAuthenHandler Gantry::Control::C::Authen
    PerlAuthzHandler  Gantry::Control::C::Authz`;
    literal HttpdConf
      `
<Location /apps/control>
    PerlSetVar page_title   'Auth Control'
    PerlSetVar app_rootp    '/apps/control'
    PerlSetVar css_rootp    '/style'
    PerlSetVar img_rootp    '/images'
    PerlSetVar root         '/home/httpd/html/gantry'

    PerlSetVar template_wrapper 'control_wrapper.tt'
</Location>

<Location /apps/control/users>
    SetHandler perl-script
    PerlHandler Gantry::Control::C::Users

    require group User_Admin
</Location>

<Location /apps/control/groups>
    SetHandler perl-script
    PerlHandler Gantry::Control::C::Groups

    require group User_Admin
</Location>

<Location /apps/control/pages>
    SetHandler perl-script
    PerlHandler Gantry::Control::C::Pages

    require group User_Admin
</Location>
`;
}
