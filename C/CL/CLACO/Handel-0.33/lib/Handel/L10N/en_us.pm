# $Id: en_us.pm 768 2005-09-05 02:25:24Z claco $
package Handel::L10N::en_us;
use strict;
use warnings;
use utf8;
use vars qw(%Lexicon);

BEGIN {
    use base 'Handel::L10N';
};

%Lexicon = (
    Language => 'English',
);

1;
__END__

=head1 NAME

Handel::L10N::en_us - Handel Language Pack: US English

=head1 AUTHOR

    Christopher H. Laco
    CPAN ID: CLACO
    claco@chrislaco.com
    http://today.icantfocus.com/blog/