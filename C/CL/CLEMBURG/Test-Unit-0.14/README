NAME
    Test::Unit - a unit testing interface for Perl

DESCRIPTION
    Test::Unit is the procedural style interface to a sophisticated
    unit testing framework for Perl that is derived from the JUnit
    testing framework for Java by Kent Beck and Erich Gamma. 

    While this framework is originally intended to support unit testing
    in an object-oriented development paradigm (with support for
    inheritance of tests etc.), Test::Unit is intended to  provide
    a simpler interface to the framework that is more suitable for
    use in a scripting style environment. 

    Therefore, Test::Unit does not provide much support for an 
    object-oriented approach to unit testing - if you want that, 
    please have a look at Test::Unit::TestCase (also included in 
    this install).

    A short tutorial on the object-oriented approach can be found
    in the documentation of Test::Unit::TestCase (the test 
    baseclass). The Test::Unit self-test suite (contained in
    in Test::Unit::tests::AllTests) is a good example of this
    approach.

    There is also a GUI based interface to the testing framework.
    The "TkTestRunner.pl" script shows how to invoke it.

    The testing framework also features adapters for tests in
    the Test::Harness style to tests in the unit testing framework
    style and vice versa - see Test::Unit::HarnessUnit and 
    Test::Unit::UnitHarness. An example of this approach is the
    self-test of the unit testing framework that you start with
    the 'make test' command (see t/all_tests.t).

PREREQUISITES
    The Tk module is needed for the GUI.

INSTALLATION
    Just perform the usual incantation:

    gunzip Test-Unit-0.14.tar.gz
    tar -xvf Test-Unit-0.14.tar 
    cd Test-Unit-0.14
    perl Makefile.PL
    make
    make test
    make install

AUTHOR
    Copyright (c) 2000, 2001 Christian Lemburg, <lemburg@acm.org>.

    All rights reserved. This program is free software; you can
    redistribute it and/or modify it under the same terms as
    Perl itself.

    Thanks go to the other PerlUnit framework people: 
    Brian Ewins, Cayte Lindner, J.E. Fritz, Zhon Johansen.

    Thanks for patches go to:
    Matthew Astley, David Esposito.

SEE ALSO
    - Test::Unit::TestCase (included in this install)
    - the README file in the examples directory
    - Refactoring. Improving The Design Of Existing Code. 
      Martin Fowler. Addison-Wesley, 1999.
    - http://www.xProgramming.com/

EXAMPLES
    Use TkTestRunner like this:

	perl -w -I./lib TkTestRunner.pl Test::Unit::tests::AllTests

    Use TestRunner.pl like this (lib adjusted for this directory):
	
	perl -w -I./lib TestRunner.pl Test::Unit::tests::AllTests
