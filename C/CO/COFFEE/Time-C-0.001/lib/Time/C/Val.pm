use strict;
use warnings;
package Time::C::Val;
$Time::C::Val::VERSION = '0.001';
use overload (
    '+' => 'add',
    '-' => 'subtract',
    '0+' => 'numify',
    '""' => 'stringify',
    '+=' => 'add_assign',
    '-=' => 'subtract_assign',
    fallback => 1,
);

sub new { my $c = shift; bless { @_ }, $c; }

sub add {
    my ($self, $other, $swap) = @_;

    my $acc = $self->{acc};
    my $meth =
      sprintf "plus_%ss", defined $self->{meth} ? $self->{meth} : $acc;
    return $self->{self}{tm}->$meth($other)->$acc();
}

sub subtract {
    my ($self, $other, $swap) = @_;

    if ($swap) { return $other - $self->{val}; }

    my $acc = $self->{acc};
    my $meth =
      sprintf "minus_%ss", defined $self->{meth} ? $self->{meth} : $acc;
    return $self->{self}{tm}->$meth($other)->$acc();
}

sub numify {
    my ($self, $other, $swap) = @_;
    $self->{val};
}

sub stringify {
    my ($self, $other, $swap) = @_;
    $self->{val};
}

sub add_assign {
    my ($self, $other) = @_;

    my $acc = $self->{acc};
    my $meth =
      sprintf "plus_%ss", defined $self->{meth} ? $self->{meth} : $acc;
    my $tm = $self->{self}{tm}->$meth($other);
    $self->{self}{tm} = $tm;
    $self->{val} = $tm->$acc();
}

sub subtract_assign {
    my ($self, $other) = @_;

    my $acc = $self->{acc};
    my $meth =
      sprintf "minus_%ss", defined $self->{meth} ? $self->{meth} : $acc;
    my $tm = $self->{self}{tm}->$meth($other);
    $self->{self}{tm} = $tm;
    $self->{val} = $tm->$acc();
}

1;

__END__

=pod

=encoding UTF-8

=head1 NAME

Time::C::Val

=head1 VERSION

version 0.001

=head1 AUTHOR

Andreas Guldstrand <andreas.guldstrand@gmail.com>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2016 by Andreas Guldstrand.

This is free software, licensed under:

  The MIT (X11) License

=cut
