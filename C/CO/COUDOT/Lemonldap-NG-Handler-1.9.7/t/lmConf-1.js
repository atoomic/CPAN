{
  "authentication": "Demo",
  "cfgAuthor": "The LemonLDAP::NG team",
  "cfgAuthorIP": "127.0.0.1",
  "cfgDate": 1428138808,
  "cfgLog": "Handler test conf",
  "cfgNum": "1",
  "cookieName": "lemonldap",
  "demoExportedVars": {
    "cn": "cn",
    "mail": "mail",
    "uid": "uid"
  },
  "domain": "example.com",
  "exportedHeaders": {
    "test1.example.com": {
      "Auth-User": "$uid"
    },
    "test2.example.com": {
      "Auth-User": "$uid"
    }
  },
  "exportedVars": {
    "UA": "HTTP_USER_AGENT"
  },
  "globalStorage": "Apache::Session::File",
  "globalStorageOptions": {
    "Directory": "t/sessions",
    "LockDirectory": "t/sessions/lock",
    "generateModule": "Lemonldap::NG::Common::Apache::Session::Generate::SHA256"
  },
  "groups": {},
  "key": "qwertyui",
  "locationRules": {
    "manager.example.com": {
      "(?#Configuration)^/(manager\\.html|conf/)": "$uid eq \"dwho\"",
      "(?#Notifications)^/notifications": "$uid eq \"dwho\" or $uid eq \"rtyler\"",
      "(?#Sessions)^/sessions": "$uid eq \"dwho\" or $uid eq \"rtyler\"",
      "default": "$uid eq \"dwho\""
    },
    "test1.example.com": {
      "^/logout": "logout_sso",
      "^/deny": "deny",
      "default": "accept"
    },
    "test2.example.com": {
      "^/logout": "logout_sso",
      "default": "accept"
    }
  },
  "macros": {
    "_whatToTrace": "$_auth eq 'SAML' ? \"$_user\\@$_idpConfKey\" : \"$_user\""
  },
  "portal": "http://auth.example.com/",
  "reloadUrls": {},
  "userDB": "Demo",
  "whatToTrace": "_whatToTrace"
}
