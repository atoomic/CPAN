# Handler to manage update cookie
package My::Package::UpdateCookie;

use Lemonldap::NG::Handler::UpdateCookie;
@ISA = qw(Lemonldap::NG::Handler::UpdateCookie);

__PACKAGE__->init(
    {

        # See Lemonldap::NG::Handler
    }
);

1;
