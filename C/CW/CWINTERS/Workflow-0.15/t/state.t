# -*-perl-*-

# $Id: state.t,v 1.3 2004/10/17 15:22:26 cwinters Exp $

use strict;
use lib 't';
use TestUtil;
use Test::More  tests => 1;

require_ok( 'Workflow::State' );
