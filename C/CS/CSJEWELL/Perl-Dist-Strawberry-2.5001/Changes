Changes for Perl extension Perl-Dist-Strawberry

2.5001 (TRIAL) To be released
    * This release corresponds to Strawberry Perl 'February' (5.12.3) Beta 2.
    + Added 5.12.3 plugin to use in testing to start with. Other plugins are
      available from CPAN or from Strawberry Perl source control.
    + Uses installed-file-checking routine to avoid crashes during relocation.

2.50 Wed 23 Feb 2011
    * This release corresponds to Strawberry Perl February 2011 Beta 1.

2.11_10 Sun 24 Oct 2010
    * This release corresponds to Strawberry Perl October 2010 Beta 1.
    + Now uses pluggable perl version framework.

2.11 Sun 01 Aug 2010
    * This release corresponds to Strawberry Perl July 2010.
    + Added link to "Ovid's CGI Course"
    + Added links to CPAN modules in the release notes.

2.10_11 Mon 19 Jul 2010
    * This release corresponds to Strawberry Perl July 2010 Beta 2.
    o RT#57929 fixed (C:\cpanplus\ rather than C:\strawberry\cpanplus 
      used)
    + RT#58079 fixed (could not find libmysql_.dll)
    + Added link to README.txt in start menu, and checkbox in .msi
	  to read it at end of installation.

2.10_10 Fri 28 May 2010
    * This release corresponds to Strawberry Perl July 2010 Beta 1. 
    + 5.12.1 support added. 
    o Split start menu.
    + Added Alien::Tidyp and Net::SSH2 to Strawberry.
    + Added more modules to 64-bit Strawberry.

2.1001 Thu 06 May 2010
    * This release corresponds to Bootstrap Perl "April" 2010 
      (which will be the last version of Bootstrap built 
       against 5.10.1)
    * This release ALSO corresponds to Strawberry Perl 5.12.0.1 
      (the May 2010 rebuild) 
    + Updated Bootstrap's list of modules.
    + Fixed bugs #43243 and #57205 in preparation for rebuild.
    - Removed 5.8.9 from default machine.
    + Added XML::Simple to Strawberry (Win32::Exe 0.13+ requires it)

2.10 Sat 01 May 2010
    * This release corresponds to Strawberry Perl April 2010.
    + Adding ability to build against 5.12.0.
    - Removing ability to build against 5.11.5 and 5.12.0-RC0.

2.02_05 Thu 25 Mar 2010
    * This release corresponds to the 5.12.0-RC0 builds of
      Strawberry Perl April 2010 Beta 1.
    + Adding ability to build against 5.12.0-RC0.

2.02_04 Sat 20 Mar 2010
    * This release corresponds to Strawberry Perl April 2010 Beta 1.
    o Changing $Config{installsitebin} to perl/site/bin directory.
    + Installing updated version of PPM that fixes bug RT#44584,
      and a site/lib dependency for the ppm.xml file.
    + Adding module-version script.
    + Adding ability to relocate (only works with 5.11.5 so far)
    + Adding ability to build against 5.11.5.
    + Updating to new versions of most libraries.

2.02_03 Fri 19 Feb 2010
	* Release script is locking up.  Changed it and trying again.

2.02_02 Fri 19 Feb 2010
	* Messed up uploading to PAUSE. Re-uploading.

2.02_01 Fri 19 Feb 2010
	* This release corresponds to Strawberry Perl Professional 5.10.1.1 Alpha 1.
    + Adding SOAP::Lite and Task::Weaken.
	
2.02 Fri 29 Jan 2010
	- This release corresponds to Strawberry Perl January 2010.
	- GDBM_File added.
	- Ability to create a merge module added.

2.01 Wed 11 Nov 2009
	- This release corresponds to Strawberry Perl October 2009.

2.00_02 Sun 27 Sep 2009
	- This release corresponds to Strawberry Perl October 2009
	  Beta 2.
	- Updated to work with Perl::Dist::WiX 1.090 versions.
	- Library-installing code split off into another module.
	- DBD::Pg, DB_File, graphics and crypto modules added.

2.00 Tue 25 Aug 2009
	- This release corresponds to Strawberry Perl July 2009.
	  (Yes, it's late.)
	  It was branched off from 1.09 for the purpose of building using
	  Perl::Dist::WiX, but all changes from 1.10 and 1.11 
	  are included, plus the ones below. (by CSJEWELL unless noted)
	- Store a Strawberry-specific e-mail in the Perl configuration
	- Onion image from perfoundation.org, Link to #win32 via Mibbit (SZABGAB)
	- Moving JSON and JSON::XS to Strawberry
	- Installing DBIx::Simple when CPANPLUS is installed
	- Installing DBD::mysql and local::lib (DBD::Pg code was added, but
	  is not used.)
	- Configure a CPANPLUS::Config with settings along the
	  lines of the CPAN::Config
	- Dropping building 5.8.8 in the default machine
	- Updated tests to include perl 5.8.9
	- Updated tests to test for dlltool error
	- Now depends on Perl::Dist::WiX 1.000 and Perl::Dist 1.16
	- Bootstrap installs Perl::Dist::WiX and its required modules
	- Upgrading to Module::Install 0.91

1.11 Wed  1 Apr 2009
	- This release corresponds to Strawberry Perl April 2009
	- Upgrading to Module::Install::DSL 0.83 (ADAMK)
	- Do not use external tar/gzip in CPAN::Config template (CHORNY)
	- Don't install Win32::Env::Path (ADAMK)
	- Removed the 1.14 version lock on DBD::SQLite (ADAMK)
	- Updated Perl::Dist dependency to 1.14 (ADAMK)

1.10 Sun 22 Feb 2009
	- This release was used to build the first Strawberry Perl
	  Portable stable release.
	- Updating to Module::Install 0.79
	- Removing the Beta tags from 5.8.9 and Portable (even though
	  we won't distribute non-beta 5.8.9 until April).

1.09 Thu 29 Jan 2009
	- This release was used to build Strawberry Perl January 2009
	- Removed the experimental berkeleydb functionality
	- Bug fix to make the ppm temporary build directory get stored
	  in the ppm.xml properly.
	- Correcting the file and distribution naming
	- Add an extra build (5.8.9) as a Beta
	- Upgrading to Perl::Dist 1.12

1.08 Sun 18 Jan 2009
	- Upgrading to Perl::Dist 1.11
	- This release is a convenient stable point.

1.07 Thu 16 Oct 2008
	- This release was used to build Strawberry Perl October 2008
	- Upgrading to Perl::Dist 1.09
	- Adding constructor and full run test scripts
	- Adding three full-run tests for 5.10.0, 5.10.0 Portable and 5.8.8
	- More aggressively suppressing HTML and man generation

1.04 Mon 11 Aug 2008
	- This release corrosponds with Strawberry Perl August 2008
	- Upgrading to Module::Install 0.77

1.03 Tue 15 Jul 2008
	- Upgrading to Module::Install 0.76
	- Adding support for Math::Pari by default
	- Updating Perl::Dist dep to 1.03 to get Vista support
	- Cleaning up the documenation to refer to the current
	  release, removed historical inaccuracies.
	- Adding support for installing path via install_patch

1.02 Wed 16 Apr 2008
	- Removed redundant docs from perldist_strawberry
	- Enabled multi-version building in perldist_strawberry

1.01 Wed 16 Apr 2008
	- Updating to use install_modules
	- Adding Perl::Dist::Bootperl

1.00 Mon 07 Apr 2008
	- Release to match Strawberry Perl 5.10.0.1 April 2008

0.99 Thu  3 Jan 2008
	- Final refactoring to new API
	- Should support both 5.10.0 and 5.8.8 variants
	- Removing URL files we don't need any more

0.53 Not released...
	- 5.10.0 Final release

0.51 Thu 18 Oct 2007
	- Beta 1 Release
	- Various repairs and fixes
	- Use binary packages from Perl-Dist-Downloads
	- Updating binary packages to newer versions
	- Add the installation of PAR::Dist
	- Add the installation of pip
	- Updated to use Perl::Dist 0.51 and Perl 5.10.0 final

0.1.2 Tue 29 Aug 2006
	- Released as Strawberry Perl 5.8.8 Alpha 2 
	- Switched to new version number scheme for Perl::Dist::Strawberry
	- Includes CPAN 1.87_57 (with CPAN::Reporter support!)
	- Includes up-to-date ExtUtils::MakeMaker (as this isn't in Bundle::CPAN)
	- Switched to standalone Win32API::File instead of the one embedded in
	  libwin32
	- Added mingw32-make binary (helps with Alien::wxWidgets)
	- Removed fake Module::Signature as Bundle::CPAN no longer includes it
    
0.000001 Sun  9 Jul 2006  
	- Initial Alpha 1 release of Strawberry Perl

Notes:
 + = something got added.
 - = something got removed.
 o = something got done.
 * = Informational.