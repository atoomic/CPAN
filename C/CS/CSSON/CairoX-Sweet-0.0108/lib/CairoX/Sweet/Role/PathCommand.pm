use CairoX::Sweet::Standard;
use strict;
use warnings;

# PODNAME: CairoX::Sweet::Role::PathCommand

role CairoX::Sweet::Role::PathCommand using Moose {

    our $VERSION = '0.0108'; # VERSION
    requires 'location', 'move_location';
}

1;

__END__

=pod

=encoding UTF-8

=head1 NAME

CairoX::Sweet::Role::PathCommand

=head1 VERSION

Version 0.0108, released 2015-03-21.

=head1 SOURCE

L<https://github.com/Csson/p5-CairoX-Sweet>

=head1 HOMEPAGE

L<https://metacpan.org/release/CairoX-Sweet>

=head1 AUTHOR

Erik Carlsson <info@code301.com>

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2015 by Erik Carlsson <info@code301.com>.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
