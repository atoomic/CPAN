use 5.14.0;
use strict;
use warnings;
use CairoX::Sweet::Standard;

our $VERSION = '0.0108'; # VERSION
# PODNAME: CairoX::Sweet::Core::MoveTo

class CairoX::Sweet::Core::MoveTo with CairoX::Sweet::Role::PathCommand using Moose {

    use CairoX::Sweet::Core::Point;

    has point => (
        is => 'ro',
        isa => Point,
        required => 1,
    );
    # default value sets in BUILDARGS
    has is_relative => (
        is => 'ro',
        isa => Bool,
        required => 1,
    );

    around BUILDARGS($orig: $self, Num $x, Num $y, Bool :$is_relative? = 0) {
        $self->$orig(point => CairoX::Sweet::Core::Point->new(x => $x, y => $y), is_relative => $is_relative);
    }
    method out {
        return $self->point->out;
    }
    method method {
        return $self->is_relative ? 'rel_move_to' : 'move_to';
    }
    method location {
        return $self->point;
    }
    method move_location(:$x = 0, :$y = 0) {
        $self->point->move(x => $x, y => $y);
    }
}

1;

__END__

=pod

=encoding UTF-8

=head1 NAME

CairoX::Sweet::Core::MoveTo

=head1 VERSION

Version 0.0108, released 2015-03-21.

=head1 SOURCE

L<https://github.com/Csson/p5-CairoX-Sweet>

=head1 HOMEPAGE

L<https://metacpan.org/release/CairoX-Sweet>

=head1 AUTHOR

Erik Carlsson <info@code301.com>

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2015 by Erik Carlsson <info@code301.com>.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
