#!/bin/false
# PODNAME: BZ::Client::API
# ABSTRACT: Abstract base class for the clients of the Bugzilla API.

use strict;
use warnings 'all';

package BZ::Client::API;
$BZ::Client::API::VERSION = '1.072';
sub api_call {
    my($class, $client, $methodName, $params) = @_;
    return $client->api_call($methodName, $params)
}

sub error {
    my($class, $client, $message, $http_code, $xmlrpc_code) = @_;
    return $client->error($message, $http_code, $xmlrpc_code)
}

1;

__END__

=pod

=encoding utf-8

=head1 NAME

BZ::Client::API - Abstract base class for the clients of the Bugzilla API.

=head1 VERSION

version 1.072

=head1 SYNOPSIS

This is an abstract base class for classes like L<BZ::Client::Product>, which
are subclassing this one, in order to inherit common functionality.

=head1 SEE ALSO

  L<BZ::Client>

=head1 AUTHORS

=over 4

=item *

Dean Hamstead <dean@bytefoundry.com.au>

=item *

Jochen Wiedmann <jochen.wiedmann@gmail.com>

=back

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2015 by Dean Hamstad.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
