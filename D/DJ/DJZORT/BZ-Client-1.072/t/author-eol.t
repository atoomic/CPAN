
BEGIN {
  unless ($ENV{AUTHOR_TESTING}) {
    require Test::More;
    Test::More::plan(skip_all => 'these tests are for testing by the author');
  }
}

use strict;
use warnings;

# this test was generated with Dist::Zilla::Plugin::Test::EOL 0.17

use Test::More 0.88;
use Test::EOL;

my @files = (
    'lib/BZ/Client.pm',
    'lib/BZ/Client/API.pm',
    'lib/BZ/Client/Bug.pm',
    'lib/BZ/Client/Bugzilla.pm',
    'lib/BZ/Client/Exception.pm',
    'lib/BZ/Client/Product.pm',
    'lib/BZ/Client/Test.pm',
    'lib/BZ/Client/User.pm',
    'lib/BZ/Client/XMLRPC.pm',
    'lib/BZ/Client/XMLRPC/Array.pm',
    'lib/BZ/Client/XMLRPC/Handler.pm',
    'lib/BZ/Client/XMLRPC/Parser.pm',
    'lib/BZ/Client/XMLRPC/Response.pm',
    'lib/BZ/Client/XMLRPC/Struct.pm',
    'lib/BZ/Client/XMLRPC/Value.pm',
    't/01load.t',
    't/10parser.t',
    't/11writer.t',
    't/20login.t',
    't/21products.t',
    't/22bugs.t',
    't/23bugzilla.t',
    't/author-critic.t',
    't/author-eol.t',
    't/release-distmeta.t',
    't/release-kwalitee.t',
    't/release-pod-syntax.t',
    't/release-portability.t',
    't/write-config.pl'
);

eol_unix_ok($_, { trailing_whitespace => 1 }) foreach @files;
done_testing;
