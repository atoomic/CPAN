package Google::Ads::AdWords::v201209::ProductExtension;
use strict;
use warnings;


__PACKAGE__->_set_element_form_qualified(1);

sub get_xmlns { 'https://adwords.google.com/api/adwords/cm/v201209' };

our $XML_ATTRIBUTE_CLASS;
undef $XML_ATTRIBUTE_CLASS;

sub __get_attr_class {
    return $XML_ATTRIBUTE_CLASS;
}


use base qw(Google::Ads::AdWords::v201209::AdExtension);
# Variety: sequence
use Class::Std::Fast::Storable constructor => 'none';
use base qw(Google::Ads::SOAP::Typelib::ComplexType);

Class::Std::initialize();

{ # BLOCK to scope variables

my %id_of :ATTR(:get<id>);
my %AdExtension__Type_of :ATTR(:get<AdExtension__Type>);
my %googleBaseCustomerId_of :ATTR(:get<googleBaseCustomerId>);
my %advertiserName_of :ATTR(:get<advertiserName>);
my %productSelection_of :ATTR(:get<productSelection>);

__PACKAGE__->_factory(
    [ qw(        id
        AdExtension__Type
        googleBaseCustomerId
        advertiserName
        productSelection

    ) ],
    {
        'id' => \%id_of,
        'AdExtension__Type' => \%AdExtension__Type_of,
        'googleBaseCustomerId' => \%googleBaseCustomerId_of,
        'advertiserName' => \%advertiserName_of,
        'productSelection' => \%productSelection_of,
    },
    {
        'id' => 'SOAP::WSDL::XSD::Typelib::Builtin::long',
        'AdExtension__Type' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
        'googleBaseCustomerId' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
        'advertiserName' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
        'productSelection' => 'Google::Ads::AdWords::v201209::ProductConditionAndGroup',
    },
    {

        'id' => 'id',
        'AdExtension__Type' => 'AdExtension.Type',
        'googleBaseCustomerId' => 'googleBaseCustomerId',
        'advertiserName' => 'advertiserName',
        'productSelection' => 'productSelection',
    }
);

} # end BLOCK







1;


=pod

=head1 NAME

Google::Ads::AdWords::v201209::ProductExtension

=head1 DESCRIPTION

Perl data type class for the XML Schema defined complexType
ProductExtension from the namespace https://adwords.google.com/api/adwords/cm/v201209.

Metadata to be used for retrieving offers from Google Base. 




=head2 PROPERTIES

The following properties may be accessed using get_PROPERTY / set_PROPERTY
methods:

=over

=item * googleBaseCustomerId


=item * advertiserName


=item * productSelection




=back


=head1 METHODS

=head2 new

Constructor. The following data structure may be passed to new():






=head1 AUTHOR

Generated by SOAP::WSDL

=cut

