package Mongoose::Role::Collapser;
$Mongoose::Role::Collapser::VERSION = '0.38';
use Moose::Role;

requires 'collapse';

=head1 NAME

Mongoose::Role::Collapser

=head1 DESCRIPTION

The collapser role. No moving parts. Used by the engine. 

=cut 

1;

