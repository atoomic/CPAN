package Mongoose::Role::Expander;
$Mongoose::Role::Expander::VERSION = '0.29';
use Moose::Role;

requires 'expand';

=head1 NAME

Mongoose::Role::Expander

=head1 VERSION

version 0.29

=head1 DESCRIPTION

The expander role. No moving parts. Used by the engine. 

=cut 

1;
