# automatically generated file, don't edit



# Copyright 2011 David Cantrell, derived from data from libphonenumber
# http://code.google.com/p/libphonenumber/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
package Number::Phone::StubCountry::SO;
use base qw(Number::Phone::StubCountry);

use strict;
use warnings;
use utf8;
our $VERSION = 1.20161206201833;

my $formatters = [
                {
                  'leading_digits' => '
            2[0-79]|
            [13-5]
          ',
                  'pattern' => '(\\d)(\\d{6})'
                },
                {
                  'pattern' => '(\\d)(\\d{7})',
                  'leading_digits' => '
            24|
            [67]
          '
                },
                {
                  'pattern' => '(\\d{2})(\\d{5,7})',
                  'leading_digits' => '
            15|
            28|
            6[1-35-9]|
            799|
            9[2-9]
          '
                },
                {
                  'pattern' => '(90\\d)(\\d{3})(\\d{3})',
                  'leading_digits' => '90'
                }
              ];

my $validators = {
                'specialrate' => '',
                'personal_number' => '',
                'pager' => '',
                'toll_free' => '',
                'mobile' => '
          (?:
            15\\d|
            2(?:
              4\\d|
              8
            )|
            6[1-35-9]?\\d{2}|
            7(?:
             [1-8]\\d|
             99?\\d
            )|
            9(?:
              0[67]|
              [2-9]
            )\\d
          )\\d{5}
        ',
                'fixed_line' => '
          (?:
            1\\d|
            2[0-79]|
            3[0-46-8]|
            4[0-7]|
            59
          )\\d{5}
        ',
                'geographic' => '
          (?:
            1\\d|
            2[0-79]|
            3[0-46-8]|
            4[0-7]|
            59
          )\\d{5}
        ',
                'voip' => ''
              };
my %areanames = (
  2521 => "Hargeisa",
  25251 => "Mangauno",
  25261 => "Mogadishu",
);
    sub new {
      my $class = shift;
      my $number = shift;
      $number =~ s/(^\+252|\D)//g;
      my $self = bless({ number => $number, formatters => $formatters, validators => $validators, areanames => \%areanames}, $class);
  
      return $self if ($self->is_valid());
      $number =~ s/(^0)//g;
      $self = bless({ number => $number, formatters => $formatters, validators => $validators, areanames => \%areanames}, $class);
    return $self->is_valid() ? $self : undef;
}
1;