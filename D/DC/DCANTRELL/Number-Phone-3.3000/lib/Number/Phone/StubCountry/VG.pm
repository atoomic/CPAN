# automatically generated file, don't edit



# Copyright 2011 David Cantrell, derived from data from libphonenumber
# http://code.google.com/p/libphonenumber/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
package Number::Phone::StubCountry::VG;
use base qw(Number::Phone::StubCountry);

use strict;
use warnings;
use utf8;
our $VERSION = 1.20161206201833;

my $formatters = [
                {
                  'pattern' => '(\\d{3})(\\d{4})'
                },
                {
                  'pattern' => '(\\d{3})(\\d{3})(\\d{4})'
                }
              ];

my $validators = {
                'toll_free' => '
          8(?:
            00|
            44|
            55|
            66|
            77|
            88
          )[2-9]\\d{6}
        ',
                'mobile' => '
          284(?:
            (?:
              3(?:
                0[0-3]|
                4[0-7]|
                68|
                9[34]
              )|
              4(?:
                4[0-6]|
                68|
                99
              )|
              54[0-57]
            )\\d{4}|
            496[6-9]\\d{3}
          )
        ',
                'geographic' => '
          284(?:
            (?:
              229|
              4(?:
                22|
                9[45]
              )|
              774|
              8(?:
                52|
                6[459]
              )
            )\\d{4}|
            496[0-5]\\d{3}
          )
        ',
                'fixed_line' => '
          284(?:
            (?:
              229|
              4(?:
                22|
                9[45]
              )|
              774|
              8(?:
                52|
                6[459]
              )
            )\\d{4}|
            496[0-5]\\d{3}
          )
        ',
                'voip' => '',
                'specialrate' => '(900[2-9]\\d{6})',
                'personal_number' => '
          5(?:
            00|
            22|
            33|
            44|
            66|
            77|
            88
          )[2-9]\\d{6}
        ',
                'pager' => ''
              };
use Number::Phone::NANP::Data;
sub areaname {
# uncoverable subroutine - no data for most NANP countries
                          # uncoverable statement
Number::Phone::NANP::Data::_areaname('1'.shift()->{number}); }

    sub new {
      my $class = shift;
      my $number = shift;
      $number =~ s/(^\+1|\D)//g;
      my $self = bless({ number => $number, formatters => $formatters, validators => $validators, }, $class);
  return $self->is_valid() ? $self : undef;
}
1;