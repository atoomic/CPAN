package Search::Elasticsearch::Role::Is_Sync;
$Search::Elasticsearch::Role::Is_Sync::VERSION = '5.01';
use Moo::Role;
use namespace::clean;

1;

# ABSTRACT: A role to mark classes which should be used with other sync classes

__END__

=pod

=encoding UTF-8

=head1 NAME

Search::Elasticsearch::Role::Is_Sync - A role to mark classes which should be used with other sync classes

=head1 VERSION

version 5.01

=head1 AUTHOR

Clinton Gormley <drtech@cpan.org>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2016 by Elasticsearch BV.

This is free software, licensed under:

  The Apache License, Version 2.0, January 2004

=cut
