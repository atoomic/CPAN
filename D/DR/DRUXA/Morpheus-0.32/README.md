Morpheus is a configuration engine that supports custom plugins. It allows any 
program that uses it to be configured and reconfigured at different layers 
like local configuration files, configuration database, environment, etc.
The overall program configuration is merged altogether from all these sources.
