#-*-perl-*-
#
# $Id: RequestParameterRequired.in,v 30.1 2007/09/13 14:32:41 balusuv Exp $
#
# (c) 1999-2007 Morgan Stanley Dean Witter and Co.
# See ..../src/LICENSE for terms of distribution.
#

package MQSeries::Command::PCF;

#
# Since PCF commands have to list the required parameters *before* any
# optional ones, MQSeries::Command::Request::_TranslatePCF uses this
# hash to indicate which parameters are required and in which order
# they must be specified.
#
# NOTE: You only have to list a command and its required parameters
# here if the command has *both* required and optional parameters, or
# if the parameter order is significant.  If the command's parameters
# are either all required or all optional, and order does not matter,
# it can be omitted entirely.
#

%RequestParameterRequired =
  (

   ChangeAuthority		=>
   {
    ObjectName			=> 1,
    ObjectType			=> 1,
    EntityName			=> 1,
    EntityType			=> 1,
   },

   ClearQueue			=>
   {
    QName			=> 1,
   },

   CopyAuthInfo                 =>
   {
    AuthInfoType                => 3,
    FromAuthInfoName            => 1,
    ToAuthInfoName              => 2,
   },

   CopyCFStruc                  =>
   {
    FromCFStrucName		=> 1,
    ToCFStrucName		=> 2,
   },

   CopyChannel                  =>
   {
    ChannelType                 => 3,
    FromChannelName             => 1,
    ToChannelName               => 2,
   },
   
   CopyChannelListener		=>
   {
    FromListenerName		=> 1,
    ToListenerName		=> 2,
   },

   CopyNamelist                 =>
   {
    FromNamelistName            => 1,
    ToNamelistName              => 2,
   },

   CopyProcess                  =>
   {
    FromProcessName             => 1,
    ToProcessName               => 2,
   },

   CopyQueue                    =>
   {
    FromQName                   => 1,
    QType                       => 3,
    ToQName                     => 2,
   },

   CopyService			=>
   {
    FromServiceName		=> 1,
    ToServiceName		=> 2,
   },

   CopyStorageClass		=>
   {
    FromStorageClassName	=> 1,
    ToStorageClassName		=> 2,
   },

   CreateAuthInfo               =>
   {
    AuthInfoName                => 1,
    AuthInfoConnName            => 3,
    AuthInfoType                => 2,
   },

   DeleteAuthorityRecord	=>
   {
    ObjectType			=> 2,
    ProfileName			=> 1,
   },

   Escape			=>
   {
    EscapeType			=> 1,
    EscapeText			=> 2,
   },

   InquireAuthorityRecords	=>
   {
    ObjectType			=> 1,
    Options			=> 2,
    ProfileName			=> 3,
   },

   InquireAuthorityService	=>
   {
    AuthServiceAttrs		=> 1,
   },

   InquireClusterQueueManager   =>
   {
    ClusterQMgrName             => 1,
   },

#   InquireConnection		=>
#   {
#    GenericConnectionId		=> 1,
#   },

   MoveQueue			=>
   {
    FromQName			=> 1,
   },

   RefreshQueueManager		=>
   {
    RefreshType			=> 1,
   },

   RefreshSecurity		=>
   {
    SecurityItem		=> 1,
   },

   ResetCluster			=>
   {
    ClusterName			=> 1,
    QMgrName			=> 2,
    Action			=> 3,
   },

   ReverifySecurity		=>
   {
    UserId			=> 1,
   },

   SetAuthorityRecord		=>
   {
    ProfileName			=> 1,
    ObjectType			=> 2,
   },

   SuspendQueueManagerCluster   =>
   {
    ClusterName                 => 1,
    ClusterNamelist             => 1,
   },

   #
   # InquireAuthority is a Morgan Stanley extension
   #
   InquireAuthority		=>
   {
    ObjectType			=> 1,
    ObjectName			=> 1,
   },
  );

#
# This *greatly* shrinks the size of this file...
#
$RequestParameterRequired{InquireAuthInfo} =
  $RequestParameterRequired{InquireAuthInfoNames} =
  $RequestParameterRequired{ChangeAuthInfo} =
  $RequestParameterRequired{DeleteAuthInfo} =
  {
   AuthInfoName         => 1,
  };

$RequestParameterRequired{InquireCFStruc} =
  $RequestParameterRequired{InquireCFStrucNames} =
  $RequestParameterRequired{InquireCFStrucStatus} =
  $RequestParameterRequired{ChangeCFStruc} =
  $RequestParameterRequired{DeleteCFStruc} =
  $RequestParameterRequired{BackupCFStruc} =
  $RequestParameterRequired{RecoverCFStruc} =
  {
   CFStrucName		=> 1,
  };

#
# NOTE: CFStruct is for backwards compatibility with pre-1.24 MQSC
#       New code should use CFStruc (no final 't')
#
$RequestParameterRequired{InquireCFStruct} =
  $RequestParameterRequired{InquireCFStructNames} =
  $RequestParameterRequired{ChangeCFStruct} =
  $RequestParameterRequired{DeleteCFStruct} =
  {
   CFStructName		=> 1,
  };

$RequestParameterRequired{CreateChannelListener} =
  $RequestParameterRequired{ChangeChannelListener} =
  {
   ListenerName                => 1,
   TransportType               => 2,
  };

$RequestParameterRequired{DeleteChannelListener} =
  $RequestParameterRequired{InquireChannelListener} =
  $RequestParameterRequired{InquireChannelListenerStatus} =
  {
   ListenerName                => 1,
  };

$RequestParameterRequired{InquireNamelist} =
  $RequestParameterRequired{CreateNamelist} =
  $RequestParameterRequired{ChangeNamelist} =
  $RequestParameterRequired{DeleteNamelist} =
  {
   NamelistName         => 1,
  };

$RequestParameterRequired{InquireProcess} =
  $RequestParameterRequired{CreateProcess} =
  $RequestParameterRequired{ChangeProcess} =
  $RequestParameterRequired{DeleteProcess} =
  {
   ProcessName          => 1,
  };

$RequestParameterRequired{CreateQueue} =
  $RequestParameterRequired{ChangeQueue} =
  {
   QName		=> 1,
   QType		=> 2,
  };

$RequestParameterRequired{InquireQueueNames} =
  $RequestParameterRequired{InquireQueue} =
  $RequestParameterRequired{InquireQueueStatus} =
  $RequestParameterRequired{DeleteQueue} =
  {
   QName		=> 1,
  };

$RequestParameterRequired{CreateChannel} =
  $RequestParameterRequired{ChangeChannel} =
  {
   ChannelName		=> 1,
   ChannelType		=> 2,
  };

$RequestParameterRequired{StopChannel} =
  $RequestParameterRequired{ResetChannel} =
  $RequestParameterRequired{PingChannel} =
  $RequestParameterRequired{InquireChannelStatus} =
  $RequestParameterRequired{InquireChannelNames} =
  $RequestParameterRequired{InquireChannel} =
  $RequestParameterRequired{DeleteChannel} =
  {
   ChannelName		=> 1,
  };

$RequestParameterRequired{InquireService} =
  $RequestParameterRequired{InquireServiceStatus} =
  $RequestParameterRequired{CreateService} =
  $RequestParameterRequired{ChangeService} =
  $RequestParameterRequired{DeleteService} =
  $RequestParameterRequired{StartService} =
  $RequestParameterRequired{StopService} =
  {
   ServiceName		=> 1,
  };

$RequestParameterRequired{InquireStorageClass} =
  $RequestParameterRequired{InquireStorageClassNames} =
  $RequestParameterRequired{CreateStorageClass} =
  $RequestParameterRequired{ChangeStorageClass} =
  $RequestParameterRequired{DeleteStorageClass} =
  {
   StorageClassName	=> 1,
  };


1;
