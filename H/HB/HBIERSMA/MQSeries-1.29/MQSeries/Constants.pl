#
# $Id: Constants.pl,v 27.1 2007/01/11 20:20:26 molinam Exp $
#
# (c) 1999-2007 Morgan Stanley Dean Witter and Co.
# See ..../src/LICENSE for terms of distribution.
#

package MQSeries::Constants;

require "MQSeries/Constants/AttributeLength.pl";
require "MQSeries/Constants/AttributeString.pl";
require "MQSeries/Constants/ReasonMacro.pl";
require "MQSeries/Constants/ReasonText.pl";
require "MQSeries/Constants/StringAttribute.pl";
require "MQSeries/Constants/ValidPutMsgRecFields.pl";

1;
