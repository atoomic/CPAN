[
   {
     'group validate_if' => 'foo',
     bar => {
       required => 1,
     },
   },
   {
     'group validate_if' => 'hem',
     haw => { required => 1 },
   },
   {
     raspberry => {
       required => 1,
     },
   },
];
