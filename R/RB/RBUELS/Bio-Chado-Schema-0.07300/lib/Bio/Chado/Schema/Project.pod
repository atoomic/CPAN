package Bio::Chado::Schema::Project;


1;

__END__
=pod

=encoding utf-8

=head1 NAME

Bio::Chado::Schema::Project

=head1 NAME

Bio::Chado::Schema::Project - The project table has been moved from general to its own module and has been
expanded to provide properties, publications and contacts.

=head1 CHADO MODULE

Classes in this namespace correspond to tables and views in the
Chado Project module.

=head1 CLASSES

These classes are part of the L<Bio::Chado::Schema> distribution.

Below is a list of classes in this module of Chado.  Each of the
classes below corresponds to a single Chado table or view.

L<Bio::Chado::Schema::Project::Project>

L<Bio::Chado::Schema::Project::ProjectContact>

L<Bio::Chado::Schema::Project::ProjectPub>

L<Bio::Chado::Schema::Project::ProjectRelationship>

L<Bio::Chado::Schema::Project::Projectprop>

=head1 AUTHOR

Robert Buels <rbuels@cpan.org>

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2009 by Robert Buels.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut

