#!/usr/bin/env perl 
use strict;
use warnings;
use feature qw(say);

use Ryu::Async;

Ryu::Async->from(
	directory => 'demo'
)->each(sub {
	say " * $_"
})->get;
