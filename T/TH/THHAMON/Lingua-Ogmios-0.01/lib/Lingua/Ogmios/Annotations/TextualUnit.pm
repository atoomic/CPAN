package Lingua::Ogmios::Annotations::TextualUnit;

# to keep and use ???

use Lingua::Ogmios::Annotations::Element;

use strict;
use warnings;

our @ISA = qw(Lingua::Ogmios::Annotations::Element);

sub new { 
    my ($class, $fields) = @_;

    return($class->SUPER::new($fields));
}
#     if (!defined $fields->{'id'}) {
# 	$fields->{'id'} = -1;
#     }
#     my $textualunit = $class->SUPER::new({
# 	'id' => $fields->{'id'},
# 				  }
# 	);
#     bless ($textualunit,$class);

#     my $i = 0;
#     my $reference_name;
#     my $ref;
#     foreach $ref ('refid_phrase', 'refid_word', 'list_refid_token') {
# 	if (defined $fields->{$ref}) {
# 	    $reference_name = $ref;
# 	    last;
# 	}
# 	$i++;
#     }
#     if ($i == 3) {
# 	die("reference (list) is not defined");
#     }
#     $textualunit->reference($reference_name, $fields->{$reference_name});    

#     if (defined $fields->{'form'}) {
# 	$textualunit->setForm($fields->{'form'});
#     }

#     if (defined $fields->{'log_id'}) {
# 	$textualunit->setLogId($fields->{'log_id'});
#     }
#     return($textualunit);
# }

sub reference {
    my $self = shift;
    my $ref;
    my $elt;

    if ((@_) && (scalar @_ == 2)) {
	$self->{'reference'} = shift;
	$self->{$self->{'reference'}} = [];
	$ref = shift;
	foreach $elt (@$ref) {
	    push @{$self->{$self->{'reference'}}}, $elt;
	}
    }
    return($self->{$self->{'reference'}});
}

sub XMLout {
    my ($self, $name, $order) = @_;
    
    return($self->SUPER::XMLout($name, $order));
}

1;

__END__

=head1 NAME

Lingua::Ogmios::Annotations::??? - Perl extension for ???.

=head1 SYNOPSIS

use Lingua::Ogmios::Annotations::???;

my $word = Lingua::Ogmios::Annotations::???::new($fields);


=head1 DESCRIPTION


=head1 METHODS

=head2 function()

    function($rcfile);

=head1 FIELDS

=over

=item *


=back


=head1 SEE ALSO


=head1 AUTHORS

Thierry Hamon <thierry.hamon@limsi.fr>

=head1 LICENSE

Copyright (C) 2013 by Thierry Hamon

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.6 or,
at your option, any later version of Perl 5 you may have available.


=cut

