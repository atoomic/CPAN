use strict;
use warnings;

use Test::More tests => 1;

use Lingua::Ogmios;
use Lingua::Ogmios::Annotations::Token;
use Lingua::Ogmios::Annotations::LogProcessing;

my $NLPPlatform = Lingua::Ogmios->new('rcfile' => 't/etc/nlpplatform-negex.rc');


# $NLPPlatform->loadDocuments(['examples/FrenchText.xml']);
# ok(defined $NLPPlatform);

# $NLPPlatform->loadDocuments(['examples/InputDocument3.xml']);
# ok(defined $NLPPlatform);

# $NLPPlatform->loadDocuments(['examples/pmid10788508-v3.xml']);

# $NLPPlatform->tokenisation;

# print STDERR $NLPPlatform->XMLout;

# ok(defined $NLPPlatform);

warn "processing examples/pmid10788508-v4.xml\n";


#$NLPPlatform->loadDocuments(['examples/FrenchText.xml']);
#$NLPPlatform->loadDocuments(['examples/pmid10788508-v4.xml']);
$NLPPlatform->loadDocuments(['examples/qald-4_biomedical_test_questions-19.xml']);

$NLPPlatform->tokenisation;

#print STDERR $NLPPlatform->XMLout;

$NLPPlatform->linguisticProcessing;

#print STDERR $NLPPlatform->XMLout;


ok(defined $NLPPlatform);

