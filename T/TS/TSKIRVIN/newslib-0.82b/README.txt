NewsLib - a News Library for Perl
=================================

NewsLib is a library of perl modules for managing Network News services.
It's meant to be used for code-reuse and sharing when writing news-based
applications.  

Installation Instructions
=========================

If you've got perl installed, it's easy:

  perl Makefile.PL
  make
  make test
  sudo make install

(If you don't have sudo installed, run the final command as root.)

If you don't have perl installed, then go install it and start over.
It'll do you good.

Existing Applications 
=====================

NewsProxy - a proxying news server
  http://www.killfile.org/~tskirvin/software/newsproxy/
