use strict;
use warnings;
package App::Spec::Completion;

our $VERSION = '0.002'; # VERSION

use Moo;

has spec => ( is => 'ro' );

1;
