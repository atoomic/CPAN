package Tapper::MCP::MessageReceiver;
# git description: v4.1.4-5-g460fee0

our $AUTHORITY = 'cpan:TAPPER';
# ABSTRACT: Tapper - Message receiver for Tapper MCP
$Tapper::MCP::MessageReceiver::VERSION = '5.0.0';
1; # End of Tapper::MCP::MessageReceiver

__END__

=pod

=encoding UTF-8

=head1 NAME

Tapper::MCP::MessageReceiver - Tapper - Message receiver for Tapper MCP

=head1 AUTHORS

=over 4

=item *

AMD OSRC Tapper Team <tapper@amd64.org>

=item *

Tapper Team <tapper-ops@amazon.com>

=back

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2016 by Advanced Micro Devices, Inc..

This is free software, licensed under:

  The (two-clause) FreeBSD License

=cut
