{   '<: $main_script :>' => {
        crypt => 0,    # crypt PAR by default
        upx   => 1,    # compress DLLs with upx by default
        clean => 1,    # clean PAR cache on exit
    },
}
