��          �   %   �      0  !   1  Y   S     �  &   �     �  -   �  
             %     ?     G  <   P  	   �  V   �  Q   �     @     D     J     M     U     [     `     e  ?  i  C   �  �   �     �  R   �     �  [   �     K     ^  $   y  
   �  
   �  |   �     1	  d   Q	  r   �	     )
  
   2
     =
     D
     S
     b
     o
     v
                                 	                                                                                   
           All fields are mandatory to fill! Authorization error or expired session.<br>Please, wait, while redirection in progress... Error Incorrect user name and / or password. Password Please, wait, while redirection in process... Processing Redirect Request is proccessing... Sign in Sign out This Website requires your browser to be JavaScript enabled. User name You are just signed out from server.<br>Please, wait, while redirection in progress... You are successfully signed in.<br>Please, wait, while redirection in progress... day month no quarter today week year yes Project-Id-Version: Pharaoh
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-02-09 10:20+0200
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
Language: ru_RU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11) ? 0 : ((n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20)) ? 1 : 2);
X-Poedit-KeywordsList: i18n;i18n:1,2
X-Poedit-Basepath: ../
X-Poedit-SourceCharset: UTF-8
X-Generator: Poedit 1.5.5
X-Poedit-SearchPath-0: bin
X-Poedit-SearchPath-1: lib
X-Poedit-SearchPath-2: templates
 Все поля обязательны для заполнения! Ошибка авторизации или устаревшая сессия.<br>Дождитесь завершения переадресации. Ошибка Неправильное имя пользователя и / или пароль. Пароль Пожалуйста, подождите завершения переадресации... Обработка Переадресация Обработка запроса... Войти Выйти Для корректной работы сайта необходимо разрешить выполнение JavaScript. Имя пользователя Сессия завершена<br>Дождитесь окончания переадресации. Успешный вход в систему<br>Дождитесь завершения переадресации. день месяц нет квартал сегодня неделя год да 