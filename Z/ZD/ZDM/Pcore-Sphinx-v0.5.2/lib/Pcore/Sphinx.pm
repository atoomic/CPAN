package Pcore::Sphinx v0.5.2;

use Pcore -dist, -class;

1;
__END__
=pod

=encoding utf8

=head1 NAME

Pcore::Sphinx

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 SEE ALSO

=head1 AUTHOR

zdm <zdm@cpan.org>

=head1 CONTRIBUTORS

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2015 by zdm.

=cut
