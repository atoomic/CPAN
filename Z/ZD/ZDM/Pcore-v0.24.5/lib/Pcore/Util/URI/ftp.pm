package Pcore::Util::URI::ftp;    ## no critic qw[NamingConventions::Capitalization]

use Pcore -class;

extends qw[Pcore::Util::URI];

has '+default_port' => ( default => 21 );

1;
__END__
=pod

=encoding utf8

=head1 NAME

Pcore::Util::URI::ftp

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head1 METHODS

=head1 SEE ALSO

=cut
