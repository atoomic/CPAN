package Pcore::API::SCM::Server;

use Pcore -role;

requires qw[scm_upstream scm_cmd scm_id scm_init scm_clone scm_releases scm_is_commited scm_addremove scm_commit scm_push scm_set_tag];

1;
__END__
=pod

=encoding utf8

=head1 NAME

Pcore::API::SCM::Server

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head1 METHODS

=head1 SEE ALSO

=cut
