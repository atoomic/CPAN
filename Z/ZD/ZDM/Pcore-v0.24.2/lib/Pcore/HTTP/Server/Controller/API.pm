package Pcore::HTTP::Server::Controller::API;

use Pcore -role;

with qw[Pcore::HTTP::Server::Controller];

requires qw[_build_api];

has api => ( is => 'ro', isa => InstanceOf ['Pcore::API::Server'] );

# TODO get auth from token, then from cookies
sub run ($self) {
    return;
}

1;
__END__
=pod

=encoding utf8

=head1 NAME

Pcore::HTTP::Server::Controller::API

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head1 METHODS

=head1 SEE ALSO

=cut
