package Pcore::Nginx v0.10.1;

use Pcore -dist;
use Pcore::GeoIP;

1;
__END__
=pod

=encoding utf8

=head1 NAME

Pcore::Nginx - Pcore nginx application

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 SEE ALSO

=head1 AUTHOR

zdm <zdm@cpan.org>

=head1 CONTRIBUTORS

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2015 by zdm.

=cut
