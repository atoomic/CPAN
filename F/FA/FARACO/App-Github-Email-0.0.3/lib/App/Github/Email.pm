use strict;
use warnings;
package App::Github::Email;
#ABSTRACT: Search and print related Github user emails.
# VERSION

=pod

=encoding UTF-8

=head1 NAME

App::Github::Email - Search and print related Github user emails.

=head1 VERSION

version 0.0.3

=head1 SYNOPSIS

	github-email [-n|-name|--name] <USERNAME> # => output related Github user emails (from event)

=head1 DESCRIPTION

Search and print Github related user emails from public event.

=head1 AUTHOR

faraco <skelic3@gmail.com>

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2017 by faraco.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut

__END__

1;
