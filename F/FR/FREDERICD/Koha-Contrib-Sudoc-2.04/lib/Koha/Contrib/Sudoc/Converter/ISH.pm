package Koha::Contrib::Sudoc::Converter::ISH;
# ABSTRACT: Convertisseur spécifique
$Koha::Contrib::Sudoc::Converter::ISH::VERSION = '2.04';
use Moose;

extends 'Koha::Contrib::Sudoc::Converter';


# Moulinette SUDOC
has sudoc => ( is => 'rw', isa => 'Sudoc', required => 1 );




# Création des exemplaires Koha en 995 en fonction des données locales SUDOC
sub itemize {
    my ($self, $record) = @_;
}




1;

__END__

=pod

=encoding UTF-8

=head1 NAME

Koha::Contrib::Sudoc::Converter::ISH - Convertisseur spÃ©cifique

=head1 VERSION

version 2.04

=head1 AUTHOR

Frédéric Demians <f.demians@tamil.fr>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2015 by Fréderic Demians.

This is free software, licensed under:

  The GNU General Public License, Version 3, June 2007

=cut
