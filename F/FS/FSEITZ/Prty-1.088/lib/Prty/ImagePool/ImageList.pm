package Prty::ImagePool::ImageList;
use base qw/Prty::Hash/;

use strict;
use warnings;

our $VERSION = 1.088;

# -----------------------------------------------------------------------------

=encoding utf8

=head1 NAME

Prty::ImagePool::ImageList - Liste der Verzeichnis-Bilder

=head1 BASE CLASS

L<Prty::Hash>

=head1 DESCRIPTION

Speicher für die Liste aller Bilder eines ImagePool-Verzeichnisses.

=head1 ATTRIBUTES

=over 4

=item imageA

Referenz auf die Liste der Bild-Objekte des Verzeichnisses.

=item imageH

Hash der Bild-Objekte. Schlüssel ist die Bildnummer.

=back

=head1 METHODS

=head2 Konstruktor

=head3 new() - Instantiiere Bild-Listen-Objekt

=head4 Synopsis

    $lst = $class->new(\@images);

=head4 Arguments

=over 4

=item @images (Default: leere Liste)

Array von Zeitraffer-Bild-Objeken.

=back

=cut

# -----------------------------------------------------------------------------

sub new {
    my ($class,$imageA) = @_;

    # Bild-Hash aufbauen

    my %hash;
    for my $img (@$imageA) {
        $hash{$img->number} = $img;
    }

    # Objekt instantiieren

    return $class->SUPER::new(
        imageA=>$imageA,
        imageH=>\%hash,
    );
}

# -----------------------------------------------------------------------------

=head2 Objektmethoden

=head3 minNumber() - Niedrigste Bildnummer

=head4 Synopsis

    $n = $lst->minNumber;

=head4 Description

Liefere die niedrigste Bildnummer. Die niedrigste Bildnummer ist
die Nummer des ersten Bildes. Ist die Liste leer, liefere 0.

=cut

# -----------------------------------------------------------------------------

sub minNumber {
    my $self = shift;
    return $self->size? $self->images->[0]->number: 0;
}

# -----------------------------------------------------------------------------

=head3 maxNumber() - Höchste Bildnummer

=head4 Synopsis

    $n = $lst->maxNumber;

=head4 Description

Liefere die höchste Bildnummer. Die höchste Bildnummer ist die
Nummer des letzten Bildes. Ist die Liste leer, liefere 0.

=cut

# -----------------------------------------------------------------------------

sub maxNumber {
    my $self = shift;
    return $self->size? $self->images->[-1]->number: 0;
}

# -----------------------------------------------------------------------------

=head2 Bilder

=head3 images() - Liste aller Bild-Objekte

=head4 Synopsis

    @images|$imageA = $lst->images;

=head4 Description

Liefere die Liste aller Bilder des Verzeichnisses. Im Skalarkontext
liefere eine Referenz auf die Liste.

=cut

# -----------------------------------------------------------------------------

sub images {
    my $self = shift;
    my $imageA = $self->{'imageA'};
    return wantarray? @$imageA: $imageA;
}

# -----------------------------------------------------------------------------

=head3 image() - Lookup Bild-Objekt nach Bild-Nummer

=head4 Synopsis

    $img = $lst->image($n);

=head4 Description

Liefere das Bild-Objekt mit Nummer $n. Existiert keine Bild-Objekt
mit Nummer $n, liefere undef.

=cut

# -----------------------------------------------------------------------------

sub image {
    my ($self,$n) = @_;
    return $self->{'imageH'}->{$n};
}

# -----------------------------------------------------------------------------

=head1 VERSION

1.088

=head1 AUTHOR

Frank Seitz, L<http://fseitz.de/>

=head1 COPYRIGHT

Copyright (C) 2016 Frank Seitz

=head1 LICENSE

This code is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

# -----------------------------------------------------------------------------

1;

# eof
