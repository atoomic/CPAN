package Prty::ImagePool::Image;
use base qw/Prty::File::Image/;

use strict;
use warnings;

our $VERSION = 1.088;

# -----------------------------------------------------------------------------

=head1 NAME

Prty::ImagePool::Image - Bild eines Image-Pool

=head1 BASE CLASS

L<Prty::File::Image>

=head1 METHODS

=head2 Konstruktor

=head3 new() - Konstruktor

=head4 Synopsis

    $img = $class->new($file);

=head4 Arguments

=over 4

=item $file

Pfad zur Bilddatei.

=back

=head4 Returns

Referenz auf Image-Pool-Bild-Objekt.

=cut

# -----------------------------------------------------------------------------

sub new {
    my ($class,$file) = @_;

    my ($n) = $file =~ m|/(\d+)[^/]*?\.[^/.]+$|;
    if (!$n) {
        $class->throw(
            q{IMG-00001: Ungueltiger Dateiname fuer Zeitraffer-Bild},
            File=>$file,
        );
    }

    my $self = $class->SUPER::new($file);
    $self->add(
        number=>$n+0,
        cacheId=>undef,
    );

    return $self;
}

# -----------------------------------------------------------------------------

=head2 Objektmethoden

Alle weiteren Methoden befinden sich bei der Basisklasse (s. Abschnitt
BASE CLASS).

=head3 number() - Nummer des Bildes

=head4 Synopsis

    $n = $img->number;

=head4 Description

Liefere die Nummer des Bildes als Zahl. Z.B. 47.

=head3 cacheId() - Cache-Id

=head4 Synopsis

    $cacheId = $img->cacheId;

=head4 Description

Liefere die Cache-Id der Datei, z.B.
'2016-01-23-A-0000x0000-webimage/000001-550x412.jpg'.

=cut

# -----------------------------------------------------------------------------

sub cacheId {
    my $self = shift;

    return $self->memoize('cacheId',sub {
        my ($self,$key) = @_;

        my $basename = $self->basename;
        my @path = split m|/|,$self->path;
        if ($path[-2] eq 'img') {
            return "$path[-3]/$basename"; # Subdirectoy-Path
        }
        return "$path[-2]/$basename"; # Cache-Path
    });
}

# -----------------------------------------------------------------------------

=head1 VERSION

1.088

=head1 AUTHOR

Frank Seitz, L<http://fseitz.de/>

=head1 COPYRIGHT

Copyright (C) 2016 Frank Seitz

=head1 LICENSE

This code is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

# -----------------------------------------------------------------------------

1;

# eof
