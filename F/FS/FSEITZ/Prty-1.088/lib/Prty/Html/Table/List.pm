package Prty::Html::Table::List;
use base qw/Prty::Html::Table::Base/;

use strict;
use warnings;

our $VERSION = 1.088;

use Prty::Html::Tag;

# -----------------------------------------------------------------------------

=encoding utf8

=head1 NAME

Prty::Html::Table::List - HTML-Tabelle zum Anzeigen einer Liste von Elementen

=head1 BASE CLASS

L<Prty::Html::Table::Base>

=head1 SYNOPSIS

Attribute von tr- und td-Elemeten setzen. Für jedes Element
wird eine Arrayreferenz geliefert:

    $e = Prty::Html::Table::List->new(
        titles=>[qw/Id Name Vorname/],
        align=>[qw/right left left/],
        rows=>\@obj,
        rowCallback=>sub {
            my ($row,$i) = @_;
    
            my $trA = [class=>'TRCLASS'];
            push my @tds,[class=>'TDCLASS',$row->get('ATTRIBUTE')];
            ...
    
            return ($trA,@tds);
        },
    );
    $html = $e->html($h);

Lediglich Werte ausgeben. Für das tr-Element wird C<undef> geliefert,
für die td-Elemente ein skalarer Wert (der Content des Elements):

    $e = Prty::Html::Table::List->new(
        titles=>[qw/Id Name Vorname/],
        align=>[qw/right left left/],
        rows=>\@obj,
        rowCallback=>sub {
            my ($row,$i) = @_;
    
            push @arr,$row->get('ATTRIBUTE');
            ...
    
            return (undef,@arr);
        },
    );
    $html = $e->html($h);

=head1 DESCRIPTION

Die Klasse erzeugt eine Tabelle aus einer Liste von Objekten. Jedes
Objekt wird durch eine Zeile dargestellt. Alle Zeilen- (tr) und
Zellenattribute (td) können kontrolliert werden. Die Klasse ist
daher sehr flexibel.

Für jedes Objekt wird die Methode $e->rowCallback() gerufen.
Die Methode bekommt das Objekt und seine (0-basierte) Position in der
Liste der Objekte (Attribut "rows") übergeben. Die Methode liefert
die Spezifikation für die Zeile (tr) und ihre Zellen (td) zurück,
wobei jede Spezifikation ein Array ist, das unmittelbar an
die Methode tag() übergeben wird.

=head1 ATTRIBUTES

Zusätzlich zu den Attributen der Basisklasse definiert die Klasse
folgende Attribute:

=over 4

=item align => \@arr (Default: [])

Ausrichtung des Kolumneninhalts.

=item allowHtml => $bool|\@titles (Default: 0)

Erlaube HTML insgesamt oder auf den Kolumnen in @titles,
d.h. ersetze die Werte der Kolumnen &, <, > I<nicht> automatisch
durch HTML-Entities.

=item empty => $str (Default: '&nbsp;')

HTML-Code, der im Body der Tabelle gesetzt wird, wenn die Liste
der Elemente leer ist. Wenn auf Leerstring, undef oder 0 gesetzt,
wird kein Body angezeigt.

=item rowCallback => $ref (Default: undef)

Referenz auf eine Subroutine, die für jedes Element die
darzustellende Zeileninformation (für tr- und td-Tag) liefert.

=item rows => \@rows (Default: [])

Liste der Elemente. Für jedes Element wird die Callback-Methode
(Attribut rowCallback) aufgerufen.

=item titles => \@titles (Default: [])

Liste der Kolumnentitel.

=back

=head1 METHODS

=head2 new() - Konstruktor

=head3 Synopsis

    $e = $class->new(@attVal);

=head3 Description

Instantiiere ein Tabellenobjekt mit den Eingenschaften @attVal und
liefere eine Referenz auf dieses Objekt zurück.

=cut

# -----------------------------------------------------------------------------

sub new {
    my $class = shift;
    # @_: @attVal

    # Defaultwerte

    my $self = $class->SUPER::new(
        align=>[],
        allowHtml=>0,
        empty=>'&nbsp;',
        rowCallback=>undef,
        rows=>[],
        titles=>[],
    );

    # Werte Konstruktoraufruf
    $self->set(@_);

    return $self;
}

# -----------------------------------------------------------------------------

=head2 html() - Generiere HTML-Code

=head3 Synopsis

    $html = $e->html($h);
    $html = $class->html($h,@attVal);

=head3 Description

Generiere HTML-Code für Tabellenobjekt $e und liefere diesen zurück.
Bei Aufruf als Klassenmethode wird das Tabellenobjekt von
der Methode aus den Argumenten @attVal instantiiert.

=cut

# -----------------------------------------------------------------------------

sub html {
    my $this = shift;
    my $h = shift;
    # @_: @attVal

    my $self = ref $this? $this: $this->new(@_);

    # Attribute

    my ($align,$allowHtml,$empty,$rowCallback,$rows,$titles) =
        $self->get(qw/align allowHtml empty rowCallback rows titles/);

    return '' if !@$titles && !@$rows;

    if (@$rows && !$rowCallback) {
        $self->throw(
            q{HTML-00001: Keine Callback-Methode (rowCallback) definiert}
        );
    }

    my %allowHtml;
    @allowHtml{@$titles} = (0) x @$titles;
    if (ref $allowHtml) {
        # $allowHtml als Referenz auf Array von bestimmten Kolumnen
        @allowHtml{@$allowHtml} = (1) x @$allowHtml;
    }
    elsif ($allowHtml) {
        # $allowHtml als boolscher Wert für alle Kolumnen
        @allowHtml{@$titles} = (1) x @$titles;
    }

    # Tabelleninhalt

    my $html = '';

    # Kopf

    if (@$titles) {
        my $ths;
        my $i = 0;
        for my $title (@$titles) {
            $ths .= $h->tag('th',
                -text=>!$allowHtml{$title},
                align=>$align->[$i++],
                '-',
                $title
            );
        }
        $html .= $h->tag('thead',
            $h->tag('tr',$ths),
        );
    }

    # Rumpf

    my $trs;
    my $i = 0;
    for my $row (@$rows) {
        my ($trA,@tds) = $rowCallback->($row,$i++);        

        my $tds;
        my $j = 0;
        for my $tdA (@tds) {
            $tds .= $h->tag('td',
                -text=>!$allowHtml{$titles->[$j]},
                align=>$align->[$j++],
                ref $tdA? @$tdA: $tdA # Array oder skalarer Wert
            );
        }

        $trs .= $h->tag('tr',
            @$trA,
            $tds
        )
    }
    if (!$trs && $empty) {
        # Keine Objekte vorhanden
        $trs = $h->tag('tr',
            $h->tag('td',
                align=>'center',
                colspan=>scalar(@$titles),
                '-',
                $empty,
            )
        );
    }
    if ($trs) {
        $html .= $h->tag('tbody',$trs);
    }

    return $self->SUPER::html($h,$html);
}

# -----------------------------------------------------------------------------

=head1 VERSION

1.088

=head1 AUTHOR

Frank Seitz, L<http://fseitz.de/>

=head1 COPYRIGHT

Copyright (C) 2016 Frank Seitz

=head1 LICENSE

This code is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

# -----------------------------------------------------------------------------

1;

# eof
