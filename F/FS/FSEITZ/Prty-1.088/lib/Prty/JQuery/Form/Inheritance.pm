package Prty::JQuery::Form::Inheritance;
use base qw/Prty::Hash/;

use strict;
use warnings;
use utf8;

our $VERSION = 1.088;

use Prty::Unindent;
use Prty::Storable;
use Prty::Html::Widget::Hidden;
use Prty::Html::Table::Simple;
use Prty::Html::Form::Layout;
use Prty::Html::Widget::Button;
use Prty::Html::Widget::CheckBox;
use Prty::Html::Fragment;

# -----------------------------------------------------------------------------

=encoding utf8

=head1 NAME

Prty::JQuery::Form::Inheritance - HTML/JavaScript-Formular für vererbbare Information

=head1 BASE CLASS

L<Prty::Hash>

=head1 DESCRIPTION

Die Klasse generiert ein Vererbungsformular und verwaltet
den Zustand in Abhängkeit von der (anfänglichen) Datenkonstellation.

Vier Datenkonstellationen sind möglich: Child-Daten yes/no,
Parent-Daten yes/no.

    Child|Parent|Initial|Edit|Buttons
    no|no|Child(empty)|Child(empty)|Speichern
    no|yes|Parent(data)|Parent(data)+Child(empty)|Speichern,Kopieren
    yes|no|Child(data)|Child(data)|Speichern,Löschen
    yes|yes|Child(data)|Parent(data)+Child(data)|Speichern,Löschen

Herbei ist;

=over 4

=item Initial

Das anfänglich dargestellte Formular (leer oder mit Daten).

=item Edit

Das/die dargestellte(n) Formular(e) nach Betätigung von
"bearbeiten" (leer oder mit Daten).

=item Buttons

Die Schaltflächen nach Betätigung von "bearbeiten".

=back

Die mit dem Attribut widgets=>\@widgets übergebenen Widgets erhalten
intern die Namen "parent-NAME" für die Parent-Widgets und "data-NAME"
für die Child-Widgets, wobei nur die Child-Widgets beim Speichern
von Daten von Bedeutung sind.

=head1 ATTRIBUTES

=over 4

=item child => \%hash (Default: undef)

Referenz auf den Hash mit den Werten für das Child-Formular.

=item debug => $bool (Default: 0)

Gib auf STDERR Informationen über die Parent- und Child-Daten aus.

=item deleteAction => $url (Default: undef)

URL, über den die Formulardaten gelöscht werden.

=item head => $html (Default: undef)

Inhalt am Anfang der Seite.

=item hidden => \@keyVal (Default: [])

Schlüssel/Wert-Paare, die als Hidden-Widgets gesetzt werden.

=item id => $id (Default: undef)

DOM-Id des Formulars.

=item layout => $html (Default: [])

Der HTML-Code des Layouts. In das Layout wird der HTML-Code der
Widgets eingesetzt.

=item onSucces => $javaScript (Default: undef)

JavaScript-Function, die nach einem erfolgreichen Speichern
gerufen wird.

=item parent => [$name,\%hash] (Default: undef)

Name des Parent-Mandanteb und Referenz auf den Hash mit den Werten
für das Parent-Formular.

=item saveAction => $url (Default: undef)

URL, über den die Formulardaten gespeichert werden.

=item title => $title (Default: undef)

Abschnittstitel des Formulars.

=item widgets => \@widgets (Default: [])

Liste der Widgets, die in das Layout eingesetzt werden.

=back

=head1 METHODS

=head2 Konstruktor

=head3 new() - Instantiiere Objekt

=head4 Synopsis

    $e = $class->new(@attVal);

=head4 Description

Instantiiere ein Formular-Objekt und liefere eine Referenz auf
dieses Objekt zurück.

=cut

# -----------------------------------------------------------------------------

sub new {
    my $class = shift;
    # @_: @attVal

    my $self = $class->SUPER::new(
        child=>undef,
        debug=>0,
        deleteAction=>undef,
        head=>undef,
        hidden=>[],
        id=>undef,
        layout=>[],
        onSuccess=>undef,
        parent=>[],
        saveAction=>undef,
        title=>undef,
        widgets=>[],
    );
    $self->set(@_);

    return $self;
}

# -----------------------------------------------------------------------------

=head2 Klassenmethoden

=head3 pluginCode() - JavaScript-Code des Plugin

=head4 Synopsis

    $javascript = $e->pluginCode;

=cut

# -----------------------------------------------------------------------------

sub pluginCode {
    my $this = shift;

    return Prty::Unindent->hereDoc(q~
    $.widget('prty.inheritanceForm',{
        options: {
            title: null,
            saveAction: null,
            onSuccess: null,
            deleteAction: null,
        },
        parentData: false, // Kennzeichen, ob Parent-Daten vorhanden sind
        childData: false, // Kennzeichen, ob Child-Daten vorhanden sind
        _create: function () {
            this.element.addClass('inheritanceForm');

            this._on(this.element,{
                'click .editCheckBox': function (event) {
                    this.__render();
                },
                'click .saveButton': function (event) {
                    this.__save();
                },
                'click .copyButton': function (event) {
                    this.__copy();
                },
                'click .deleteButton': function (event) {
                    this.__delete();
                },
            });

            // Ermittln, welchen Art von Daten vorhanden ist

            this.parentData =
                $('.parentTable',this.element).data('parent')? true: false;
            this.childData =
                $('.childTable',this.element).data('child')? true: false;

            // Formular inital darstellen

            this.__render()
        },
        __save: function () {
            // Prüfe, ob das Formular Daten enthält

            var dataFound = 0;
            $('.childTable :input',this.element).each(function () {
                var name = this.name;
                var val = $(this).val();
                if (val != '') {
                    dataFound++;
                }
            });
            if (!dataFound) {
                alert('Bitte füllen Sie das Formular aus');
                return;
            }

            var instance = this;
            var url = this.options.saveAction;
            var data = 'op=save'+'&'
                +$(':input',this.element).serialize();

            $.ajax({
                url: url,
                type: 'POST',
                data: data,
                async: false,
                success: function (data,textStatus,jqXHR) {
                    if (data) {
                        alert(data);
                        return;
                    }
                    instance.childData = true;
                    $('.editCheckBox',instance.element)
                        .prop('checked',false);
                    instance.__render();
                    if (instance.options.onSuccess) {
                        instance.options.onSuccess(data,textStatus,jqXHR,
                            'save');
                    }
                },
                error: function () {
                    alert('FEHLER: Speichern fehlgeschlagen');
                },
            });
        },
        __clear: function () {
            // Child-Formular Feldinhalte löschen
            $('.childTable :input',this.element).each(function () {
                var $this = $(this);
                if ($this.is(':radio,:checkbox')) {
                    $this.prop('checked',false);
                }
                else {
                    $this.val('');
                }
            });
        },
        __copy: function () {
            var $parentInputs = $('.parentTable :input',this.element);
            var $childInputs = $('.childTable :input',this.element);
            for (var i = 0; i < $parentInputs.length; i++) {
                var $parentInput = $($parentInputs.get(i));
                var $childInput = $($childInputs.get(i));
                if ($parentInput.is(':radio,:checkbox')) {
                    $childInput.prop('checked',
                        $parentInput.prop('checked'));
                }
                else {
                    $childInput.val($parentInput.val());
                }
            }
            this.__enableButton('.deleteButton');
        },
        __delete: function () {
            if (!this.childData) {
                this.__clear();
                this.__disableButton('.deleteButton');
            }
            else if (confirm('"'+this.options.title+'"'
                    +' des Mandanten wirklich löschen?')) {
                var instance = this;
                var url = this.options.deleteAction;
                var data = 'op=delete'+'&'
                    +$(':input',this.element).serialize();

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: data,
                    async: false,
                    success: function () {
                        instance.childData = false;
                        $('.editCheckBox',instance.element)
                            .prop('checked',false);
                        instance.__render();
                        instance.__clear();
                        if (instance.options.onSuccess) {
                            instance.options.onSuccess(data,textStatus,jqXHR,
                                'delete');
                        }
                    },
                    error: function () {
                        alert('FEHLER: Speichern fehlgeschlagen');
                    },
                });
            }
        },
        __enableButton: function (selector) {
            var $button = $(selector,this.element);
            $button.prop('disabled',false);
            $button.removeClass('disabled');
            $button.addClass('enabled');
        },
        __disableButton: function (selector) {
            var $button = $(selector,this.element);
            $button.prop('disabled',true);
            $button.removeClass('enabled');
            $button.addClass('disabled');
        },
        __render: function () {
            var $parentTable = $('.parentTable',this.element);
            var $childTable = $('.childTable',this.element);
            var $childWidgets = $(':input',$childTable);
            var edit = $('.editCheckBox',this.element).is(':checked');

            if (edit) {
                if (this.parentData) {
                    $parentTable.show();
                }
                $childTable.show();

                // Child-Widgets enablen

                $childWidgets.prop('disabled',false);
                $childWidgets.removeClass('disabled');
                $childWidgets.addClass('enabled');

                // Buttons enablen

                if (this.options.saveAction) {
                    this.__enableButton('.saveButton');
                }

                if (!this.childData) {
                    // if (this.options.parentData) {
                    if (this.parentData) {
                        this.__enableButton('.copyButton');
                    }
                }
                else if (this.options.deleteAction) {
                    this.__enableButton('.deleteButton');
                }
            }
            else { // view
                if (this.parentData && !this.childData) {
                    $parentTable.show();
                    $childTable.hide();
                }
                else {
                    $parentTable.hide();
                    $childTable.show();
                }

                // Child-Widgets disablen

                $childWidgets.prop('disabled',true);
                $childWidgets.removeClass('enabled');
                $childWidgets.addClass('disabled');

                // Buttons disablen

                this.__disableButton('.saveButton');
                this.__disableButton('.copyButton');
                this.__disableButton('.deleteButton');
            }
        }
    });
    ~);
}

# -----------------------------------------------------------------------------

=head2 Objektmethoden

=head3 html() - Generiere HTML

=head4 Synopsis

    $html = $e->html($h);
    $html = $class->html($h,@attVal);

=head4 Description

Generiere den HTML-Code des Formular-Objekts und liefere diesen
zurück. Als Klassenmethode gerufen, wird das Objekt intern erzeugt
und mit den Attributen @attVal instantiiert.

=cut

# -----------------------------------------------------------------------------

sub html {
    my $this = shift;
    my $h = shift;

    my $self = ref $this? $this: $this->new(@_);

    my ($childH,$debug,$deleteAction,$head,$hiddenA,$id,$layoutA,$onSuccess,
        $parentA,$saveAction,$title,$widgetA) = $self->get(qw/child
        debug deleteAction head hidden id layout onSuccess parent
        saveAction title widgets/);

    # FIXME: getrennte Attribute
    my ($parentName,$parentH) = @$parentA;
    $parentName ||= 'geerbt';
    
    # Zu den Formularzeilen den Zeilentyp (form-widget) hinzufügen

    my @rows;
    my $columns = 0;
    for (@$layoutA) {
        if (ref $_->[0]) {
            push @rows,['form-widget',@$_];
        }
        else {
            push @rows,[@$_];
        }
        my $n = grep {ref} @$_;
        if ($n > $columns) {
            $columns = $n;
        }
    }

    # Parent- und Child-Widgets instantiieren (unabhängig davon,
    # ob Parent- und Child-Daten vorliegen)

    my (@parentWidgets,@childWidgets);
    my $parentValues = 0;
    my $childValues = 0;
    for (@$widgetA) {
        my $name = $_->name;
    
        my $w = Prty::Storable->clone($_);
        $w->set(disabled=>1); # da disabled, wird nicht übertragen
        $w->set(class=>'disabled');
        if ($parentH) {
            my $val = $parentH->{$name};
            if (defined $val && $val ne '') {
                $parentValues++;
            }
            $w->value($val);
        }
        push @parentWidgets,$w;
    
        $w = Prty::Storable->clone($_);
        $w->name('data-'.$name); # Präfix für Erkennung beim Speichern
        $w->set(disabled=>1);
        $w->set(class=>'disabled');
        if ($childH) {
            my $val = $childH->{$name};
            if (defined $val && $val ne '') {
                $childValues++;
            }
            $w->value($childH->{$name});
        }
        push @childWidgets,$w;
    }

    # Wenn keine Widget-Daten vorhanden sind, den Child-
    # bzw. Parent-Hash unterdrücken.

    if (!$parentValues) {
        $parentH = undef;
    }
    if (!$childValues) {
        $childH = undef;
    }

    # Hidden-Felder zum Child-Formular hinzufügen

    for (my $i = 0; $i < @$hiddenA; $i += 2) {
        push @childWidgets,Prty::Html::Widget::Hidden->new(
            name=>$hiddenA->[$i],
            value=>$hiddenA->[$i+1],
        );
    }
    
    # Parent-Formular. Wird angezeigt, wenn Parent-Daten und
    # keine Child-Daten vorliegen.

    my $layout = Prty::Html::Table::Simple->html($h,
        class=>'parentTable form',
        data=>[
            parent=>$parentValues,
        ],
        rows=>[
            ['form-section',
                [colspan=>$columns,
                    $title? "$title ($parentName)": $parentName]],
            @rows,
        ],
    );

    my $lay1 = Prty::Html::Form::Layout->new(
        layout=>$layout,
        widgets=>\@parentWidgets,
    );

    # Child-Formular. Wird angezeigt, wenn Child-Daten vorliegen
    # oder keine Parent-Daten vorliegen.

    $layout = Prty::Html::Table::Simple->html($h,
        class=>'childTable form',
        data=>[
            child=>$childValues,
        ],
        rows=>[
            ['form-section',
                [colspan=>$columns,$title? $title: 'lokal']],
            @rows,
        ],
    );
    
    # Platzhalter-Namen an Namen der Child-Widgets anpassen
    $layout =~ s/__([A-Z0-9_]+?)__/__DATA-$1__/g;

    my $lay2 = Prty::Html::Form::Layout->new(
        layout=>$layout,
        widgets=>\@childWidgets,
    );

    # Button-Zeile
    
    my $but = Prty::Html::Form::Layout->new(
        layout=>Prty::Html::Table::Simple->html($h,
            class=>'form',
            rows=>[
                [['__SAVE__ __COPY__ __DELETE__ __EDIT__']],
            ],
        ),
        widgets=>[
            Prty::Html::Widget::Button->new(
                class=>'saveButton',
                name=>'save',
                content=>'Speichern',
            ),
            Prty::Html::Widget::Button->new(
                class=>'copyButton',
                name=>'copy',
                content=>'Kopieren',
            ),
            Prty::Html::Widget::Button->new(
                class=>'deleteButton',
                name=>'delete',
                content=>'Löschen',
            ),
            Prty::Html::Widget::CheckBox->new(
                class=>'editCheckBox enabled',
                name=>'edit',
                option=>1,
                label=>'Bearbeiten',
            ),
        ],
    );

    return Prty::Html::Fragment->html($h,
        html=>$h->cat(
            $head,
            $h->tag('form',
                id=>$id,
                Prty::Html::Table::Simple->html($h,
                    rows=>[
                        [[$lay1->html($h)],[],[$lay2->html($h)]],
                        [[colspan=>3,$but->html($h)]],
                    ],
                ),
            ),
        ),
    );
}

# -----------------------------------------------------------------------------

=head3 instantiate() - Generiere JavaScript-Code zum Instantiieren des Widget

=head4 Synopsis

    $javaScript = $e->instantiate;

=cut

# -----------------------------------------------------------------------------

sub instantiate {
    my $self = shift;

    my ($deleteAction,$id,$onSuccess,$saveAction,$title) =
        $self->get(qw/deleteAction id onSuccess saveAction title/);

    my @att;
    if ($title) {
        push @att,"title: '$title'";
    }
    if ($saveAction) {
        push @att,"saveAction: '$saveAction'";
    }
    if ($onSuccess) {
        $onSuccess = Prty::Unindent->trim($onSuccess);
        $onSuccess =~ s/\n/\n    /g;
        push @att,"onSuccess: $onSuccess";
    }
    if ($deleteAction) {
        push @att,"deleteAction: '$deleteAction'";
    }
       
    return sprintf q|$('#%s').inheritanceForm({%s});|,$id,
        "\n    ".join(",\n    ",@att)."\n";
}

# -----------------------------------------------------------------------------

=head1 VERSION

1.088

=head1 AUTHOR

Frank Seitz, L<http://fseitz.de/>

=head1 COPYRIGHT

Copyright (C) 2016 Frank Seitz

=head1 LICENSE

This code is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

# -----------------------------------------------------------------------------

1;

# eof
