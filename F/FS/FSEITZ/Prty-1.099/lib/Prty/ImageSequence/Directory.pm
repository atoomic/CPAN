package Prty::ImageSequence::Directory;
use base qw/Prty::Hash/;

use strict;
use warnings;

our $VERSION = 1.099;

use Prty::DirHandle;
use Prty::ImageSequence::File;

# -----------------------------------------------------------------------------

=encoding utf8

=head1 NAME

Prty::ImageSequence::Directory - Bildsequenz-Verzeichnis

=head1 BASE CLASS

L<Prty::Hash>

=head1 SYNOPSIS

    # Klasse laden
    use %CLASS;
    
    # Instantiiere Bildsequenz
    $seq = Prty::ImageSequence::Directory->new('/my/image/dir');
    
    # Anzahl der enthaltenen Bilder
    $n = $seq->count;
    
    # Niedrigste Bildnummer
    $n = $seq->minNumber;
    
    # Höchste Bildnummer
    $n = $seq->maxNumber;
    
    # alle Bilder
    @images = $seq->images;
    
    # Bereich von Bildern
    
    @images = $seq->images('113-234');
    @images = $seq->images('290-');
    @images = $seq->images('422');
    
    # Lookup eines Bildes
    $img = $seq->image(422);

=head1 DESCRIPTION

Ein Objekt der Klasse repräsentiert eine Sequenz (eine geordnete
Liste) von Bildern, die in einem Verzeichnis gespeichert sind. Die
Bild-Dateinamen haben den Aufbau

    NNNNNN-WIDTHxHEIGHT[-NAME].EXT

Hierbei ist NNNNNN die Bild-Nummer. Die Bilder sind nach ihrer
Bild-Nummer geordnet. Ferner sind die Bilder nach ihrer
Bild-Nummer indiziert, so dass sie effizient über die Bild-Nummer
nachgeschlagen werden können.

=head1 ATTRIBUTES

=over 4

=item imageA

Array der Bild-Objekte des Verzeichnisses, nach Bild-Nummer sortiert.

=item imageH

Hash der Bild-Objekte, mit Bild-Nummer als Schlüssel.

=back

=head1 METHODS

=head2 Konstruktor

=head3 new() - Instantiiere Bildsequenz-Objekt

=head4 Synopsis

    $seq = $class->new($dir);

=head4 Description

Instantiiere ein Bildsequenz-Objekt aus den Bildern in
Verzeichnis $dir und liefere eine Referenz auf dieses Objekt
zurück.

=head4 Arguments

=over 4

=item $dir

Bild-Verzeichnis.

=back

=head4 Returns

Referenz auf Bild-Sequenz-Objekt

=cut

# -----------------------------------------------------------------------------

sub new {
    my ($class,$dir) = @_;

    # Bilddateien ermitteln

    my @images;
    my $dh = Prty::DirHandle->new($dir);
    while (my $file = $dh->next) {
        if ($file eq '.' || $file eq '..') {
            next;
        }
        if ($file !~ /^\d{6}(-.*)?\.([^.]+)$/) {
            $class->throw(
                q{IMGPOOL-00001: Illegal image name},
                File=>$file,
            );
        }
        push @images,Prty::ImageSequence::File->new("$dir/$file");
    }
    $dh->close;

    # Bild-Array sortieren
    @images = sort {$a->number <=> $b->number} @images;

    # Bild-Hash aufbauen

    my %hash;
    for my $img (@images) {
        $hash{$img->number} = $img;
    }

    # Sequenz-Objekt instantiieren

    return $class->SUPER::new(
        imageA=>\@images,
        imageH=>\%hash,
    );
}

# -----------------------------------------------------------------------------

=head2 Objektmethoden

=head3 count() - Anzahl der Bilder

=head4 Synopsis

    $n = $seq->count;

=head4 Description

Liefere die Anzahl der in der Sequenz enthaltenen Bilder.

=head4 Returns

Integer >= 0

=cut

# -----------------------------------------------------------------------------

sub count {
    my $self = shift;
    return scalar @{$self->images};
}

# -----------------------------------------------------------------------------

=head3 minNumber() - Niedrigste Bildnummer

=head4 Synopsis

    $n = $seq->minNumber;

=head4 Description

Liefere die niedrigste Bildnummer. Die niedrigste Bildnummer ist
die Nummer des ersten Bildes. Ist die Liste leer, liefere 0.

=head4 Returns

Integer >= 0

=cut

# -----------------------------------------------------------------------------

sub minNumber {
    my $self = shift;
    return $self->count? $self->images->[0]->number: 0;
}

# -----------------------------------------------------------------------------

=head3 maxNumber() - Höchste Bildnummer

=head4 Synopsis

    $n = $seq->maxNumber;

=head4 Description

Liefere die höchste Bildnummer. Die höchste Bildnummer ist die
Nummer des letzten Bildes. Ist die Liste leer, liefere 0.

=head4 Returns

Integer >= 0

=cut

# -----------------------------------------------------------------------------

sub maxNumber {
    my $self = shift;
    return $self->count? $self->images->[-1]->number: 0;
}

# -----------------------------------------------------------------------------

=head2 Bilder

=head3 images() - Liste von Bild-Objekten

=head4 Synopsis

    @images|$imageA = $seq->images;
    @images|$imageA = $seq->images($range);

=head4 Description

Liefere die Liste aller Bild-Objekte oder die Liste der
Bild-Objekte des Range $range.  Bild-Nummern, zu denen kein Bild
exisitert, werden übergangen.

=head4 Arguments

=over 4

=item $range

Bild-Nummer oder Bild-Nummern-Bereich. Mögliche Angaben:

    N    - Bild mit Nummer N
    N-M  - Bilder von Nummer N bis M (aufsteigend)
    M-N  - Bilder von Nummer M bis N (absteigend)
    N-   - Bilder von Nummer N bis zum letzten Bild
    -M   - Bilder vom ersten Bild bis Bild M

=back

=head4 Returns

Liste der Bild-Objekte. Im Skalarkontext liefere eine Referenz
auf die Liste.

=cut

# -----------------------------------------------------------------------------

sub images {
    my $self = shift;
    # @_: $range

    if (@_) {
        my $range = shift;

        my @images;
        if (index($range,'-') == -1) {
            # N -> Einzelbild
            if (my $img = $self->image($range)) {
                push @images,$img;
            }
        }
        else {
            # Bereichsangabe: N-M oder M-N (absteigend) N- oder -M

            my ($n,$m) = split /-/,$range;
            $n ||= $self->minNumber;
            $m ||= $self->maxNumber;

            if ($n < $m) {
                # Aufsteigend (vorwärts)

                for (my $i = $n; $i <= $m; $i++) {
                    if (my $img = $self->image($i)) {
                        push @images,$img;
                    }
                }
            }
            else {
                # Absteigend (rückwärts)

                for (my $i = $n; $i >= $m; $i--) {
                    if (my $img = $self->image($i)) {
                        push @images,$img;
                    }
                }
            }
        }
    
        return wantarray? @images: \@images;
    }
    else {
        my $imageA = $ self->{'imageA'};
        return wantarray? @$imageA: $imageA;
    }

    # not reached
}

# -----------------------------------------------------------------------------

=head3 image() - Lookup Bild-Objekt nach Bild-Nummer

=head4 Synopsis

    $img = $seq->image($n);

=head4 Description

Liefere das Bild-Objekt mit Bild-Nummer $n. Existiert keine
Bild-Objekt mit Nummer $n, liefere undef.

=head4 Arguments

=over 4

=item $n

Bild-Nummer

=back

=head4 Returns

Bild-Objekt oder C<undef>.

=cut

# -----------------------------------------------------------------------------

sub image {
    my ($self,$n) = @_;
    return $self->{'imageH'}->{$n};
}

# -----------------------------------------------------------------------------

=head1 VERSION

1.099

=head1 AUTHOR

Frank Seitz, L<http://fseitz.de/>

=head1 COPYRIGHT

Copyright (C) 2016 Frank Seitz

=head1 LICENSE

This code is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

# -----------------------------------------------------------------------------

1;

# eof
