package Prty::ClassConfig;
use base qw/Prty::Object/;

use strict;
use warnings;

our $VERSION = 1.086;

# -----------------------------------------------------------------------------

=head1 NAME

Prty::ClassConfig - Definiere Information auf Klassenebene

=head1 BASE CLASS

L<Prty::Object>

=head1 METHODS

=head2 def() - Setze Information

=head3 Synopsis

    $this->def(@keyVal);

=cut

# -----------------------------------------------------------------------------

sub def {
    my $class = ref $_[0] || $_[0]; shift;
    # @_: @keyVal

    no strict 'refs';
    my $ref = *{"$class\::ClassConfig"}{HASH};

    while (@_) {
        my $key = shift;
        my $val = shift;

        if ($class->can($key)) {
            $class->$key($val);
        }
        else {
            $ref->{$key} = $val;
        }
    }

    return;
}

# -----------------------------------------------------------------------------

=head2 getDef() - Liefere Konfigurationsvariablen

=head3 Synopsis

    @vals|$val = $this->getDef(@keys);

=cut

# -----------------------------------------------------------------------------

sub getDef {
    my $class = ref $_[0] || $_[0]; shift;
    # @_: @keys

    no strict 'refs';
    my $ref = *{"$class\::ClassConfig"}{HASH};

    my @arr;
    while (@_) {
        my $key = shift;

        if ($class->can($key)) {
            push @arr,$class->$key;
        }
        else {
            push @arr,$ref->{$key};
        }
    }

    return wantarray? @arr: $arr[0];
}

# -----------------------------------------------------------------------------

=head1 VERSION

1.086

=head1 AUTHOR

Frank Seitz, L<http://fseitz.de/>

=head1 COPYRIGHT

Copyright (C) 2016 Frank Seitz

=head1 LICENSE

This code is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

# -----------------------------------------------------------------------------

1;

# eof
