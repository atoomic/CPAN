package Prty::Html::Widget::TextArea;
use base qw/Prty::Html::Widget/;

use strict;
use warnings;

our $VERSION = 1.086;

use Prty::Html::Tag;

# -----------------------------------------------------------------------------

=encoding utf8

=head1 NAME

Prty::Html::Widget::TextArea - Mehrzeiliges Textfeld

=head1 BASE CLASS

L<Prty::Html::Widget>

=head1 ATTRIBUTES

=over 4

=item id => $id (Default: undef)

Id des Textfelds.

=item class => $class (Default: undef)

CSS Klasse des Textfelds.

=item disabled => $bool (Default: 0)

Widget kann nicht editiert werden.

=item hidden => $bool (Default: 0)

Widget ist (aktuell) unsichtbar.

=item cols => $n (Default: undef)

Sichtbare Breite des Texteingabefeldes in Zeichen.

=item name => $name (Default: undef)

Name des Feldes.

=item onKeyUp => $js (Default: undef)

JavaScript-Handler.

=item rows => $n (Default: undef)

Sichtbare Höhe des Texteingabefeldes in Zeilen.

=item style => $style (Default: undef)

CSS Definition (inline).

=item value => $str (Default: undef)

Anfänglicher Wert des Felds.

=back

=head1 METHODS

=head2 new() - Konstruktor

=head3 Synopsis

    $e = $class->new(@attVal);

=cut

# -----------------------------------------------------------------------------

sub new {
    my $class = shift;
    # @_: @attVal

    # Defaultwerte

    my $self = $class->SUPER::new(
        class=>undef,
        cols=>undef,
        disabled=>0,
        hidden=>0,
        id=>undef,
        name=>undef,
        onKeyUp=>undef,
        rows=>undef,
        style=>undef,
        value=>undef,
    );

    # Werte Konstruktoraufruf
    $self->set(@_);

    return $self;
}

# -----------------------------------------------------------------------------

=head2 html() - Generiere HTML-Code

=head3 Synopsis

    $html = $e->html;
    $html = $class->html(@attVal);

=cut

# -----------------------------------------------------------------------------

sub html {
    my $this = shift;

    my $self = ref $this? $this: $this->new(@_);

    # Attribute

    my ($class,$cols,$disabled,$id,$name,$onKeyUp,$rows,$style,$value) =
        $self->get(qw/class cols disabled id name onKeyUp rows style value/);

    # Generierung

    my $h = Prty::Html::Tag->new;

    return $h->tag('textarea',
        id=>$id,
        name=>$name,
        class=>$class,
        style=>$style,
        disabled=>$disabled,
        onkeyup=>$onKeyUp,
        cols=>$cols,
        rows=>$rows,
        $value
    );    
}

# -----------------------------------------------------------------------------

=head1 VERSION

1.086

=head1 AUTHOR

Frank Seitz, L<http://fseitz.de/>

=head1 COPYRIGHT

Copyright (C) 2016 Frank Seitz

=head1 LICENSE

This code is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

# -----------------------------------------------------------------------------

1;

# eof
