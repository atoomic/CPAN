package Prty::JQuery::DataTable;
use base qw/Prty::Hash/;

use strict;
use warnings;

our $VERSION = 1.086;

use Prty::Hash;
use Prty::Html::Table::List;

# -----------------------------------------------------------------------------

=encoding utf8

=head1 NAME

Prty::JQuery::DataTable - Erzeuge eine jQuery DataTables-Tabelle

=head1 BASE CLASS

L<Prty::Hash>

=head1 ATTRIBUTES

=over 4

=item class => $class (Default: undef)

CSS-Klasse der DataTable.

=item columns => \@columns (Default: [])

Spezifikation der Kolumnen. Jeder Eintrag ist ein Hash mit den
Komponenten:

    {
        titel=>$title,
    }

=item id => $id (Default: undef)

DOM-Id der DataTable.

=back

=head1 METHODS

=head2 Konstruktor

=head3 new() - Instantiiere DataTables-Objekt

=head4 Synopsis

    $obj = $class->new(@attVal);

=head4 Description

Instantiiere ein DataTables-Objekt und liefere eine
Referenz auf dieses Objekt zurück.

=cut

# -----------------------------------------------------------------------------

sub new {
    my $class = shift;
    # @_: @attVal

    my $self = $class->SUPER::new(
        class=>undef,
        columns=>[],
        id=>undef,
        rowCallback=>undef,
        rows=>[],
    );
    $self->set(@_);

    return $self;
}

# -----------------------------------------------------------------------------

=head2 Objektmethoden

=head3 html() - Generiere HTML

=head4 Synopsis

    $html = $obj->html($h);
    $html = $class->html($h,@attVal);

=head4 Description

Generiere den HTML-Code des DataTables-Objekts und liefere
diesen zurück. Als Klassenmethode gerufen, wird das Objekt intern
mit den Attributen @attVal instantiiert.

=cut

# -----------------------------------------------------------------------------

sub html {
    my $this = shift;
    my $h = shift;

    my $self = ref $this? $this: $this->new(@_);

    my ($class,$columnA,$id,$rowCallback,$rowA) =
        $self->get(qw/class columns id rowCallback rows/);

    # Column-Hashes auf Korrektheit prüfen

    for my $h (@$columnA) {
        Prty::Hash->validate($h,[qw/title/]);
    }

    # HTML generieren

    if (!@$columnA) {
        return '';
    }

    my @titles;
    for my $col (@$columnA) {
        push @titles,$col->{'title'};
    }

    return $h->cat(
        -placeholders=>[
            __ID__=>$id,
        ],
        Prty::Html::Table::List->html($h,
            allowHtml=>1,
            border=>0,
            class=>$class,
            empty=>undef,
            id=>$id,
            rowCallback=>$rowCallback,
            rows=>$rowA,
            titles=>\@titles,
        ),
        $h->tag('script',q|
            $('#__ID__').DataTable({
                paging: false,
                info: false,
                fixedHeader: true,
                stateSave: true,
                // order: [[2,'asc']],
                dom: 'ft',
                language: {
                    search: 'Suche:',
                    zeroRecords: 'Keine Daten',
                },
            });
        |),
    );
}

# -----------------------------------------------------------------------------

=head1 VERSION

1.086

=head1 AUTHOR

Frank Seitz, L<http://fseitz.de/>

=head1 COPYRIGHT

Copyright (C) 2016 Frank Seitz

=head1 LICENSE

This code is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

# -----------------------------------------------------------------------------

1;

# eof
