package Prty::ContentProcessor;
use base qw/Prty::Hash/;

use strict;
use warnings;

our $VERSION = 1.097;

use Prty::Option;
use Prty::Path;
use Time::HiRes ();
use Prty::Perl;
use Prty::Hash;
use Prty::Section::Parser;
use Prty::Section::Object;
use Prty::DestinationTree;
use Prty::Formatter;
use Prty::PersistentHash;

# -----------------------------------------------------------------------------

=encoding utf8

=head1 NAME

Prty::ContentProcessor - Prozessor für Abschnitts-Dateien

=head1 BASE CLASS

L<Prty::Hash>

=head1 SYNOPSIS

    use Prty::ContentProcessor;
    
    $cop = Prty::ContentProcessor->new('.mytool',-extension=>['.myt']);
    $cop->registerType('MyTool::Program::Shell','.mprg','[Program]',Language=>'Shell');
    ...
    $cop->load(@paths)->generate->test;

=head1 DESCRIPTION

Ein  Objekt der Klasse repräsentiert einen Prozessor für
Abschnitts-Dateien, also Dateien, die von einem Abschnittsparser
(s. Klasse Prty::Section::Parser) geparsed werden.

=head1 METHODS

=head2 Konstruktor

=head3 new() - Instantiiere ContentProcessor

=head4 Synopsis

    $cop = $class->new($storageDir,@opt);

=head4 Description

Instantiiere ein ContentProcessor-Objekt und liefere eine Referenz
auf dieses Objekt zurück.

=head4 Arguments

=over 4

=item $storage

Der Name des Storage-Verzeichnisses ohne Verzeichnisanteil,
z.B. '.storage'.

=back

=head4 Options

=over 4

=item -extensions => \@extensions Liste der Datei-Erweiterungen der

Dateien, die der ContentProcessor verarbeitet. Hinzu kommen die
Erweiterungen, die die per registerType() hinzugefügten
Entitäts-Typen definieren. Die Extension wird ohne Punkt
angegeben.

=item -init => $bool (Default: 0)

Erzeuge und initialisiere Storage.

=back

=head4 Returns

ContentProcessor-Objekt

=cut

# -----------------------------------------------------------------------------

sub new {
    my $class = shift;
    my $storage = shift;

    # Optionen

    my $extensions = [];
    my $init = 0;

    Prty::Option->extract(\@_,
        -extensions=>\$extensions,
        -init=>\$init,
    );

    # Prüfen, ob Storage-Verzeichnis existert

    if (!-d $storage) {
        if ($init) {
            # Storage-Verzeichnis erzeugen
    
            Prty::Path->mkdir($storage);
            Prty::Path->mkdir("$storage/db");
            Prty::Path->mkdir("$storage/def");
            Prty::Path->mkdir("$storage/pure");
        }
        else {
            die "ERROR: Directory $storage does not exist\n";
        }
    }
    
    # Objekt instantiieren

    my $self = $class->SUPER::new(
        t0=>scalar Time::HiRes::gettimeofday,
        storage=>$storage,
        baseExtensionA=>$extensions,
        pluginA=>[],
        entityA=>[],
        fileA=>[],
        parsedSections=>0,
        parsedLines=>0,
        parsedChars=>0,
        parsedBytes=>0,
        # memoize
        extensionRegex=>undef,
        stateDb=>undef,
    );

    return $self;
}

# -----------------------------------------------------------------------------

=head2 Plugins

=head3 registerType() - Registriere Entitäts-Typ

=head4 Synopsis

    $cop->registerType($pluginClass,$extension,$sectionType,@keyVal);

=head4 Description

Registriere Plugin-Klasse $pluginClass für Abschnitts-Objekte mit
Identifier $sectionType und den Eigenschaften @keyVal. Die
Plugin-Klasse wird automatisch geladen, falls sie noch nicht
vorhanden ist (sie kann für Tests also auch "inline" definiert
werden).

Die Plugin-Definition wird intern auf einem Hash-Objekt
gespeichert, das vom ContentProcessor mit den instantiierten
Entitten verknüpft wird.

=head4 Arguments

=over 4

=item $pluginClass

Name der Plugin-Klasse, z.B. 'Program::Shell'.

=item $extension

Datei-Erweiterung für Dateien dieses Typs, ohne Punkt, z.B. 'prg'.

=item $sectionType

Abschnitts-Bezeichner ohne Klammerung, z.B. 'Program'.

=item @keyVal

Abschnitts-Attribute, die über den Abschnitts-Bezeichner hinaus
den Dateityp kennzeichnen, z.B. Language=>'Shell'.

=back

=head4 Returns

nichts

=cut

# -----------------------------------------------------------------------------

sub registerType {
    my ($self,$pluginClass) = splice @_,0,2;
    # @_: $extension,$sectionType,@keyVal

    Prty::Perl->loadClass($pluginClass);
    push @{$self->{'pluginA'}},Prty::Hash->new(
        class=>$pluginClass,
        extension=>shift,
        sectionType=>shift,
        keyValA=>\@_,
    );
        
    return;
}

# -----------------------------------------------------------------------------

=head2 Operationen

=head3 load() - Lade Eingabe-Dateien

=head4 Synopsis

    $cop = $cop->load(@opt,@paths);

=head4 Description

Lade die Eingabe-Dateien der Pfade @paths.

=head4 Arguments

=over 4

=item @paths

Liste der Verzeichnisse und Dateien. Der Pfad '-' bedeutet STDIN.

=back

=head4 Options

=over 4

=item -commit => $bool (default: 0)

Vergleiche die Entitäten gegen den Storage und übertrage die
Veränderungen. Geänderte Entitäten werden in der in der Datenbank
als geändert gekennzeichnet.

=back

=head4 Returns

ContentProcessor-Objekt (für Method-Chaining)

=cut

# -----------------------------------------------------------------------------

sub load {
    my $self = shift;
    # @_: @opt,@paths

    # Argumente und Optionen

    my $commit = 0;

    Prty::Option->extract(\@_,
        -commit=>\$commit,
    );
    my @paths = @_;

    # Ermittele die zu verarbeitenden Dateien
    
    printf STDERR "%.3f ==find==\n",
        Time::HiRes::gettimeofday-$self->get('t0');
        
    my @files;
    for my $path (@paths) {
        if (-d $path) {
            # Verzeichnis: nach Dateien durchsuchen

            push @files,Prty::Path->find($path,
                -type=>'f',
                -pattern=>$self->extensionRegex,
            );
        }
        else {
            # Datei: direkt hinzufügen
            push @files,$path;
        }
    }
    
    printf STDERR "%.3f ==parse==\n",
        Time::HiRes::gettimeofday-$self->get('t0');

    # Instantiiere Parser

    my $par = Prty::Section::Parser->new(
        encoding=>'utf-8',
    );

    # Parse Dateien zu Entitäten

    my $entityA = $self->get('entityA');
    for my $file (@files) {
        $par->parse($file,sub {
            my $sec = Prty::Section::Object->new(@_);

            if ($sec->brackets eq '[]') {
                # Wandele Abschnitts-Objekt in Entität
    
                my $plg = $self->plugin($sec);
                if (!$plg) {
                    # Fehler: Für jeden Haupt-Abschnitt muss ein
                    # Plugin definiert worden sein
    
                    $sec->error(
                        q{COP-00001: Missing plugin for main section},
                         Section=>$sec->fullType,
                    );
                }
                push @$entityA,$plg->class->create($sec,$self,$plg);
            }
            elsif (@$entityA) {
                # Füge weiteren Abschnitt zu Entität hinzu
    
                my $ent = $entityA->[-1];
                $ent->addSubSection($sec);
                $ent->transferSource($sec);
            }
            else {
                # Fehler: Erster Abschnitt ist kein []-Abschnitt
    
                $sec->error(
                    q{COP-00002: First section must be a []-section},
                     Section=>$sec->fullType,
                );
            }

            # Abschnittsobjekt gegen unabsichtliche Erweiterungen sperren
            $sec->lockKeys;

            return;
        });
    }

    # Statistische Daten sichern

    $self->set(parsedSections=>$par->get('parsedSections'));
    $self->set(parsedLines=>$par->get('parsedLines'));
    $self->set(parsedChars=>$par->get('parsedChars'));
    $self->set(parsedBytes=>$par->get('parsedBytes'));

    if ($commit) {    
        printf STDERR "%.3f ==commit==\n",
            Time::HiRes::gettimeofday-$self->get('t0');

        # Gleiche Eingabedateien mit Storage-Dateien ab

        my $defDir = $self->storage('def');

        my $dt = Prty::DestinationTree->new($defDir,
            -quiet=>0,
            -language=>'en',
            -outHandle=>\*STDERR,
        );
        $dt->addDir($defDir);

        for my $ent (@$entityA) {
            my $file = $defDir.'/'.$ent->entityId.'.'.$ent->plugin->extension;
            $dt->addFile($file,$ent->sourceRef,
                -encoding=>'utf-8',
                -onUpdate=>sub {
                    $self->state($ent,1);
                },
            );
        }
        $self->stateDb->sync;

        # Lösche überzählige Storage-Dateien (nach Rückfrage)

        $dt->cleanup(1,$self->getRef('t0'));
        $dt->close;
    }

    return $self;
}

# -----------------------------------------------------------------------------

=head3 generate() - Generiere Ausgabe-Dateien

=head4 Synopsis

    $cop = $cop->generate($dir);

=head4 Description

Generiere in Zielverzeichnis $dir die Ausgabe-Dateien aller
Entitäten.

=head4 Returns

ContentProcessor-Objekt (für Method-Chaining)

=cut

# -----------------------------------------------------------------------------

sub generate {
    my ($self,$dir) = @_;
    # @_: @opt

    printf STDERR "%.3f ==generate==\n",
        Time::HiRes::gettimeofday-$self->get('t0');

    # Argumente und Optionen

    my $force = 0;

    Prty::Option->extract(\@_,
        -force=>\$force,
    );

    my $pureDir = $self->storage('pure');
    my $dt = Prty::DestinationTree->new($dir,$pureDir,
        -quiet=>0,
        -language=>'en',
        -outHandle=>\*STDERR,
    );
    $dt->addDir($dir);
    $dt->addDir($pureDir);

    my $entityA = $self->get('entityA');
    for my $ent (@$entityA) {
        my $codeWritten = 0;
        for my $fil ($ent->files) {
            my $written = $dt->addFile($dir.'/'.$fil->name,
                -encoding=>'utf-8',
                -generate=>$self->state($ent) == 1 || $force,
                -onGenerate=>sub {
                    return $fil->generate;
                },
            );
            if ($written && $fil->isCode) {
                $codeWritten++;
            }
        }

        if ($ent->testable) {
            my $written = $dt->addFile($pureDir.'/'.$ent->entityId.'.pure',
                -encoding=>'utf-8',
                -generate=>$codeWritten,
                -onGenerate=>sub {
                    return $ent->pureCode;
                },
            );
            if ($written) {
                $self->state($ent,2);
                next;
            }
        }
    
        $self->state($ent,0);
    }
    $self->stateDb->sync;

    # Lösche überzählige Dateien (ohne Rückfrage)

    $dt->cleanup;
    $dt->close;

    return $self;
}
    

# -----------------------------------------------------------------------------

=head3 test() - Teste geänderten Code

=head4 Synopsis

    $cop = $cop->test;

=head4 Returns

ContentProcessor-Objekt (für Method-Chaining)

=cut

# -----------------------------------------------------------------------------

sub test {
    my $self = shift;

    printf STDERR "%.3f ==test==\n",
        Time::HiRes::gettimeofday-$self->get('t0');
    
    return $self;
}

# -----------------------------------------------------------------------------

=head2 Statistik

=head3 info() - Informationszeile

=head4 Synopsis

    $str = $cop->info;

=head4 Description

Liefere eine Informationszeile mit statistischen Informationen, die
am Ende der Verarbeitung ausgegeben werden kann.

=head4 Returns

Zeichenkette

=cut

# -----------------------------------------------------------------------------

sub info {
    my $self = shift;

    my $entities = @{$self->get('entityA')};
    my $files = @{$self->get('fileA')};
    
    return sprintf '%.3f sec; Entities: %s; Sections: %s; Lines: %s'.
            '; Bytes: %s; Chars: %s',
        Time::HiRes::gettimeofday-$self->get('t0'),
        Prty::Formatter->readableNumber($entities,','),
        Prty::Formatter->readableNumber($self->get('parsedSections'),','),
        Prty::Formatter->readableNumber($self->get('parsedLines'),','),
        Prty::Formatter->readableNumber($self->get('parsedBytes'),','),
        Prty::Formatter->readableNumber($self->get('parsedChars'),',');
}

# -----------------------------------------------------------------------------

=head2 Intern

=head3 extensionRegex() - Regex zum Auffinden von Eingabe-Dateien

=head4 Synopsis

    $regex = $cop->extensionRegex;

=head4 Description

Liefere den regulären Ausdruck, der die Dateinamen matcht, die vom
ContentProcessor verarbeitet werden. Der Regex wird genutzt, wenn
ein I<Verzeichnis> nach Eingabe-Dateien durchsucht wird.

=head4 Returns

kopilierter Regex

=cut

# -----------------------------------------------------------------------------

sub extensionRegex {
    my $self = shift;

    return $self->memoize('extensionRegex',sub {
        my ($self,$key) = @_;
        
        # Dateinamen-Erweiterungen ermitteln. Verschiedene
        # Plugin-Klassen können identische Datei-Erweiterungen haben,
        # deswegen filtern wir über einen Hash.

        my %extension;
        for my $k (@{$self->{'baseExtensionA'}}) {
            $extension{$k}++;
        }
        for my $plg (@{$self->{'pluginA'}}) {
            $extension{$plg->extension}++;
        }        

        # Regex erstellen

        my $regex;
        for (sort keys %extension) {
            my $ext = $_;
            if ($regex) {
                $regex .= '|';
            }
            $regex .= $ext;
        }

        return qr/\.($regex)$/;
    });
}

# -----------------------------------------------------------------------------

=head3 plugin() - Ermittele Plugin zu Abschnitts-Objekt

=head4 Synopsis

    $plg = $cop->plugin($sec);

=head4 Description

Ermittele das Plugin zu Abschnitts-Objekt $sec. Existiert
kein Plugin zu dem Abschnitts-Objekt, liefere C<undef>.

=head4 Arguments

=over 4

=item $sec

Abschnitts-Objekt

=back

=head4 Returns

Plugin-Objekt

=cut

# -----------------------------------------------------------------------------

my %Plugins; # Section-Type => Liste der Plugins
    
sub plugin {
    my ($self,$sec) = @_;

    if (!%Plugins) {
        # Indiziere Plugins nach Section-Type

        for my $plg (@{$self->{'pluginA'}}) {
            push @{$Plugins{$plg->sectionType}},$plg;
        }
    }
        
    # Prüfe Section gemäß Plugin-Kriterien

    if (my $pluginA = $Plugins{$sec->type}) {
        for my $plg (@$pluginA) {
            my $ok = 1;
            my $a = $plg->keyValA;
            for (my $i = 0; $i < @$a; $i += 2) {
                if ($sec->get($a->[$i]) ne $a->[$i+1]) {
                    $ok = 0;
                    last;
                }
            }
            if ($ok) {
                return $plg;
            }
        }
    }
        
    return undef; # kein passendes Plugin gefunden
}

# -----------------------------------------------------------------------------

=head3 state() - Liefere/Setze persistenten Entitäts-Status

=head4 Synopsis

    $state = $cop->state($ent);
    $state = $cop->state($ent,$state);

=head4 Description

Liefere/Setze den persistenten Status der Entität $ent. Der
Entitäts-Status ist persistent und bleibt daher über
Programmaufrufe hinweg erhalten.

Eine Entität besitzt einen von vier Status:

=over 4

=item 0

Nichts zu tun. Die Entität wurde nicht geändert.

=item 1

Die Entitäts-Datei wurde geändert. Die Ausgabe-Dateien der
Entität müssen neu generiert werden.

=item 2

Der Code der Entität hat sich geändert. Die Entität und alle
abhängigen Entitäten müssen getestet werden.

=item 3

Nur die Entität selbst muss getestet werden. Die Entität
selbst wurde nicht geändert, hängt aber von einer Entität ab,
die geändert wurde, oder ihre Testdateien oder Testdaten
wurden geändert, was keinen Test der abhängigen Entitäten
erfordert.

=back

Ohne Parameter aufgerufen, liefert die Methode den aktuellen
Zustand der Entität. Mit Parameter gerufen, setzt die Methode den
Zustand, wobei dieser persistent gespeichert wird.

=cut

# -----------------------------------------------------------------------------

sub state {
    my ($self,$ent) = splice @_,0,2;
    # @_: $state

    my $h = $self->stateDb;
    my $entityId = $ent->entityId;

    if (@_) {
        my $state = shift;
        $h->set($entityId=>$state);
        return $state;
    }

    return $h->get($entityId);
}

# -----------------------------------------------------------------------------

=head3 stateDb() - Persistenter Hash für Entitäts-Status

=head4 Synopsis

    $h = $cop->stateDb;

=head4 Description

Liefere eine Referenz auf den persistenten Hash, der den Status
von Entitäten speichert.

=cut

# -----------------------------------------------------------------------------

sub stateDb {
    my $self = shift;

    return $self->memoize('stateDb',sub {
        my ($self,$key) = @_;
        
        my $file = $self->storage('db/entity-state.db');
        return Prty::PersistentHash->new($file,'rw');
    });
}

# -----------------------------------------------------------------------------

=head3 storage() - Pfad zum oder innerhalb Storage

=head4 Synopsis

    $path = $cop->storage;
    $path = $cop->storage($subPath);

=head4 Description

Liefere den Pfad des Storage, ggf. ergänzt um den Sub-Pfad
$subPath.

=head4 Arguments

=over 4

=item $subPath

Ein Sub-Pfad innerhalb des Storage.

=back

=head4 Returns

Pfad

=cut

# -----------------------------------------------------------------------------

sub storage {
    my $self = shift;
    # @_: $subPath

    my $path = $self->{'storage'};
    if (@_) {
        $path .= '/'.shift;
    }

    return $path;
}

# -----------------------------------------------------------------------------

=head1 VERSION

1.097

=head1 AUTHOR

Frank Seitz, L<http://fseitz.de/>

=head1 COPYRIGHT

Copyright (C) 2016 Frank Seitz

=head1 LICENSE

This code is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

# -----------------------------------------------------------------------------

1;

# eof
