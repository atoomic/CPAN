#########################################################################################
# Package       HiPi::Apps::Control::Data::GPIOPADJ8
# Description:  Data From GPIO PAD1
# Created       Tue Feb 26 04:46:27 2013
# SVN Id        $Id: GPIOPAD1.pm 3 2015-03-12 01:07:32Z Mark Dootson $
# Copyright:    Copyright (c) 2015 Mark Dootson
# Licence:      This work is free software; you can redistribute it and/or modify it 
#               under the terms of the GNU General Public License as published by the 
#               Free Software Foundation; either version 3 of the License, or any later 
#               version.
#########################################################################################

package HiPi::Apps::Control::Data::GPIOPADJ8;

#########################################################################################

use 5.14.0;
use strict;
use warnings;
use parent qw( HiPi::Apps::Control::Data::Pad );
use HiPi::Constant qw( :raspberry );
use Wx qw( wxTheApp );
use HiPi::Apps::Control::Constant qw( :padpin );

our $VERSION = '0.22';

our @pinmap = (
    DNC_PIN_3V3,  DNC_PIN_5V0,  RPI_PIN_3,  DNC_PIN_5V0,  RPI_PIN_5,
    DNC_PIN_GND,  RPI_PIN_7,    RPI_PIN_8,  DNC_PIN_GND,  RPI_PIN_10,
    RPI_PIN_11,   RPI_PIN_12,   RPI_PIN_13, DNC_PIN_GND,  RPI_PIN_15,
    RPI_PIN_16,   DNC_PIN_3V3,  RPI_PIN_18, RPI_PIN_19,   DNC_PIN_GND,
    RPI_PIN_21,   RPI_PIN_22,   RPI_PIN_23, RPI_PIN_24,   DNC_PIN_GND,
    RPI_PIN_26,   RPI_PIN_27,   RPI_PIN_28, RPI_PIN_29,   DNC_PIN_GND,
    RPI_PIN_31,   RPI_PIN_32,   RPI_PIN_33, DNC_PIN_GND,  RPI_PIN_35,
    RPI_PIN_36,   RPI_PIN_37,   RPI_PIN_38, DNC_PIN_GND,  RPI_PIN_40,
);

sub new {
    my ($class, $readonly) = @_;
    my @objmap = @pinmap;
    my $self = $class->SUPER::new('Raspberry Pi GPIO Pad J8', \@objmap, $readonly);
    return $self;
}

sub get_gpio_pinnumber {
    my($self, $rpipin) = @_;
    return $pinmap[$rpipin -1];
}

1;
