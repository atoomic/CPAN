#########################################################################################
# Package       HiPi::Interface::ENER002
# Description:  Control Energenie ENER002 sockets
# Copyright:    Copyright (c) 2016 Mark Dootson
# Licence:      This work is free software; you can redistribute it and/or modify it 
#               under the terms of the GNU General Public License as published by the 
#               Free Software Foundation; either version 3 of the License, or any later 
#               version.
#########################################################################################

package HiPi::Interface::ENER002;

#########################################################################################

use strict;
use warnings;
use parent qw( HiPi::Interface );
use Carp;

__PACKAGE__->create_accessors( qw( groupid backend repeat) );

our $VERSION = '0.20';

our @EXPORT = ();
our @EXPORT_OK = ();
our %EXPORT_TAGS = ( all => \@EXPORT_OK );

# Switch Data
# $data = $switchmask->[$socketnum - 1]->[$offon];
# where $socketnum == 0 | 1 | 2 | 3 | 4 and $offon == 0|1;
# when  $socketnum == 0 then $offon is applied to all sockets

my $_switchdata = [
    [ 0b1100, 0b1101 ], # off / on all sockets
    [ 0b1110, 0b1111 ], # off / on  socket 1
    [ 0b0110, 0b0111 ], # off / on  socket 2
    [ 0b1010, 0b1011 ], # off / on  socket 3
    [ 0b0010, 0b0011 ], # off / on  socket 4
];

sub new {
    my( $class, %userparams ) = @_;
    
    my %params = (
        backend      => 'ENER314_RT',
        groupid      => 0x6C6C6,
        device       => undef,
        repeat       => 8,
    );
    
    foreach my $key (sort keys(%userparams)) {
        $params{$key} = $userparams{$key};
    }
    
    unless( defined($params{device}) ) {
        
        # reasonable repeat values
        $params{repeat} ||= 15;
        $params{repeat} = 100 if $params{repeat} > 100;
        $params{repeat} = 15 if $params{repeat} < 15;
        
        if ( $params{backend} eq 'ENER314_RT' ) {
            # Two way configurable board
            require HiPi::Controller::ENER314_RT;
            my $dev = HiPi::Controller::ENER314_RT->new();
            $params{device} = $dev;
        ##} elsif( $params{backend} eq 'ENER314' ) { 
        ##    # simple 1 way single group board
        ##    require HiPi::Interface::ENER314;
        ##    my $dev = HiPi::Interface::ENER314->new();
        ##    $params{device} = $dev;
        } else {
            croak qq(Invalid backend $params{backend} specified);
        }
        
    }
    my $self = $class->SUPER::new(%params);
    
    return $self;
}

sub pair_socket {
    my($self, $socket, $seconds) = @_;
    croak(qq(Invalid socket $socket)) unless $socket =~ /^1|2|3|4$/;
    $seconds ||= 10;
    
    # broadcast for 10 seconds;
    my $endtime = time() + $seconds;
    my $data = $_switchdata->[$socket]->[0]; # broadcast 'off' message for socket
    
    while ( $endtime >= time() ) {
        $self->device->ener002_pair_socket( $self->groupid, $data, $self->repeat );
    }
}

sub switch_socket {
    my($self, $socket, $offon) = @_;
    croak(qq(Invalid socket $socket)) unless $socket =~ /^0|1|2|3|4$/;
    $offon = ( $offon ) ? 1 : 0;
    my $data = $_switchdata->[$socket]->[$offon];
    $self->device->ener002_switch_socket( $self->groupid, $data, $self->repeat );
}

# test what we actually send 
sub dump_message {
    my($self, $socket, $offon) = @_;
    croak(q(Method requires backend 'ENER314_RT')) if $self->backend ne 'ENER314_RT';
    croak(qq(Invalid socket $socket)) unless $socket =~ /^0|1|2|3|4$/;
    $offon = ( $offon ) ? 1 : 0;
    my $data = $_switchdata->[$socket]->[$offon];
    my @tvals = $self->device->hrf_make_ook_message( $self->groupid, $data );
    
    # print preamble
    print sprintf("preamble : 0x%x, 0x%x, 0x%x, 0x%x\n", @tvals[0..3]);
    # print group id
    print sprintf("group id : 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x\n", @tvals[4..13]);
    # print data
    print sprintf("set data : 0x%x, 0x%x\n", @tvals[14..15]);
}

1;

__END__