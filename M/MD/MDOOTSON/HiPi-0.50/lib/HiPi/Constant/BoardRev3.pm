#########################################################################################
# Package       HiPi::Constant::BoardRev3
# Description:  Constants for Raspberry Pi board revision 2
# Created       Fri Nov 23 22:32:56 2012
# SVN Id        $Id: BoardRev3.pm 50 2016-03-07 17:00:01Z Mark Dootson $
# Copyright:    Copyright (c) 2015 Mark Dootson
# Licence:      This work is free software; you can redistribute it and/or modify it 
#               under the terms of the GNU General Public License as published by the 
#               Free Software Foundation; either version 3 of the License, or any later 
#               version.
#########################################################################################

package HiPi::Constant::BoardRev3;

#########################################################################################

use strict;
use warnings;

our $VERSION = '0.20';

package HiPi::Constant;


sub gpio_2_rpitext {
    my $gpiotext = shift;
    my %lookup = (
        GPIO_02 => 'Pad J8 Pin 3',
        GPIO_03 => 'Pad J8 Pin 5',
        GPIO_04 => 'Pad J8 Pin 7',
        GPIO_14 => 'Pad J8 Pin 8',
        GPIO_15 => 'Pad J8 Pin 10',
        GPIO_17 => 'Pad J8 Pin 11',
        GPIO_18 => 'Pad J8 Pin 12',
        GPIO_27 => 'Pad J8 Pin 13',
        GPIO_22 => 'Pad J8 Pin 15',
        GPIO_23 => 'Pad J8 Pin 16',
        GPIO_24 => 'Pad J8 Pin 18',
        GPIO_10 => 'Pad J8 Pin 19',
        GPIO_09 => 'Pad J8 Pin 21',
        GPIO_25 => 'Pad J8 Pin 22',
        GPIO_11 => 'Pad J8 Pin 23',
        GPIO_08 => 'Pad J8 Pin 24',
        GPIO_07 => 'Pad J8 Pin 26',
        GPIO_00 => 'Pad J8 Pin 27',
        GPIO_01 => 'Pad J8 Pin 28',
        GPIO_05 => 'Pad J8 Pin 29',
        GPIO_06 => 'Pad J8 Pin 31',
        GPIO_12 => 'Pad J8 Pin 32',
        GPIO_13 => 'Pad J8 Pin 33',
        GPIO_19 => 'Pad J8 Pin 35',
        GPIO_16 => 'Pad J8 Pin 36',
        GPIO_26 => 'Pad J8 Pin 37',
        GPIO_20 => 'Pad J8 Pin 38',
        GPIO_21 => 'Pad J8 Pin 40',
        
    );
    if(exists($lookup{$gpiotext})) {
        return $lookup{$gpiotext}
    } else {
        return 'Unknown';
    }
}


#-------------------------------------------
# Constants to convert RPI pin ids
# to Broadcom Pin numbers
#-------------------------------------------

use constant {
    RPI_HIGH               =>  1,
    RPI_LOW                =>  0,
    RPI_BOARD_REVISION     =>  2,
    RPI_PAD1_PIN_3         =>  2,
    RPI_PAD1_PIN_5         =>  3,
    RPI_PAD1_PIN_7         =>  4,
    RPI_PAD1_PIN_8         => 14,
    RPI_PAD1_PIN_10        => 15,
    RPI_PAD1_PIN_11        => 17,
    RPI_PAD1_PIN_12        => 18,
    RPI_PAD1_PIN_13        => 27,
    RPI_PAD1_PIN_15        => 22,
    RPI_PAD1_PIN_16        => 23,
    RPI_PAD1_PIN_18        => 24,
    RPI_PAD1_PIN_19        => 10,
    RPI_PAD1_PIN_21        =>  9,
    RPI_PAD1_PIN_22        => 25,
    RPI_PAD1_PIN_23        => 11,
    RPI_PAD1_PIN_24        =>  8,
    RPI_PAD1_PIN_26        =>  7,
    
    RPI_PAD5_PIN_3         => 28,
    RPI_PAD5_PIN_4         => 29,
    RPI_PAD5_PIN_5         => 30,
    RPI_PAD5_PIN_6         => 31,
    
    RPI_INT_NONE           => 0x00,
    RPI_INT_FALL           => 0x01,
    RPI_INT_RISE           => 0x02,
    RPI_INT_BOTH           => 0x03,
    RPI_INT_AFALL          => 0x04,
    RPI_INT_ARISE          => 0x08,
    RPI_INT_HIGH           => 0x10,
    RPI_INT_LOW            => 0x20,
    
    RPI_PINMODE_INPT       => 0,
    RPI_PINMODE_OUTP       => 1,
    RPI_PINMODE_ALT0       => 4,
    RPI_PINMODE_ALT1       => 5,
    RPI_PINMODE_ALT2       => 6,
    RPI_PINMODE_ALT3       => 7,
    RPI_PINMODE_ALT4       => 3,
    RPI_PINMODE_ALT5       => 2,

    I2C0_SDA	           => 28,
    I2C0_SCL	           => 29,
    I2C1_SDA	           => 2,
    I2C1_SCL	           => 3,
    I2C_SDA	               => 2,
    I2C_SCL	               => 3,
    ID_SD	               => 0,
    ID_SC	               => 1,
    
    RPI_PUD_NULL           => -1,
    RPI_PUD_OFF            => 0,
    RPI_PUD_DOWN           => 1,
    RPI_PUD_UP             => 2,
    
    # pad 1
    WPI_PIN_0   => 17,
    WPI_PIN_1   => 18,
    WPI_PIN_2   => 27,
    WPI_PIN_3   => 22,
    WPI_PIN_4   => 23,
    WPI_PIN_5   => 24,
    WPI_PIN_6   => 25,
    WPI_PIN_7   => 4,
    WPI_PIN_8   => 2,
    WPI_PIN_9   => 3,
    WPI_PIN_10  => 8,
    WPI_PIN_11  => 7,
    WPI_PIN_12  => 10,
    WPI_PIN_13  => 9,
    WPI_PIN_14  => 11,
    WPI_PIN_15  => 14,
    WPI_PIN_16  => 15,
    # pad 5
    WPI_PIN_17  => 28,
    WPI_PIN_18  => 29,
    WPI_PIN_19  => 30,
    WPI_PIN_20  => 31,
    # J8 Additions
    WPI_PIN_21  => 5,
    WPI_PIN_22  => 6,
    WPI_PIN_23  => 13,
    WPI_PIN_24  => 19,
    WPI_PIN_25  => 26,
    
    WPI_PIN_26  => 12,
    WPI_PIN_27  => 16,
    WPI_PIN_28  => 20,
    WPI_PIN_29  => 21,
    WPI_PIN_30  => 0,
    WPI_PIN_31  => 1,
    
    # generic RPI const
    # all pins
    RPI_PIN_3  =>  2,
    RPI_PIN_5  =>  3,
    RPI_PIN_7  =>  4,
    RPI_PIN_8  => 14,
    RPI_PIN_10 => 15,
    RPI_PIN_11 => 17,
    RPI_PIN_12 => 18,
    RPI_PIN_13 => 27,
    RPI_PIN_15 => 22,
    RPI_PIN_16 => 23,
    RPI_PIN_18 => 24,
    RPI_PIN_19 => 10,
    RPI_PIN_21 =>  9,
    RPI_PIN_22 => 25,
    RPI_PIN_23 => 11,
    RPI_PIN_24 =>  8,
    RPI_PIN_26 =>  7,
    RPI_PIN_27 => 0,
    RPI_PIN_28 => 1,
    RPI_PIN_29 => 5,
    RPI_PIN_31 => 6,
    RPI_PIN_32 => 12,
    RPI_PIN_33 => 13,
    RPI_PIN_35 => 19,
    RPI_PIN_36 => 16,
    RPI_PIN_37 => 26,
    RPI_PIN_38 => 20,
    RPI_PIN_40 => 21,
    
};

our @_rpi_const = qw(
    RPI_HIGH RPI_LOW RPI_BOARD_REVISION
    RPI_PAD1_PIN_3 RPI_PAD1_PIN_5 RPI_PAD1_PIN_7 RPI_PAD1_PIN_8 
    RPI_PAD1_PIN_10 RPI_PAD1_PIN_11 RPI_PAD1_PIN_12 RPI_PAD1_PIN_13
    RPI_PAD1_PIN_15 RPI_PAD1_PIN_16 RPI_PAD1_PIN_18 RPI_PAD1_PIN_19
    RPI_PAD1_PIN_21 RPI_PAD1_PIN_22 RPI_PAD1_PIN_23 RPI_PAD1_PIN_24 
    RPI_PAD1_PIN_26
    RPI_PAD5_PIN_3 RPI_PAD5_PIN_4 RPI_PAD5_PIN_5 RPI_PAD5_PIN_6
    RPI_INT_NONE RPI_INT_FALL RPI_INT_RISE RPI_INT_BOTH
    RPI_INT_AFALL RPI_INT_ARISE RPI_INT_HIGH RPI_INT_LOW
    RPI_PINMODE_INPT RPI_PINMODE_OUTP RPI_PINMODE_ALT0 RPI_PINMODE_ALT1
    RPI_PINMODE_ALT2 RPI_PINMODE_ALT3 RPI_PINMODE_ALT4 RPI_PINMODE_ALT5
    RPI_PUD_NULL RPI_PUD_OFF RPI_PUD_DOWN RPI_PUD_UP
    
    RPI_PIN_3
    RPI_PIN_5
    RPI_PIN_7 
    RPI_PIN_8  
    RPI_PIN_10 
    RPI_PIN_11 
    RPI_PIN_12 
    RPI_PIN_13 
    RPI_PIN_15 
    RPI_PIN_16 
    RPI_PIN_18 
    RPI_PIN_19 
    RPI_PIN_21 
    RPI_PIN_22
    RPI_PIN_23 
    RPI_PIN_24 
    RPI_PIN_26 
    RPI_PIN_27 
    RPI_PIN_28 
    RPI_PIN_29 
    RPI_PIN_31 
    RPI_PIN_32 
    RPI_PIN_33 
    RPI_PIN_35 
    RPI_PIN_36
    RPI_PIN_37 
    RPI_PIN_38
    RPI_PIN_40 
    
    gpio_2_rpitext );

our @_i2c_const = qw( I2C0_SDA I2C0_SCL I2C1_SDA I2C1_SCL I2C_SDA I2C_SCL ID_SD ID_SC );

our @_wiring_const = qw(
        WPI_PIN_0  WPI_PIN_1  WPI_PIN_2  WPI_PIN_3  WPI_PIN_4
        WPI_PIN_5  WPI_PIN_6  WPI_PIN_7  WPI_PIN_8  WPI_PIN_9
        WPI_PIN_10 WPI_PIN_11 WPI_PIN_12 WPI_PIN_13 WPI_PIN_14
        WPI_PIN_15 WPI_PIN_16
        WPI_PIN_17 WPI_PIN_18 WPI_PIN_19 WPI_PIN_20
        WPI_PIN_21 WPI_PIN_22 WPI_PIN_23 WPI_PIN_24 WPI_PIN_25
        WPI_PIN_26 WPI_PIN_27 WPI_PIN_28 WPI_PIN_29 WPI_PIN_30
        WPI_PIN_31
        );

1;
