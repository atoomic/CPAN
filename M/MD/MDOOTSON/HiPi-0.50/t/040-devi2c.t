#!perl

# SVN Id $Id: 040-devi2c.t 3 2015-03-12 01:07:32Z Mark Dootson $

use Test::More tests => 1;

BEGIN {
    use_ok( 'HiPi::Device::I2C' );
}

1;
