package Ruby::VersionManager::Config;

use 5.010;
use feature 'say';
use warnings;
use strict;

# placeholder

1;

__END__

=head1 NAME

Ruby::VersionManager::Config

=head1 WARNING!

This is an unstable development release not ready for production!

=head1 VERSION

Version 0.003020

=head1 SYNOPSIS

Ruby::VersionManager::Config is not yet in use.

=head1 AUTHOR

Matthias Krull, C<< <m.krull at uninets.eu> >>

=head1 BUGS

Report bugs at:

=over 2

=item * Ruby::VersionManager issue tracker

L<https://github.com/uninets/p5-Ruby-VersionManager/issues>

=item * support at uninets.eu

C<< <m.krull at uninets.eu> >>

=back

=head1 SUPPORT

=over 2

=item * Technical support

C<< <m.krull at uninets.eu> >>

=back

=cut

