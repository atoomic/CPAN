Revision history for DBIx::Class

0.05007 2006-02-24 00:59:00
        - tweak to Componentised for Class::C3 0.11
        - fixes for auto-inc under MSSQL

0.05006 2006-02-17 15:32:40
        - storage fix for fork() and workaround for Apache::DBI
        - made update(\%hash) work on row as well as rs
        - another fix for count with scalar group_by
        - remove dependency on Module::Find in 40resultsetmanager.t (RT #17598)

0.05005 2006-02-13 21:24:51
        - remove build dependency on version.pm

0.05004 2006-02-13 20:59:00
        - allow specification of related columns via cols attr when primary 
          keys of the related table are not fetched
        - fix count for group_by as scalar
        - add horrific fix to make Oracle's retarded limit syntax work
        - remove Carp require

0.05003 2006-02-08 17:50:20
        - add component_class accessors and use them for *_class
        - small fixes to Serialize and ResultSetManager
        - prevent accidental table-wide update/delete on row-object 
          from PK-less table 
        - rollback on disconnect, and disconnect on DESTROY
        - fixes to deep search and search_relateduser 

0.05002 2006-02-06 12:12:03
        - Added recommends for Class::Inspector
        - Added skip_all to t/40resultsetmanager.t if no Class::Inspector available

0.05001 2006-02-05 15:28:10
        - debug output now prints NULL for undef params
        - multi-step prefetch along the same rel (e.g. for trees) now works
        - added multi-join (join => [ 'foo', 'foo' ]), aliases second to foo_2
        - hack PK::Auto::Pg for "table" names referencing a schema
        - find() with attributes works
        - added experimental Serialize and ResultSetManager components
        - added code attribute recording to DBIx::Class
        - fix to find() for complex resultsets
        - added of $storage->debugcb(sub { ... })
        - added $source->resultset_attributes accessor
        - added include_columns rs attr

0.05000 2006-02-01 16:48:30
        - assorted doc fixes
        - remove ObjectCache, not yet working in 0.05
        - let many_to_many rels have attrs
        - fix ID method in PK.pm to be saner for new internals
        - fix t/30dbicplain.t to use ::Schema instead of
          Catalyst::Model::DBIC::Plain

0.04999_06 2006-01-28 21:20:32
        - fix Storage/DBI (tried to load deprecated ::Exception component)

0.04999_05 2006-01-28 20:13:52
        - count will now work for grouped resultsets
        - added accessor => option to column_info to specify accessor name
        - added $schema->populate to load test data (similar to AR fixtures)
	- removed cdbi-t dependencies, only run tests if installed
	- Removed DBIx::Class::Exception
	- unified throw_exception stuff, using Carp::Clan
	- report query when sth generation fails.
        - multi-step prefetch!
        - inheritance fixes
        - test tweaks

0.04999_04 2006-01-24 21:48:21
        - more documentation improvements
        - add columns_info_for for vendor-specific column info (Zbigniew Lukasiak)
        - add SQL::Translator::Producer for DBIx::Class table classes (Jess Robinson)
        - add unique constraint declaration (Daniel Westermann-Clark)
        - add new update_or_create method (Daniel Westermann-Clark)
        - rename ResultSetInstance class to ResultSetProxy, ResultSourceInstance
          to ResultSourceProxy, and TableInstance to ResultSourceProxy::Table
        - minor fixes to UUIDColumns
        - add debugfh method and ENV magic for tracing SQL (Nigel Metheringham)

0.04999_03 2006-01-20 06:05:27
        - imported Jess Robinson's SQL::Translator::Parser::DBIx::Class
        - lots of internals cleanup to eliminate result_source_instance requirement
        - added register_column and register_relationship class APIs
        - made Storage::DBI use prepare_cached safely (thanks to Tim Bunce)
        - many documentation improvements (thanks guys!)
        - added ->connection, ->connect, ->register_source and ->clone schema methods
	- Use croak instead of die for user errors.

0.04999_02 2006-01-14 07:17:35
        - Schema is now self-contained; no requirement for co-operation
        - add_relationship, relationships, relationship_info, has_relationship
        - relationship handling on ResultSource
        - all table handling now in Table.pm / ResultSource.pm
        - added GROUP BY and DISTINCT support
        - hacked around SQL::Abstract::Limit some more in DBIC::SQL::Abstract
          (this may have fixed complex quoting)
        - moved inflation to inflate_result in Row.pm
        - added $rs->search_related
        - split compose_namespace out of compose_connection in Schema
        - ResultSet now handles find
        - various *_related methods are now ->search_related->*
        - added new_result to ResultSet

0.04999_01 2005-12-27 03:33:42
        - search and related methods moved to ResultSet
        - select and as added to ResultSet attrs
        - added DBIx::Class::Table and TableInstance for table-per-class
        - added DBIx::Class::ResultSetInstance which handles proxying
          search etc. as a superclass of DBIx::Class::DB
        - assorted test and code cleanup work

0.04001 2005-12-13 22:00:00
        - Fix so set_inflated_column calls set_column
        - Syntax errors in relationship classes are now reported
        - Better error detection in set_primary_key and columns methods
        - Documentation improvements
        - Better transaction support with txn_* methods
        - belongs_to now works when $cond is a string
        - PK::Auto::Pg updated, only tries primary keys instead of all cols

0.04 2005-11-26
        - Moved get_simple and set_simple into AccessorGroup
        - Made 'new' die if given invalid columns
        - Added has_column and column_info to Table.pm
        - Refactored away from direct use of _columns and _primaries
        - Switched from NEXT to Class::C3

0.03004
        - Added an || '' to the CDBICompat stringify to avoid null warnings
	- Updated name section for manual pods
	
0.03003 2005-11-03 17:00:00
        - POD fixes.
        - Changed use to require in Relationship/Base to avoid import.

0.03002 2005-10-20 22:35:00
        - Minor bugfix to new (Row.pm)
        - Schema doesn't die if it can't load a class (Schema.pm)
        - New UUID columns plugin (UUIDColumns.pm)
        - Documentation improvements.

0.03001 2005-09-23 14:00:00
        - Fixes to relationship helpers
        - IMPORTANT: prefetch/schema combination bug fix

0.03    2005-09-19 19:35:00
        - Paging support
        - Join support on search
        - Prefetch support on search

0.02    2005-08-12 18:00:00
        - Test fixes.
        - Performance improvements.
        - Oracle primary key support.
        - MS-SQL primary key support.
        - SQL::Abstract::Limit integration for database-agnostic limiting.

0.01    2005-08-08 17:10:00
        - initial release
