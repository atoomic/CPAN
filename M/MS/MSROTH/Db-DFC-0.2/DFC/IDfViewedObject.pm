# ------------------------------------------------------------------- #
# IDfViewedObject
# com.documentum.registry.IDfViewedObject
# ------------------------------------------------------------------- #
package IDfViewedObject;
@ISA = (IDfClientRegistryObject);


use JPL::AutoLoader;
use JPL::Class 'com::documentum::registry::IDfViewedObject';

#
# IDfViewedObject contains no additional methods or attributes than those
# inherited from IDfClientRegistryObject
#

1;
#__EOF__