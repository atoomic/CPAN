use strict;
use warnings;
package Passwd::Keyring::Auto::App::Command;
# ABSTRACT: base class for passwd_keyring commands
use App::Cmd::Setup -command;

# #pod =method log
# #pod
# #pod This method calls the C<log> method of the application's chrome.
# #pod
# #pod =cut
#
# sub log {
#   $_[0]->app->chrome->logger->log($_[1]);
# }

1;
