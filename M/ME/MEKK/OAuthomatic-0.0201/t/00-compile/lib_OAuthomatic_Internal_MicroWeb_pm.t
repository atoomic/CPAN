use strict;
use warnings;

# This test was generated for <lib/OAuthomatic/Internal/MicroWeb.pm>
# using by Dist::Zilla::Plugin::Test::Compile::PerFile ( Test::Compile::PerFile ) version 0.003000
# with template 01-basic.t.tpl

use Test::More 0.89 tests => 1;

require_ok("lib/OAuthomatic/Internal/MicroWeb.pm");

