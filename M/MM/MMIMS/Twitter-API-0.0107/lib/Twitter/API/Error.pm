package Twitter::API::Error;
# ABSTRACT: Twitter API exception
$Twitter::API::Error::VERSION = '0.0107';
use Moo;
use Try::Tiny;
use Devel::StackTrace;
use namespace::clean;

use overload '""' => sub { shift->error };

with 'Throwable';

#pod =method http_request
#pod
#pod Returns the L<HTTP::Request> object used to make the Twitter API call.
#pod
#pod =method http_response
#pod
#pod Returns the L<HTTP::Response> object for the API call.
#pod
#pod =method twitter_error
#pod
#pod Returns the inflated JSON error response from Twitter (if any).
#pod
#pod =cut

has context => (
    is       => 'ro',
    required => 1,
    handles  => {
        http_request  => 'http_request',
        http_response => 'http_response',
        twitter_error => 'result',
    },
);

#pod =method stack_trace
#pod
#pod Returns a L<Devel::StackTrace> object encapsulating the call stack so you can discover, where, in your application the error occurred.
#pod
#pod =method stack_frame
#pod
#pod Delegates to C<<stack_trace->frame>>. See L<Devel::StackTrace> for details.
#pod
#pod =method next_stack_fram
#pod
#pod Delegates to C<<stack_trace->next_frame>>. See L<Devel::StackTrace> for details.
#pod
#pod =cut

has stack_trace => (
    is       => 'ro',
    init_arg => undef,
    builder  => '_build_stack_trace',
    handles => {
        stack_frame      => 'frame',
        next_stack_frame => 'next_frame',
    },
);

sub _build_stack_trace {
    my $seen;
    my $this_sub = (caller 0)[3];
    Devel::StackTrace->new(frame_filter => sub {
        my $caller = shift->{caller};
        my $skip = $caller->[0] =~ /^(?:Twitter::API|Throwable|Role::Tiny)\b/
            || $caller->[3] eq $this_sub;
        ($seen ||= $skip) && !$skip || 0;
    });
}

#pod =method error
#pod
#pod Returns a reasonable string representation of the exception. If Twitter
#pod returned error information in the form of a JSON body, it is mined for error
#pod text. Otherwise, the HTTP response status line is used. The stack frame is
#pod mined for the point in your application where the request initiated and
#pod appended to the message.
#pod
#pod When used in a string context, C<error> is called to stringify exception.
#pod
#pod =cut

has error => (
    is => 'lazy',
);

sub _build_error {
    my $self = shift;

    my $error = $self->twitter_error_text || $self->http_response->status_line;
    my ( $location ) = $self->stack_frame(0)->as_string =~ /( at .*)/;
    return $error . ($location || '');
}

sub twitter_error_text {
    my $self = shift;
    # Twitter does not return a consistent error structure, so we have to
    # try each known (or guessed) variant to find a suitable message...

    return '' unless $self->twitter_error;
    my $e = $self->twitter_error;

    return ref $e eq 'HASH' && (
        # the newest variant: array of errors
        exists $e->{errors}
            && ref $e->{errors} eq 'ARRAY'
            && exists $e->{errors}[0]
            && ref $e->{errors}[0] eq 'HASH'
            && exists $e->{errors}[0]{message}
            && $e->{errors}[0]{message}

        # it's single error variant
        || exists $e->{error}
            && ref $e->{error} eq 'HASH'
            && exists $e->{error}{message}
            && $e->{error}{message}

        # the original error structure (still applies to some endpoints)
        || exists $e->{error} && $e->{error}

        # or maybe it's not that deep (documentation would be helpful, here,
        # Twitter!)
        || exists $e->{message} && $e->{message}
    ) || ''; # punt
}

#pod =method twitter_error_code
#pod
#pod Returns the numeric error code returned by Twitter, or 0 if there is none. See
#pod L<https://dev.twitter.com/overview/api/response-codes> for details.
#pod
#pod =cut

sub twitter_error_code {
    my $self = shift;

    return $self->twitter_error
        && exists $self->twitter_error->{errors}
        && exists $self->twitter_error->{errors}[0]
        && exists $self->twitter_error->{errors}[0]{code}
        && $self->twitter_error->{errors}[0]{code}
        || 0;
}

#pod =method is_token_error
#pod
#pod Returns true if the error represents a problem with the access token or its
#pod Twitter account, rather than with the resource being accessed.
#pod
#pod Some Twitter error codes indicate a problem with authentication or the
#pod token/secret used to make the API call. For example, the account has been
#pod suspended or access to the application revoked by the user. Other error codes
#pod indicate a problem with the resource requested. For example, the target account
#pod no longer exists.
#pod
#pod =cut

# Some twitter errors result from issues with the target of a call. Others,
# from an issue with the tokens used to make the call. In the latter case, we
# should just retry with fresh tokens. Return true for the latter type.
sub is_token_error {
    my $self = shift;

    # 32:  could not authenticate you
    # 64:  this account is suspended
    # 88:  rate limit exceeded for this token
    # 89:  invalid or expired tokens
    #
    # Twitter documents error 226 as spam/bot-like behavior,
    # but they actually send 326! So, we'll look for both.
    # 226: this account locked for bot-like behavior
    # 326: To protect our users from spam...
    my $code = $self->twitter_error_code;
    for ( 32, 64, 88, 89, 226, 326 ) {
        return 1 if $_ == $code;
    }
    return 0;
}

#pod =method http_response_code
#pod
#pod Delegates to C<<http_response->code>>. Returns the HTTP status code of the
#pod response.
#pod
#pod =cut

sub http_response_code { shift->http_response->code }

#pod =method is_pemanent_error
#pod
#pod Returns true for HTTP status codes representing an error and with values less
#pod than 500. Typically, retrying an API call with one of these statuses right away
#pod will simply result in the same error, again.
#pod
#pod =cut

sub is_permanent_error { shift->http_response_code < 500 }

#pod =method is_temporary_error
#pod
#pod Returns true or HTTP status codes of 500 or greater. Often, these errors
#pod indicate a transient condition. Retrying the API call right away may result in
#pod success. See the L<RetryOnError|Twitter::API::Trait::RetryOnError> for
#pod automatically retrying temporary errors.
#pod
#pod =cut

sub is_temporary_error { !shift->is_permanent_error }

1;

__END__

=pod

=encoding UTF-8

=head1 NAME

Twitter::API::Error - Twitter API exception

=head1 VERSION

version 0.0107

=head1 SYNOPSIS

    use Try::Tiny;
    use Twitter::API;
    use Twitter::API::Util 'is_twitter_api_error';

    my $client = Twitter::API->new(%options);

    try {
        my $r = $client->get('account/verify_credentials');
    }
    catch {
        die $_ unless _is_twitter_api_error;

        warn "Twitter says: ", $_->twitter_error_text;
    };

=head1 DESCRIPTION

Twitter::API dies, throwing a Twitter::API::Error exception when it receives an
error. The error object contains information about the error so your code can
decide how to respond to various error conditions.

=head1 METHODS

=head2 http_request

Returns the L<HTTP::Request> object used to make the Twitter API call.

=head2 http_response

Returns the L<HTTP::Response> object for the API call.

=head2 twitter_error

Returns the inflated JSON error response from Twitter (if any).

=head2 stack_trace

Returns a L<Devel::StackTrace> object encapsulating the call stack so you can discover, where, in your application the error occurred.

=head2 stack_frame

Delegates to C<<stack_trace->frame>>. See L<Devel::StackTrace> for details.

=head2 next_stack_fram

Delegates to C<<stack_trace->next_frame>>. See L<Devel::StackTrace> for details.

=head2 error

Returns a reasonable string representation of the exception. If Twitter
returned error information in the form of a JSON body, it is mined for error
text. Otherwise, the HTTP response status line is used. The stack frame is
mined for the point in your application where the request initiated and
appended to the message.

When used in a string context, C<error> is called to stringify exception.

=head2 twitter_error_code

Returns the numeric error code returned by Twitter, or 0 if there is none. See
L<https://dev.twitter.com/overview/api/response-codes> for details.

=head2 is_token_error

Returns true if the error represents a problem with the access token or its
Twitter account, rather than with the resource being accessed.

Some Twitter error codes indicate a problem with authentication or the
token/secret used to make the API call. For example, the account has been
suspended or access to the application revoked by the user. Other error codes
indicate a problem with the resource requested. For example, the target account
no longer exists.

=head2 http_response_code

Delegates to C<<http_response->code>>. Returns the HTTP status code of the
response.

=head2 is_pemanent_error

Returns true for HTTP status codes representing an error and with values less
than 500. Typically, retrying an API call with one of these statuses right away
will simply result in the same error, again.

=head2 is_temporary_error

Returns true or HTTP status codes of 500 or greater. Often, these errors
indicate a transient condition. Retrying the API call right away may result in
success. See the L<RetryOnError|Twitter::API::Trait::RetryOnError> for
automatically retrying temporary errors.

=head1 AUTHOR

Marc Mims <marc@questright.com>

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2015-2016 by Marc Mims.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
