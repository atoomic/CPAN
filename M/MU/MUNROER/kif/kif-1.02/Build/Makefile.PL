use ExtUtils::MakeMaker;

require 5.8.0 ;

WriteMakefile
   (NAME => "Build::Build",
    VERSION_FROM => "../Build.pm"
   );
