package HTTP::API::Client::Splunk;
$HTTP::API::Client::Splunk::VERSION = '0.01';
use strict;
use warnings;

=head1 NAME

HTTP::API::Client::Splunk - Splunk API Client

=head1 USAGE

 use HTTP::API::Client::Splunk;

 my $splunk = HTTP::API::Client::Splunk->new;

 $splunk->login( $login_url, $username, $password );

 $splunk->get( $data_url );

 my $data = $splunk->json_response;

=cut

use Mouse;

extends "HTTP::API::Client";

no Mouse;

sub login {
    my $self = shift;
    my $url  = _url_include_output_json(shift);
    my $user = shift;
    my $pass = shift;
    local $self->{content_type} = "application/x-www-form-urlencoded";
    $self->SUPER::post( $url, { username => $user, password => $pass } );
    my $key = $self->json_response->{sessionKey}
      or return 0;
    $self->auth_token("Splunk $key");
    return 1;
}

sub send {
    my $self   = shift;
    my $method = shift || "GET";
    my $path   = _url_include_output_json(shift);
    return $self->SUPER::send( $method, $path, @_ );
}

sub _url_include_output_json {
    my $url = shift
      or return;
    if ( $url !~ /output_mode=json/ ) {
        if ( $url =~ /\?/ ) {
            $url .= "&output_mode=json";
        }
        else {
            $url .= "?output_mode=json";
        }
    }
    return $url;
}

1;
