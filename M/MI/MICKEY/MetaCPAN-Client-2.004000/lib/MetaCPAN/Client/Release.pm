use strict;
use warnings;
package MetaCPAN::Client::Release;
# ABSTRACT: A Release data object
$MetaCPAN::Client::Release::VERSION = '2.004000';
use Moo;

with 'MetaCPAN::Client::Role::Entity';

my %known_fields = (
    scalar => [qw<
        abstract
        archive
        author
        authorized
        date
        distribution
        download_url
        first
        id
        maturity
        name
        status
        version
        version_numified
    >],

    arrayref => [qw<
        dependency
        license
        provides
    >],

    hashref => [qw<
        metadata
        resources
        stat
        tests
    >],
);

my @known_fields =
    map { @{ $known_fields{$_} } } qw< scalar arrayref hashref >;

foreach my $field (@known_fields) {
    has $field => (
        is      => 'ro',
        lazy    => 1,
        default => sub {
            my $self = shift;
            return $self->data->{$field};
        },
    );
}

sub _known_fields { return \%known_fields }

1;

__END__

=pod

=encoding UTF-8

=head1 NAME

MetaCPAN::Client::Release - A Release data object

=head1 VERSION

version 2.004000

=head1 SYNOPSIS

my $release = $mcpan->release('Moose');

=head1 DESCRIPTION

A MetaCPAN release entity object.

=head1 ATTRIBUTES

=head2 status

=head2 date

=head2 author

=head2 maturity

=head2 id

=head2 authorized

=head2 download_url

=head2 first

=head2 archive

=head2 version

=head2 name

=head2 version_numified

=head2 distribution

=head2 abstract

=head2 dependency

Array-Ref.

=head2 license

Array-Ref.

=head2 provides

Array-Ref.

=head2 metadata

Hash-Ref.

=head2 resources

Hash-Ref.

=head2 stat

Hash-Ref.

=head2 tests

Hash-Ref.

=head1 AUTHORS

=over 4

=item *

Sawyer X <xsawyerx@cpan.org>

=item *

Mickey Nasriachi <mickey@cpan.org>

=back

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2016 by Sawyer X.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
