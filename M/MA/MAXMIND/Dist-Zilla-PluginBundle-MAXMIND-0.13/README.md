# NAME

Dist::Zilla::PluginBundle::MAXMIND - MaxMind's plugin bundle

# VERSION

version 0.13

# AUTHOR

Dave Rolsky <drolsky@maxmind.com>

# CONTRIBUTORS

- Mark Fowler <mfowler@maxmind.com>
- Mateu X Hunter <mhunter@maxmind.com>

# COPYRIGHT AND LICENSE

This software is copyright (c) 2014 - 2016 by MaxMind, Inc..

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
