use Test::More tests => 7;

use ok 'HTTP::Router';
use ok 'HTTP::Router::Route';
use ok 'HTTP::Router::RouteSet';
use ok 'HTTP::Router::Match';
use ok 'HTTP::Router::Mapper';
use ok 'HTTP::Router::Resources';
use ok 'HTTP::Router::Debug';
