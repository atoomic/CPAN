# Copyrights 2001-2016 by [Mark Overmeer].
#  For other contributors see ChangeLog.
# See the manual pages for details on the licensing terms.
# Pod stripped from pm file by OODoc 2.02.

package MailBox::Test::55locking::Definition;
use vars '$VERSION';
$VERSION = '2.120';


sub name     {"Mail::Box::Locker; folder locking"}
sub critical {0}
sub skip     { undef }

1;
