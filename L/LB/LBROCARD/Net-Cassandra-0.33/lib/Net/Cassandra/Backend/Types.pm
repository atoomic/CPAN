#
# Autogenerated by Thrift
#
# DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
#
require 5.6.0;
use strict;
use warnings;
use Net::Cassandra::Backend::Thrift;

package Net::Cassandra::Backend::column_t;
use Class::Accessor;
use base('Class::Accessor');
Net::Cassandra::Backend::column_t->mk_accessors( qw( columnName value timestamp ) );
sub new {
my $classname = shift;
my $self      = {};
my $vals      = shift || {};
$self->{columnName} = undef;
$self->{value} = undef;
$self->{timestamp} = undef;
  if (UNIVERSAL::isa($vals,'HASH')) {
    if (defined $vals->{columnName}) {
      $self->{columnName} = $vals->{columnName};
    }
    if (defined $vals->{value}) {
      $self->{value} = $vals->{value};
    }
    if (defined $vals->{timestamp}) {
      $self->{timestamp} = $vals->{timestamp};
    }
  }
return bless($self,$classname);
}

sub getName {
  return 'Net::Cassandra::Backend::column_t';
}

sub read {
  my $self  = shift;
  my $input = shift;
  my $xfer  = 0;
  my $fname;
  my $ftype = 0;
  my $fid   = 0;
  $xfer += $input->readStructBegin(\$fname);
  while (1) 
  {
    $xfer += $input->readFieldBegin(\$fname, \$ftype, \$fid);
    if ($ftype == Net::Cassandra::Backend::TType::STOP) {
      last;
    }
    SWITCH: for($fid)
    {
      /^1$/ && do{      if ($ftype == Net::Cassandra::Backend::TType::STRING) {
        $xfer += $input->readString(\$self->{columnName});
      } else {
        $xfer += $input->skip($ftype);
      }
      last; };
      /^2$/ && do{      if ($ftype == Net::Cassandra::Backend::TType::STRING) {
        $xfer += $input->readString(\$self->{value});
      } else {
        $xfer += $input->skip($ftype);
      }
      last; };
      /^3$/ && do{      if ($ftype == Net::Cassandra::Backend::TType::I64) {
        $xfer += $input->readI64(\$self->{timestamp});
      } else {
        $xfer += $input->skip($ftype);
      }
      last; };
        $xfer += $input->skip($ftype);
    }
    $xfer += $input->readFieldEnd();
  }
  $xfer += $input->readStructEnd();
  return $xfer;
}

sub write {
  my $self   = shift;
  my $output = shift;
  my $xfer   = 0;
  $xfer += $output->writeStructBegin('Net::Cassandra::Backend::column_t');
  if (defined $self->{columnName}) {
    $xfer += $output->writeFieldBegin('columnName', Net::Cassandra::Backend::TType::STRING, 1);
    $xfer += $output->writeString($self->{columnName});
    $xfer += $output->writeFieldEnd();
  }
  if (defined $self->{value}) {
    $xfer += $output->writeFieldBegin('value', Net::Cassandra::Backend::TType::STRING, 2);
    $xfer += $output->writeString($self->{value});
    $xfer += $output->writeFieldEnd();
  }
  if (defined $self->{timestamp}) {
    $xfer += $output->writeFieldBegin('timestamp', Net::Cassandra::Backend::TType::I64, 3);
    $xfer += $output->writeI64($self->{timestamp});
    $xfer += $output->writeFieldEnd();
  }
  $xfer += $output->writeFieldStop();
  $xfer += $output->writeStructEnd();
  return $xfer;
}

package Net::Cassandra::Backend::batch_mutation_t;
use Class::Accessor;
use base('Class::Accessor');
Net::Cassandra::Backend::batch_mutation_t->mk_accessors( qw( table key cfmap ) );
sub new {
my $classname = shift;
my $self      = {};
my $vals      = shift || {};
$self->{table} = undef;
$self->{key} = undef;
$self->{cfmap} = undef;
  if (UNIVERSAL::isa($vals,'HASH')) {
    if (defined $vals->{table}) {
      $self->{table} = $vals->{table};
    }
    if (defined $vals->{key}) {
      $self->{key} = $vals->{key};
    }
    if (defined $vals->{cfmap}) {
      $self->{cfmap} = $vals->{cfmap};
    }
  }
return bless($self,$classname);
}

sub getName {
  return 'Net::Cassandra::Backend::batch_mutation_t';
}

sub read {
  my $self  = shift;
  my $input = shift;
  my $xfer  = 0;
  my $fname;
  my $ftype = 0;
  my $fid   = 0;
  $xfer += $input->readStructBegin(\$fname);
  while (1) 
  {
    $xfer += $input->readFieldBegin(\$fname, \$ftype, \$fid);
    if ($ftype == Net::Cassandra::Backend::TType::STOP) {
      last;
    }
    SWITCH: for($fid)
    {
      /^1$/ && do{      if ($ftype == Net::Cassandra::Backend::TType::STRING) {
        $xfer += $input->readString(\$self->{table});
      } else {
        $xfer += $input->skip($ftype);
      }
      last; };
      /^2$/ && do{      if ($ftype == Net::Cassandra::Backend::TType::STRING) {
        $xfer += $input->readString(\$self->{key});
      } else {
        $xfer += $input->skip($ftype);
      }
      last; };
      /^3$/ && do{      if ($ftype == Net::Cassandra::Backend::TType::MAP) {
        {
          my $_size0 = 0;
          $self->{cfmap} = {};
          my $_ktype1 = 0;
          my $_vtype2 = 0;
          $xfer += $input->readMapBegin(\$_ktype1, \$_vtype2, \$_size0);
          for (my $_i4 = 0; $_i4 < $_size0; ++$_i4)
          {
            my $key5 = '';
            my $val6 = [];
            $xfer += $input->readString(\$key5);
            {
              my $_size7 = 0;
              $val6 = [];
              my $_etype10 = 0;
              $xfer += $input->readListBegin(\$_etype10, \$_size7);
              for (my $_i11 = 0; $_i11 < $_size7; ++$_i11)
              {
                my $elem12 = undef;
                $elem12 = new Net::Cassandra::Backend::column_t();
                $xfer += $elem12->read($input);
                push(@{$val6},$elem12);
              }
              $xfer += $input->readListEnd();
            }
            $self->{cfmap}->{$key5} = $val6;
          }
          $xfer += $input->readMapEnd();
        }
      } else {
        $xfer += $input->skip($ftype);
      }
      last; };
        $xfer += $input->skip($ftype);
    }
    $xfer += $input->readFieldEnd();
  }
  $xfer += $input->readStructEnd();
  return $xfer;
}

sub write {
  my $self   = shift;
  my $output = shift;
  my $xfer   = 0;
  $xfer += $output->writeStructBegin('Net::Cassandra::Backend::batch_mutation_t');
  if (defined $self->{table}) {
    $xfer += $output->writeFieldBegin('table', Net::Cassandra::Backend::TType::STRING, 1);
    $xfer += $output->writeString($self->{table});
    $xfer += $output->writeFieldEnd();
  }
  if (defined $self->{key}) {
    $xfer += $output->writeFieldBegin('key', Net::Cassandra::Backend::TType::STRING, 2);
    $xfer += $output->writeString($self->{key});
    $xfer += $output->writeFieldEnd();
  }
  if (defined $self->{cfmap}) {
    $xfer += $output->writeFieldBegin('cfmap', Net::Cassandra::Backend::TType::MAP, 3);
    {
      $output->writeMapBegin(Net::Cassandra::Backend::TType::STRING, Net::Cassandra::Backend::TType::LIST, scalar(keys %{$self->{cfmap}}));
      {
        while( my ($kiter13,$viter14) = each %{$self->{cfmap}}) 
        {
          $xfer += $output->writeString($kiter13);
          {
            $output->writeListBegin(Net::Cassandra::Backend::TType::STRUCT, scalar(@{${viter14}}));
            {
              foreach my $iter15 (@{${viter14}}) 
              {
                $xfer += ${iter15}->write($output);
              }
            }
            $output->writeListEnd();
          }
        }
      }
      $output->writeMapEnd();
    }
    $xfer += $output->writeFieldEnd();
  }
  $xfer += $output->writeFieldStop();
  $xfer += $output->writeStructEnd();
  return $xfer;
}

package Net::Cassandra::Backend::superColumn_t;
use Class::Accessor;
use base('Class::Accessor');
Net::Cassandra::Backend::superColumn_t->mk_accessors( qw( name columns ) );
sub new {
my $classname = shift;
my $self      = {};
my $vals      = shift || {};
$self->{name} = undef;
$self->{columns} = undef;
  if (UNIVERSAL::isa($vals,'HASH')) {
    if (defined $vals->{name}) {
      $self->{name} = $vals->{name};
    }
    if (defined $vals->{columns}) {
      $self->{columns} = $vals->{columns};
    }
  }
return bless($self,$classname);
}

sub getName {
  return 'Net::Cassandra::Backend::superColumn_t';
}

sub read {
  my $self  = shift;
  my $input = shift;
  my $xfer  = 0;
  my $fname;
  my $ftype = 0;
  my $fid   = 0;
  $xfer += $input->readStructBegin(\$fname);
  while (1) 
  {
    $xfer += $input->readFieldBegin(\$fname, \$ftype, \$fid);
    if ($ftype == Net::Cassandra::Backend::TType::STOP) {
      last;
    }
    SWITCH: for($fid)
    {
      /^1$/ && do{      if ($ftype == Net::Cassandra::Backend::TType::STRING) {
        $xfer += $input->readString(\$self->{name});
      } else {
        $xfer += $input->skip($ftype);
      }
      last; };
      /^2$/ && do{      if ($ftype == Net::Cassandra::Backend::TType::LIST) {
        {
          my $_size16 = 0;
          $self->{columns} = [];
          my $_etype19 = 0;
          $xfer += $input->readListBegin(\$_etype19, \$_size16);
          for (my $_i20 = 0; $_i20 < $_size16; ++$_i20)
          {
            my $elem21 = undef;
            $elem21 = new Net::Cassandra::Backend::column_t();
            $xfer += $elem21->read($input);
            push(@{$self->{columns}},$elem21);
          }
          $xfer += $input->readListEnd();
        }
      } else {
        $xfer += $input->skip($ftype);
      }
      last; };
        $xfer += $input->skip($ftype);
    }
    $xfer += $input->readFieldEnd();
  }
  $xfer += $input->readStructEnd();
  return $xfer;
}

sub write {
  my $self   = shift;
  my $output = shift;
  my $xfer   = 0;
  $xfer += $output->writeStructBegin('Net::Cassandra::Backend::superColumn_t');
  if (defined $self->{name}) {
    $xfer += $output->writeFieldBegin('name', Net::Cassandra::Backend::TType::STRING, 1);
    $xfer += $output->writeString($self->{name});
    $xfer += $output->writeFieldEnd();
  }
  if (defined $self->{columns}) {
    $xfer += $output->writeFieldBegin('columns', Net::Cassandra::Backend::TType::LIST, 2);
    {
      $output->writeListBegin(Net::Cassandra::Backend::TType::STRUCT, scalar(@{$self->{columns}}));
      {
        foreach my $iter22 (@{$self->{columns}}) 
        {
          $xfer += ${iter22}->write($output);
        }
      }
      $output->writeListEnd();
    }
    $xfer += $output->writeFieldEnd();
  }
  $xfer += $output->writeFieldStop();
  $xfer += $output->writeStructEnd();
  return $xfer;
}

package Net::Cassandra::Backend::batch_mutation_super_t;
use Class::Accessor;
use base('Class::Accessor');
Net::Cassandra::Backend::batch_mutation_super_t->mk_accessors( qw( table key cfmap ) );
sub new {
my $classname = shift;
my $self      = {};
my $vals      = shift || {};
$self->{table} = undef;
$self->{key} = undef;
$self->{cfmap} = undef;
  if (UNIVERSAL::isa($vals,'HASH')) {
    if (defined $vals->{table}) {
      $self->{table} = $vals->{table};
    }
    if (defined $vals->{key}) {
      $self->{key} = $vals->{key};
    }
    if (defined $vals->{cfmap}) {
      $self->{cfmap} = $vals->{cfmap};
    }
  }
return bless($self,$classname);
}

sub getName {
  return 'Net::Cassandra::Backend::batch_mutation_super_t';
}

sub read {
  my $self  = shift;
  my $input = shift;
  my $xfer  = 0;
  my $fname;
  my $ftype = 0;
  my $fid   = 0;
  $xfer += $input->readStructBegin(\$fname);
  while (1) 
  {
    $xfer += $input->readFieldBegin(\$fname, \$ftype, \$fid);
    if ($ftype == Net::Cassandra::Backend::TType::STOP) {
      last;
    }
    SWITCH: for($fid)
    {
      /^1$/ && do{      if ($ftype == Net::Cassandra::Backend::TType::STRING) {
        $xfer += $input->readString(\$self->{table});
      } else {
        $xfer += $input->skip($ftype);
      }
      last; };
      /^2$/ && do{      if ($ftype == Net::Cassandra::Backend::TType::STRING) {
        $xfer += $input->readString(\$self->{key});
      } else {
        $xfer += $input->skip($ftype);
      }
      last; };
      /^3$/ && do{      if ($ftype == Net::Cassandra::Backend::TType::MAP) {
        {
          my $_size23 = 0;
          $self->{cfmap} = {};
          my $_ktype24 = 0;
          my $_vtype25 = 0;
          $xfer += $input->readMapBegin(\$_ktype24, \$_vtype25, \$_size23);
          for (my $_i27 = 0; $_i27 < $_size23; ++$_i27)
          {
            my $key28 = '';
            my $val29 = [];
            $xfer += $input->readString(\$key28);
            {
              my $_size30 = 0;
              $val29 = [];
              my $_etype33 = 0;
              $xfer += $input->readListBegin(\$_etype33, \$_size30);
              for (my $_i34 = 0; $_i34 < $_size30; ++$_i34)
              {
                my $elem35 = undef;
                $elem35 = new Net::Cassandra::Backend::superColumn_t();
                $xfer += $elem35->read($input);
                push(@{$val29},$elem35);
              }
              $xfer += $input->readListEnd();
            }
            $self->{cfmap}->{$key28} = $val29;
          }
          $xfer += $input->readMapEnd();
        }
      } else {
        $xfer += $input->skip($ftype);
      }
      last; };
        $xfer += $input->skip($ftype);
    }
    $xfer += $input->readFieldEnd();
  }
  $xfer += $input->readStructEnd();
  return $xfer;
}

sub write {
  my $self   = shift;
  my $output = shift;
  my $xfer   = 0;
  $xfer += $output->writeStructBegin('Net::Cassandra::Backend::batch_mutation_super_t');
  if (defined $self->{table}) {
    $xfer += $output->writeFieldBegin('table', Net::Cassandra::Backend::TType::STRING, 1);
    $xfer += $output->writeString($self->{table});
    $xfer += $output->writeFieldEnd();
  }
  if (defined $self->{key}) {
    $xfer += $output->writeFieldBegin('key', Net::Cassandra::Backend::TType::STRING, 2);
    $xfer += $output->writeString($self->{key});
    $xfer += $output->writeFieldEnd();
  }
  if (defined $self->{cfmap}) {
    $xfer += $output->writeFieldBegin('cfmap', Net::Cassandra::Backend::TType::MAP, 3);
    {
      $output->writeMapBegin(Net::Cassandra::Backend::TType::STRING, Net::Cassandra::Backend::TType::LIST, scalar(keys %{$self->{cfmap}}));
      {
        while( my ($kiter36,$viter37) = each %{$self->{cfmap}}) 
        {
          $xfer += $output->writeString($kiter36);
          {
            $output->writeListBegin(Net::Cassandra::Backend::TType::STRUCT, scalar(@{${viter37}}));
            {
              foreach my $iter38 (@{${viter37}}) 
              {
                $xfer += ${iter38}->write($output);
              }
            }
            $output->writeListEnd();
          }
        }
      }
      $output->writeMapEnd();
    }
    $xfer += $output->writeFieldEnd();
  }
  $xfer += $output->writeFieldStop();
  $xfer += $output->writeStructEnd();
  return $xfer;
}

package Net::Cassandra::Backend::CqlResult_t;
use Class::Accessor;
use base('Class::Accessor');
Net::Cassandra::Backend::CqlResult_t->mk_accessors( qw( errorCode errorTxt resultSet ) );
sub new {
my $classname = shift;
my $self      = {};
my $vals      = shift || {};
$self->{errorCode} = undef;
$self->{errorTxt} = undef;
$self->{resultSet} = undef;
  if (UNIVERSAL::isa($vals,'HASH')) {
    if (defined $vals->{errorCode}) {
      $self->{errorCode} = $vals->{errorCode};
    }
    if (defined $vals->{errorTxt}) {
      $self->{errorTxt} = $vals->{errorTxt};
    }
    if (defined $vals->{resultSet}) {
      $self->{resultSet} = $vals->{resultSet};
    }
  }
return bless($self,$classname);
}

sub getName {
  return 'Net::Cassandra::Backend::CqlResult_t';
}

sub read {
  my $self  = shift;
  my $input = shift;
  my $xfer  = 0;
  my $fname;
  my $ftype = 0;
  my $fid   = 0;
  $xfer += $input->readStructBegin(\$fname);
  while (1) 
  {
    $xfer += $input->readFieldBegin(\$fname, \$ftype, \$fid);
    if ($ftype == Net::Cassandra::Backend::TType::STOP) {
      last;
    }
    SWITCH: for($fid)
    {
      /^1$/ && do{      if ($ftype == Net::Cassandra::Backend::TType::I32) {
        $xfer += $input->readI32(\$self->{errorCode});
      } else {
        $xfer += $input->skip($ftype);
      }
      last; };
      /^2$/ && do{      if ($ftype == Net::Cassandra::Backend::TType::STRING) {
        $xfer += $input->readString(\$self->{errorTxt});
      } else {
        $xfer += $input->skip($ftype);
      }
      last; };
      /^3$/ && do{      if ($ftype == Net::Cassandra::Backend::TType::LIST) {
        {
          my $_size39 = 0;
          $self->{resultSet} = [];
          my $_etype42 = 0;
          $xfer += $input->readListBegin(\$_etype42, \$_size39);
          for (my $_i43 = 0; $_i43 < $_size39; ++$_i43)
          {
            my $elem44 = undef;
            {
              my $_size45 = 0;
              $elem44 = {};
              my $_ktype46 = 0;
              my $_vtype47 = 0;
              $xfer += $input->readMapBegin(\$_ktype46, \$_vtype47, \$_size45);
              for (my $_i49 = 0; $_i49 < $_size45; ++$_i49)
              {
                my $key50 = '';
                my $val51 = '';
                $xfer += $input->readString(\$key50);
                $xfer += $input->readString(\$val51);
                $elem44->{$key50} = $val51;
              }
              $xfer += $input->readMapEnd();
            }
            push(@{$self->{resultSet}},$elem44);
          }
          $xfer += $input->readListEnd();
        }
      } else {
        $xfer += $input->skip($ftype);
      }
      last; };
        $xfer += $input->skip($ftype);
    }
    $xfer += $input->readFieldEnd();
  }
  $xfer += $input->readStructEnd();
  return $xfer;
}

sub write {
  my $self   = shift;
  my $output = shift;
  my $xfer   = 0;
  $xfer += $output->writeStructBegin('Net::Cassandra::Backend::CqlResult_t');
  if (defined $self->{errorCode}) {
    $xfer += $output->writeFieldBegin('errorCode', Net::Cassandra::Backend::TType::I32, 1);
    $xfer += $output->writeI32($self->{errorCode});
    $xfer += $output->writeFieldEnd();
  }
  if (defined $self->{errorTxt}) {
    $xfer += $output->writeFieldBegin('errorTxt', Net::Cassandra::Backend::TType::STRING, 2);
    $xfer += $output->writeString($self->{errorTxt});
    $xfer += $output->writeFieldEnd();
  }
  if (defined $self->{resultSet}) {
    $xfer += $output->writeFieldBegin('resultSet', Net::Cassandra::Backend::TType::LIST, 3);
    {
      $output->writeListBegin(Net::Cassandra::Backend::TType::MAP, scalar(@{$self->{resultSet}}));
      {
        foreach my $iter52 (@{$self->{resultSet}}) 
        {
          {
            $output->writeMapBegin(Net::Cassandra::Backend::TType::STRING, Net::Cassandra::Backend::TType::STRING, scalar(keys %{${iter52}}));
            {
              while( my ($kiter53,$viter54) = each %{${iter52}}) 
              {
                $xfer += $output->writeString($kiter53);
                $xfer += $output->writeString($viter54);
              }
            }
            $output->writeMapEnd();
          }
        }
      }
      $output->writeListEnd();
    }
    $xfer += $output->writeFieldEnd();
  }
  $xfer += $output->writeFieldStop();
  $xfer += $output->writeStructEnd();
  return $xfer;
}

package Net::Cassandra::Backend::NotFoundException;
use base('Net::Cassandra::Backend::Thrift::TException');
use Class::Accessor;
use base('Class::Accessor');
sub new {
my $classname = shift;
my $self      = {};
my $vals      = shift || {};
return bless($self,$classname);
}

sub getName {
  return 'Net::Cassandra::Backend::NotFoundException';
}

sub read {
  my $self  = shift;
  my $input = shift;
  my $xfer  = 0;
  my $fname;
  my $ftype = 0;
  my $fid   = 0;
  $xfer += $input->readStructBegin(\$fname);
  while (1) 
  {
    $xfer += $input->readFieldBegin(\$fname, \$ftype, \$fid);
    if ($ftype == Net::Cassandra::Backend::TType::STOP) {
      last;
    }
    SWITCH: for($fid)
    {
        $xfer += $input->skip($ftype);
    }
    $xfer += $input->readFieldEnd();
  }
  $xfer += $input->readStructEnd();
  return $xfer;
}

sub write {
  my $self   = shift;
  my $output = shift;
  my $xfer   = 0;
  $xfer += $output->writeStructBegin('Net::Cassandra::Backend::NotFoundException');
  $xfer += $output->writeFieldStop();
  $xfer += $output->writeStructEnd();
  return $xfer;
}

package Net::Cassandra::Backend::InvalidRequestException;
use base('Net::Cassandra::Backend::Thrift::TException');
use Class::Accessor;
use base('Class::Accessor');
Net::Cassandra::Backend::InvalidRequestException->mk_accessors( qw( why ) );
sub new {
my $classname = shift;
my $self      = {};
my $vals      = shift || {};
$self->{why} = undef;
  if (UNIVERSAL::isa($vals,'HASH')) {
    if (defined $vals->{why}) {
      $self->{why} = $vals->{why};
    }
  }
return bless($self,$classname);
}

sub getName {
  return 'Net::Cassandra::Backend::InvalidRequestException';
}

sub read {
  my $self  = shift;
  my $input = shift;
  my $xfer  = 0;
  my $fname;
  my $ftype = 0;
  my $fid   = 0;
  $xfer += $input->readStructBegin(\$fname);
  while (1) 
  {
    $xfer += $input->readFieldBegin(\$fname, \$ftype, \$fid);
    if ($ftype == Net::Cassandra::Backend::TType::STOP) {
      last;
    }
    SWITCH: for($fid)
    {
      /^1$/ && do{      if ($ftype == Net::Cassandra::Backend::TType::STRING) {
        $xfer += $input->readString(\$self->{why});
      } else {
        $xfer += $input->skip($ftype);
      }
      last; };
        $xfer += $input->skip($ftype);
    }
    $xfer += $input->readFieldEnd();
  }
  $xfer += $input->readStructEnd();
  return $xfer;
}

sub write {
  my $self   = shift;
  my $output = shift;
  my $xfer   = 0;
  $xfer += $output->writeStructBegin('Net::Cassandra::Backend::InvalidRequestException');
  if (defined $self->{why}) {
    $xfer += $output->writeFieldBegin('why', Net::Cassandra::Backend::TType::STRING, 1);
    $xfer += $output->writeString($self->{why});
    $xfer += $output->writeFieldEnd();
  }
  $xfer += $output->writeFieldStop();
  $xfer += $output->writeStructEnd();
  return $xfer;
}

package Net::Cassandra::Backend::UnavailableException;
use base('Net::Cassandra::Backend::Thrift::TException');
use Class::Accessor;
use base('Class::Accessor');
sub new {
my $classname = shift;
my $self      = {};
my $vals      = shift || {};
return bless($self,$classname);
}

sub getName {
  return 'Net::Cassandra::Backend::UnavailableException';
}

sub read {
  my $self  = shift;
  my $input = shift;
  my $xfer  = 0;
  my $fname;
  my $ftype = 0;
  my $fid   = 0;
  $xfer += $input->readStructBegin(\$fname);
  while (1) 
  {
    $xfer += $input->readFieldBegin(\$fname, \$ftype, \$fid);
    if ($ftype == Net::Cassandra::Backend::TType::STOP) {
      last;
    }
    SWITCH: for($fid)
    {
        $xfer += $input->skip($ftype);
    }
    $xfer += $input->readFieldEnd();
  }
  $xfer += $input->readStructEnd();
  return $xfer;
}

sub write {
  my $self   = shift;
  my $output = shift;
  my $xfer   = 0;
  $xfer += $output->writeStructBegin('Net::Cassandra::Backend::UnavailableException');
  $xfer += $output->writeFieldStop();
  $xfer += $output->writeStructEnd();
  return $xfer;
}

1;
