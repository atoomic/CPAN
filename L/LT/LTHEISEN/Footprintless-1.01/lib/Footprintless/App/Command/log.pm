use strict;
use warnings;

package Footprintless::App::Command::log;
$Footprintless::App::Command::log::VERSION = '1.01';
# ABSTRACT: Provides access to log files.
# PODNAME: Footprintless::App::Command::log

use Footprintless::App -command;
use Footprintless::Util qw(exit_due_to);

my $logger = Log::Any->get_logger();

sub _configure_logging {
    my ( $self, $level ) = @_;
    require Log::Any::Adapter;
    Log::Any::Adapter->set( 'Stderr',
        log_level => Log::Any::Adapter::Util::numeric_level($level) );
}

sub execute {
    my ( $self,       $opts,   $args )        = @_;
    my ( $coordinate, $action, @action_args ) = @$args;

    if ( $opts->{log} ) {
        $self->_configure_logging( $opts->{log} );
    }

    if ( $action eq 'follow' ) {
        eval {
            $self->{log}->follow(
                ( $opts->{until} ? ( until => $opts->{until} ) : () ),
                runner_options => { out_handle => \*STDOUT }
            );
        };
        exit_due_to($@) if ($@);
    }
    elsif ( $action eq 'tail' ) {
        eval { $self->{log}->tail(@action_args); };
        exit_due_to($@) if ($@);
    }
    elsif ( $action eq 'head' ) {
        eval { $self->{log}->head(@action_args); };
        exit_due_to($@) if ($@);
    }
    elsif ( $action eq 'cat' ) {
        eval { $self->{log}->cat(@action_args); };
        exit_due_to($@) if ($@);
    }
    elsif ( $action eq 'grep' ) {
        eval { $self->{log}->grep(@action_args); };
        exit_due_to($@) if ($@);
    }
}

sub opt_spec {
    return (
        [ "log=s",   "will set the log level", ],
        [ "until=s", "a perl regex pattern indicating the follow should stop", ],
    );
}

sub usage_desc {
    return "fpl %o [COORDINATE]";
}

sub validate_args {
    my ( $self, $opts, $args ) = @_;
    my ( $coordinate, $action ) = @$args;

    $self->usage_error("coordinate is required") unless @$args;

    my $footprintless = $self->app()->footprintless();
    eval { $self->{log} = $footprintless->log( $args->[0] ); };
    $self->usage_error("invalid coordinate [$coordinate]: $@") if ($@);

    $self->usage_error("invalid action [$action]")
        unless ( $action =~ /^cat|follow|grep|head|tail$/ );
}

1;

__END__

=pod

=head1 NAME

Footprintless::App::Command::log - Provides access to log files.

=head1 VERSION

version 1.01

=head1 SYNOPSIS

  fpl overlay project.environment.component.overlay clean
  fpl overlay project.environment.component.overlay initialize
  fpl overlay project.environment.component.overlay update
  fpl overlay project.environment.component.overlay # same as update

=head1 DESCRIPTION

Performs actions on an overlay.  The available actions are:

    clean        removes all files/folders handled by this overlay
    initialize   clean, then combine the base files and the processed template
                 files, then deploy
    update       process the template files, then deploy

If no action is specified, C<update> is implied.  For detailed configuration 
see L<Footprintless::Log>. 

=head1 AUTHOR

Lucas Theisen <lucastheisen@pastdev.com>

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2016 by Lucas Theisen.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=head1 SEE ALSO

Please see those modules/websites for more information related to this module.

=over 4

=item *

L<Footprintless|Footprintless>

=back

=cut
