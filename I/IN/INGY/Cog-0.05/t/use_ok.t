use Test::More tests => 10;

use_ok 'Cog';
use_ok 'Cog::App';
use_ok 'Cog::Cog';
use_ok 'Cog::Command';
use_ok 'Cog::Config';
use_ok 'Cog::Maker';
use_ok 'Cog::Page';
use_ok 'Cog::Plugin';
use_ok 'Cog::Store';
use_ok 'Cog::WebApp';
