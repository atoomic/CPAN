package Cog::Cog;

our $VERSION = '0.05';

# This module is here for a CPAN indexing point. Cog is currently
# indexed to CNC::Cog. Cog based modules should list Cog::Cog as the
# prerequisite module for now.

1;
