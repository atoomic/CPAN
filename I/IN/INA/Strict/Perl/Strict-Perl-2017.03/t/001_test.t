use 5.00503;
use strict;
use Test::Simply 'tests' => 30;

use vars qw(@test);

BEGIN {
    @test = (

    '001_void.pl' => [<<'END', 'notdie'],
END

    '002_1.pl' => [<<'END', 'notdie'],
1;
END

    '003_exit.pl' => [<<'END', 'notdie'],
exit;
END

    '004_die.pl' => [<<'END', 'die'],
die;
END

    '005_strict.pl' => [<<'END', 'notdie'],
use strict;
END

    '006_strictperl_exit.pl' => [<<'END', 'notdie'],
use Strict::Perl <%MODULEVERSION%>;
use vars qw($VERSION);
$VERSION = 1;
exit;
END

    '007_strictperl_die.pl' => [<<'END', 'die'],
use Strict::Perl <%MODULEVERSION%>;
use vars qw($VERSION);
$VERSION = 1;
die;
END

    '008_must_moduleversion.pl' => [<<'END', 'die'],
use Strict::Perl;
use vars qw($VERSION);
$VERSION = 1;
exit;
END

    '009_must_moduleversion_match.pl' => [<<'END', 'die'],
use Strict::Perl 9999.99;
use vars qw($VERSION);
$VERSION = 1;
exit;
END

    '010_must_scriptversion.pl' => [<<'END', 'die'],
use Strict::Perl <%MODULEVERSION%>;
exit;
END

    '011_strict.pl' => [<<'END', 'die'],
use Strict::Perl <%MODULEVERSION%>;
$VERSION = 1;
$VAR = 1;
exit;
END

    '012_warnings.pl' => [<<'END', 'die'],
use Strict::Perl <%MODULEVERSION%>;
use vars qw($VERSION);
print "VERSION=$VERSION";
exit;
END

    '013_autodie.pl' => [<<'END', 'die'],
use Strict::Perl <%MODULEVERSION%>;
use vars qw($VERSION);
$VERSION = 1;
open(FILE,'not_exists.txt');
close(FILE);
exit;
END

    '014_badword_goto.pl' => [<<'END', 'die'],
use Strict::Perl <%MODULEVERSION%>;
use vars qw($VERSION);
$VERSION = 1;
goto LABEL;
print "LINE=", __LINE__, "\n";
LABEL:
print "LINE=", __LINE__, "\n";
exit;
END

    '015_badword_given.pl' => [<<'END', 'die'],
use Strict::Perl <%MODULEVERSION%>;
use vars qw($VERSION);
$VERSION = 1;
given ($_) {
    if ($_ =~ /Strict::Perl/) {
        print "Strict::Perl\n";
    }
}
exit;
END

    '016_badword_when.pl' => [<<'END', 'die'],
use Strict::Perl <%MODULEVERSION%>;
use vars qw($VERSION);
$VERSION = 1;
for ($_) {
    when ($_ =~ /Strict::Perl/) {
        print "Strict::Perl\n";
    }
}
exit;
END

    '017_badvariable.pl' => [<<'END', 'die'],
use Strict::Perl <%MODULEVERSION%>;
use vars qw($VERSION);
$VERSION = 1;
print "\$[=($[)\n";
exit;
END

    '018_goodvariable.pl' => [<<'END', 'notdie'],
use Strict::Perl <%MODULEVERSION%>;
use vars qw($VERSION);
$VERSION = 1;
print "\$^W=($^W)\n";
exit;
END

    '019_badoperator.pl' => [<<'END', 'die'],
use Strict::Perl <%MODULEVERSION%>;
use vars qw($VERSION);
$VERSION = 1;
if ('Strict' ~~ 'Perl') {
    print "Strict::Perl\n";
}
exit;
END

    '020_bareword.pl' => [<<'END', 'notdie'],
use Strict::Perl <%MODULEVERSION%>;
use vars qw($VERSION);
$VERSION = 1;
open(FILE,$0);
close(FILE);
exit;
END

    '021_fileno_0.pl' => [<<'END', 'notdie'],
use Strict::Perl <%MODULEVERSION%>;
use vars qw($VERSION);
$VERSION = 1;
print "fileno(STDIN)=(",fileno(STDIN),")\n";
exit;
END

    '022_fileno_undef.pl' => [<<'END', 'die'],
use Strict::Perl <%MODULEVERSION%>;
use vars qw($VERSION);
$VERSION = 1;
print "fileno(FILE)=(",fileno(FILE),")\n";
exit;
END

    '023_unlink.pl' => [<<'END', 'notdie'],
use Strict::Perl <%MODULEVERSION%>;
use vars qw($VERSION);
$VERSION = 1;
unlink('not_exists.txt');
exit;
END

    '024_use_Thread.pl' => [<<'END', 'die'],
use Strict::Perl <%MODULEVERSION%>;
use vars qw($VERSION);
$VERSION = 1;
use Thread;
exit;
END

    '025_use_threads.pl' => [<<'END', 'die'],
use Strict::Perl <%MODULEVERSION%>;
use vars qw($VERSION);
$VERSION = 1;
use threads;
exit;
END

    '026_use_encoding.pl' => [<<'END', 'die'],
use Strict::Perl <%MODULEVERSION%>;
use vars qw($VERSION);
$VERSION = 1;
use encoding;
exit;
END

    '027_use_Switch.pl' => [<<'END', 'die'],
use Strict::Perl <%MODULEVERSION%>;
use vars qw($VERSION);
$VERSION = 1;
use Switch;
exit;
END

    '028_sigiled_keyword.pl' => [<<'END', 'notdie'],
use Strict::Perl <%MODULEVERSION%>;
use vars qw($VERSION);
$VERSION = 1;
my $goto = 1;
END

    '029_line.pl' => [<<'END', 'notdie'],
@rem = '
goto HERE
:HERE
@rem ';
#line 6
use Strict::Perl <%MODULEVERSION%>;
use vars qw($VERSION);
$VERSION = 1;
END

    '030___END__.pl' => [<<'END', 'notdie'],
use Strict::Perl <%MODULEVERSION%>;
use vars qw($VERSION);
$VERSION = 1;
__END__
goto
END

    );
}

# get $Strict::Perl::VERSION
BEGIN {
    require Strict::Perl;
}

while (@test > 0) {
    my $scriptname    = shift @test;
    my($script,$want) = @{shift @test};

    open(SCRIPT,"> $scriptname") || die "Can't open file: $scriptname\n";
    $script =~ s/<%MODULEVERSION%>/$Strict::Perl::VERSION/;
    print SCRIPT $script;
    close(SCRIPT);

    my $rc;
    if ($^O eq 'MSWin32') {
        $rc = system(qq{$^X $scriptname >NUL 2>NUL});
    }
    else {
        $rc = system(qq{$^X $scriptname >/dev/null 2>/dev/null});
    }
    unlink($scriptname);

    if ($want eq 'die') {
        ok(($rc>>8) != 0, "rc=($rc) perl/$] $scriptname $want");
    }
    else{
        ok(($rc>>8) == 0, "rc=($rc) perl/$] $scriptname $want");
    }
}

__END__
