package Net::OpenStack::Compute::AuthRole;
use Any::Moose 'Role';

has auth_url     => (is => 'rw', required => 1);
has user         => (is => 'ro', required => 1);
has password     => (is => 'ro', required => 1);
has project_id   => (is => 'ro');
has region       => (is => 'ro');
has service_name => (is => 'ro');
has is_rax_auth  => (is => 'ro', isa => 'Bool'); # Rackspace auth
has verify_ssl   => (is => 'ro', isa => 'Bool', default => 1);

1;

__END__
=pod

=head1 NAME

Net::OpenStack::Compute::AuthRole

=head1 VERSION

version 1.0900

=head1 AUTHOR

Naveed Massjouni <naveedm9@gmail.com>

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2011 by Naveed Massjouni.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut

