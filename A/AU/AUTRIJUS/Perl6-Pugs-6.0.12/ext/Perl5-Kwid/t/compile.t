use lib 't', 'lib';
use strict;
use warnings;
use Test::More tests => 6;

use_ok("Kwid");
use_ok("Kwid::AST");
use_ok("Kwid::Base");
use_ok("Kwid::HTML");
use_ok("Kwid::Loader");
use_ok("Kwid::Parser");
