
use v6;
use Test;

plan 5;

use_ok("Date");
use_ok("Duration");
use_ok("Time::Zone");
use_ok("Duration::Gregorian");
use_ok("Date::Gregorian");
