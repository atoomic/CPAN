
package Perl6::Instance::Attribute;

use strict;
use warnings;

use base 'Perl6::Attribute';

1;

__END__

=pod

=head1 NAME

Perl6::Instance::Attribute - Instance Attributes in the Perl 6 Meta Model

=head1 DESCRIPTION

=head1 SUPERCLASS

=over 4

=item I<Perl6::Attribute>

=back

=head1 AUTHOR

Stevan Little E<lt>stevan@iinteractive.comE<gt>

=cut
