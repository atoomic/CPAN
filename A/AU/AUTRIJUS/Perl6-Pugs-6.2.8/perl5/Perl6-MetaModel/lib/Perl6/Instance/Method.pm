
package Perl6::Instance::Method;

use strict;
use warnings;

use base 'Perl6::Method';

1;

__END__

=pod

=head1 NAME

Perl6::Instance::Method - Instance Methods in the Perl 6 Meta Model

=head1 DESCRIPTION

=head1 SUPERCLASS

=over 4

=item I<Perl6::Method>

=back

=head1 AUTHOR

Stevan Little E<lt>stevan@iinteractive.comE<gt>

=cut
