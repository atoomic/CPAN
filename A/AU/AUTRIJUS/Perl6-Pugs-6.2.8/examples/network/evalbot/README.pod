=head1 NAME

Evalbot

=head1 DESCRIPTION

This is a small IRC bot using C<Net::IRC> to C<eval()> commands.

=head1 ARCHITECTURE

C<evalbot.p6> is the main bot, written in Perl 6. To run a command, it spawns
C<evalhelper.p5>, a Perl 5 program. This sets up the necessary environment
(C<PUGS_SAFEMODE>, redirection of C<STDOUT> and C<STDERR> to a temporary file,
resource limits, etc.). Finnaly, C<evalhelper.p5> runs C<pugs>.

=head1 INSTALLATION

There's no installation, simply run C<evalbot.p6> supplying a nick and an IRC
server to connect to:

    $ pugs evalbot.p6 evalbot6 irc.freenode.net:6667

You don't have to restart C<evalbot.p6> when you've installed a new Pugs, as a
new C<pugs> is spawned on each command to eval.

=head1 AUTHOR

Ingo Blechschmidt, C<E<lt>iblech@web.deE<gt>>
