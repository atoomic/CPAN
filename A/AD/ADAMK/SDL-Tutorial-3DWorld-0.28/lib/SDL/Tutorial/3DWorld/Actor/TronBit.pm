package SDL::Tutorial::3DWorld::Actor::TronBit;


=pod

=head1 NAME

SDL::Tutorial::3DWorld::Actor::TronBit - An attempt to create "Bit" from Tron

=head1 DESCRIPTION

An attempt to make a complex actor, the "bit" character from tron.

Contains multiple sub-models, continually morphing and moving as a whole.

B<THIS CLASS DOES NOT WORK, AND ACTS ONLY AS A PLACEHOLDER FOR FUTURE WORK>

=cut

use strict;
use warnings;
use SDL::Tutorial::3DWorld::OpenGL ();

our $VERSION = '0.28';

1;

=pod

=head1 SUPPORT

Bugs should be reported via the CPAN bug tracker at

L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=SDL-Tutorial-3DWorld>

=head1 AUTHOR

Adam Kennedy E<lt>adamk@cpan.orgE<gt>

=head1 SEE ALSO

L<SDL>, L<OpenGL>

=head1 COPYRIGHT

Copyright 2010 Adam Kennedy.

This program is free software; you can redistribute
it and/or modify it under the same terms as Perl itself.

The full text of the license can be found in the
LICENSE file included with this module.

=cut
