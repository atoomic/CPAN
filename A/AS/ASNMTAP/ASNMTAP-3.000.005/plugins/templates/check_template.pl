#!/usr/bin/perl
# ----------------------------------------------------------------------------------------------------------
# � Copyright 2003-2006 by Alex Peeters [alex.peeters@citap.be]
# ----------------------------------------------------------------------------------------------------------
# 2006/02/26, v3.000.005, making Asnmtap v3.000.005 compatible
# ----------------------------------------------------------------------------------------------------------

use strict;
use warnings;           # Must be used in test mode only. This reduce a little process speed
#use diagnostics;       # Must be used in test mode only. This reduce a lot of process speed

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

use ASNMTAP::Asnmtap::Plugins v3.000.005;
use ASNMTAP::Asnmtap::Plugins qw(:PLUGINS);

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

my $objectPlugins = ASNMTAP::Asnmtap::Plugins->new (
  _programName        => 'check_template.pl',
  _programDescription => "General plugin template for the '$APPLICATION'",
  _programVersion     => '3.000.005',
  _programUsagePrefix => '-w|--warning <warning> -c|--critical <critical>',
  _programHelpPrefix  => '-w, --warning=<WARNING>
   warning threshold with more than one type of threshold
-c, --critical=<CRITICAL>
   critical threshold with more than one type of threshold',
  _programGetOptions  => ['host|H=s', 'warning|w=s', 'critical|c=s', 'timeout|t:i', 'trendline|T:i'],
  _timeout            => 30,
  _debug              => 0);

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Start plugin  - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

my $host = $objectPlugins->getOptionsArgv ('host');

my $warning = $objectPlugins->getOptionsArgv ('warning');
my $critical = $objectPlugins->getOptionsArgv ('critical');

$objectPlugins->pluginValue ( stateValue => $ERRORS{CRITICAL} );
$objectPlugins->pluginValue ( alert => '.1.' );

$objectPlugins->pluginValues ( { stateValue => $ERRORS{WARNING}, alert => '.2.', error => '.3.' }, $TYPE{APPEND} );

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# End plugin  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

$objectPlugins->exit (7);

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

__END__

=head1 NAME

check_template.pl

General plugin template for the 'Application Monitor'

The ASNMTAP plugins come with ABSOLUTELY NO WARRANTY.

=head1 AUTHOR

Alex Peeters [alex.peeters@citap.be]

=head1 COPYRIGHT NOTICE

(c) Copyright 2000-2006 by Alex Peeters [alex.peeters@citap.be],
                        All Rights Reserved.

=head1 LICENSE

This ASNMTAP CPAN library and Plugin templates are free software; you can redistribute it and/or modify it under the same terms as Perl itself.

The other parts of ASNMTAP may be used and modified free of charge by anyone so long as this copyright notice and the comments above remain intact. By using this code you agree to indemnify Alex Peeters from any liability that might arise from it's use.

Selling the code for this program without prior written consent is expressly forbidden. In other words, please ask first before you try and make money off of my program.

Obtain permission before redistributing this software over the Internet or in any other medium. In all cases copyright and header must remain intact.

=cut
