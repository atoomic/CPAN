use Mojo::Base -strict;
use Test::Mojo;
use Test::More;
use File::Spec::Functions;
use Mojolicious;
use lib '.';
use t::Api;

my $n = 0;

#
# This test checks that "require: false" is indeed false
# https://github.com/jhthorsen/swagger2/issues/39
#

my $app = Mojolicious->new;
$app->plugin(Swagger2 => {url => 't/data/petstore2.json'});

my $t = Test::Mojo->new($app);

$t::Api::RES = [{id => 123, name => "kit-cat"}];
$t->get_ok('/v1/pets')->status_is(200)->json_is('/0/id', 123)->json_is('/0/name', 'kit-cat');

done_testing;
