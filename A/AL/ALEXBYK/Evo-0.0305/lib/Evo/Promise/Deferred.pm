package Evo::Promise::Deferred;
use Evo '-Class *';

has 'promise';
has 'called', optional, rw;

sub reject ($self, $r) {
  return if $self->called;
  $self->called(1)->promise->d_reject_continue($r);
}

sub resolve ($self, $v) {
  return if $self->called;
  $self->called(1)->promise->d_resolve_continue($v);
}

1;

__END__

=pod

=encoding UTF-8

=head1 NAME

Evo::Promise::Deferred

=head1 VERSION

version 0.0305

=head1 AUTHOR

alexbyk.com

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2016 by alexbyk.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
