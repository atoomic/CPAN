
###############################################################################
#
# Win32::GUI - Perl-Win32 Graphical User Interface Extension
#
# 29 Jan 1997 by Aldo Calpini <dada@perl.it>
#
# Version: 0.0.558 (15 Jan 2001)
#
# Copyright (c) 1997..2001 Aldo Calpini. All rights reserved.
# This program is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.
#
###############################################################################

Win32::GUI is a Win32-platform native graphical user interface toolkit for perl. 
basically, it's an XS implementation of most of the functions found in 
user32.dll and gdi32.dll, with an object oriented perl interface and an 
event-based dialog model that mimic the functionality of visual basic. 

homepage: http://dada.perl.it