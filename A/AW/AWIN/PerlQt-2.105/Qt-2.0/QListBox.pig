#include <qlistbox.h>

suicidal virtual class QListBox : virtual QScrollView {
    enum SelectionMode { Single, Multi, Extended };
    enum LayoutMode { FixedNumber, FitToWidth, FitToHeight, Variable };

    QListBox(QWidget * = 0, const char * = 0, Qt::WFlags = 0);
    virtual ~QListBox();
    void centerCurrentItem();
    void changeItem(const QString &, int);
    void changeItem(const QPixmap &, int);
    void changeItem(const QListBoxItem *, int);
    void clear();
    virtual void clearSelection() slot;
    QListBox::LayoutMode columnMode() const;
    uint count() const;
    int currentItem() const;
    virtual void ensureCurrentVisible() slot;
    int index(const QListBoxItem *) const;
    void insertItem(const QListBoxItem *, int = -1);
    void insertItem(const QPixmap &, int = -1);
    void insertItem(const QString &, int = -1);
    void insertStringList(const QStringList &, int = -1);
    bool isMultiSelection() const;
    bool isSelected(int) const;
    bool isSelected(const QListBoxItem *) const;
    QListBoxItem *item(int) const;
    QListBoxItem *itemAt(QPoint) const;
    int itemHeight(int = 0) const;
    QRect itemRect(QListBoxItem *) const;
    bool itemVisible(int);
    bool itemVisible(const QListBoxItem *);
    long maxItemWidth() const;
    virtual QSize minimumSizeHint() const;
    int numColumns() const;
    int numRows() const;
    int numItemsVisible() const;
    const QPixmap *pixmap(int) const;
    void removeItem(int);
    QListBox::LayoutMode rowMode() const;
    QListBox::SelectionMode selectionMode() const;
    virtual void setBottomItem(int);
    virtual void setColumnMode(QListBox::LayoutMode);
    virtual void setColumnMode(QListBox::LayoutMode, int) : $this->QListBox::setColumnMode($2);
    virtual void setCurrentItem(int);
    virtual void setCurrentItem(QListBoxItem *);
    virtual void setFont(const QFont &);
    virtual void setMultiSelection(bool);
    virtual void setRowMode(QListBox::LayoutMode);
    virtual void setRowMode(QListBox::LayoutMode, int) : $this->QListBox::setRowMode($2);
    void setSelected(int, bool);
    virtual void setSelectionMode(QListBox::SelectionMode);
    virtual void setTopItem(int);
    virtual void setVariableHeight(bool);
    virtual void setVariableWidth(bool);
    virtual QSize sizeHint() const;
    void takeItem(const QListBoxItem *);
    QString text(int) const;
    int topItem() const;
    void triggerUpdate(bool);
    bool variableHeight() const;
    bool variableWidth() const;
    virtual void viewportPaintEvent(QPaintEvent *);
protected:
    void doLayout() const;
    virtual void focusInEvent(QFocusEvent *);
    virtual void focusOutEvent(QFocusEvent *);
    void highlighted(int) signal;
    void highlighted(const QString &) signal;
    void highlighted(QListBoxItem *) signal;
    virtual void keyPressEvent(QKeyEvent *);
    virtual void mouseDoubleClickEvent(QMouseEvent *);
    virtual void mouseMoveEvent(QMouseEvent *);
    virtual void mousePressEvent(QMouseEvent *);
    virtual void mouseReleaseEvent(QMouseEvent *);
    virtual void paintCell(QPainter *, int, int);
    virtual void resizeEvent(QResizeEvent *);
    void selected(int) signal;
    void selected(const QString &) signal;
    void selected(QListBoxItem *) signal;
    void selectionChanged() signal;
    virtual void showEvent(QShowEvent *);
    void toggleCurrentItem();
    void updateItem(int);
    void updateItem(QListBoxItem *);
;    void updateVector();    // Where is this?
    virtual void viewportMousePressEvent(QMouseEvent *);
    virtual void viewportMouseReleaseEvent(QMouseEvent *);
    virtual void viewportMouseDoubleClickEvent(QMouseEvent *);
    virtual void viewportMouseMoveEvent(QMouseEvent *);
} Qt::ListBox;
