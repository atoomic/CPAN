#ifndef PCLIPBRD_H
#define PCLIPBRD_H

/*
 * Definition of nothing.
 *
 * Copyright (C) 1997, Ashley Winters <jql@accessone.com>
 *
 * You may distribute under the terms of the LGPL as specified in the
 * README file
 */

#undef bool
#include "qclipbrd.h"
#include "pobject.h"

#endif  // PCLIPBRD_H
