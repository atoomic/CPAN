#ifndef PPICTURE_H
#define PPICTURE_H

/*
 * Declaration of the PCursor class
 *
 * Copyright (C) 1997, Ashley Winters <jql@accessone.com>
 *
 * You may distribute under the terms of the LGPL as specified in the
 * README file
 */

#include "qpicture.h"
#include "pqt.h"

class PPicture : public QPicture {
public:
    PPicture() {}
};

#endif  // PPICTURE_H
