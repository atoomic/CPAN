package QToolTip;

use strict;
use vars qw($VERSION @ISA);

require DynaLoader;

@ISA = qw(DynaLoader);

$VERSION = '1.02';
bootstrap QToolTip $VERSION;

1;
__END__

=head1 NAME

QToolTip - Interface to the Qt QToolTip and QToolTipGroup classes

=head2 Member functions

=head1 SYNOPSIS

C<use QToolTip;>

=head2 QToolTip

=head2 Member functions

add,
remove

=head2 QToolTipGroup

=head2 Member functions

new

=head1 DESCRIPTION

What you see is what you get.

=head1 AUTHOR

Ashley Winters <jql@accessone.com>
