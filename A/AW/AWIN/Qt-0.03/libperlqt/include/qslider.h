/*
 * If this file is included, we're compiling with Qt 1.1
 * Otherwise, we're compiling with Qt >=1.2.
 * A pretty simple hack.
 */

#define pQT_VERSION pQT_11
