#ifndef PCLIPBRD_H
#define PCLIPBRD_H

/*
 * Declaration of the PCursor class
 *
 * Copyright (C) 1997, Ashley Winters <jql@accessone.com>
 *
 * You may distribute under the terms of the LGPL as specified in the
 * README file
 */

#undef bool
#include "qclipbrd.h"
#include "ppixmap.h"
#include "pqt.h"

#endif  // PCLIPBRD_H
