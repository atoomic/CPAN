#ifndef PPAINTD_H
#define PPAINTD_H

/*
 * Declaration of the PPaintDevice class
 *
 * Copyright (C) 1997, Ashley Winters <jql@accessone.com>
 *
 * You may distribute under the terms of the LGPL as specified in the
 * README file
 */

#undef bool
#include "qpaintd.h"
#include "pqt.h"

class PPaintDevice;

#endif  // PPAINTD_H
