This file documents the revision history for the Perl library Validation::Class.

TODO (0000-00-00)
    - track class() usage and bubble-up errors
    - rewrite tests (using a simpler abstraction)
    - REMOVE ARRAY::UNIQUE YOU TWIT

6.05 (2012-04-13)
    * fixed major bug for V::C field accessors when setting values

6.00 (2012-04-12)
    * added POD for the lowercase filter
    * relocated pre-filtering to include exploded fields
    * field accessors always grab the collapsed value
    * improved overall parameter handling with array parameter support

5.98 (2012-04-12)
    * added get_fields to V::C::Engine
    * validate in V::C::Engine restores exploded params

5.96 (2012-04-07)
    * misc cleanup, version bump

5.95 (2012-04-07)
    * trialing new style of testing using BDD (no-commitment)
    * fixed V::C::Exporter passing V::C keywords
    * V::C POD update

5.90 (2012-04-05)
    * allow all array-like directives (e.g. "options") to take an arrayref
    * decouple validator from validating-directives
    * fixed unreported bug in the apply_filters method setting default value
    * allow default to execute a coderef

5.85 (2012-04-05)
    * added Validation::Class::Exporter

5.83 (2012-03-24)
    * creates keyword config defaults on access

5.82 (2012-03-22)
    * bug fix: minor but major typo in Engine.pm template method broken
      build keyword functionality

5.80 (2012-03-20)
    * bug fix: Class.pm gotcha in attribute keyword, (used once typo error)

5.80 (2012-03-20)
    * bug fix: config merging while using class as class and role in parrallel
    * added copy_errors method
    
5.75 (2012-03-18)
    * add before, after and around method modifiers
    * set/load -> base to be renamed config
    * accessors created at compile-time instead of run-time
    * POD changes
    * added readonly directive
    * added default_value method
    * added get_classes method

5.68 (2012-03-17)
    * POD changes, more tests
    * fixed self-validiting method arg handling in Class.pm
    * renamed utility functions and update the class method in Engine.pm
    * load/set keyword gets slight syntax change
    * fixed load/set backwards-compatibility bug

5.64 (2012-03-15)
    * fixed attribute method not being exported
    * miscellaneous tweaks, some new tests

5.63 (2012-03-13)
    * set/load -> base copies routines from the specified class if not exist
    * attributes get registered in the config hash
    * Slight POD changes
    
5.62 (2012-03-12)
    * added "set" keyword as an alias to "load"
    
5.60 (2012-03-10)
    * complete POD rewrite
    * rename Plugins.pod to Cookbook.pod
    * methods have output validation

5.51 (2012-03-08)
    * add build/bld keywords to register methods to be executed on instantiation

5.50 (2012-03-08)
    * automatic creation of class attributes based on param values
    * added method and mth keywords in preparation for the new OO system in v6
    * added ignore_failure flag (set true) to prevent methods from croaking on error
    * added report_failure flag (set false) registering an error on method failure

5.22 (2012-02-28)
    * positioning load_plugins and load_classes methods for depreciation

5.22 (2012-02-28)
    * positioning load_plugins and load_classes methods for depreciation

5.16 (2012-02-20)
    * added the set_method method, tests, updated some POD
    * update load method to inherit configuration data from other classes

5.15 (2012-02-20)
    * cleaned up various error handling approaches, editorialized some POD also

5.10 (2012-02-17)
    * flip-flopped again with regards to the version number style
    * class() returns undefined instead of dying with AWEFUL(tm) error per mst

5.0.2 (2012-02-13)
    * Version Bump
    * Fixed tests breaking installation (V::C::Moose related)

5.0.1 (2012-02-13)
    * Public version bump (tested)

5.0.0_02 (2012-02-11)
    * Fixed error in the has() attribute maker

5.0.0_01 (2012-02-11)
    * Updated 2 plugins tests

5.0.0_00 (2012-02-11)
    * Removed Moose and assorted dependencies, changed/chaning plugin design

4.01003514 (2012-02-05)
    * Udated changes file, fixed unreported bug in the class() method

4.01003407 (2012-02-04) 
    * Udated POD, added new load() method for easy configuration, new versioning
      style and changes file

3.6.60000 (2012-02-02)
    * error and label directives now handle multiline strings with DWIM formatting

3.6.50000 (2012-02-02)
    * Parameters arrayref magic doesn't overwrite existing array element rules

3.6.30000 (2012-02-02)
    * Updated POD (lots of updates)
    * Updated the class method, intelligent handling of nested parameters within
      child classes

3.6.00000 (2012-02-01)
    * Updated POD (lots of updates)
    * Added the set_errors and get_errors methods

3.5.40000 (2012-01-28)
    * Implement a profile mechanism for quickly executing a validation sequence

3.4.40000 (2012-01-28)
    * Handling of parameters submitted as arrayrefs (n-levels deep)

3.4.30000 (2012-01-27)
    * Intelligent handling of parameters submitted as arrayrefs

3.3.30000 (2012-01-26)
    * POD updates

3.3.20000 (2012-01-23)
    * Fixed minor error in using the param method to assign an empty string
    * Added a clear_queue method 

3.2.20000 (2012-01-23)
    * Fixed the matches directive (had bad comparison expression)

3.2.10000 (2012-01-23)
    * Add a stash method/object for injecting instance specific data into
      custom validation routine

3.1.10000 (2011-12-06)
    * Updated POD, Fixed broken MANIFEST

3.1.00000 (2011-12-06)
    * Updated POD
    * Added Validation::Class::MooseRules, superior validation for your
      Moose class
    * Added Validation::Class::Simple, adds drop-in data validation for
      scripts, etc

3.0.00000 (2011-12-04)
    * Completly refactored all code (mainly for aesthetics and readability)
    * added Validation::Class::Simple for drop-in data validation
    * renamed sanitize method to normalize
    * added fld, mxn, flt, dir keyword aliases
    * new and updated POD, tests, etc

2.7.70000 (2011-11-29)
    * Fixed custom filter execution issue (esp. with multi-filters)

2.7.60000 (2011-11-26)
    * Fixed post-validation filtering logic

2.7.50000 (2011-11-26)
    * Updated POD, removed disclaimer documented filtering directive
    * Auto-apply filtering directive - allow field-level filtering option

2.7.00000 (2011-11-19)
    * Updated POD, documented filtering attributes and appy_filters method
    * Now supports pre/post filtering

2.6.00000 (2011-11-09)
    * Updated POD, documented clone method and regex spec
    * Validate method accepts regexps' to validate variable field
    * Clone method allows the creating/cloning of fields on-the-fly

2.5.20000 (2011-11-09)
    * Now using Perl::Critic (fixed various syntactical nuances)
    * Add PerlTidy to the build process

2.5.10000 (2011-11-08)
    * Updated POD
    * The class method now uses shortnames and classnames. Fields
      without labels are not prefixed with 'Field' when an error message is
      generated

2.5.00000 (2011-11-05)
    * Updated/Fixed POD
    * Moved filtering and initialization routines into the sanitize method
      which will run per validate() call

2.4.70000 (2011-11-03)
    * Updated POD, removed the filter directive
    * Adjusted how singular and multiple values are merged and applied
    * Applies default directive at instantiation

2.4.50000 (2011-11-02)
    * Updated POD, and Plugins POD
    * Updated pattern validator tests and functionality

2.4.40000 (2011-10-28)
    * Updated POD, documented writing Plugins
    * Added instatiation hook for plugins

2.4.30000 (2011-10-28)
    * Refactored the plugin system

2.4.00000 (2011-10-28)
    * Updated POD, documented new plugin system
    * Added support for a plugin system

2.3.40000 (2011-10-27)
    * Updated POD, documented new toggle feature
    * Added support for toggling and default directives
    * Custom validation now has an auto-generated error message
    * Added new directives, (default, depends_on, max alpha digits
      sum and symbols as well as min alpha digits sum and symbols

2.1.00000 (2011-10-27)
    * Updated POD
    * Ability to load (child) related classes
    * (Major Fix) Each class now operates independently

2.0.00000 (2011-10-24)
    * Updated POD
    * Added queue (persistent fields) functionality
    * Added stashed attribute and reset, param, and queue methods

1.9.50000 (2011-10-23)
    * Completely rewritten with Moose
    * Fixed mixin and mixin_field merging policy
