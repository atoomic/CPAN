package API::Basecamp::Class;

use Import::Into;

our $VERSION = '0.03'; # VERSION

sub import {
    my $target = caller;

    Data::Object::Class->import::into($target);
    Data::Object::Signatures->import::into($target);
    Data::Object::Library->import::into($target => -types);

    return;
}

1;
