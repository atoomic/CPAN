# ABSTRACT: Data::Seek Data Structure Container
package Data::Seek::Data;

use parent 'Data::Object::Hash';

our $VERSION = '0.08'; # VERSION

sub decode {
    return shift->unfold;
}

sub encode {
    return shift->fold;
}

1;

__END__

=pod

=encoding UTF-8

=head1 NAME

Data::Seek::Data - Data::Seek Data Structure Container

=head1 VERSION

version 0.08

=head1 SYNOPSIS

    use Data::Seek::Data;

=head1 DESCRIPTION

Data::Seek::Data is a class within L<Data::Seek> which acts as a container for a
data structure which is intended to be provided to L<Data::Seek::Search>.

=head1 METHODS

=head2 decode

    my $hash = $data->decode;

The decode method unflatten/unfolds the data structure returning a hash object.

=head2 encode

    my $hash = $data->encode;

The encode method flatten/fold the data structure returning a hash object.

=head1 AUTHOR

Al Newkirk <anewkirk@ana.io>

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2014 by Al Newkirk.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
