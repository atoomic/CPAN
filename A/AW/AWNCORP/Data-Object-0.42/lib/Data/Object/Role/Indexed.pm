# Indexed Data Type Role for Perl 5
package Data::Object::Role::Indexed;

use 5.010;
use Data::Object::Role;

our $VERSION = '0.42'; # VERSION

requires 'slice';

1;
