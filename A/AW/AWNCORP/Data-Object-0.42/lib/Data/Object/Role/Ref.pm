# Reference Data Type Role for Perl 5
package Data::Object::Role::Ref;

use 5.010;
use Data::Object::Role;

with 'Data::Object::Role::Defined';

our $VERSION = '0.42'; # VERSION

1;
