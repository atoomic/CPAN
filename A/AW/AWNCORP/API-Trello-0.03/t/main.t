use Test::More;

use_ok 'API::Trello';
use_ok 'API::Trello::Class';
use_ok 'API::Trello::Client';
use_ok 'API::Trello::Exception';

ok 1 and done_testing;
