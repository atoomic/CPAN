use Bubblegum::Object::Instance;
use Test::More;

can_ok 'Bubblegum::Object::Instance', 'new';
can_ok 'Bubblegum::Object::Instance', 'data';

done_testing;
