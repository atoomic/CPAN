package Upload::View::TT;

use strict;
use base 'Catalyst::View::TT';

=head1 NAME

Upload::V::TT - TT View Component

=head1 SYNOPSIS

See L<Upload>

=head1 DESCRIPTION

TT View Component.

=head1 AUTHOR

root

=head1 LICENSE

This library is free software . You can redistribute it and/or modify
it under the same terms as perl itself.

=cut

1;
