#
# (c) Jan Gehring <jan.gehring@gmail.com>
#
# vim: set ts=2 sw=2 tw=0:
# vim: set expandtab:

package Rex::Args::Single;

use strict;
use warnings;

our $VERSION = '0.57.0'; # VERSION

sub get { return 1; }

1;
