package HTML::Mason;
# Copyright (c) 1998-99 by Jonathan Swartz. All rights reserved.
# This program is free software; you can redistribute it and/or modify it
# under the same terms as Perl itself.

use 5.004;

$HTML::Mason::VERSION = '1.05';

use HTML::Mason::Parser;
use HTML::Mason::Interp;

1;
