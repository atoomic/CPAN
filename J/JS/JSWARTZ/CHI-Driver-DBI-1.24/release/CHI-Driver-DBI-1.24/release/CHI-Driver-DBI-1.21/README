NAME
    CHI::Driver::DBI - Use DBI for cache storage

SYNOPSIS
     use CHI;

     my $dbh   = DBI->connect(...);
     my $cache = CHI->new( driver => 'DBI', dbh => $dbh, );
     OR
     my $cache = CHI->new( driver => 'DBI', dbh => $dbh, dbh_ro => $dbh_ro, );

DESCRIPTION
    This driver uses a database table to store the cache. The newest
    versions of MySQL and SQLite work are known to work. Other RDBMSes
    should work.

    Why cache things in a database? Isn't the database what people are
    trying to avoid with caches? This is often true, but a simple primary
    key lookup is extremely fast in many databases and this provides a
    shared cache that can be used when less reliable storage like memcached
    is not appropriate. Also, the speed of simple lookups on MySQL when
    accessed over a local socket is very hard to beat. DBI is fast.

ATTRIBUTES
    namespace
        The namespace you pass in will be appended to the `table_prefix' and
        used as a table name. That means that if you don't specify a
        namespace or table_prefix the cache will be stored in a table called
        `chi_Default'.

    table_prefix
        This is the prefix that is used when building a table name. If you
        want to just use the namespace as a literal table name, set this to
        undef. Defaults to `chi_'.

    dbh The main, or rw, DBI handle used to communicate with the db. If a
        dbh_ro handle is defined then this handle will only be used for
        writing.

        This attribute can be set after object creation as well, so in a
        persistent environment like mod_perl or FastCGI you may keep an
        instance of the cache around and set the dbh on each request after
        checking it with ping().

    dbh_ro
        The optional DBI handle used for read-only operations. This is to
        support master/slave RDBMS setups.

    sql_strings
        Hashref of SQL strings to use in the different cache operations. The
        strings are built depending on the RDBMS that dbh is attached to.

METHODS
    BUILD
        Standard issue Moose BUILD method. Used to build the sql_strings. If
        the parameter `create_table' to `new()' was set to true, it will
        attempt to create the db table. For Mysql and SQLite the statement
        is "create if not exists..." so it's generally harmless.

    fetch
    store
    remove
    clear
    get_keys
    get_namespaces
        Not supported at this time.

Authors
    Original version by Justin DeVuyst and Perrin Harkins. Currently
    maintained by Jonathan Swartz.

COPYRIGHT & LICENSE
    Copyright (C) Justin DeVuyst

    This program is free software; you can redistribute it and/or modify it
    under the same terms as Perl itself.

