#!/usr/bin/perl -w
 
use Test::More tests => 2;

use_ok( 'RayApp::mod_perl' );
use_ok( 'RayApp::mod_perl_Storable' );
