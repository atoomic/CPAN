package Mojolicious::Plugin::LinkEmbedder::Link::Game;
use Mojo::Base 'Mojolicious::Plugin::LinkEmbedder::Link';

1;

=encoding utf8

=head1 NAME

Mojolicious::Plugin::LinkEmbedder::Link::Game - Base class for Game links

=head1 DESCRIPTION

This class inherit from L<Mojolicious::Plugin::LinkEmbedder::Link>.

=head1 AUTHOR

Jan Henning Thorsen - C<jhthorsen@cpan.org>

Marcus Ramberg - C<mramberg@cpan.org>

=cut
