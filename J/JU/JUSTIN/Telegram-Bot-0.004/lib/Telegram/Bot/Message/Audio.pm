package Telegram::Bot::Message::Audio;
$Telegram::Bot::Message::Audio::VERSION = '0.004';
# ABSTRACT: The base class for Telegram message 'Audio' type.

use Mojo::Base 'Telegram::Bot::Message';

has 'file_id';
has 'duration';
has 'mime_type';
has 'file_size';

sub is_array { return; }

sub fields {
  return { scalar => [qw/file_id width height file_size/]
         };
}

1;

__END__

=pod

=encoding UTF-8

=head1 NAME

Telegram::Bot::Message::Audio - The base class for Telegram message 'Audio' type.

=head1 VERSION

version 0.004

=head1 AUTHOR

Justin Hawkins <justin@eatmorecode.com>

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2016 by Justin Hawkins.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
