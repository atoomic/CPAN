package Telegram::Bot::Message::Sticker;
$Telegram::Bot::Message::Sticker::VERSION = '0.004';
# ABSTRACT: The base class for Telegram message 'Sticker' type.

use Mojo::Base 'Telegram::Bot::Message';
use Telegram::Bot::Message::PhotoSize;

has 'file_id';
has 'width';
has 'height';
has 'thumb';
has 'file_size';

sub is_array { return; }

sub fields {
  return { scalar => [qw/file_id width height file_size/],
           'Telegram::Bot::Message::PhotoSize' => [qw/thumb/] 
         };
}

1;

__END__

=pod

=encoding UTF-8

=head1 NAME

Telegram::Bot::Message::Sticker - The base class for Telegram message 'Sticker' type.

=head1 VERSION

version 0.004

=head1 AUTHOR

Justin Hawkins <justin@eatmorecode.com>

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2016 by Justin Hawkins.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
