Revision history for DBIx::Class

0.08009 2008-01-20 13:30
        - Made search_rs smarter about when to preserve the cache to fix
          mm prefetch usage
        - Added Storage::DBI subclass for MSSQL over ODBC. 
        - Added freeze, thaw and dclone methods to Schema so that thawed
          objects will get re-attached to the schema.
        - Moved dbicadmin to JSON::Any wrapped JSON.pm for a sane API
        - introduced DBIx::Class::set_inflated_columns
        - DBIx::Class::Row::copy uses set_inflated_columns

0.08008 2007-11-16 14:30:00
        - Fixed join merging bug (test from Zby)
        - When adding relationships, it will throw an exception if you get the
          foreign and self parts the wrong way round in the condition
        - ResultSetColumn::func() now returns all results if called in list
          context; this makes things like func('DISTINCT') work as expected
        - Many-to-many relationships now warn if the utility methods would 
          clash
        - InflateColumn::DateTime now accepts an extra parameter of timezone
          to set timezone on the DT object (thanks Sergio Salvi)
        - Added sqlt_deploy_hook to result classes so that indexes can be 
          added.
        - Added startup checks to warn loudly if we appear to be running on 
          RedHat systems from perl-5.8.8-10 and up that have the bless/overload
          patch applied (badly) which causes 2x -> 100x performance penalty.
          (Jon Schutz)
        - ResultSource::reverse_relationship_info can distinguish between 
          sources using the same table
        - Row::insert will now not fall over if passed duplicate related objects
        - Row::copy will not fall over if you have two relationships to the 
          same source with a unique constraint on it

0.08007 2007-09-04 19:36:00
        - patch for Oracle datetime inflation (abram@arin.net)
        - added on_disconnect_do
        - on_connect_do and on_disconnect_do take coderefs and arrayrefs

0.08006 2007-08-12 15:12:00
        - Move to using Class::C3::Componentised
        - Remove warn statement from DBIx::Class::Row

0.08005 2007-08-06 
        - add timestamp fix re rt.cpan 26978 - no test yet but change
          clearly should cause no regressions
        - provide alias for related_resultset via local() so it's set
          correctly at resultset construction time (fixes RestrictWithObject)
        - fixes bind params in debug statements
          (original test from abraxxa)
        - fixed storage->connected fork bug
          (test and fix from Radu Greab)
        - add 1; to AccessorGroup.pm for stuff that still uses it
        - refactor Statistics to create debugging filehandle to fix bug with
          closed STDERR, update docs and modify Versioned to use Statistics
          (original fix from diz)

0.08004 2007-08-06 19:00:00
        - fix storage connect code to not trigger bug via auto-viv 
          (test from aherzog)
        - fixup cursor_class to be an 'inherited' attr for per-package defaults
        - add default_resultset_attributes entry to Schema
        - optimisation in DBI::Cursor to check software_limit before falling
          back to base Cursor->all
        - fix bug with create_multi not inserting non-storage objects
          (test and fix from davinchi)
        - DBIx::Class::AccessorGroup made empty subclass of
          Class::Accessor::Grouped
        - fixed an ugly bug regarding $dbh->{AutoCommit} and transactions
        - ensure_class_loaded handles non-classnames better.
        - non-destructive hashref handling for connect_info options
        - count no longer returns negative values after slice
          (report and test from JOHANL)
        - rebless before building datetime_parser
          (patch from mattlaw / Matt Lawrence)

0.08003 2007-07-14 18:01:00
        - improved populate bulk_insert mode
        - fixed up multi_create to be more intelligent about PK<->PK rels
        - fix many-many rels to not use set_columns
        - Unmarked deploy as experimental since it isn't anymore
        - Removed Cwd dep since it's not required and causes problems
          with debian packaging
        - Patch to fix ? in data for NoBindVars (from Tom Hukins)
        - Restored mk_classaccessor method for compatibility
        - Fixed group_by problem with oracle limit syntax
        - Fixed attr merging problem
        - Fixed $rs->get_column w/prefetch  problem

0.08002 2007-06-20 06:10:00
        - add scope guard to Row::insert to ensure rollback gets called
        - more heuristics in Row::insert to try and get insert order right
        - eliminate vestigial code in PK::Auto
        - more expressive DBI errors
        - soften errors during deploy
        - ensure_connected before txn_begin to catch stomping on transaction
          depth
        - new method "rethrow" for our exception objects

0.08001 2007-06-17 21:21:02
        - Cleaned up on_connect handling for versioned
        - removed DateTime use line from multi_create test
        - hid DBIx::ContextualFetch::st override in CDBICompat

0.08000 2007-06-17 18:06:12
        - Fixed DBIC_TRACE debug filehandles to set ->autoflush(1)
        - Fixed circular dbh<->storage in HandleError with weakref

0.07999_06 2007-06-13 04:45:00
        - tweaked Row.pm to make last_insert_id take multiple column names
        - Fixed DBIC::Storage::DBI::Cursor::DESTROY bug that was
          messing up exception handling
        - added exception objects to eliminate stacktrace/Carp::Clan
          output redundancy
        - setting $ENV{DBIC_TRACE} defaults stacktrace on.
        - added stacktrace option to Schema, makes throw_exception
          use "confess"
        - make database handles use throw_exception by default
        - make database handles supplied by a coderef use our
          standard HandleError/RaiseError/PrintError
        - add "unsafe" connect_info option to suppress our setting
          of HandleError/RaiseError/PrintError
        - removed several redundant evals whose sole purpose was to
          provide extra debugging info
        - fixed page-within-page bug (reported by nilsonsfj)
        - fixed rare bug when database is disconnected inbetween
          "$dbh->prepare_cached" and "$sth->execute"

0.07999_05 2007-06-07 23:00:00
        - Made source_name rw in ResultSource
        - Fixed up SQL::Translator test/runtime dependencies
        - Fixed t/60core.t in the absence of DateTime::Format::MySQL
        - Test cleanup and doc note (ribasushi)

0.07999_04 2007-06-01 14:04:00
        - pulled in Replication storage from branch and marked EXPERIMENTAL
        - fixup to ensure join always LEFT after first LEFT join depthwise
        - converted the vendor tests to use schema objects intead of schema
          classes, made cleaned more reliable with END blocks
        - versioning support via DBIx::Class::Schema::Versioned
        - find/next now return undef rather than () on fail from Bernhard Graf
        - rewritten collapse_result to fix prefetch
        - moved populate to resultset
        - added support for creation of related rows via insert and populate
        - transaction support more robust now in the face of varying AutoCommit
          and manual txn_begin usage
        - unbreak back-compat for Row/ResultSet->new_result
        - Added Oracle/WhereJoins.pm for Oracle >= 8 to support
          Oracle <= 9i, and provide Oracle with a better join method for
          later versions.  (I use the term better loosely.)
        - The SQL::T parser class now respects a relationship attribute of
          is_foreign_key_constrain to allow explicit control over wether or
          not a foreign constraint is needed
        - resultset_class/result_class now (again) auto loads the specified
          class; requires Class::Accessor::Grouped 0.05002+
        - added get_inflated_columns to Row
        - %colinfo accessor and inflate_column now work together
        - More documentation updates
        - Error messages from ->deploy made more informative
        - connect_info will now always return the arguments it was
          originally given
        - A few small efficiency improvements for load_classes
          and compose_namespace

0.07006 2007-04-17 23:18:00
        - Lots of documentation updates
        - deploy now takes an optional 'source_names' parameter (dec)
        - Quoting for for columns_info_for
        - RT#25683 fixed (multiple open sths on DBD::Sybase)
        - CDBI compat infers has_many from has_a (Schwern)
        - Fix ddl_filename transformation (Carl Vincent)

0.07999_02 2007-01-25 20:11:00
        - add support for binding BYTEA and similar parameters (w/Pg impl)
        - add support to Ordered for multiple ordering columns
        - mark DB.pm and compose_connection as deprecated
        - switch tests to compose_namespace
        - ResultClass::HashRefInflator added
        - Changed row and rs objects to not have direct handle to a source,
          instead a (schema,source_name) tuple of type ResultSourceHandle

0.07005 2007-01-10 18:36:00
        - fixup changes file
        - remove erroneous .orig files - oops

0.07004 2007-01-09 21:52:00
        - fix find_related-based queries to correctly grep the unique key
        - fix InflateColumn to inflate/deflate all refs but scalar refs

0.07003 2006-11-16 11:52:00
        - fix for rt.cpan.org #22740 (use $^X instead of hardcoded "perl")
        - Tweaks to resultset to allow inflate_result to return an array
        - Fix UTF8Columns to work under Perl <= 5.8.0
        - Fix up new_result in ResultSet to avoid alias-related bugs
        - Made new/update/find handle 'single' rel accessor correctly
        - Fix NoBindVars to be safer and handle non-true bind values
        - Don't blow up if columns_info_for returns useless results
        - Documentation updates

0.07999_01 2006-10-05 21:00:00
        - add connect_info option "disable_statement_caching"
        - create insert_bulk using execute_array, populate uses it
        - added DBIx::Class::Schema::load_namespaces, alternative to
          load_classes
        - added source_info method for source-level metadata (kinda like
          column_info)
        - Some of ::Storage::DBI's code/docs moved to ::Storage
        - DBIx::Class::Schema::txn_do code moved to ::Storage
        - Storage::DBI now uses exceptions instead of ->ping/->{Active} checks
        - Storage exceptions are thrown via the schema class's throw_exception
        - DBIx::Class::Schema::throw_exception's behavior can be modified via
          ->exception_action
        - columns_info_for is deprecated, and no longer runs automatically.
          You can make it work like before via
          __PACKAGE__->column_info_from_storage(1) for now
        - Replaced DBIx::Class::AccessorGroup and Class::Data::Accessor with
          Class::Accessor::Grouped. Only user noticible change is to
          table_class on ResultSourceProxy::Table (i.e. table objects in
          schemas) and, resultset_class and result_class in ResultSource.
          These accessors no longer automatically require the classes when
          set.

0.07002 2006-09-14 21:17:32
        - fix quote tests for recent versions of SQLite
        - added reference implementation of Manual::Example
        - backported column_info_from_storage accessor from -current, but
        - fixed inflate_datetime.t tests/stringify under older Test::More
        - minor fixes for many-to-many relationship helpers
        - cleared up Relationship docs, and fixed some typos
        - use ref instead of eval to check limit syntax (to avoid issues with
          Devel::StackTrace)
        - update ResultSet::_cond_for_update_delete to handle more complicated
          queries
        - bugfix to Oracle columns_info_for
        - remove_columns now deletes columns from _columns

0.07001 2006-08-18 19:55:00
        - add directory argument to deploy()
        - support default aliases in many_to_many accessors.
        - support for relationship attributes in many_to_many accessors.
        - stop search_rs being destructive to attrs
        - better error reporting when loading components
        - UTF8Columns changed to use "utf8" instead of "Encode"
        - restore automatic aliasing in ResultSet::find() on nonunique queries
        - allow aliases in ResultSet::find() queries (in cases of relationships
          with prefetch)
        - pass $attrs to find from update_or_create so a specific key can be
          provided
        - remove anonymous blesses to avoid major speed hit on Fedora Core 5's
          Perl and possibly others; for more information see:
          https://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=196836
        - fix a pathological prefetch case
        - table case fix for Oracle in columns_info_for
        - stopped search_rs deleting attributes from passed hash

0.07000 2006-07-23 02:30:00
        - supress warnings for possibly non-unique queries, since
          _is_unique_query doesn't infer properly in all cases
        - skip empty queries to eliminate spurious warnings on ->deploy
        - fixups to ORDER BY, tweaks to deepen some copies in ResultSet
        - fixup for RowNum limit syntax with functions

0.06999_07 2006-07-12 20:58:05
        - fix issue with from attr copying introduced in last release

0.06999_06 2006-07-12 17:16:55
        - documentation for new storage options, fix S::A::L hanging on to $dbh
        - substantial refactor of search_related code to fix alias numbering
        - don't generate partial unique keys in ResultSet::find() when a table
          has more than one unique constraint which share a column and only one
          is satisfied
        - cleanup UTF8Columns and make more efficient
        - rename DBIX_CLASS_STORAGE_DBI_DEBUG to DBIC_TRACE (with compat)
        - rename _parent_rs to _parent_source in ResultSet
        - new FAQ.pod!

0.06999_05 2006-07-04 14:40:01
        - fix issue with incorrect $rs->{attrs}{alias}
        - fix subclassing issue with source_name
        - tweak quotes test to output text on failure
        - fix Schema->txn_do to not fail as a classmethod

0.06999_04 2006-06-29 20:18:47
        - disable cdbi-t/02-Film.t warning tests under AS perl
        - fixups to MySQL tests (aka "work round mysql being retarded")
        - compat tweaks for Storage debug logging

0.06999_03 2006-06-26 21:04:44
        - various documentation improvements
        - fixes to pass test suite on Windows
        - rewrote and cleaned up SQL::Translator tests
        - changed relationship helpers to only call ensure_class_loaded when the
          join condition is inferred
        - rewrote many_to_many implementation, now provides helpers for adding
          and deleting objects without dealing with the link table
        - reworked InflateColumn implementation to lazily deflate where
          possible; now handles passing an inflated object to new()
        - changed join merging to not create a rel_2 alias when adding a join
          that already exists in a parent resultset
        - Storage::DBI::deployment_statements now calls ensure_connected
          if it isn't passed a type
        - fixed Componentized::ensure_class_loaded
        - InflateColumn::DateTime supports date as well as datetime
        - split Storage::DBI::MSSQL into MSSQL and Sybase::MSSQL
        - fixed wrong debugging hook call in Storage::DBI
        - set connect_info properly before setting any ->sql_maker things

0.06999_02 2006-06-09 23:58:33
        - Fixed up POD::Coverage tests, filled in some POD holes
        - Added a warning for incorrect component order in load_components
        - Fixed resultset bugs to do with related searches
        - added code and tests for Componentized::ensure_class_found and
          load_optional_class
        - NoBindVars + Sybase + MSSQL stuff
        - only rebless S::DBI if it is still S::DBI and not a subclass
        - Added `use' statement for DBD::Pg in Storage::DBI::Pg
        - stopped test relying on order of unordered search
        - bugfix for join-types in nested joins using the from attribute
        - obscure prefetch problem fixed
        - tightened up deep search_related
        - Fixed 'DBIx/Class/DB.pm did not return a true value' error
        - Revert change to test for deprecated find usage and swallow warnings
        - Slight wording change to new_related() POD
        - new specific test for connect_info coderefs
        - POD clarification and content bugfixing + a few code formatting fixes
        - POD::Coverage additions
        - fixed debugfh
        - Fix column_info stomping

0.06999_01 2006-05-28 17:19:30
        - add automatic naming of unique constraints
        - marked DB.pm as deprecated and noted it will be removed by 1.0
        - add ResultSetColumn
        - refactor ResultSet code to resolve attrs as late as possible
        - merge prefetch attrs into join attrs
        - add +select and +as attributes to ResultSet
        - added InflateColumn::DateTime component
        - refactor debugging to allow for profiling using Storage::Statistics
        - removed Data::UUID from deps, made other optionals required
        - modified SQLT parser to skip dupe table names
        - added remove_column(s) to ResultSource/ResultSourceProxy
        - added add_column alias to ResultSourceProxy
        - added source_name to ResultSource
        - load_classes now uses source_name and sets it if necessary
        - add update_or_create_related to Relationship::Base
        - add find_or_new to ResultSet/ResultSetProxy and find_or_new_related
          to Relationship::Base
        - add accessors for unique constraint names and coulums to
          ResultSource/ResultSourceProxy
        - rework ResultSet::find() to search unique constraints
        - CDBICompat: modify retrieve to fix column casing when ColumnCase is
          loaded
        - CDBICompat: override find_or_create to fix column casing when
          ColumnCase is loaded
        - reorganized and simplified tests
        - added Ordered
        - added the ability to set on_connect_do and the various sql_maker
          options as part of Storage::DBI's connect_info.

0.06003 2006-05-19 15:37:30
        - make find_or_create_related check defined() instead of truth
        - don't unnecessarily fetch rels for cascade_update
        - don't set_columns explicitly in update_or_create; instead use
          update($hashref) so InflateColumn works
        - fix for has_many prefetch with 0 related rows
        - make limit error if rows => 0
        - added memory cycle tests and a long-needed weaken call

0.06002 2006-04-20 00:42:41
        - fix set_from_related to accept undef
        - fix to Dumper-induced hash iteration bug
        - fix to copy() with non-composed resultsource
        - fix to ->search without args to clone rs but maintain cache
        - grab $self->dbh once per function in Storage::DBI
        - nuke ResultSource caching of ->resultset for consistency reasons
        - fix for -and conditions when updating or deleting on a ResultSet

0.06001
        - Added fix for quoting with single table
        - Substantial fixes and improvements to deploy
        - slice now uses search directly
        - fixes for update() on resultset
        - bugfix to Cursor to avoid error during DESTROY
        - transaction DBI operations now in debug trace output

0.06000 2006-03-25 18:03:46
        - Lots of documentation improvements
        - Minor tweak to related_resultset to prevent it storing a searched rs
        - Fixup to columns_info_for when database returns type(size)
        - Made do_txn respect void context (on the off-chance somebody cares)
        - Fix exception text for nonexistent key in ResultSet::find()

0.05999_04 2006-03-18 19:20:49
        - Fix for delete on full-table resultsets
        - Removed caching on count() and added _count for pager()
        - ->connection does nothing if ->storage defined and no args
          (and hence ->connect acts like ->clone under the same conditions)
        - Storage::DBI throws better exception if no connect info
        - columns_info_for made more robust / informative
        - ithreads compat added, fork compat improved
        - weaken result_source in all resultsets
        - Make pg seq extractor less sensitive.

0.05999_03 2006-03-14 01:58:10
        - has_many prefetch fixes
        - deploy now adds drop statements before creates
        - deploy outputs debugging statements if DBIX_CLASS_STORAGE_DBI_DEBUG
            is set

0.05999_02 2006-03-10 13:31:37
        - remove test dep on YAML
        - additional speed tweaks for C3
        - allow scalarefs passed to order_by to go straight through to SQL
        - renamed insert_or_update to update_or_insert (with compat alias)
        - hidden lots of packages from the PAUSE Indexer

0.05999_01 2006-03-09 18:31:44
        - renamed cols attribute to columns (cols still supported)
        - added has_column_loaded to Row
        - Storage::DBI connect_info supports coderef returning dbh as 1st arg
        - load_components() doesn't prepend base when comp. prefixed with +
        - $schema->deploy
        - HAVING support
        - prefetch for has_many
        - cache attr for resultsets
        - PK::Auto::* no longer required since Storage::DBI::* handle auto-inc
        - minor tweak to tests for join edge case
        - added cascade_copy relationship attribute
          (sponsored by Airspace Software, http://www.airspace.co.uk/)
        - clean up set_from_related
        - made copy() automatically null out auto-inc columns
        - added txn_do() method to Schema, which allows a coderef to be
          executed atomically

0.05007 2006-02-24 00:59:00
        - tweak to Componentised for Class::C3 0.11
        - fixes for auto-inc under MSSQL

0.05006 2006-02-17 15:32:40
        - storage fix for fork() and workaround for Apache::DBI
        - made update(\%hash) work on row as well as rs
        - another fix for count with scalar group_by
        - remove dependency on Module::Find in 40resultsetmanager.t (RT #17598)

0.05005 2006-02-13 21:24:51
        - remove build dependency on version.pm

0.05004 2006-02-13 20:59:00
        - allow specification of related columns via cols attr when primary
          keys of the related table are not fetched
        - fix count for group_by as scalar
        - add horrific fix to make Oracle's retarded limit syntax work
        - remove Carp require
        - changed UUIDColumns to use new UUIDMaker classes for uuid creation
        using whatever module may be available

0.05003 2006-02-08 17:50:20
        - add component_class accessors and use them for *_class
        - small fixes to Serialize and ResultSetManager
        - rollback on disconnect, and disconnect on DESTROY

0.05002 2006-02-06 12:12:03
        - Added recommends for Class::Inspector
        - Added skip_all to t/40resultsetmanager.t if no Class::Inspector
        available

0.05001 2006-02-05 15:28:10
        - debug output now prints NULL for undef params
        - multi-step prefetch along the same rel (e.g. for trees) now works
        - added multi-join (join => [ 'foo', 'foo' ]), aliases second to foo_2
        - hack PK::Auto::Pg for "table" names referencing a schema
        - find() with attributes works
        - added experimental Serialize and ResultSetManager components
        - added code attribute recording to DBIx::Class
        - fix to find() for complex resultsets
        - added of $storage->debugcb(sub { ... })
        - added $source->resultset_attributes accessor
        - added include_columns rs attr

0.05000 2006-02-01 16:48:30
        - assorted doc fixes
        - remove ObjectCache, not yet working in 0.05
        - let many_to_many rels have attrs
        - fix ID method in PK.pm to be saner for new internals
        - fix t/30dbicplain.t to use ::Schema instead of
          Catalyst::Model::DBIC::Plain

0.04999_06 2006-01-28 21:20:32
        - fix Storage/DBI (tried to load deprecated ::Exception component)

0.04999_05 2006-01-28 20:13:52
        - count will now work for grouped resultsets
        - added accessor => option to column_info to specify accessor name
        - added $schema->populate to load test data (similar to AR fixtures)
        - removed cdbi-t dependencies, only run tests if installed
        - Removed DBIx::Class::Exception
        - unified throw_exception stuff, using Carp::Clan
        - report query when sth generation fails.
        - multi-step prefetch!
        - inheritance fixes
        - test tweaks

0.04999_04 2006-01-24 21:48:21
        - more documentation improvements
        - add columns_info_for for vendor-specific column info (Zbigniew
        Lukasiak)
        - add SQL::Translator::Producer for DBIx::Class table classes (Jess
        Robinson)
        - add unique constraint declaration (Daniel Westermann-Clark)
        - add new update_or_create method (Daniel Westermann-Clark)
        - rename ResultSetInstance class to ResultSetProxy, ResultSourceInstance
          to ResultSourceProxy, and TableInstance to ResultSourceProxy::Table
        - minor fixes to UUIDColumns
        - add debugfh method and ENV magic for tracing SQL (Nigel Metheringham)

0.04999_03 2006-01-20 06:05:27
        - imported Jess Robinson's SQL::Translator::Parser::DBIx::Class
        - lots of internals cleanup to eliminate result_source_instance
        requirement
        - added register_column and register_relationship class APIs
        - made Storage::DBI use prepare_cached safely (thanks to Tim Bunce)
        - many documentation improvements (thanks guys!)
        - added ->connection, ->connect, ->register_source and ->clone schema
        methods
        - Use croak instead of die for user errors.

0.04999_02 2006-01-14 07:17:35
        - Schema is now self-contained; no requirement for co-operation
        - add_relationship, relationships, relationship_info, has_relationship
        - relationship handling on ResultSource
        - all table handling now in Table.pm / ResultSource.pm
        - added GROUP BY and DISTINCT support
        - hacked around SQL::Abstract::Limit some more in DBIC::SQL::Abstract
          (this may have fixed complex quoting)
        - moved inflation to inflate_result in Row.pm
        - added $rs->search_related
        - split compose_namespace out of compose_connection in Schema
        - ResultSet now handles find
        - various *_related methods are now ->search_related->*
        - added new_result to ResultSet

0.04999_01 2005-12-27 03:33:42
        - search and related methods moved to ResultSet
        - select and as added to ResultSet attrs
        - added DBIx::Class::Table and TableInstance for table-per-class
        - added DBIx::Class::ResultSetInstance which handles proxying
          search etc. as a superclass of DBIx::Class::DB
        - assorted test and code cleanup work

0.04001 2005-12-13 22:00:00
        - Fix so set_inflated_column calls set_column
        - Syntax errors in relationship classes are now reported
        - Better error detection in set_primary_key and columns methods
        - Documentation improvements
        - Better transaction support with txn_* methods
        - belongs_to now works when $cond is a string
        - PK::Auto::Pg updated, only tries primary keys instead of all cols

0.04 2005-11-26
        - Moved get_simple and set_simple into AccessorGroup
        - Made 'new' die if given invalid columns
        - Added has_column and column_info to Table.pm
        - Refactored away from direct use of _columns and _primaries
        - Switched from NEXT to Class::C3

0.03004
        - Added an || '' to the CDBICompat stringify to avoid null warnings
        - Updated name section for manual pods
0.03003 2005-11-03 17:00:00
        - POD fixes.
        - Changed use to require in Relationship/Base to avoid import.

0.03002 2005-10-20 22:35:00
        - Minor bugfix to new (Row.pm)
        - Schema doesn't die if it can't load a class (Schema.pm)
        - New UUID columns plugin (UUIDColumns.pm)
        - Documentation improvements.

0.03001 2005-09-23 14:00:00
        - Fixes to relationship helpers
        - IMPORTANT: prefetch/schema combination bug fix

0.03    2005-09-19 19:35:00
        - Paging support
        - Join support on search
        - Prefetch support on search

0.02    2005-08-12 18:00:00
        - Test fixes.
        - Performance improvements.
        - Oracle primary key support.
        - MS-SQL primary key support.
        - SQL::Abstract::Limit integration for database-agnostic limiting.

0.01    2005-08-08 17:10:00
        - initial release

