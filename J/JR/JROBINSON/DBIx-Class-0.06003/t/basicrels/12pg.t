use Test::More;
use lib qw(t/lib);
use DBICTest;
use DBICTest::BasicRels;

require "t/run/12pg.tl";
run_tests(DBICTest->schema);
