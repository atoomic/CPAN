package Bundle::Locale::CLDR::World;

use version;

our $VERSION = version->declare('v0.29.0');

=head1 NAME Bundle::Locale::CLDR::World

=head1 CONTENTS

Bundle::Locale::CLDR::Asia 0.29.0
Bundle::Locale::CLDR::Africa 0.29.0
Bundle::Locale::CLDR::Europe 0.29.0
Bundle::Locale::CLDR::Oceania 0.29.0
Bundle::Locale::CLDR::Americas 0.29.0
Bundle::Locale::CLDR::Europeanunion 0.29.0

=cut

1;

