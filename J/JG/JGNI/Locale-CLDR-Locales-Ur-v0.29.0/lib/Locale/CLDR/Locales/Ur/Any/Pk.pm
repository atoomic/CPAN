=head1

Locale::CLDR::Locales::Ur::Any::Pk - Package for language Urdu

=cut

package Locale::CLDR::Locales::Ur::Any::Pk;
# This file auto generated from Data\common\main\ur_PK.xml
#	on Fri 29 Apr  7:31:14 pm GMT

use version;

our $VERSION = version->declare('v0.29.0');

use v5.10.1;
use mro 'c3';
use utf8;
use if $^V ge v5.12.0, feature => 'unicode_strings';

use Types::Standard qw( Str Int HashRef ArrayRef CodeRef RegexpRef );
use Moo;

extends('Locale::CLDR::Locales::Ur::Any');
no Moo;

1;

# vim: tabstop=4
