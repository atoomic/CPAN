=head1

Locale::CLDR::Locales::Yue::Any::Hk - Package for language Cantonese

=cut

package Locale::CLDR::Locales::Yue::Any::Hk;
# This file auto generated from Data\common\main\yue_HK.xml
#	on Fri 29 Apr  7:32:55 pm GMT

use version;

our $VERSION = version->declare('v0.29.0');

use v5.10.1;
use mro 'c3';
use utf8;
use if $^V ge v5.12.0, feature => 'unicode_strings';

use Types::Standard qw( Str Int HashRef ArrayRef CodeRef RegexpRef );
use Moo;

extends('Locale::CLDR::Locales::Yue::Any');
no Moo;

1;

# vim: tabstop=4
