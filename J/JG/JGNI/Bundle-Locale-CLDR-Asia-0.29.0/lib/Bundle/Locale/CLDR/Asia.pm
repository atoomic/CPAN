package Bundle::Locale::CLDR::Asia;

use version;

our $VERSION = version->declare('v0.29.0');

=head1 NAME Bundle::Locale::CLDR::Asia

=head1 CONTENTS

Bundle::Locale::CLDR::Centralasia 0.29.0
Bundle::Locale::CLDR::Easternasia 0.29.0
Bundle::Locale::CLDR::Westernasia 0.29.0
Bundle::Locale::CLDR::Southernasia 0.29.0
Bundle::Locale::CLDR::Southeastasia 0.29.0

=cut

1;

