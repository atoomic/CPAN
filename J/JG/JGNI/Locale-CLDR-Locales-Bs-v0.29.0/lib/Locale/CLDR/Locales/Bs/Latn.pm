=head1

Locale::CLDR::Locales::Bs::Latn - Package for language Bosnian

=cut

package Locale::CLDR::Locales::Bs::Latn;
# This file auto generated from Data\common\main\bs_Latn.xml
#	on Fri 29 Apr  6:55:00 pm GMT

use version;

our $VERSION = version->declare('v0.29.0');

use v5.10.1;
use mro 'c3';
use utf8;
use if $^V ge v5.12.0, feature => 'unicode_strings';

use Types::Standard qw( Str Int HashRef ArrayRef CodeRef RegexpRef );
use Moo;

extends('Locale::CLDR::Locales::Bs');
no Moo;

1;

# vim: tabstop=4
