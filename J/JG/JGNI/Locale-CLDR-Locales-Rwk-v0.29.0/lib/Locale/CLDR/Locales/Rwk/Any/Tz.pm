=head1

Locale::CLDR::Locales::Rwk::Any::Tz - Package for language Rwa

=cut

package Locale::CLDR::Locales::Rwk::Any::Tz;
# This file auto generated from Data\common\main\rwk_TZ.xml
#	on Fri 29 Apr  7:23:52 pm GMT

use version;

our $VERSION = version->declare('v0.29.0');

use v5.10.1;
use mro 'c3';
use utf8;
use if $^V ge v5.12.0, feature => 'unicode_strings';

use Types::Standard qw( Str Int HashRef ArrayRef CodeRef RegexpRef );
use Moo;

extends('Locale::CLDR::Locales::Rwk::Any');
no Moo;

1;

# vim: tabstop=4
