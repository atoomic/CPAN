package Locale::CLDR::Locales::Bg::Any;

# This file auto generated
#	on Fri 29 Apr  6:52:56 pm GMT

use version;

our $VERSION = version->declare('v0.29.0');

use v5.10.1;
use mro 'c3';
use if $^V ge v5.12.0, feature => 'unicode_strings';

use Moo;

extends('Locale::CLDR::Locales::Bg');

no Moo;

1;
