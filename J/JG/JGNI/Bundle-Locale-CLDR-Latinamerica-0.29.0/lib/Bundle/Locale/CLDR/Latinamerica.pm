package Bundle::Locale::CLDR::Latinamerica;

use version;

our $VERSION = version->declare('v0.29.0');

=head1 NAME Bundle::Locale::CLDR::Latinamerica

=head1 CONTENTS

Bundle::Locale::CLDR::Caribbean 0.29.0
Bundle::Locale::CLDR::Southamerica 0.29.0
Bundle::Locale::CLDR::Centralamerica 0.29.0

=cut

1;

