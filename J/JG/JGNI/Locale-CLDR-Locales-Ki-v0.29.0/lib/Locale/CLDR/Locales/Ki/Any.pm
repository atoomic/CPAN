package Locale::CLDR::Locales::Ki::Any;

# This file auto generated
#	on Fri 29 Apr  7:11:44 pm GMT

use version;

our $VERSION = version->declare('v0.29.0');

use v5.10.1;
use mro 'c3';
use if $^V ge v5.12.0, feature => 'unicode_strings';

use Moo;

extends('Locale::CLDR::Locales::Ki');

no Moo;

1;
