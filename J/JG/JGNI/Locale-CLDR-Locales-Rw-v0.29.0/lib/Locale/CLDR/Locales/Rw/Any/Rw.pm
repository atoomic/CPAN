=head1

Locale::CLDR::Locales::Rw::Any::Rw - Package for language Kinyarwanda

=cut

package Locale::CLDR::Locales::Rw::Any::Rw;
# This file auto generated from Data\common\main\rw_RW.xml
#	on Fri 29 Apr  7:23:50 pm GMT

use version;

our $VERSION = version->declare('v0.29.0');

use v5.10.1;
use mro 'c3';
use utf8;
use if $^V ge v5.12.0, feature => 'unicode_strings';

use Types::Standard qw( Str Int HashRef ArrayRef CodeRef RegexpRef );
use Moo;

extends('Locale::CLDR::Locales::Rw::Any');
no Moo;

1;

# vim: tabstop=4
