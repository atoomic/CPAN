package Locale::CLDR::Locales::Sah::Any;

# This file auto generated
#	on Fri 29 Apr  7:23:53 pm GMT

use version;

our $VERSION = version->declare('v0.29.0');

use v5.10.1;
use mro 'c3';
use if $^V ge v5.12.0, feature => 'unicode_strings';

use Moo;

extends('Locale::CLDR::Locales::Sah');

no Moo;

1;
