
package Paper::Specs::Axxxx;
use strict;
use base qw(Paper::Specs::base::brand);

=head1 Paper::Axxxx

Information about labels and card forms that some company makes

=cut

sub specs {

    return {
        name_short => "Axxxx",
        name_long  => "Axxxx-Dxxxxxxxx",
    };

}

1;

