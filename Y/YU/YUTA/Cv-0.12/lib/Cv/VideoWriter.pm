# -*- mode: perl; coding: utf-8; tab-width: 4; -*-

package Cv::VideoWriter;

use 5.008008;
use strict;
use warnings;

BEGIN {
	Cv::aliases(
		[ 'cvWriteFrame' ],
		);
}

1;
__END__
