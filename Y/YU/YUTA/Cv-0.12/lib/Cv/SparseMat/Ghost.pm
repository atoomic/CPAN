# -*- mode: perl; coding: utf-8; tab-width: 4; -*-

package Cv::SparseMat::Ghost;

use 5.008008;
use strict;
use warnings;

use Cv::SparseMat;
our @ISA = qw(Cv::SparseMat);

sub DESTROY {
}

1;
__END__
