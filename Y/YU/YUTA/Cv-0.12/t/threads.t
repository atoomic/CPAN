# -*- mode: perl; coding: utf-8; tab-width: 4 -*-

#  Before `make install' is performed this script should be runnable with
#  `make test'. After `make install' it should work as `perl Cv.t'

#########################

# change 'tests => 1' to 'tests => last_test_to_print';

use Test::More qw(no_plan);
# use Test::More tests => 13;

BEGIN {
	use_ok('Cv');
}

use File::Basename;
use Time::HiRes qw(usleep);
my $verbose = Cv->hasGUI;

use Config;

SKIP: {
	skip('can not use threads', 1) unless $Config{useithreads};
	eval 'use threads;';
	eval 'use Thread::Queue;';

	my $sample = dirname($0) . "/../sample";
	my @imgs = grep { -f $_ } glob("$sample/*.png");

	sub sub1 {
		my $q = shift;
		foreach (@imgs) {
			my $img = Cv->LoadImage($_);
			$q->enqueue($img);
			usleep(200_000);
			bless $img, join('::', blessed $img, 'Ghost');
		}
		$q->enqueue(undef);
	}
	
	my $nr_imgs = 0;
	my $q = Thread::Queue->new;
	my $thr = threads->new(\&sub1, $q);
	while (my $img = $q->dequeue) {
		$nr_imgs++;
		ok($img->isa('Cv::Image'));
		if ($verbose) {
			$img->show;
			Cv->waitKey(100);
		}
	}
	$thr->join;
	
	ok($nr_imgs == @imgs);
}

