package Mira::CLI;

use strict;
use warnings;
use App::Cmd::Setup -app;

$Mira::CLI::VERSION = '0.05';



1;

__END__

=pod

=encoding UTF-8

=head1 NAME

Mira::CLI - Mira application

=head1 VERSION

version 0.3

=head1 AUTHOR

Kiavash Mazi

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2016 by Kiavash Mazi.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
