use Test::More tests => 1;

BEGIN {
      use_ok( 'Sledge::Template::Xslate' );
}

diag( "Testing Sledge::Template::Xslate $Sledge::Template::Xslate::VERSION" );
