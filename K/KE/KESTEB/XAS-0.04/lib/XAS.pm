package XAS;

our $VERSION = '0.04';

1;

__END__

=head1 NAME

XAS - A framework for distributed applicatons

=head1 DESCRIPTION

Frameworks mean differant things to differant people. In this case we are
trying to present a consistent environment, option handling, logging,
communications, notifications and database access in distributed applications. 

=over 4

=item B<L<Objectives|XAS::Docs::Objectives>>

=item B<L<Installation|XAS::Docs::Installation>>

=item B<L<Configuration|XAS::Docs::Configuration>>

=item B<L<System Startup|XAS::Docs::Startup>>

=item B<L<Base Modules|XAS::Docs::Base>>

=item B<L<Theory of Operation|XAS::Docs::Theory>>

=back

=head1 WHAT NOW?

Wow, you have a running system, now what do you do with it. As stated above
this system is a framework. It dosen't do anything on it's own. You need to 
write the code to do somethimg useful. The tools are provided and examples are
available. 

=head1 SEE ALSO

 XAS::Docs::Base
 XAS::Docs::Configuration
 XAS::Docs::Installation
 XAS::Docs::Objectives
 XAS::Docs::Startup
 XAS::Docs::Theory

=head1 SUPPORT 

The latest and greatest is always available at:

    http://svn.kesteb.us/repos/XAS-Base

=head1 AUTHOR

Kevin L. Esteb, C<< <kevin (at) kesteb.us> >>

=head1 COPYRIGHT & LICENSE

Copyright 2012 Kevin L. Esteb, all rights reserved.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

=cut
