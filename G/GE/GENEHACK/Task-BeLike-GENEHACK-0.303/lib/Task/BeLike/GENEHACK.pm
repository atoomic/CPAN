package Task::BeLike::GENEHACK;
our $AUTHORITY = 'cpan:GENEHACK';
$Task::BeLike::GENEHACK::VERSION = '0.303';
# ABSTRACT: individuality via conformity

use strict;
use warnings;
use 5.010;


1;

__END__

=pod

=encoding UTF-8

=head1 NAME

Task::BeLike::GENEHACK - individuality via conformity

=head1 VERSION

version 0.303

=head1 TASK CONTENTS

=head2 Modules I use

=head3 L<App::Cmd>

=head3 L<App::GitGot>

=head3 L<App::MiseEnPlace>

=head3 L<App::Nopaste>

=head3 L<App::Uni>

=head3 L<App::cpanminus>

=head3 L<App::cpanoutdated>

=head3 L<App::pmuninstall>

=head3 L<Audio::M4P::QuickTime>

=head3 L<Data::Printer>

=head3 L<DateTime>

=head3 L<Devel::Cover>

=head3 L<Dist::Zilla::App::Command::cover>

=head3 L<Dist::Zilla::Plugin::MakeMaker::Awesome>

=head3 L<Dist::Zilla::Plugin::OSPrereqs>

=head3 L<Dist::Zilla::PluginBundle::GENEHACK>

=head3 L<Git::Wrapper>

=head3 L<HiD>

=head3 L<Imager>

=head3 L<List::MoreUtils>

=head3 L<Module::CoreList>

=head3 L<Module::Install>

=head3 L<Module::Which>

=head3 L<Moo>

=head3 L<Moose>

=head3 L<MooseX::App::Cmd>

=head3 L<MooseX::amine>

=head3 L<Perl::Tidy>

=head3 L<Pod::Coverage::TrustPod>

=head3 L<Pod::Cpandoc>

=head3 L<Reply>

=head3 L<Term::ReadLine::Perl>

=head3 L<Test::Class>

=head3 L<Test::Exception>

=head3 L<Test::MockObject::Extends>

=head3 L<Test::Most>

=head3 L<Test::Pod>

=head3 L<Test::Pod::Coverage>

=head3 L<Text::FindIndent>

=head3 L<Try::Tiny>

=head3 L<YAML>

=head3 L<autodie>

=head3 L<local::lib>

=head3 L<namespace::autoclean>

=head1 AUTHOR

John SJ Anderson <genehack@genehack.org>

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2015 by John SJ Anderson.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
