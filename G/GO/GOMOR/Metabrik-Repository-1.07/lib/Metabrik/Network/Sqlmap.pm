#
# $Id: Sqlmap.pm,v 1db92d72d9a2 2015/02/16 07:14:38 gomor $
#
# network::sqlmap Brik
#
package Metabrik::Network::Sqlmap;
use strict;
use warnings;

use base qw(Metabrik);

sub brik_properties {
   return {
      revision => '$Revision: 1db92d72d9a2 $',
      tags => [ qw(unstable network security scanner sqlmap sql injection) ],
      attributes => {
         datadir => [ qw(datadir) ],
         cookie => [ qw(string) ],
         parameter => [ qw(parameter_name) ],
         request_file => [ qw(file) ],
         args => [ qw(sqlmap_arguments) ],
         output_file => [ qw(file) ],
      },
      attributes_default => {
         request_file => 'sqlmap_request.txt',
         parameter => 'parameter',
         args => '--ignore-proxy -v 3 --level=5 --risk=3 --user-agent "Mozilla"',
      },
      commands => {
         start => [ ],
      },
   };
}

# python /usr/share/sqlmap-dev/sqlmap.py -p PARAMETER -r /root/XXX/outil_sqlmap/request.raw --ignore-proxy -v 3 --level=5 --risk=3 --user-agent "Mozilla" 2>&1 | tee /root/XXX/outil_sqlmap/XXX.txt
sub start {
   my $self = shift;

   my $datadir = $self->datadir;
   my $args = $self->args;
   my $cookie = $self->cookie;
   my $parameter = $self->parameter;
   my $request_file = $datadir.'/'.$self->request_file;
   my $output_file = $datadir.'/'.$self->output_file;

   if (! -f $request_file) {
      return $self->log->error("start: request file [$request_file] not found");
   }

   my $cmd = "sqlmap -p $parameter $args -r $request_file";
   if (defined($output_file)) {
      $cmd .= ' 2>&1 | tee '.$output_file;
   }

   system($cmd);

   return 1;
}

1;

__END__

=head1 NAME

Metabrik::Network::Sqlmap - network::sqlmap Brik

=head1 COPYRIGHT AND LICENSE

Copyright (c) 2014-2015, Patrice E<lt>GomoRE<gt> Auffret

You may distribute this module under the terms of The BSD 3-Clause License.
See LICENSE file in the source distribution archive.

=head1 AUTHOR

Patrice E<lt>GomoRE<gt> Auffret

=cut
