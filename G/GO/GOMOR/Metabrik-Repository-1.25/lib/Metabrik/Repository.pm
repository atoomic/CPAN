#
# $Id: Repository.pm,v 0f8f3b87ae30 2016/10/11 17:07:01 gomor $
#
package Metabrik::Repository;
use strict;
use warnings;

# Breaking.Feature.Fix
our $VERSION = '1.25';
our $FIX = '0';

1;

__END__

=head1 NAME

Metabrik::Repository - Metabrik Briks repository

=head1 COPYRIGHT AND LICENSE

Copyright (c) 2014-2016, Patrice E<lt>GomoRE<gt> Auffret

You may distribute this module under the terms of The BSD 3-Clause License.
See LICENSE file in the source distribution archive.

=head1 AUTHOR

Patrice E<lt>GomoRE<gt> Auffret

=cut
