#
# $Id: Repository.pm,v 3595633d3216 2015/10/29 21:25:42 gomor $
#
package Metabrik::Repository;
use strict;
use warnings;

our $VERSION = '1.11';

1;

__END__

=head1 NAME

Metabrik::Repository - Metabrik Briks repository

=head1 COPYRIGHT AND LICENSE

Copyright (c) 2014-2015, Patrice E<lt>GomoRE<gt> Auffret

You may distribute this module under the terms of The BSD 3-Clause License.
See LICENSE file in the source distribution archive.

=head1 AUTHOR

Patrice E<lt>GomoRE<gt> Auffret

=cut
