use 5.010;
use strict;
use warnings;
package Benchmark::Perl::Formance::Analyzer;
BEGIN {
  $Benchmark::Perl::Formance::Analyzer::AUTHORITY = 'cpan:SCHWIGON';
}
# ABSTRACT: Benchmark::Perl::Formance - analyze results
$Benchmark::Perl::Formance::Analyzer::VERSION = '0.006';
1;

__END__

=pod

=encoding UTF-8

=head1 NAME

Benchmark::Perl::Formance::Analyzer - Benchmark::Perl::Formance - analyze results

=head1 AUTHOR

Steffen Schwigon <ss5@renormalist.net>

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2015 by Steffen Schwigon.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
