use 5.010;
use strict;
use warnings;
package Benchmark::Perl::Formance::Analyzer;
# git description: v0.006-9-gc1f28da

our $AUTHORITY = 'cpan:SCHWIGON';
# ABSTRACT: Benchmark::Perl::Formance - analyze results
$Benchmark::Perl::Formance::Analyzer::VERSION = '0.007';
1;

__END__

=pod

=encoding UTF-8

=head1 NAME

Benchmark::Perl::Formance::Analyzer - Benchmark::Perl::Formance - analyze results

=head1 AUTHOR

Steffen Schwigon <ss5@renormalist.net>

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2016 by Steffen Schwigon.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
