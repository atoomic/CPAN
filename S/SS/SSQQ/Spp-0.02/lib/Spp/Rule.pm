package Spp::Rule;

=head1 NAME

Spp::Rule - The grammar rule of Spp programming language.

=head1 VERSION

Version 0.01

=cut

our $VERSION = '0.01';


=head1 SYNOPSIS

Quick summary of what the module does.

Perhaps a little code snippet.

    use Spp::Rule qw($Spp_rule);

    my $rule = eval_atom($Spp_rule);

=head1 EXPORT

$Spp_rule

=cut

use Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw($Spp_rule);

our $Spp_rule = ["exprs",[["list",[["sym","context"],["sym","Spp"]]],["list",[["sym","rule"],["sym","TOP"],["rule",["token",[["assert","^"],["rept",[["branch",[["rtoken","ws"],["ctoken","atom"]]],[1,-1,"+"]]],["assert",'$']]]]]],["list",[["sym","rule"],["sym","ws"],["rule",["rept",[["branch",[["rept",[["cclass","\\s"],[1,-1,"+"]]],["rtoken","comment"],["rtoken","pod"]]],[1,-1,"+"]]]]]],["list",[["sym","rule"],["sym","comment"],["rule",["token",["#",["rept",[["chclass",[["flip"],"\\n"]],[0,-1,"*"]]],["assert",'$$']]]]]],["list",[["sym","rule"],["sym","pod"],["rule",["token",[["assert","^^"],"=pod",["look",[["any"],[0,-1,"*?"],["group",[["assert","^^"],"=end",["assert",'$$']]]]]]]]]],["list",[["sym","rule"],["sym","atom"],["rule",["branch",[["ctoken","int"],["ctoken","Str"],["ctoken","String"],["ctoken","Hash"],["ctoken","Array"],["ctoken","List"],["ctoken","Rule"],["ctoken","Keyword"],["ctoken","Sarray"],["ctoken","dot"],["ctoken","sym"]]]]]],["list",[["sym","rule"],["sym","int"],["rule",["token",[["rept",["-",[0,1,"?"]]],["rept",[["cclass","\\d"],[1,-1,"+"]]]]]]]],["list",[["sym","rule"],["sym","char"],["rule",["token",["\\",["any"]]]]]],["list",[["sym","rule"],["sym","Str"],["rule",["token",["'",["rept",[["branch",[["ctoken","sstr"],["ctoken","char"]]],[0,-1,"*"]]],"'"]]]]],["list",[["sym","rule"],["sym","Keyword"],["rule",["token",[":",["ctoken","kstr"]]]]]],["list",[["sym","rule"],["sym","dot"],["rule","."]]],["list",[["sym","rule"],["sym","sstr"],["rule",["rept",[["chclass",[["flip"],"\\\\","\\'"]],[1,-1,"+"]]]]]],["list",[["sym","rule"],["sym","String"],["rule",["token",["\"",["rept",[["branch",[["ctoken","dstr"],["ctoken","inter"],["ctoken","char"]]],[0,-1,"*"]]],"\""]]]]],["list",[["sym","rule"],["sym","dstr"],["rule",["rept",[["chclass",[["flip"],"\\\\","\\\"","\\{"]],[1,-1,"+"]]]]]],["list",[["sym","rule"],["sym","inter"],["rule",["token",["{",["rept",[["branch",[["rtoken","ws"],["ctoken","atom"]]],[0,-1,"*"]]],"}"]]]]],["list",[["sym","rule"],["sym","List"],["rule",["token",["(",["rept",[["branch",[["rtoken","ws"],["rept",[",",[1,-1,"+"]]],["ctoken","atom"]]],[0,-1,"*"]]],")"]]]]],["list",[["sym","rule"],["sym","Array"],["rule",["token",["[",["rept",[["branch",[["rtoken","ws"],["rept",[",",[1,-1,"+"]]],["ctoken","atom"]]],[0,-1,"*"]]],"]"]]]]],["list",[["sym","rule"],["sym","Hash"],["rule",["token",["{",["rept",[["branch",[["rtoken","ws"],["rept",[",",[1,-1,"+"]]],["ctoken","Pair"]]],[0,-1,"*"]]],"}"]]]]],["list",[["sym","rule"],["sym","Pair"],["rule",["token",[["ctoken","atom"],["rtoken","ws"],["rept",[["group",["=>",["rtoken","ws"]]],[0,1,"?"]]],["ctoken","atom"]]]]]],["list",[["sym","rule"],["sym","sym"],["rule",["rept",[["chclass",["_",["range","a-z"],["range","A-Z"],"+","\\-","*","%","=",">","<","?","!","~",'$',"@","^","&","|",":","/"]],[1,-1,"+"]]]]]],["list",[["sym","rule"],["sym","kstr"],["rule",["rept",[["chclass",["_",["range","a-z"],["range","A-Z"],["range","0-9"],"\\-",'$',"@","%","<",">",".","&","|","!","*","?","+","~","=",":","^","/"]],[1,-1,"+"]]]]]],["list",[["sym","rule"],["sym","Sarray"],["rule",["token",[":[",["rept",[["branch",[["ctoken","blank"],["ctoken","cstr"],["ctoken","char"]]],[1,-1,"+"]]],"]"]]]]],["list",[["sym","rule"],["sym","cstr"],["rule",["rept",[["chclass",[["flip"],"\\\\","\\]",["cclass","\\s"]]],[1,-1,"+"]]]]]],["list",[["sym","rule"],["sym","blank"],["rule",["rept",[["cclass","\\s"],[1,-1,"+"]]]]]],["list",[["sym","rule"],["sym","Rule"],["rule",["token",[":{",["rept",[["branch",[["rtoken","ws"],["ctoken","ratom"]]],[0,-1,"*"]]],"}"]]]]],["list",[["sym","rule"],["sym","ratom"],["rule",["branch",[["ctoken","Group"],["ctoken","look"],["ctoken","rept"],["ctoken","any"],["ctoken","branch"],["ctoken","lbranch"],["ctoken","cclass"],["ctoken","Alias"],["token",["<",["ctoken","ctoken"],">"]],["token",["<.",["ctoken","rtoken"],">"]],["token",["<!",["ctoken","gtoken"],">"]],["ctoken","Chclass"],["ctoken","Strs"],["ctoken","assert"],["ctoken","Action"],["ctoken","Str"],["ctoken","char"],["ctoken","chars"]]]]]],["list",[["sym","rule"],["sym","Group"],["rule",["token",["(",["rept",[["branch",[["rtoken","ws"],["ctoken","ratom"]]],[0,-1,"*"]]],")"]]]]],["list",[["sym","rule"],["sym","look"],["rule",["strs",[]]]]],["list",[["sym","rule"],["sym","rept"],["rule",["strs",[]]]]],["list",[["sym","rule"],["sym","any"],["rule","."]]],["list",[["sym","rule"],["sym","branch"],["rule","||"]]],["list",[["sym","rule"],["sym","lbranch"],["rule","|"]]],["list",[["sym","rule"],["sym","cclass"],["rule",["token",["\\",["chclass",["d","s","w","D","S","W"]]]]]]],["list",[["sym","rule"],["sym","ctoken"],["rule",["rtoken","name"]]]],["list",[["sym","rule"],["sym","rtoken"],["rule",["rtoken","name"]]]],["list",[["sym","rule"],["sym","gtoken"],["rule",["rtoken","name"]]]],["list",[["sym","rule"],["sym","name"],["rule",["token",[["chclass",[["range","a-z"],["range","A-Z"],"_"]],["rept",[["chclass",[["range","a-z"],["range","A-Z"],"_","\\-",":"]],[0,-1,"*"]]]]]]]],["list",[["sym","rule"],["sym","Alias"],["rule",["token",['$<',["ctoken","name"],">=",["ctoken","ratom"]]]]]],["list",[["sym","rule"],["sym","Strs"],["rule",["token",["<",["rept",[["cclass","\\s"],[1,-1,"+"]]],["rept",[["branch",[["ctoken","str"],["rept",[["cclass","\\s"],[1,-1,"+"]]]]],[1,-1,"+"]]],">"]]]]],["list",[["sym","rule"],["sym","str"],["rule",["rept",[["chclass",[["flip"],"\\>",["cclass","\\s"]]],[1,-1,"+"]]]]]],["list",[["sym","rule"],["sym","Chclass"],["rule",["token",["[",["rept",[["ctoken","flip"],[0,1,"?"]]],["rept",[["branch",[["rtoken","ws"],["ctoken","catom"]]],[1,-1,"+"]]],"]"]]]]],["list",[["sym","rule"],["sym","flip"],["rule","^"]]],["list",[["sym","rule"],["sym","catom"],["rule",["branch",[["ctoken","cclass"],["ctoken","char"],["ctoken","range"],["ctoken","cchar"]]]]]],["list",[["sym","rule"],["sym","range"],["rule",["token",[["cclass","\\S"],"-",["cclass","\\S"]]]]]],["list",[["sym","rule"],["sym","cchar"],["rule",["chclass",[["flip"],"\\\\","\\]",["cclass","\\s"]]]]]],["list",[["sym","rule"],["sym","assert"],["rule",["strs",[]]]]],["list",[["sym","rule"],["sym","Action"],["rule",["token",["{",["rept",[["branch",[["rtoken","ws"],["ctoken","atom"]]],[0,-1,"*"]]],"}"]]]]],["list",[["sym","rule"],["sym","chars"],["rule",["rtoken","name"]]]],["list",[["sym","end"],["sym","Spp"]]]]];

=head1 AUTHOR

Michael Song, C<< <10435916 at qq.com> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-spp at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=Spp>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.

=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc Spp::Rule

You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=Spp>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/Spp>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/Spp>

=item * Search CPAN

L<http://search.cpan.org/dist/Spp/>

=back

=head1 ACKNOWLEDGEMENTS

=head1 LICENSE AND COPYRIGHT

Copyright 2015 Michael Song.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=cut

1; # End of Spp::Rule
