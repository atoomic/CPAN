package Spp;

use Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(spp repl);

=head1 NAME

Spp - String prepare Parser

=head1 VERSION

Version 0.01

=cut

our $VERSION = '0.02';

=head1 SYNOPSIS

Spp is a programming language, but also is parser tool of programming 
language.

    use Spp qw(spp repl);

    if ($ARGV[0]) { spp($ARGV[0]) } else { repl() }

then shell would ouput:

    This is Spp REPL implement with Perl5. type 'exit' to exit.
    >>>

=head1 EXPORT

    spp
    repl

=cut


use 5.020;
use Carp qw(croak);
use experimental qw(switch autoderef);
use Scalar::Util qw(looks_like_number);
use Spp::Tools;
use Spp::Optimizer   qw(opt_atom);
use Spp::AtomToValue qw(atom_to_value atoms_to_value);
use Spp::ValueToAtom qw(value_to_atom);
use Spp::Rule        qw($Spp_rule);

#######################################################
## global variable
#######################################################
# Rule name
our $RULE = 'Spp';
# Rule Door of token name
our $DOOR = 'TOP';
# Boot context
our $MAIN = 'Main';
# Current context
our $CONTEXT = $MAIN;
# Symbol Table, use Hash
our $ST = { $MAIN => {} };
# context stack, use Array
our $STACK = [ $MAIN ];
# current Call name
our $CALL = uuid();
# Call stack
our $CSTACK = [ $CALL ];
# current Block
our $BLOCK = uuid();
# Block stack
our $BSTACK = [ $BLOCK ];
# test case counter
our $TC = 0;
# Debug mode
our $DEBUG = 0;
# register Main as context name
$ST->{$MAIN}{$MAIN} = [ 'context', $MAIN ];
# restrain return table
$ST->{$CALL} = {};
# register Block in symbol table
$ST->{$BLOCK} = {};
# register ARGV to Symbol table
$ST->{$MAIN}{'ARGV'} = value_to_atom([ @ARGV ]);

###########################################
# built-in sub map
###########################################
our $Op_map = {
  '!='   => sub { eval_ne(@_)     },
  '+'    => sub { eval_add(@_)    },
  '+='   => sub { eval_inc(@_)    },
  '-'    => sub { eval_sub(@_)    },
  '<'    => sub { eval_lt(@_)     },
  '<<'   => sub { eval_push(@_)   },
  '<='   => sub { eval_le(@_)     },
  '='    => sub { eval_assign(@_) },
  '=='   => sub { eval_eq(@_)     },
  '>'    => sub { eval_gt(@_)     },
  '>='   => sub { eval_ge(@_)     },
  '>>'   => sub { eval_unshift(@_)},
  'and'  => sub { eval_and(@_)    },
  '&&'   => sub { eval_and(@_)    },
  'in'   => sub { eval_in(@_)     },
  'is'   => sub { eval_is(@_)     },
  'or'   => sub { eval_or(@_)     },
  '||'   => sub { eval_or(@_)     },
  '~~'   => sub { eval_match(@_)  },
};

our $Spp_map = {
  '!='      => sub { eval_ne(@_)      },
  '+'       => sub { eval_add(@_)     },
  '+='      => sub { eval_inc(@_)     },
  '-'       => sub { eval_sub(@_)     },
  '<'       => sub { eval_lt(@_)      },
  '<='      => sub { eval_le(@_)      },
  '=='      => sub { eval_eq(@_)      },
  '>'       => sub { eval_gt(@_)      },
  '>='      => sub { eval_ge(@_)      },
  'and'     => sub { eval_and(@_)     },
  '&&'      => sub { eval_and(@_)     },
  'begin'   => sub { eval_exprs(@_)   },
  'block'   => sub { $BLOCK           },
  'bool'    => sub { eval_bool(@_)    },
  'break'   => sub { eval_break()     },
  'call'    => sub { $CALL            },
  'case'    => sub { eval_case(@_)    },
  'context' => sub { eval_context(@_) },
  'def'     => sub { eval_def(@_)     },
  'defined' => sub { eval_defined(@_) },
  'delete'  => sub { eval_delete(@_)  },
  'end'     => sub { eval_end(@_)     },
  'to-i'    => sub { eval_to_i(@_)    },
  'exit'    => sub { exit()           },
  'exists'  => sub { eval_exists(@_)  },
  'for'     => sub { eval_for(@_)     },
  'if'      => sub { eval_if(@_)      },
  'is-false'=> sub { eval_is_false(@_)},
  'is-true' => sub { eval_is_true(@_) },
  'is-str'  => sub { eval_is_str(@_)  },
  'is-array'=> sub { eval_is_array(@_)},
  'import'  => sub { eval_import(@_)  },
  'load'    => sub { eval_load(@_)    },
  'match'   => sub { eval_match(@_)   },
  'my'      => sub { eval_my(@_)      },
  'next'    => sub { eval_next()      },
  'not'     => sub { eval_not(@_)     },
  'ok'      => sub { eval_ok(@_)      },
  'or'      => sub { eval_or(@_)      },
  '||'      => sub { eval_or(@_)      },
  'uuid'    => sub { uuid()           },
  'return'  => sub { eval_return(@_)  },
  'rule'    => sub { eval_rule(@_)    },
  'say'     => sub { eval_say(@_)     },
  'set'     => sub { eval_set(@_)     },
  'shift'   => sub { eval_shift(@_)   },
  'trim'    => sub { eval_trim(@_)    },
  'type'    => sub { eval_type(@_)    },
  'value'   => sub { eval_value(@_)   },
  'while'   => sub { eval_while(@_)   },
};

our $Perl_map = {
  'eval'      => sub { eval_atom(@_)   },
  'fill'      => sub { fill_array(@_)  },
  'first'     => sub { first(@_)       },
  'format'    => sub { sprintf(@_)     },
  'from-json' => sub { decode_json(@_) },
  'index'     => sub { array_index(@_) },
  'join'      => sub { perl_join(@_)   },
  'last'      => sub { tail(@_)        },
  'len'       => sub { len(@_)         },
  'load_file' => sub { load_file(@_)   },
  'max'       => sub { perl_max(@_)    },
  'opt'       => sub { opt_atom(@_)    },
  'read-file' => sub { read_file(@_)   },
  'rest'      => sub { rest(@_)        },
  'see'       => sub { see(@_)         },
  'split'     => sub { perl_split(@_)  },
  'substr'    => sub { perl_substr(@_) },
  'to-json'   => sub { encode_json(@_) },
  'to-rule'   => sub { to_rule(@_)     },
  'write-file'=> sub { write_file(@_)  },
  'zip'       => sub { perl_zip(@_)    },
};

sub atom_to_str {
  my $atom = shift;
  my $type = type($atom);
  my $value = value($atom);
  return atoms_to_strs($atom) if is_perl_array($type);
  given ($type) {
    when ('dot')     { return '.' }
    when ('any')     { return '.' }
    when ('nil')     { return 'nil' }
    when ('true')    { return 'true' }
    when ('false')   { return 'false' }
    when ('sym')     { return $value }
    when ('context') { return $value }
    when ('assert')  { return $value }
    when ('cclass')  { return $value }
    when ('ctoken')  { return "<" . $value . ">" }
    when ('rtoken')  { return "<." . $value .">" }
    when ('gtoken')  { return "<!" . $value . ">" }
    when ('int')     { return "$value" }
    when ('ref')     { return join($value, '.') }
    when ('str')     { return str_to_str($value) }
    when ('hash')    { return hash_to_str($value) }
    when ('lambda')  { return lambda_to_str($value) }
    when ('array')   { return array_to_str($value) }
    when ('hash')    { return hash_to_str($value) }
    when ('list')    { return list_to_str($value) }
    when ('exprs')   { return list_to_str($value) }
    when ('string')  { return string_to_str($value) }
    when ('rule')    { return rule_to_str($value) }
    when ('token')   { return token_to_str($value) }
    when ('group')   { return group_to_str($value) }
    when ('branch')  { return branch_to_str($value) }
    when ('lbranch') { return lbranch_to_str($value) }
    when ('rept')    { return rept_to_str($value) }
    when ('look')    { return look_to_str($value) }
    when ('strs')    { return strs_to_str($value) }
    when ('alias')   { return alias_to_str($value) }
    when ('chclass') { return chclass_to_str($value) }
    when ('action')  { return atoms_to_strs($value) }
    when ('char')    { return char_to_str($value) }
    default { error("Unknown atom to str: $type") }
  }
}

sub string_to_str {
  my $value = shift;
  return atom_to_str(eval_string($value));
}

sub lambda_to_str {
  my $value = shift;
  my ($args, $exprs) = @{$value};
  my $args_strs = atoms_to_strs($args);
  my $args_str = perl_join($args_strs, ' ');
  my $exprs_strs = atoms_to_strs($exprs);
  my $exprs_str = perl_join($exprs_strs, ' ');
  return "(def ($args_str) $exprs_str)";
}

sub atoms_to_strs {
  my $atoms = shift;
  my $strs = [];
  for my $atom (values $atoms) {
    push $strs, atom_to_str($atom);
  }
  return $strs;
}

sub hash_to_str {
  my $hash = shift;
  my $strs = [];
  for my $pair (@{$hash}) {
    my ($key, $value) = @{$pair};
    my $key_str = atom_to_str($key);
    my $value_str = atom_to_str($value);
    my $pair_str = "$key_str => $value_str";
    push $strs, $pair_str;
  }
  my $hash_str = perl_join($strs, ',');
  return "{$hash_str}";
}

sub list_to_str {
  my $atoms = shift;
  my $strs = atoms_to_strs($atoms);
  my $list_str = perl_join($strs, ' ');
  return "($list_str)";
}

sub array_to_str {
  my $values = shift;
  my $strs = atoms_to_strs($values);
  my $array_str = perl_join($strs, ',');
  return "[$array_str]";
}

sub strchar_to_str {
  my $char = shift;
  given ($char) {
    when ("\n") { return '\\n'  }
    when ("\t") { return '\\t'  }
    when ("\r") { return '\\r'  }
    when ("\\") { return '\\\\' }
    when ("'")  { return "\\'"  }
    default { return $char }
  }
}

sub char_to_str {
  my $char = shift;
  given ($char) {
    when ("\n") { return '\\n' }
    when ("\t") { return '\\t' }
    when ("\r") { return '\\r' }
    default { return "\\$char" }
  }
}

sub to_chars {
  my $str = shift;
  my @chars = split '', $str;
  return [ @chars ];
}

sub str_to_str {
  my $str = shift;
  my $chars = [];
  for my $char (values to_chars($str)) {
    push $chars, strchar_to_str($char);
  }
  my $str_str = perl_join($chars);
  return "'$str'";
}

sub rule_to_str {
  my $atom = shift;
  my $atom_str = atom_to_str($atom);
  return ":{ $atom_str }";
}

sub token_to_str {
  my $token = shift;
  my $strs = atoms_to_strs($token);
  my $token_str = join ' ', @{$strs};
  return $token_str;
}

sub group_to_str {
  my $tokens = shift;
  my @strs;
  for my $token (values $tokens) {
    push @strs, atom_to_str($token);
  }
  my $group_str = join ' ', @strs;
  return "( $group_str )";
}

sub lbranch_to_str {
  my $branches = shift;
  my @strs;
  for my $token (@{$branches}) {
    push @strs, atom_to_str($token);
  }
  my $branch_str = join ' | ', @strs;
  return $branch_str;
}

sub branch_to_str {
  my $branches = shift;
  my @strs;
  for my $token (@{$branches}) {
    push @strs, atom_to_str($token);
  }
  my $branch_str = join ' || ', @strs;
  return $branch_str;
}

sub rept_to_str {
   my $rept = shift;
   my $atom_str = atom_to_str(first($rept));
   my $rept_str = tail(tail($rept));
   return ($atom_str . $rept_str);
}

sub look_to_str {
   my $look = shift;
   my $rept_str = rept_to_str($look);
   my $look_str = atom_to_str($look->[2]);
   return "$rept_str $look_str";
}

sub strs_to_str {
  my $atoms = shift;
  my $strs_str = join ' ', @{$atoms};
  return "< $strs_str >";
}

sub alias_to_str {
  my $atoms = shift;
  my ($alias_name, $atom) = @{$atoms};
  my $atom_str = atom_to_str($atom);
  return "<$alias_name>=$atom_str";
}

sub chclass_to_str {
  my $atoms = shift;
  my @chclass_list;
  for my $atom (values $atoms) {
    if (type($atom) eq 'flip') {
      push @chclass_list, '^';
    } else {
      push @chclass_list, value($atom);
    }
  }
  my $chclass_str = join '', @chclass_list;
  return "[$chclass_str]";
}

######################################
# basic tool of match
######################################

sub get_token_atom {
  my $name = shift;
  for my $context (values $STACK) {
    if (exists $ST->{$context}{$name}) {
      my $token = $ST->{$context}{$name};
      return value($token) if is_rule($token);
    }
  }
  error "token: <$name> not defined!";
}

sub match_rule {
  my ($rule, $match_str) = @_;
  if ( trim($match_str) eq '') { return [] }
  my $cursor = create_cursor($match_str);
  in_context($rule);
  my $door_atom = get_token_atom($DOOR);
  my $match = match_atom($door_atom, $cursor);
  out_context($rule);
  if ($DEBUG == 2) {
    # show_match_log($cursor);
  }
  if ( is_false($match) ) {
    say 'Could not match';
    # show_match_log($cursor);
  }
  return $match;
}

sub create_cursor {
  my $str = shift;
  my $trim_str = trim($str);
  return {
    STR  => $trim_str,
    POS  => 0,
    LEN  => len($trim_str),
    LOG => [],
  };
}

sub eval_to_i {
  my $args = shift;
  my $atom = eval_atom(first($args));
  if (is_str($atom)) {
    if (looks_like_number(value($atom))) {
      return ['int', 0 + value($atom) ];
    }
    my $value = value($atom);
    error("could not transfer str: $value to number");
  }
  return $atom if is_int($atom);
  my $type = type($atom);
  error("could not transfer $type type to number"); 
}

sub in_context {
  my $name = shift;
  unless (exists $ST->{$MAIN}{$name}) {
    $ST->{$MAIN}{$name} = ['context', $name];
    $ST->{$name} = {};
  }
  $CONTEXT = $name;
  unshift $STACK, $name;
  return ['context', $name];
}

sub out_context {
  my $name = shift;
  if ($CONTEXT eq $name) {
    shift $STACK;
    $CONTEXT = first($STACK);
    return ['context', $name];
  }
  error("Could not end $name from $CONTEXT");
}

sub in_call {
  my $context = shift;
  unshift $CSTACK, $context;
  $CALL = $context;
  unless (exists $ST->{$context}) {
    $ST->{$context} = {};
    unshift $STACK, $context;
    $CONTEXT = $context;
    return ['context', $context];
  }
  error("into exists block: $context");
}

sub out_call {
  my $context = shift;
  shift $CSTACK;
  $CALL = first($CSTACK);
  if ($context eq $CONTEXT) {
    delete $ST->{$context};
    shift $STACK;
    $CONTEXT = first($STACK);
    return ['context', $context];
  }
  error("Out block $context != $CONTEXT");
}

sub in_block {
  my $context = shift;
  unless (exists $ST->{$context}) {
    $ST->{$context} = {};
    unshift $STACK, $context;
    $CONTEXT = $context;
    $BLOCK = $context;
    unshift $BSTACK, $context;
    return ['context', $context];
  }
  error("into block exists: $context");
}

sub out_block {
  my $context = shift;
  if ($context eq $CONTEXT) {
    delete $ST->{$context};
    shift $STACK;
    shift $BSTACK;
    $CONTEXT = first($STACK);
    $BLOCK = first($BSTACK);
    return ['context', $context];
  }
  error("Out block $context != $CONTEXT");
}

sub eval_local_declare {
  my ($sym, $value) = @_;
  my $name = value($sym);
  if (exists $ST->{$CONTEXT}{$name}) {
    error("symbol Have defined: $name");
  }
  $ST->{$CONTEXT}{$name} = $value;
  return $sym;
}

sub eval_multi_local_declare {
  my ($syms, $values) = @_;
  return ['true'] if len($syms) == 0;
  if (all_is_sym($syms)) {
    for my $sym_value (values perl_zip($syms, $values)) {
      my ($sym, $value) = @{$sym_value};
      eval_local_declare($sym, $value);
    }
    return ['list', $syms];
  }
  error("only could bind symbol: $syms");
}

sub eval_sym_assign {
  my ($sym, $value) = @_;
  my $name = value($sym);
  for my $context (values $STACK) {
    if (exists $ST->{$context}{$name}) {
      $ST->{$context}{$name} = $value;
      return $sym;
    }
  }
  error("Assign undefined symbol: $name");
}

sub eval_syms_assign {
  my ($syms, $values) = @_;
  for my $sym_value (values zip($syms, $values)) {
    my ($sym, $value) = @{$sym_value};
    eval_sym_assign($sym, $value);
  }
  return ['list', $syms];
}

sub to_rule {
  my ($grammar_file, $rule_file) = @_;
  my $parse_str = read_file($grammar_file);
  my $match_ast = match_rule($RULE, $parse_str);
  if (is_match($match_ast)) {
    # see $match_ast;
    my $opt_ast  = opt_atom($match_ast);
    if (is_same($opt_ast, $Spp_rule)) {
      say "ok, to-rule make same rule with spp rule";
    }
    write_file($rule_file, to_str($opt_ast));
    return ['true'];
  }
  error("Could not transfer $grammar_file to rules");
}

##########################################
# Debug routine
##########################################

sub match_atom {
  my ($atom, $cursor) = @_;
  my $match = _match_atom($atom, $cursor);
  my $flag = 1;
  $flag = 0 if is_false($match);
  my $pos = $cursor->{POS};
  push $cursor->{LOG}, [ $flag, $atom, $pos ];
  return $match;
}

sub name_match {
  my ( $name, $match ) = @_;
  return ['false']         if is_fail($match);
  return ['true']          if is_true($match);
  return [$name, $match]   if is_str($match);
  return $match            if $name =~ /^[a-z_]/;
  return [$name, [$match]] if is_match_atom($match);
  return [$name, $match];
}

sub gather_match {
  my ( $gather, $match ) = @_;
  return $match if is_fail($match);
  return $match if is_true($gather);
  if ( is_str($gather) ) {
    if ( is_true($match) ) { return $gather }
    if ( is_str($match) ) { return $gather . $match }
    if ( is_match_atom($match) )  { return $match }
    if ( is_match_atoms($match) ) { return $match }
  }
  if (is_true($match) or is_str($match)) { return $gather }
  if ( is_match_atom($gather) ) {
    if ( is_match_atom($match) ) { return [ $gather, $match ] }
    if ( is_match_atoms($match) ) { return [ $gather, @{$match} ] }
  }
  if ( is_match_atoms($gather) ) {
    if ( is_match_atom($match) ) { return [ @{$gather}, $match ] }
    if ( is_match_atoms($match) ) { return [ @{$gather}, @{$match} ] }
  }
  error([$gather,$match]);
}

######################
# match interface
######################

sub _match_atom {
  my ($atom, $cursor) = @_;
  my $type = type($atom);
  my $value = value($atom);
  given ($type) {
    when ('true')    { return ['true']  }
    when ('false')   { return ['false'] }
    when ('any')     { return match_any($cursor) }
    when ('char')    { return match_str($value, $cursor)   }
    when ('str')     { return match_str($value, $cursor)    }
    when ('token')   { return match_token($value, $cursor)  }
    when ('group')   { return match_token($value, $cursor)  }
    when ('branch')  { return match_branch($value, $cursor) }
    when ('lbranch') { return match_lbranch($value, $cursor)}
    when ('rept')    { return match_rept($value, $cursor)   }
    when ('look')    { return match_look($value, $cursor)   }
    when ('strs')    { return match_strs($value, $cursor)   }
    when ('chclass') { return match_chclass($value, $cursor)}
    when ('alias')   { return match_alias($value, $cursor)  }
    when ('action')  { return match_action($value, $cursor) }
    when ('ctoken')  { return match_ctoken($value, $cursor) }
    when ('rtoken')  { return match_rtoken($value, $cursor) }
    when ('gtoken')  { return match_gtoken($value, $cursor) }
    when ('assert')  { return match_assert($value, $cursor) }
    when ('cclass')  { return match_cclass($value, $cursor) }
    default { error("Unknown match atom name: $type") }
  }
}

sub match_ctoken {
  my ($name, $cursor) = @_;
  my $atom = get_token_atom($name);
  my $match = match_atom($atom, $cursor);
  return name_match($name, $match);
}

sub match_rtoken {
  my ($name, $cursor) = @_;
  my $atom = get_token_atom($name);
  my $pos_cache = $cursor->{POS};
  my $match = match_atom($atom, $cursor);
  if (is_match($match)) {
    my $str = $cursor->{STR};
    my $pos = $cursor->{POS};
    my $pos_len = $pos - $pos_cache;
    my $match_str = substr($str, $pos_cache, $pos_len);
    return $match_str;
  }
  return ['false'];
}

sub match_alias {
  my ($ast, $cursor) = @_;
  my ($alias_name, $alias_atom) = @{ $ast };
  my $match = ['false'];
  if (type($alias_atom) eq 'ctoken') {
    my $token_name = value($alias_atom);
    my $token_atom = get_token_atom($token_name);
    $match = match_atom($token_atom, $cursor);
  } else {
    $match = match_atom($alias_atom, $cursor);
  }
  return name_match($alias_name, $match);
}

sub match_gtoken {
  my ($name, $cursor) = @_;
  my $atom = get_token_atom($name);
  my $pos_cache = $cursor->{POS};
  my $match = match_atom($atom, $cursor);
  $cursor->{POS} = $pos_cache;
  if (is_false($match)) { return ['true'] }
  return ['false'];
}

sub match_token {
  my ($atoms, $cursor) = @_;
  my $gather = ['true'];
  foreach my $atom (values $atoms) {
    my $match = match_atom($atom, $cursor);
    if (is_false($match)) { return ['false'] }
    $gather = gather_match($gather, $match);
  }
  return $gather;
}

# match branch, first match would return
sub match_branch {
  my ($branch, $cursor) = @_;
  my $pos_cache = $cursor->{POS};
  foreach my $atom (values $branch) {
    my $match = match_atom($atom, $cursor);
    if (is_match($match)) { return $match }
    $cursor->{POS} = $pos_cache;
  }
  return ['false'];
}

sub match_lbranch {
  my ($branch, $cursor) = @_;
  my $pos_cache = $cursor->{POS};
  my $max_len = 0;
  my $max_match = ['false'];
  foreach my $atom (values $branch) {
    my $match = match_atom($atom, $cursor);
    if (is_match($match)) {
      # if match ok, get match str length
      my $match_str_len = $cursor->{POS} - $pos_cache;
      # if match str longest than have even matched length
      if ($match_str_len >= $max_len) {
        $max_len = $match_str_len;
        $max_match = $match;
      }
      # reset Pos foreach next match
      $cursor->{POS} = $pos_cache;
    }
  }
  $cursor->{POS} += $max_len;
  return $max_match;
}

sub match_strs {
  my ($strs, $cursor) = @_;
  my $max_len = 0;
  my $max_str = ['false'];
  foreach my $str (values $strs) {
    my $len = len($str);
    my $apply_str = apply_char($len, $cursor);
    if ($str eq $apply_str) {
      if ($len >= $max_len) {
        $max_len = $len;
        $max_str = $str;
      }
    }
  }
  # if not match ok, then Pos not change and return ['false']
  $cursor->{POS} += $max_len;
  return $max_str;
}

sub match_look {
  my ($look, $cursor) = @_;
  my ($atom, $rept, $look_atom) = @{$look};
  my $gather = ['true'];
  my $match_time = 0;
  my ($min_time, $max_time) = @{$rept};
  if ($match_time >= $min_time) {
    my $match = match_atom($look_atom, $cursor);
    if (is_match($match)) {
      return gather_match($gather, $match);
    }
  }

  while ($match_time != $max_time) {
    my $pos_cache = $cursor->{POS};
    my $match = match_atom($atom, $cursor);

    if (is_false($match)) {
      return ['false'] if $match_time < $min_time;
      $cursor->{POS} = $pos_cache;
      my $look_match = match_atom($look_atom, $cursor);
      return ['false'] if is_false($look_match);
      return gather_match($gather, $look_match);
    }

    $match_time += 1;
    $gather = gather_match($gather, $match);

    if ($match_time >= $min_time) {
      $pos_cache = $cursor->{POS};
      my $look_match = match_atom($look_atom, $cursor);
      if (is_match($look_match)) {
        return gather_match($gather, $look_match);
      }
      $cursor->{POS} = $pos_cache;
    }
  }
  return ['false'];
}

sub match_rept {
  my ($atom_rept, $cursor) = @_;
  my $gather = ['true'];
  my $match_time = 0;
  my ($atom, $rept) = @{$atom_rept};
  my ($min_time, $max_time) = @{$rept};

  while ($match_time != $max_time) {
    my $pos_cache = $cursor->{POS};
    my $match = match_atom($atom, $cursor);
    if (is_false($match)) {
      if ($match_time < $min_time) { return ['false'] }
      $cursor->{POS} = $pos_cache;
      return $gather;
    } else {
      $match_time += 1;
      $gather = gather_match($gather, $match);
    }
  }
  return $gather;
}

sub match_action {
  my ($exprs, $cursor) = @_;
  my $exprs_value = eval_atom($exprs);
  my $type = type($exprs_value);
  my $value = value($exprs_value);
  given ($type) {
    when ('nil')   { return ['false']                    }
    when ('true')  { return ['true']                     }
    when ('false') { return ['false']                    }
    when ('str')   { return match_str($value, $cursor)   }
    when ('array') { return match_array($value, $cursor) }
    default { error("Not implement action: $type")       }
  }
}

sub match_array {
  my ($array, $cursor) = @_;
  my $strs = [];
  foreach my $str (values $array) {
    push $strs, value($str) if is_str($str);
  }
  return match_strs($strs, $cursor);
}

sub match_any {
  my $cursor = shift;
  my $char = apply_char(1, $cursor);
  if (len($char) == 1) {
    $cursor->{POS}++;
    return $char;
  }
  return ['false'];
}

sub match_str {
  my ($str, $cursor) = @_;
  my $str_len = len($str);
  my $apply_str = apply_char($str_len, $cursor);
  if ($str eq $apply_str) {
    $cursor->{POS} += $str_len;
    return $str;
  }
  return ['false']
}

sub match_chclass {
  my ($atom, $cursor) = @_;
  my $char = apply_char(1, $cursor);
  my $class_str = chclass_to_str($atom);
  if ($char =~ /$class_str/) {
    $cursor->{POS}++;
    return $char;
  }
  return ['false'];
}

sub match_cclass {
  my ($cclass, $cursor) = @_;
  my $char = apply_char(1, $cursor);
  if ($char =~ /$cclass/) {
    $cursor->{POS}++;
    return $char;
  }
  return ['false'];
}

sub match_assert {
  my ($str, $cursor) = @_;
  given ($str) {
    when ('^') { return bool($cursor->{POS} == 0) }
    when ('$') {
      return bool($cursor->{POS} >= $cursor->{LEN});
    }
    when ('^^') {
      # say $cursor->{STR};
      # say ord(apply_char(-1, $cursor));
      return ['true'] if apply_char(-1, $cursor) =~ /\n/;
      return ['true'] if $cursor->{POS} == 0;
      return ['false'];
    }
    when ('$$') {
      return ['true'] if apply_char(1, $cursor) =~ /\n/;
      return ['true'] if $cursor->{POS} >= $cursor->{LEN};
      return ['false'];
    }
    default { error("Unknown assert str: $str") }
  }
}

#########################################
# eval atom
#########################################

sub eval_atom {
  my $atom = shift;
  my $type = type($atom);
  my $value = value($atom);
  given ($type) {
    when ('list')   { return eval_list($value)   }
    when ('sym')    { return eval_sym($value)    }
    when ('string') { return eval_string($value) }
    when ('array')  { return eval_array($value)  }
    when ('hash')   { return eval_hash($value)   }
    when ('exprs')  { return eval_exprs($value)  }
    default { return $atom }
  }
}

sub eval_args {
  my $atoms = shift;
  my $atoms_array = [];
  for my $atom (values $atoms) {
    push $atoms_array, eval_atom($atom);
  }
  return $atoms_array;
}

sub eval_list {
  my $atoms = shift;
  if ( len($atoms) == 3 and is_sym(second($atoms)) ) {
    my $op_name = value(second($atoms));
    if (exists $Op_map->{$op_name}) {
      my $op_call = $Op_map->{$op_name};
      if (is_func($op_call)) {
        my $args = [ first($atoms), tail($atoms) ];
        return $op_call->($args);
      }
    }
  }
  # tail if (return 1 if 1)
  if (len($atoms) > 2 and is_if($atoms->[-2])) {
    my $cond_expr = tail($atoms);
    return ['false'] if is_fail(eval_atom($cond_expr));
    my $true_expr = subarray($atoms, 0, -3);
    return eval_list($true_expr);
  }

  my $head_atom = first($atoms);
  if (is_sym($head_atom)) {
    my $name = value($head_atom);
    if (exists $Spp_map->{$name}) {
      my $eval_call = $Spp_map->{$name};
      if (is_func($eval_call)) {
        my $args = rest($atoms);
        return $eval_call->($args);
      }
    }

    if (exists $Perl_map->{$name}) {
      my $perl_call = $Perl_map->{$name};
      if (is_func($perl_call)) {
        my $args = eval_args(rest($atoms));
        # see $args;
        my $values = atoms_to_value($args);
        my $return_value = $perl_call->(@{$values});
        return value_to_atom($return_value);
      }
    }
  }

  # user defined method
  my $head_value = eval_atom($head_atom);
  if ( is_lambda($head_value) ) {
    my $lambda_exprs = value($head_value);
    my $args = eval_args(rest($atoms));
    return def_call($lambda_exprs, $args);
  }

  # if symbol is imported (use package [import symbols])
  # (import symbols values)
  if ( is_ref($head_value) ) {
    my ($context, $sym) = @{ value($head_value) };
    in_context($context);
    my $args = unshift rest($atoms), $sym;
    my $return_value = eval_list($args);
    out_context($context);
    return $return_value;
  }

  # call other package method (context.method values)
  if (is_context($head_value)) {
    if ( is_dot(second($atoms)) ) {
      my $context = value($head_value);
      my $args = subarray($atoms, 2, -1);
      in_context($context);
      my $return_value = eval_list($args);
      out_context($context);
      return $return_value;
    }
  }

  my $args = eval_args(rest($atoms));
  if (is_str($head_value)) {
    # str call use str itself
    my $value = value($head_value);
    return str_call($value, $args);
  }
  if (is_array($head_value)) {
    # array_call use ['array', ...]
    return array_call($head_value, $args);
  }
  if (is_hash($head_value)) {
    # hash call use ['hash', ...]
    return hash_call($head_value, $args);
  }
  my $head_value_str = value($head_value);
  error("Have not implement ($head_value_str ..)");
}

# if exists sym records, return its value
# else should return symbol itself
sub eval_sym {
  my $name = shift;
  for my $context (values $STACK) {
    if (exists $ST->{$context}{$name}) {
      return $ST->{$context}{$name};
    }
  }
  return ['sym', $name];
}

sub eval_string {
  my $atoms = shift;
  my $values = eval_args($atoms);
  my $strs = [];
  for my $value (values $values) {
    if (is_str($value)) {
      push $strs, value($value);
    } else {
      push $strs, atom_to_str($value);
    }
  }
  return perl_join($strs);
}

sub eval_array {
  my $atoms = shift;
  my $values = [];
  for my $atom (values $atoms) {
    push $values, eval_atom($atom);
  }
  return ['array', $values];
}

sub eval_hash {
  my $pairs = shift;
  my $hash_value = [];
  for my $pair (values $pairs) {
    my $pair_value = eval_args($pair);
    push $hash_value, $pair_value;
  }
  return ['hash', $hash_value ];
}

sub eval_return {
  my $args = shift;
  my $atom = first($args);
  $ST->{$CALL}{':return'} = 1;
  return eval_atom($atom);
}

sub eval_break {
  $ST->{$BLOCK}{':break'} = 1;
  return ['true'];
}

sub eval_next {
  $ST->{$BLOCK}{':next'} = 1;
  return ['true'];
}

sub eval_exprs {
  my $exprs = shift;
  my $return_value = ['true'];
  for my $expr (values $exprs) {
    $return_value = eval_atom($expr);
    last if $ST->{$CALL}{':return'} == 1;
    last if $ST->{$BLOCK}{':next'} == 1;
    last if $ST->{$BLOCK}{':break'} == 1;
  }
  return $return_value;
}

# call user defined sub or sub interface
sub def_call {
  my ($lambda_exprs, $real_args) = @_;
  my ($formal_args, $exprs) = @{$lambda_exprs};
  my $context = uuid();
  in_call($context);
  eval_multi_local_declare($formal_args, $real_args);
  my $return_value = eval_exprs($exprs);
  out_call($context);
  return $return_value;
}

sub str_call{
  my ($str, $nums) = @_;
  if (is_int(first($nums))) {
    my $index = value(first($nums));
    if (len($nums) == 1) {
      return substr($str, $index, 1);
    }
    if (len($nums) == 2 and is_int(tail($nums))) {
      my $to_index = value(tail($nums));
      my $str_len = $to_index - $index + 1;
      if ($to_index < 0) {
        $str_len = len($str) + $to_index + 1 - $index;
      }
      return substr($str, $index, $str_len);
    }
  }
  error("syntax error str call");
}

# (array 1) (array 1 2) (array 2 -3) (array [1 2]
sub array_call {
  my ($array, $nums) = @_;
  # (array 1)
  if (is_int(first($nums))) {
    my $index = value(first($nums));
    my $array_value = value($array);
    if (len($nums) == 1) {
      my $element = $array_value->[$index];
      return ['nil'] unless $element;
      return $element;
    }
    # (array 1 2) (array 1 -2)
    if (len($nums) == 2 and is_int(tail($nums))) {
      my $to_index = value(tail($nums));
      my $len_array = $to_index - $index + 1;
      if ($to_index < 0) {
        $len_array = len($array_value) + $to_index + 1 + $index;
      }
      my $elements = subarray($array_value, $index, $len_array);
      return ['nil'] if $elements eq '';
      return ['array', $elements];
    }
  }
  # (array [1 2]) => array[1][2]
  if (is_array(first($nums))) {
    my $index_array = value(first($nums));
    if (all_is_int($index_array)) {
      my $indexs = atoms_to_value($index_array);
      my $array_value = [ @{$array} ];
      for my $index (values $indexs) {
        return ['nil'] unless is_array($array_value);
        $array_value = value($array_value);
        $array_value = $array_value->[$index];
      }
      return $array_value;
    }
  }
}

# hash call (hash key) (hash key-one key-two)
sub hash_call {
  my ($hash_atom, $keys) = @_;
  # make a copy
  my $hash = [ @{$hash_atom} ];
  for my $key (values $keys) {
    return ['nil'] unless is_hash($hash);
    my $index_hash = get_hash_key_value($hash, $key);
    $hash = tail($index_hash) if $hash;
  }
  return $hash;
}

sub get_hash_key_value {
  my ($hash, $look_key) = @_;
  if (is_hash($hash)) {
    my $index = 0;
    for my $pair (values value($hash)) {
      my ($key, $value) = @{ $pair };
      return [$index, $value] if is_same($look_key, $key);
      $index++;
    }
    return 0;
  }
  error("Could not get key value escept Hash");
}

sub set_hash_key_value {
  my ($hash, $key, $value) = @_;
  if (is_hash($hash)) {
    my $index_value = get_hash_key_value($hash, $key, $value);
    my $hash_value = value($hash);
    if ($index_value) {
      my $index = first($index_value);
      $hash_value->[$index][1] = $value;
    } else {
      push $hash_value, [[$key, $value]];
    }
    return ['hash', $hash_value ];
  }
}

# (set hash key value)
# only support hash set
sub eval_set {
  my $args = shift;
  if (len($args) > 2) {
    my $atoms = eval_args($args);
    my $hash = first($atoms);
    if (is_hash($hash)) {
      my $value = tail($atoms);
      pop $atoms;
      my $keys = rest($atoms);
      my $key = first($keys);
      # (set hash key value)
      if (len($keys) == 1) {
        return set_hash_key_value($hash, $key, $value);
      }
      # (set hash key1 key2 value)
      if (len($keys) == 2) {
        my $tail_key = tail($keys);
        my $index_value = get_hash_key_value($hash, $key);
        my $sub_value = ['hash',[[$tail_key, $value]]];
        if ($index_value) {
          my ($index, $sub_hash) = @{ $index_value };
          if (is_hash($sub_hash)) {
            $sub_value = set_hash_key_value($sub_hash, $tail_key, $value);
          }
          return set_hash_key_value($hash, $key, $sub_value);
        } else {
          my $hash_value = value($hash);
          push $hash_value, [[ $key, $sub_value ]];
          return ['hash', $hash_value];
        }
      }
    } else {
      error("Spp only implement set <Hash> value");
    }
  }
  error("(set syntax error: argument less than 3)");
}

# add rule value to symbol table
sub eval_rule {
  my $args = shift;
  my $atoms = eval_args($args);
  my ($name, $rule) = @{ $atoms };
  if (is_sym($name) and is_rule($rule)) {
    return eval_local_declare($name, $rule);
  }
  error("Syntax error: (rule .. )");
}

# get Spp value type
sub eval_type {
  my $args = shift;
  my $atom = first($args);
  return type(eval_atom($atom));
}

sub eval_value {
  my $args = shift;
  my $atom = first($args);
  return value_to_atom(value(eval_atom($atom)));
}

sub eval_is {
  my $args = shift;
  my $atoms = eval_args($args);
  my ($sym, $type) = @{ $atoms };
  my $type_str = type($type);
  if ($type_str eq 'str') {
    if (type($sym) eq value($type)) {
      return ['true'];
    }
    return ['false'];
  }
  error("could not compare $type_str, only accept str");
}

sub eval_shift {
  my $args = shift;
  my $sym = first($args);
  my $array = eval_atom($sym);
  my $array_type = type($array);
  if (is_array($array)) {
    my $array_value = value($array);
    shift $array_value;
    # if could not dethory data, should make an copy of array
    return ['array', $array_value];
  }
  error("could not shift $array_type");
}

sub eval_say {
  my $args = shift;
  if (len($args) > 1) {
    my $atoms = eval_args($args);
    my $atoms_str = get_atoms_str($atoms);
    say perl_join($atoms_str);
    return ['true'];
  }
  if (len($args) == 1) {
    my $atom = first($args);
    say atom_to_str(eval_atom($atom));
    return ['true'];
  }
  error("(say with blank argrument");
}

sub eval_context {
  my $args = shift;
  # (context) return current CONTEXT
  return $CONTEXT if len($args) == 0;
  my $atom = first($args);
  my $context = eval_atom($atom);
  my $type_str = type($context);
  # if is sym or existed context
  if (in($type_str, ['sym', 'context'])) {
    my $context_name = tail($context);
    return in_context($context_name);
  }
  error("(context .. ) syntax error");
}

sub eval_assign {
  my $atoms = shift;
  my $head_atom = first($atoms);
  if (is_sym($head_atom)) {
    my $sym = $head_atom;
    my $value = eval_atom(tail($atoms));
    return eval_sym_assign($sym, $value);
  }
  # (x y z) = [1 2 3]
  if (is_list($head_atom)) {
    my $syms = value($head_atom);
    my $array = eval_atom(tail($atoms));
    if (all_is_sym($syms) and is_array($array)) {
      my $values = value($array);
      return eval_syms_assign($syms, $values);
    }
  }
  error("assign syntax error: $atoms");
}

sub eval_end {
  my $args = shift;
  my $atom = first($args);
  if (is_sym($atom)) {
    if ($CONTEXT eq tail($atom)) {
      return out_context($CONTEXT);
    }
    error("out context is not $CONTEXT")
  }
  my $atom_type = type($atom);
  error("Syntax of (end ..): not context $atom_type");
}

sub eval_defined {
  my $args = shift;
  my $atom = first($args);
  # (exists symbol): if it have been defined
  if (is_sym($atom)) {
    my $name = value($atom);
    for my $context (values $STACK) {
      return ['true'] if exists $ST->{$context}{$name};
    }
    return ['false'];
  }
  error("Symtax error: (defined ..)");
}

sub eval_trim {
  my $args = shift;
  my $str_atom = first($args);
  if (is_str($str_atom)) {
    return trim(value($str_atom));
  }
  error "Trim only support String";
}

sub eval_exists {
  my $args = shift;
  my $atoms = eval_args($args);
  my ($atom, $key) = @{$atoms};
  if (is_hash($atom)) {
    return ['true'] if get_hash_key_value($atom, $key);
    return ['false'];
  }
  if (is_str($atom)) {
    return ['true'] if -e value($atom);
    return ['false'];
  }
}

sub eval_eq {
  my $atoms = shift;
  my $values = eval_args($atoms);
  my $first_value = first($values);
  for my $value (values rest($values)) {
    return ['false'] unless is_same($value, $first_value);
  }
  return ['true'];
}

sub eval_ne {
  my $args = shift;
  return ['false'] if is_true(eval_eq($args));
  return ['true'];
}

sub eval_bool {
  my $args = shift;
  my $value = eval_atom(first($args));
  return ['false'] if is_fail($value);
  return ['true'];
}

sub eval_is_false {
  my $args = shift;
  my $atom = eval_atom(first($args));
  return ['true'] if is_false($atom);
  return ['false'];
}

sub eval_is_true {
  my $args = shift;
  my $atom = eval_atom(first($args));
  return ['true'] if is_true($atom);
  return ['false'];
}

sub eval_is_str {
  my $args = shift;
  my $atom = eval_atom(first($args));
  return ['true'] if is_str($atom);
  return ['false'];
}

sub eval_is_array {
  my $args = shift;
  my $atom = eval_atom(first($args));
  return ['true'] if is_array($atom);
  return ['false'];
}

sub eval_not {
  my $args = shift;
  my $atom = eval_bool($args);
  return ['true'] if is_false($atom);
  return ['false'];
}

sub eval_and {
  my $atoms = shift;
  for my $atom (values $atoms) {
    return ['false'] if is_fail(eval_atom($atom));
  }
  return ['true'];
}

sub eval_or {
  my $atoms = shift;
  for my $atom (values $atoms) {
    return ['true'] unless is_fail(eval_atom($atom));
  }
  return ['false'];
}

sub eval_delete {
  my $args = shift;
  if (len($args) == 2) {
    my $eval_args = eval_args($args);
    my ($atom, $key) = @{ $eval_args };
    # (delete hash key)
    if (is_hash($atom)) {
      my $hash_value = value($atom);
      my $new_hash_value = [];
      for my $value (values $hash_value) {
        next if is_same($key, first($value));
        push $new_hash_value, $value;
      }
      return ['hash', $new_hash_value];
    }
    if (is_array($atom)) {
      my $array_value = value($atom);
      my $new_array_value = [];
      for my $value (values $array_value) {
        next if is_same($key, $value);
        push $new_array_value, $value;
      }
      return ['array', $new_array_value];
    }
  }
  # (delete symbol) => undefined
  my $sym = first($args);
  if (len($args) == 1 and is_sym($sym)) {
    my $name = value($sym);
    for my $context (values $STACK) {
      if (exists $ST->{$context}{$name}) {
        delete $ST->{$context}{$name};
        return ['true'];
      }
    }
    return ['false'];
  }
  error("Spp only implement delete symbol or hash key");
}

sub eval_case {
  my $atoms = shift;
  my $case_atom = first($atoms);
  my $case_value = eval_atom($case_atom);
  my $case_exprs = rest($atoms);
  for my $branch (values $case_exprs) {
    if (is_list($branch)) {
      my $branch_exprs = value($branch);
      if (is_when(first($branch_exprs))) {
        my $cond_atom = $branch_exprs->[1];
        if (is_same($case_value, $cond_atom)) {
          my $true_exprs = subarray($branch_exprs, 2, -1);
          return eval_exprs($true_exprs);
        }
      }
      if ( is_else($branch_exprs) ) {
        my $true_exprs = subarray($branch_exprs, 1, -1);
        return eval_exprs($true_exprs);
      }
    }
  }
  return ['false'];
}

sub eval_if {
  my $atoms = shift;
  if (len($atoms) < 2 ) { error("if syntax error: elements < 2") }
  my $cond_expr = $atoms->[0];
  my $true_atoms = [ $atoms->[1] ];
  my $index = 3;
  while ($index < len($atoms)) {
    my $atom = $atoms->[$index];
    if (is_elsif($atom)) {
      if (is_true($cond_expr)) {
        return eval_exprs($true_atoms);
      }
      my $if_exprs = subarray($atoms, $index+1, -1);
      return eval_if($if_exprs);
    }
    if (is_else($atom)) {
      if (is_true($cond_expr)) {
        return eval_atom($true_atoms);
      }
      my $else_atoms = subarray($atoms, $index+1, -1);
      return eval_exprs($else_atoms);
    } else {
      push($true_atoms, $atom);
      $index++;
    }
  }
  return ['false'] if is_fail($cond_expr);
  return eval_exprs($true_atoms);
}

sub eval_while {
  my $atoms = shift;
  if (len($atoms) < 2) { error('(while ...) args less 2') }
  my $guide_expr = first($atoms);
  my $while_exprs = rest($atoms);
  my $return_value = ['true'];
  while (is_true(eval_bool([$guide_expr]))) {
    my $context = uuid();
    in_block($context);
    $return_value = eval_exprs($while_exprs);
    if ($ST->{$CALL}{':return'} == 1) {
      out_block($context);
      return $return_value;
    }
    if ($ST->{$CONTEXT}{':break'} == 1) {
      out_block($context);
      last;
    }
    out_block($context);
  }
  return $return_value;
}

# (for x in array (next if (x eq 2)))
sub eval_for {
  my $atoms = shift;
  if (len($atoms) < 4) {
    error("syntax error for: args less 4");
  }
  my ($var, $in, $atom, @exprs) = @{ $atoms };
  my $exprs = [ @exprs ];
  if (is_in($in)) {
    my $eval_atom = eval_atom($atom);
    if (is_sym($var) and is_array($eval_atom)) {
      my $array_value = value($eval_atom);
      return eval_for_array($var, $array_value, $exprs);
    }
    # (for (key value) in hash (do sth))
    if (is_list($var) and is_hash($eval_atom)) {
      my $pair = value($var);
      my $hash_value = value($eval_atom);
      if (all_is_sym($pair)) {
        return eval_for_hash($pair, $hash_value, $exprs);
      }
    }
  }
  error('for syntax error ..');
}

sub eval_for_array {
  my ($var, $array, $exprs) = @_;
  my $return_value = ['true'];
  for my $element (values $array) {
    my $context = uuid();
    in_block($context);
    eval_local_declare($var, $element);
    $return_value = eval_exprs($exprs);
    if ($ST->{$CALL}{':return'} == 1) {
      out_block($context);
      return $return_value;
    }
    if ($ST->{$CONTEXT}{':break'} == 1) {
      out_block($context);
      last;
    }
    out_block($context);
  }
  return $return_value;
}

# (for x y in hash (do sth))
# hash is saved use perl built-hash
sub eval_for_hash {
  my ($var_pair, $hash, $exprs) = @_;
  my ($k_var, $v_var) = @{$var_pair};
  my $return_value = ['true'];
  # Spp hash implement with Perl array
  for my $pair (values $hash) {
    my ($k, $v) = @{$pair};
    my $context = uuid();
    in_block($context);
    eval_local_declare($k_var, $k);
    eval_local_declare($v_var, $v);
    $return_value = eval_exprs($exprs);
    if ($ST->{$CALL}{':return'} == 1) {
      out_block($context);
      return $return_value;
    }
    if ($ST->{$CONTEXT}{':break'} == 1) {
      out_block($context);
      last;
    }
    out_block($context);
  }
  return $return_value;
}

sub eval_def {
  my $atoms = shift;
  my $def_name = first($atoms);
  if (is_sym($def_name)) {
    my $lambda_exprs = eval_lambda(rest($atoms));
    return eval_local_declare($def_name, $lambda_exprs);
  }
  if (is_list($def_name)) {
    my $lambda_exprs = eval_lambda($atoms);
    return $lambda_exprs;
  }
  error("define syntax (#{atoms})");
}

sub eval_lambda {
  my $atoms = shift;
  # see $atoms;
  if (len($atoms) < 2) { error('define accept 2 argment') }
  my $lambda_exprs = rest($atoms);
  if (len($atoms) == 0) { error('define accept 1 exprssion') }
  if (is_list(first($atoms))) {
    my $args = value(first($atoms));
    if (len($args) == 0) {
      return ['lambda', [[], $lambda_exprs]];
    }
    if (all_is_sym($args)) {
      return ['lambda', [$args, $lambda_exprs]];
    }
    error("lambda args should is symbols");
  }
  error("lambda args should is list");
}

sub eval_ok {
  my $args = shift;
  my ($expr, $expect) = @{$args};
  my $expr_str    = atom_to_str($expr);
  my $expect_str  = atom_to_str($expect);
  my $message_str = "$expr_str == $expect_str";
  my $expr_get    = eval_atom($expr);
  my $expect_get  = eval_atom($expect);
  $TC++;
  if ( is_same($expr_get, $expect_get) ) {
    say("ok $TC - $message_str");
  } else {
    say("not $TC - $message_str");
    my $expr_get_str = atom_to_str($expr_get);
    say("get: $expr_get_str expect: $expect_str");
  }
  return ['true'];
}

sub eval_push {
  my $args = shift;
  my $eval_atoms = eval_args($args);
  my ($array, $element) = @{ $eval_atoms };
  if (is_array($array)) {
    my $array_value = value($array);
    push $array_value, $element;
    return ['array', $array_value];
  }
}

sub eval_unshift {
  my $atoms = shift;
  my $eval_atoms = eval_args($atoms);
  my ($element, $array) = @{ $eval_atoms };
  if (is_array($array)) {
    my $array_value = value($array);
    unshift $array_value, $element;
    return ['array', $array_value];
  }
}

sub eval_my {
  my $atoms = shift;
  if (len($atoms) == 1) {
    # (my x)
    if (is_sym(first($atoms))) {
      my $sym = $atoms->[0];
      return eval_local_declare($sym, ['nil']);
    }
    # (my (x y z))
    if (is_list($atoms->[0])) {
      my $syms = $atoms->[0][1];
      my $values = fill(len($syms), ['nil']);
      return eval_multi_local_declare($syms, $values);
    }
  }

  if (len($atoms) == 2) {
    # (my x 1)
    if (type(first($atoms)) eq 'sym') {
      my $sym = first($atoms);
      my $value = eval_atom(tail($atoms));
      return eval_local_declare($sym, $value);
    }
    # (my (x y z) [1 2])
    if ( is_list(first($atoms))) {
      my $syms = tail(first($atoms));
      my $array = eval_atom(tail($atoms));
      if (is_array($array)) {
        my $values = tail($array);
        return eval_multi_local_declare($syms, $values);
      }
      error("my value not array: $array");
    }
  }
  error("my syntax error: { atoms_to_strs(atoms) }");
}

sub eval_inc {
  my $args = shift;
  my $sym = first($args);
  if (is_sym($sym)) {
    my $atoms = eval_args($args);
    my $add_value = eval_add($atoms);
    return eval_sym_assign($sym, $add_value);
  }
  error("inc syntax error");
}

sub eval_load {
  my $args = shift;
  my $file = first($args);
  if (is_str($file)) {
    my $file_name = value($file);
    my $parse_str = read_file($file_name);
    my $match_ast = match_rule($RULE, $parse_str);
    if ( is_match($match_ast) ) {
      return eval_atom(opt_atom($match_ast));
    }
  }
  error("Load only could accept file name");
}

sub eval_import {
  my $atoms = shift;
  my $args = eval_args($atoms);
  my ($context, $syms) = @{$args};
  if (is_context($context) and is_array($syms)) {
    my $c_name = value($context);
    if ( all_is_sym(value($syms)) ) {
      for my $sym (values value($syms)) {
        my $name = value($sym);
        if ($ST->{$c_name}{$name}) {
          my $ref = ['ref', [$c_name, $sym]];
          $ST->{$CONTEXT}{$name} = $ref;
        } else {
          error("Not find symbol: $name in: $c_name");
        }
      }
      return ['list', value($syms)];
    }
  }
  error("import only support context name");
}

sub eval_add {
  my $args = shift;
  my $atoms = eval_args($args);
  my $values = [ map { value($_) } @{$atoms} ];
  if (all_is_str($atoms)) {
    return perl_join($values);
  }
  if (all_is_int($atoms)) {
    return perl_sum($values);
  }
  if (all_is_array($atoms)) {
    return ['array', perl_concat($values) ];
  }
}

sub perl_sum {
  my $nums = shift;
  my $sum = 0;
  for my $value (values $nums) {
    $sum += $value;
  }
  return $sum;
}

sub perl_concat {
  my $arrays = shift;
  my $concat_array = [];
  for my $array (values $arrays) {
    push $concat_array, @{$array};
  }
  return $concat_array;
}

sub eval_sub {
  my $args = shift;
  my $atoms = eval_args($args);
  if (all_is_int($atoms)) {
    my $values = atoms_to_value($atoms);
    my $value = first($values);
    for my $num (values rest($values)) {
      $value = $value - $num;
    }
    return value_to_atom($value);
  }
  error(" - only implement int")
}

sub eval_zip {
  my $args = shift;
  my $values = atoms_to_value(eval_args($args));
  my $return_value = zip($values);
  return value_to_atom($return_value);
}

sub eval_in {
  my $args = shift;
  my $atoms = eval_args($args);
  my ($element, $array) = @{ $atoms };
  if (is_array($array)) {
    return ['true'] if in($element, value($array));
    return ['false'];
  }
  error("Only implement element in <array>");
}

# todos data structure compare
# int would save as ['int', 1]
sub eval_lt {
  my $args = shift;
  my $values = eval_args($args);
  if (all_is_int($values)) {
    my $first_value = value(first($values));
    for my $value (values rest($values)) {
      return ['false'] if $first_value >= value($value);
    }
    return ['true'];
  }
  error("great than only implement int");
}

sub eval_ge {
  my $args = shift;
  return ['true'] if is_false(eval_lt($args));
  return ['false'];
}

sub eval_gt {
  my $args = shift;
  my $values = eval_args($args);
  if (all_is_int($values)) {
    my $first_value = value(first($values));
    for my $value (values rest($values)) {
      return ['false'] if $first_value <= value($value);
    }
    return ['true'];
  }
  error("compare >= only implement int");
}

sub eval_le {
  my $args = shift;
  return ['true'] if is_false(eval_gt($args));
  return ['false'];
}

sub eval_match {
  my $args = shift;
  my $atoms = eval_args($args);
  my ($str, $rule) = @{ $atoms };
  my $type_str = type($str);
  my $type_rule = type($rule);
  if ($type_str eq 'str') {
    if ($type_rule eq 'str') {
      if ($str =~ /$rule/) {
        return ['true'];
      }
      return ['false'];
    }
    if ($type_rule eq 'rule') {
      my $cursor = create_cursor($str);
      my $rule_name = value($rule);
      my $match = match_atom($rule_name, $cursor);
      if (is_false($match)) { return ['false'] };
      return value_to_atom($match);
    }
    if ($type_rule eq 'context') {
      my $context_name = value($rule);
      my $match = match_rule($context_name, $str);
      if (is_false($match)) { return ['false'] }
      return value_to_atom($match);
    }
  }
  error("$type_str Could not ~~ $type_rule");
}

sub show_match_log {
  my $cursor = shift;
  my $LEN = $cursor->{LEN};
  my $STR = $cursor->{STR};
  my $POS = $cursor->{POS};
  my $STACK = $cursor->{LOG};
  for my $log (values $STACK) {
    my ($flag, $atom, $pos) = @{$log};
    my $str = substr($STR, $pos, $pos + 30);
    my $rule = atom_to_str($atom);
    my $rule_str = sprintf('%-30.30s', $rule);
    my $flag_str = 'ok';
    $flag_str = '  ' if $flag == 0;
    say "$flag_str | $rule_str |$str| [$pos, $LEN]";
  }
}

sub boot_spp {
  my $boot_rule = eval_atom($Spp_rule);
  return ['true'] if is_same( $boot_rule, ['context', $RULE] );
  error("Could not Boot Spp");
}

sub spp {
  my $spp_file = shift;
  my $spp_str = trim(read_file($spp_file));
  boot_spp();
  my $match = match_rule($RULE, $spp_str);
  return ['nil'] if is_str($match);
  if (is_match($match)) {
    # see $match;
    my $opt_ast = opt_atom($match);
    # see $opt_ast;
    return atom_to_str(eval_atom($opt_ast));
  }
  # error("Could not parse $spp_str");
  return ['false'];
}

sub repl {
  boot_spp();
  say "This is Spp REPL implement with Perl5, type 'exit' exit.";
  while (1) {
    print '>>> ';
    my $line = <STDIN>;
    my $str = trim($line);
    exit() if $str eq 'exit';
    $DEBUG = 2;
    my $match = match_rule($RULE, $str);
    $DEBUG = 0;
    if (is_match($match)) {
      # see $match;
      my $opt_ast = opt_atom($match);
      # see $opt_ast;
      my $eval_value = eval_atom($opt_ast);
      say atom_to_str($eval_value);
    } else {
      say "Could not parse str: $str"
    }
  }
}

=head1 AUTHOR

Michael Song, C<< <10435916 at qq.com> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-spp at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=Spp>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.

=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc Spp


You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=Spp>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/Spp>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/Spp>

=item * Search CPAN

L<http://search.cpan.org/dist/Spp/>

=back

=head1 ACKNOWLEDGEMENTS

=head1 LICENSE AND COPYRIGHT

Copyright 2015 Michael Song.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


=cut

1; # End of Spp
