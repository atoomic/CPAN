#pragma once

class MyStatic {
public:
    int val;
    MyStatic (int val) : val(val) { }
    ~MyStatic () {}
};

class MyStaticChild : public MyStatic {
public:
    int val2;
    MyStaticChild (int val, int val2) : val2(val2), MyStatic(val) { }
};

typedef MyStatic      PTRMyStatic;
typedef MyStaticChild PTRMyStaticChild;
