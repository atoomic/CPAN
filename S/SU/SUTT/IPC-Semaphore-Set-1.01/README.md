[![Build Status](https://travis-ci.org/shaneutt/p5-ipc-semaphore-set.svg?branch=master)](https://travis-ci.org/shaneutt/p5-ipc-semaphore-set.svg?branch=master)

NAME
    IPC::Semaphore::Set

DESCRIPTION
    An abstract interface to semaphore sets and their resources.

    A semaphore is an abstract data type that is provided by the system to
    give access control to common resources by multiple processes in
    parallel programming or in a multi-user environment.

    A semaphore 'set' is the set of resources the system provides by an
    identification number, and the values (availability) of those resources.

    Resources are the semaphores themselves in the set.

    You could, for instance, use a semaphore to lock on a single file
    between multiple processes by saying that the set has one resource (one
    file) and that the resource has one availability (one process can use it
    at one time). You could also represent a series of network printers.
    Perhaps you have five printers that all have the ability to do ten jobs.
    You could create the semaphore set with five resources, each resource
    with ten availability.

    This module tries to "Do The Right Thing". It assumes a lot about what
    you're looking for when you call '->new', and basically will set you up
    with a semaphore set that has at least one resource with at least one
    availability. If this assumption is wrong for your purposes, pay close
    attention to the options for '->new'.

SYNOPSIS
    Get/Create a semaphore set:

            my $semset = IPC::Semaphore::Set->new(
                    key_name     => 'my_lock',
                    resources    => 2,
                    availability => 3,
            );

    The above semaphore set has two resource, each of those resources has an
    availability of three.

    Now you can get the first resource (resource 0):

            my $resource = $semset->resource;

    Or you can select the resource explicitly:

            my $resource = $semset->resource(1);

    But note that with two resources total, one is our last resource because
    we start at "0".

    You can make conditionals checking whether or not a lock is available on
    the resource:

            if ($semset->resource->lock) {
                    # ... can use resource!
            } else {
                    # ... can't use resource!
            }

    You can simply wait for resource availability:

            $semset->resource->lockWait;
            # ... resource is now available

    You can die if the resource isn't currently available:

            $semset->resource->lockOrDie;
            # ... if we're here we have a lock

METHODS
    new Get a new IPC::Semaphore::Set object. If 'key' is provided, get or
        create a semaphore with that key. if 'key_name' is provided,
        generate a key based off of the ascii character codes of that name.
        If neither is provided, a new 'private' semaphore set will be
        created (note that 'private' is how SysV refers to it, but this is
        something of a misnomer).

        ARGUMENTS

            key A numeric key to get/create the semaphore.

            key_name
                A "word" key to get/create the semaphore.

            value
                How much value (availability) the resources in the set
                should have. Is ignored if the semaphore existed previously,
                and is optional. Defaults to 1.

            resources
                How many resources (semaphores) will be in the set. Is
                ignored if the semaphore existed previously, and is
                optional. Defaults to 1.

            flags
                IPC::SysV flags that will be used to create the semaphore
                set.

                Defaults to the following:

                        S_IRUSR | S_IWUSR | IPC_CREAT | SEM_UNDO

                Which means it creates it if it doesn't exist, keeps track
                of ownership, and will clean up it's changes after exit.

    resource
        Returns a IPC::Semaphore::Set::Resource object.

        A "resource" is an abstraction around a semaphore in a set. For
        every semaphore present in your semaphore set you will have a
        "resource" to reference that.

        ARGUMENTS

            number
                The number of the resource in the semaphore set

            semaphore
                The IPC::Semaphore object that the resource is a part of.

            key The number that represents the set.

            cleanup_object
                Boolean. If enabled the object DESTROY will revert changes
                to the resource.

                Defaults to 1.

        RESOURCE METHODS

            lock
                Attempts to get a lock on the resource and returns boolean.

            lockWait
                Waits until a lock becomes available on the resource then
                returns 1.

            lockWaitTimeout
                Takes first argument as seconds to wait for a lock, or
                defaults to 3 seconds.

                Returns boolean.

            lockWaitTimeoutDie
                Takes first arguments as seconds to wait for a lock, or
                defaults to 3 seconds.

                Dies if a lock can not be established, or returns 1.

            lockOrDie
                Attempts to get a lock on the resource and dies if it can
                not. Returns 1 otherwise.

            addValue
                Adds a single point of value to the resource

            number
                Returns the number of the resource in its semaphore set

            semaphore
                Returns the IPC::Semaphore the resource is a part of.

            value
                Returns the value of the current semaphore resource.

    resources
        Returns a list or arrayref of all the IPC::Semaphore::Set::Resource
        objects available for this semaphore set.

    id  Returns the numeric system ID of the semaphore set.

    key Returns the numeric key if available.

    keyName
        Returns the 'word' key if used.

    remove
        Remove the semaphore set entirely from the system.

    sem Returns the internal 'IPC::Semaphore' object.

