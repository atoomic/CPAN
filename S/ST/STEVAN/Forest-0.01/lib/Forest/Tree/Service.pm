package Forest::Tree::Service;
use Moose::Role;

our $VERSION   = '0.01';
our $AUTHORITY = 'cpan:STEVAN';

has 'tree_index' => (
    is   => 'rw',
    does => 'Forest::Tree::Indexer',
);

1;

__END__

=pod

=head1 NAME

Forest::Tree::Service - An abstract role for tree services

=head1 DESCRIPTION

This is an abstract role for tree indexers.

=head1 ATTRIBUTES

=over 4

=item I<tree_index>

=back

=head1 BUGS

All complex software has bugs lurking in it, and this module is no 
exception. If you find a bug please either email me, or add the bug
to cpan-RT.

=head1 AUTHOR

Stevan Little E<lt>stevan.little@iinteractive.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright 2008 Infinity Interactive, Inc.

L<http://www.iinteractive.com>

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
