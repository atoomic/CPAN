NAME
    Probe::MachineInfo - The great new Probe::MachineInfo!

SYNOPSIS
    Probe::MachineInfo aims to be a set of collector/probe classes which
    allow the extraction of information about the configuration of a
    machine. For example the number of CPUs it has and how much memory it
    has.

    This release is really just a taster release of what's to come. The
    architecture may change quite a lot. At the moment there is a bit of a
    Unix bias because that's the type of operating system that I do almost
    all of my real work on. However I also hope to add support for Windows
    hosts at some earlyish stage.

    Quick summary of what the module does.

    Perhaps a little code snippet.

        use Probe::MachineInfo;

        my $foo = Probe::MachineInfo->new();
        ...

INSTALLATION
    This module uses Module::Build for its installation. To install this
    module type the following:

      perl Build.PL
      ./Build
      ./Build test
      ./Build install

    If you do not have Module::Build type:

      perl Makefile.PL

    to fetch it. Or use CPAN or CPANPLUS and fetch it "manually".

DEPENDENCIES
    See the Build.PL for a list of dependencies

TODO
  General
    *   Windows support

  Metric Probe Additions
    *   NIS related information

    *   Memory

    *   Packages/Rpms installed

    *   Disk & Partition Setup

    *   Filesystem setup ([v]ftsab)

    *   Network Link Speed

    *   Keepalive on Solaris

    *   Ipaddress on Windows

    *   LVM / RAID / Metadevice

BUGS
    Please report any bugs or feature requests to
    "bug-config-machineinfo@rt.cpan.org", or through the web interface at
    <http://rt.cpan.org>. I will be notified, and then you'll automatically
    be notified of progress on your bug as I make changes.

AUTHOR
    Sagar R. Shah, "<sagarshah@softhome.net>"

COPYRIGHT & LICENSE
    Copyright 2005 Sagar R. Shah, All Rights Reserved.

    This program is free software; you can redistribute it and/or modify it
    under the same terms as Perl itself.

